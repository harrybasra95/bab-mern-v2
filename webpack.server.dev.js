var webpack = require('webpack');
var path = require('path');
const Dotenv = require('dotenv-webpack');
var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
     .filter(function(x) {
          return ['.bin'].indexOf(x) === -1;
     })
     .forEach(function(mod) {
          nodeModules[mod] = 'commonjs ' + mod;
     });

module.exports = {
     entry: './src/server/app.js',
     target: 'node',
     output: {
          path: path.join(__dirname, 'build'),
          filename: 'backend.js'
     },
     node: {
          __dirname: false
     },
     externals: nodeModules,
     plugins: [
          new Dotenv({ path: './dev.env' }),
          new webpack.IgnorePlugin(/\.(css|less)$/),
          new webpack.BannerPlugin({
               banner: 'require("source-map-support").install();',
               raw: true,
               entryOnly: false
          })
     ],
     devtool: 'sourcemap'
};
