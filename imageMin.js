const imagemin = require('./_modules/imagemin');
const imageminWebp = require('imagemin-webp');
// const imageminMozjpeg = require('imagemin-mozjpeg');
// const fs = require('fs');

const imageMinFunction = async () => {
     return await imagemin(
          ['./build/public/img/**/*.{jpg,png,jpeg}'],
          './build/images',
          {
               use: [imageminWebp({ quality: 50 })]
          }
     );
};

// const imageMinFunction2 = async () => {
//      const files = await imagemin(
//           ['./build/public/img/**/*.{jpg,png}'],
//           'destination_dir',
//           { plugins: [imageminMozjpeg({ quality: 50 })] }
//      );
//      files.forEach(({ path, data }) => {
//           fs.writeFile(path.replace('.jpg', '_min.jpg'), data, function(err) {
//                if (err) {
//                     return console.log(err);
//                }
//           });
//      });
// };
module.exports = { imageMinFunction };
