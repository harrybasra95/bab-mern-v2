require('dotenv/config');
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const server = require('http').Server(app);
const fs = require('fs');
const user = require('./models/user');
const mailgun = require('./models/mailgun');
const delhivery = require('./delhivery/delhivery');
const request = require('request');
const csrf = require('csurf');
const passport = require('passport');
const io = require('socket.io').listen(server).sockets;
const chokidar = require('chokidar');
const path = require('path');
const cssWatcher = chokidar.watch('./public/css/*.css');
const ejsWatcher = chokidar.watch('./views');
const {
    expressServices,
    passportServices,
    intervalFunctions
} = require('./services');
const {
    addressRoutes,
    adminRoutes,
    authRoutes,
    cartRoutes,
    checkoutRoutes,
    frontRoutes,
    pageApiRoutes,
    testRoutes,
    trackingRoutes,
    passportRoutes,
    couponRoutes,
    reactSessionRoutes,
    authApiRoutes,
    orderRoutes,
    userRoutes
} = require('./routes');

mongoose.connect(process.env.DATABASE);
expressServices.use(app);
passportServices.use(passport);
intervalFunctions.intervals();
let lastUrl;

app.post('/2co', (req, res) => {
    const md5 = require('md5');
    const order = require('./models/orders');
    req.session.selected_country = req.session.selected_country
        ? req.session.selected_country
        : 'none';
    const x = `${'YjQ0MjIzMDMtYTM3Ni00MDgyLThhOGMtOWI2MmE0YTM0NGEz' +
        '103444918'}${req.body.order_number}${req.body.total}`;
    if (
        md5(x).toUpperCase() == req.body.key &&
        req.body.credit_card_processed == 'Y'
    ) {
        order.findOne(
            { ordernumber: req.body.merchant_order_id },
            (err, savedOrder) => {
                delhivery.pincode(
                    savedOrder.deliveryaddress.pincode,
                    (err, response, body) => {
                        if (!JSON.parse(body).delivery_codes || err) {
                            res.send({
                                success: false,
                                message:
                                    'Delivery is not possible at this address.'
                            });
                        } else {
                            delhivery.waybill(
                                'fetch',
                                (err, response, body) => {
                                    const waybill = JSON.parse(body);
                                    const detail = {
                                        deliveryaddress: {}
                                    };
                                    detail.deliveryaddress =
                                        savedOrder.deliveryaddress;
                                    detail.waybill = waybill;
                                    detail.total_amount = savedOrder.totalprice;
                                    detail.phone =
                                        savedOrder.persondetails.phone;
                                    detail.shipping_mode = 'Express';
                                    detail.quantity = savedOrder.totalqty;
                                    detail.ordernumber = savedOrder.ordernumber;
                                    if (savedOrder.paymenttype == 'cod') {
                                        detail.payment_type = 'COD';
                                    } else {
                                        detail.payment_type = 'Pre­paid';
                                    }
                                    detail.order_date = new Date();
                                    detail.cod_amount = savedOrder.totalprice;
                                    detail.name = `${
                                        savedOrder.persondetails.fname
                                    } ${savedOrder.persondetails.lname}`;
                                    delhivery.packagecreate(
                                        detail,
                                        (err, response, body) => {
                                            if (
                                                JSON.parse(body).success ==
                                                false
                                            ) {
                                                mailgun.delivery_failed(
                                                    user,
                                                    savedOrder
                                                );
                                            }
                                            savedOrder.checked = true;
                                            savedOrder.waybill = waybill;
                                            if (
                                                savedOrder.paymenttype ==
                                                    'cc' &&
                                                savedOrder.paymentdetails &&
                                                savedOrder.paymentdetails
                                                    .length != 0
                                            ) {
                                                if (
                                                    !savedOrder.paymentdetails ||
                                                    savedOrder.paymentdetails
                                                        .length === 0
                                                ) {
                                                    savedOrder.paymentdetails =
                                                        req.body;
                                                }
                                            }
                                            savedOrder.status = 'OR';
                                            savedOrder.deliverydetails = JSON.parse(
                                                body
                                            );
                                            if (
                                                savedOrder.coupon &&
                                                savedOrder.coupon.length != 0
                                            ) {
                                                user.findById(
                                                    savedOrder.userid,
                                                    (err, foundUser) => {
                                                        foundUser.coupons.push(
                                                            savedOrder.coupon
                                                                ._id
                                                        );
                                                        foundUser.save(() => {
                                                            savedOrder.save(
                                                                (
                                                                    err,
                                                                    savedOrder
                                                                ) => {
                                                                    const user = {
                                                                        name:
                                                                            savedOrder
                                                                                .persondetails
                                                                                .fname,
                                                                        email:
                                                                            savedOrder
                                                                                .persondetails
                                                                                .eadd
                                                                    };
                                                                    mailgun.order_recieved(
                                                                        user,
                                                                        savedOrder
                                                                    );
                                                                    if (
                                                                        savedOrder.paymenttype ==
                                                                        'cod'
                                                                    ) {
                                                                        res.send(
                                                                            {
                                                                                success: true,
                                                                                type:
                                                                                    '1',
                                                                                url: `/order/${
                                                                                    savedOrder.ordernumber
                                                                                }`
                                                                            }
                                                                        );
                                                                    } else {
                                                                        req.flash(
                                                                            'success',
                                                                            'Thank you for shopping at Being Beast Store'
                                                                        );
                                                                        res.redirect(
                                                                            `/order/${
                                                                                savedOrder.ordernumber
                                                                            }`
                                                                        );
                                                                    }
                                                                    if (err) {
                                                                        throw err;
                                                                    }
                                                                }
                                                            );
                                                        });
                                                    }
                                                );
                                            } else {
                                                savedOrder.save(
                                                    (err, savedOrder) => {
                                                        const user = {
                                                            name:
                                                                savedOrder
                                                                    .persondetails
                                                                    .fname,
                                                            email:
                                                                savedOrder
                                                                    .persondetails
                                                                    .eadd
                                                        };
                                                        mailgun.order_recieved(
                                                            user,
                                                            savedOrder
                                                        );
                                                        if (
                                                            savedOrder.paymenttype ==
                                                            'cod'
                                                        ) {
                                                            res.send({
                                                                success: true,
                                                                type: '1',
                                                                url: `/order/${
                                                                    savedOrder.ordernumber
                                                                }`
                                                            });
                                                        } else {
                                                            req.flash(
                                                                'success',
                                                                'Thank you for shopping at Being Beast Store'
                                                            );
                                                            res.redirect(
                                                                `/order/${
                                                                    savedOrder.ordernumber
                                                                }`
                                                            );
                                                        }
                                                        if (err) {
                                                            throw err;
                                                        }
                                                    }
                                                );
                                            }
                                        }
                                    );
                                }
                            );
                        }
                    }
                );
            }
        );
    }
});

app.get('/redirect/callback', (req, res) => {
    res.redirect(lastUrl);
    if (req.user) {
        request.get(
            `https://in.babclothing.com/attachtrackid/${req.user.username}/${
                req.session.userid
            }`,
            err => {
                console.log(err);
            }
        );
    }
});

//======================chat

app.use('/admin', adminRoutes);
app.use('/api', authApiRoutes);
app.use('/api/session', reactSessionRoutes);
app.use('/api/order', orderRoutes);
app.use('/api/user', userRoutes);
app.use('/api/tracking',trackingRoutes);
app.use('/api', pageApiRoutes);
app.get('/*', (req, res) => {
    res.sendFile('index.html', { root: path.join(__dirname, '/public') }, err => {
         if (err) {
              res.status(500).send(err);
         }
    });
});
app.use(
    addressRoutes,
    testRoutes,
    passportRoutes,
    cartRoutes,
    couponRoutes
);

app.post('/saveChat', (req, res) => {
    res.send(req.body);
    // req.session.chat = req.body.chat;
    // res.send(true);
});

app.get('/loggerFile', (req, res) => {
    const logs = JSON.parse(fs.readFileSync('./logs/requests.json', 'utf-8'));
    res.render('logger', { logs });
    // res.sendFile(path.join(__dirname, './logs', 'requests.json'));
});

let chatData = 0;
io.on('connection', client => {
    global.client = client;
    cssWatcher.on('change', path => {
        if (path.indexOf('.css') > 0 || path.indexOf('.js') > 0) {
            if (client) {
                client.emit('file-change');
            }
        }
    });

    ejsWatcher.on('change', path => {
        if (path.indexOf('.ejs') > 0) {
            if (client) {
                client.emit('file-change');
            }
        }
    });

    client.on('join', data => {
        client.join(data.room);
        if (data.t == 'c') {
            chatData = data.chat;
        }
        if (data.chat) {
            chatLength = data.chat.length;
        }
        io.in(data.room).emit('message', `New user joined ${data.room} room!`);
    });
    client.on('admin', data => {
        const obj = {
            m: data.msg,
            t: 'a',
            d: new Date()
        };
        chatData.push(obj);
        client.join(data.room);
        io.in(data.room).emit('admin', data.msg);
    });
    client.on('client', data => {
        const obj = {
            m: data.msg,
            t: 'c',
            d: new Date()
        };
        chatData.push(obj);
        client.join(data.room);
        io.in(data.room).emit('client', data.msg);
    });
    client.on('sale', data => {
        io.emit('sale', data.msg);
    });
    client.on('disconnect', () => {
        request.post('http://localhost:4000/saveChat', {
            form: {
                chat: chatData
            }
        });
    });
});

//====================
app.use(checkoutRoutes);
app.use(csrf());
app.use(authRoutes);
app.use(frontRoutes);

server.listen(process.env.PORT || 4000, () => {
    console.log('server is running!!! at local host 4000');
});
