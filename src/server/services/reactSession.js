const { ReactSession } = require('../models');

module.exports = {
    addTrackId: async (trackId, token) => {
        try {
            if (token) {
                const savedSession = await ReactSession.findByIdAndUpdate(
                     token,
                     {
                          trackId,
                     },
                     { new: true }
                );
                return savedSession;
            }
        } catch (error) {
            console.log(error);
        }
    },
    userLoggedIn: async (user, token) => {
        try {
            if (token) {
                const savedSession = await ReactSession.findByIdAndUpdate(
                    token,
                    {
                        user,
                        isLoggedIn: true
                    },
                    { new: true }
                );
                return savedSession;
            }
        } catch (error) {
            console.log(error);
        }
    },
    addProductToCart: async (product, quantity, token, selectedCountry) => {
        try {
            if (token) {
                const foundSession = await ReactSession.findById(token);
                if (foundSession) {
                    const cart = foundSession.cart;
                    const products = { ...cart.products };
                    const id = product._id;
                    let productPrice = product.pricechart[selectedCountry];
                    if (product.sale > 0) {
                        productPrice -= (product.sale / productPrice) * 100;
                        if (selectedCountry === 'in') {
                            productPrice = Math.round(productPrice);
                        } else {
                            productPrice = Number(productPrice).toFixed(2);
                        }
                    }
                    if (cart.products && id in cart.products) {
                        products[id].productTotalQuantity += quantity;
                        products[id].productTotalPrice += productPrice;
                    } else {
                        products[id] = {
                            product,
                            productTotalQuantity: quantity,
                            productTotalPrice: productPrice
                        };
                    }
                    cart.products = { ...products };
                    cart.totalPrice += productPrice;
                    cart.totalQuantity += quantity;
                    const savedSession = await ReactSession.findByIdAndUpdate(
                        token,
                        { cart },
                        { new: true }
                    );
                    return savedSession;
                }
            }
        } catch (error) {
            throw error;
        }
    },
    removeProductFromCart: async (id, token) => {
        try {
            if (token) {
                const foundSession = await ReactSession.findById(token);
                if (foundSession) {
                    const cart = foundSession.cart;
                    const products = { ...cart.products };
                    if (cart.products && id in cart.products) {
                        cart.totalPrice -= cart.products[id].productTotalPrice;
                        cart.totalQuantity -=
                            cart.products[id].productTotalQuantity;
                        delete products[id];
                    }
                    cart.products = { ...products };
                    const savedSession = await ReactSession.findByIdAndUpdate(
                        token,
                        { cart },
                        { new: true }
                    );
                    return savedSession;
                }
            }
        } catch (error) {
            throw error;
        }
    },
    userLogOut: async token => {
        try {
            if (token) {
                const savedSession = await ReactSession.findByIdAndUpdate(
                    token,
                    {
                        user: null,
                        isLoggedIn: false
                    },
                    { new: true }
                );
                return savedSession;
            }
        } catch (error) {
            console.log(error);
        }
    }
};
