const { Order } = require('../models');
const { delhiveryServices } = require('../services');

const intervals = () => {
    setInterval(async () => {
        const foundOrders = await Order.find({ status: 'S' });
        if (foundOrders && foundOrders.length > 0) {
            foundOrders.forEach(singleOrder => {
                delhiveryServices.track(singleOrder.waybill, (err, body) => {
                    if (err) {
                        console.log('err');
                    } else {
                        let data = JSON.parse(body.body);
                        data = data.ShipmentData['0'].Shipment;
                        if (data.Status.Status.toLowerCase() === 'delivered') {
                            singleOrder.status = 'D';
                            data = JSON.parse(body.body);
                            data = data.ShipmentData['0'].Shipment.Scans;
                            let p = data[
                                data.length - 1
                            ].ScanDetail.StatusDateTime.split('T')[0];
                            p = p.split('-');
                            const x = new Date(
                                p[0],
                                Number(p[1]) - 1,
                                Number(p[2]) + 1,
                                0,
                                0,
                                0,
                                0
                            );
                            singleOrder.deliverydate = x;
                            singleOrder.save();
                        }
                    }
                });
            });
        }
    }, 21600000);
    setInterval(() => {
        Order.find({ status: 'D' }, (err, foundOrders) => {
            if (foundOrders.length > 0) {
                foundOrders.forEach(singleOrder => {
                    const date = singleOrder.deliverydate;
                    date.setDate(date.getDate() + 15);
                    if (date - new Date() < 0) {
                        singleOrder.status = 'OS';
                        singleOrder.save();
                    }
                });
            }
        });
    }, 21600000);
};

module.exports = { intervals };
