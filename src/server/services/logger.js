const fs = require('fs');
const path = require('path');
const loggerPath = path.join(__dirname, '../logs', 'requests.json');
const loggerErrorPath = path.join(__dirname, '../logs', 'error.json');

module.exports = {
     addErrorLog: (req, message) => {
          const loggerObj = {
               date: new Date(),
               type: req.method,
               url: req.originalUrl,
               params: { ...req.params },
               body: { ...req.body },
               headers: { ...req.headers },
               message
          };

          let loggerFile;
          if (!fs.existsSync(loggerErrorPath)) {
               loggerFile = [];
          } else {
               loggerFile = JSON.parse(fs.readFileSync(loggerErrorPath));
          }
          loggerFile.reverse();
          loggerFile.push(loggerObj);
          loggerFile.reverse();
          if (loggerFile.length > 500) {
               loggerFile.pop();
          }
          fs.writeFileSync(
               loggerErrorPath,
               JSON.stringify(loggerFile),
               'utf-8'
          );
     },
     addLog: req => {
          if (req.originalUrl === '/loggerFile') {
               return true;
          }
          const loggerObj = {
               date: new Date(),
               type: req.method,
               url: req.originalUrl,
               params: { ...req.params },
               body: { ...req.body },
               headers: { ...req.headers }
          };

          let loggerFile;
          if (!fs.existsSync(loggerPath)) {
               loggerFile = [];
          } else {
               loggerFile = JSON.parse(fs.readFileSync(loggerPath));
          }
          loggerFile.reverse();
          loggerFile.push(loggerObj);
          loggerFile.reverse();
          if (loggerFile.length > 500) {
               loggerFile.pop();
          }
          fs.writeFileSync(loggerPath, JSON.stringify(loggerFile), 'utf-8');
     }
};
