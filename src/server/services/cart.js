module.exports = {
    generateArray: cart => {
        return Object.keys(cart.products).map(key => cart.products[key]);
    }
};
