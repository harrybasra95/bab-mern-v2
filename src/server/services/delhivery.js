const request = require('request');
const axios = require('axios');
const qs = require('qs');
const { delhiveryToken } = require('../config');

const cancel = async waybill => {
     const headers = {
          Authorization: `Token ${delhiveryToken}`,
          'Content-type': 'application/json'
     };
     const { data } = await axios.post(
          'https://track.delhivery.com/api/p/edit',
          {
               waybill: String(waybill),
               cancellation: true
          },
          {
               headers
          }
     );
     return data;
};

const waybill = async () => {
     try {
          const { data } = await axios.get(
               `https://track.delhivery.com/waybill/api/fetch/json/?cl=BEINGBEAST&token=${delhiveryToken}`
          );
          return data;
     } catch (error) {
          return error;
     }
};

const track = async waybill => {
     const headers = {
          Authorization: 'Token b66d81bf1c79e745eacbf6938e43d521bb299d0e',
          'Content-type': 'application/json'
     };
     const { data } = await axios.get(
          `https://track.delhivery.com/api/packages/json/?token=${delhiveryToken}&format=json&waybill=${waybill}`,
          { headers }
     );
     return data;
};

const packing = async waybill => {
     const headers = {
          Authorization: 'Token b66d81bf1c79e745eacbf6938e43d521bb299d0e',
          'Content-type': 'application/json'
     };
     const { data } = await axios.get(
          `https://track.delhivery.com/api/p/packing_slip?wbns=${waybill}`,
          { headers }
     );
     return data;
};
const pickup = function(data, cb) {
     const options = {
          method: 'POST',
          url: 'https://track.delhivery.com/fm/request/new/',
          headers: {
               Authorization: 'Token b66d81bf1c79e745eacbf6938e43d521bb299d0e',
               'Content-type': 'application/json'
          },
          body:
               '{"pickup_location": "BEINGBEAST",\
			    "pickup_time": "09:43:42",\
			    "pickup_date": "20181230",\
			    "expected_package_count": "1"} '
     };
     request(options, cb);
};

const pincode = async pincode => {
     const { data } = await axios.get(
          `https://track.delhivery.com/c/api/pin-codes/json/?token=${delhiveryToken}&filter_codes=${pincode}`
     );
     return data;
};
const packagecreate = async ({
     deliveryaddress,
     total_amount,
     quantity,
     phone,
     cod_amount,
     order_date,
     payment_type,
     ordernumber,
     name,
     waybill
}) => {
     const deliveryData = {
          shipments: [
               {
                    city: deliveryaddress.city,
                    waybill: waybill,
                    total_amount: total_amount,
                    seller_cst: '03092214174',
                    name: name,
                    weight: quantity * 0.24,
                    country: deliveryaddress.country,
                    quantity: quantity,
                    seller_tin: '03092214174',
                    state: deliveryaddress.state,
                    shipping_mode: 'Express',
                    phone: phone,
                    add: deliveryaddress.street_no,
                    cod_amount: cod_amount,
                    order_date: order_date,
                    pin: deliveryaddress.pincode,
                    products_desc: 'Gymwear Apparel',
                    payment_mode: payment_type,
                    order: ordernumber
               }
          ],
          pickup_location: {
               city: 'JALANDHAR',
               state: 'Punjab',
               name: 'BEINGBEAST',
               pin: '144002'
          }
     };
     const reqData = {
          format: 'json',
          data: JSON.stringify(deliveryData)
     };
     const { data } = await axios.post(
          'https://track.delhivery.com/cmu/push/json/?token=b66d81bf1c79e745eacbf6938e43d521bb299d0e&format=json',
          qs.stringify(reqData)
     );
     return data;
};

module.exports.waybill = waybill;
module.exports.track = track;
module.exports.cancel = cancel;
module.exports.packing = packing;
module.exports.pickup = pickup;
module.exports.pincode = pincode;
module.exports.packagecreate = packagecreate;
