const { findDuplicates } = require('../helpers');

module.exports = {
    isCouponApplicableOnProducts: (newCart, coupon) => {
        const duplicatesArray = findDuplicates(
            coupon.notonproducts.concat(module.exports.getUniqueIds(newCart))
        );
        return duplicatesArray.length === 0;
    },
    getUniqueIds(newCart) {
        const arr = [];
        Object.keys(newCart.products).forEach(key => {
            arr.push(newCart.products[key].product.uniqueid);
        });
        return arr;
    }
};
