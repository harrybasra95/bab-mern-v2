const cookieParser = require('cookie-parser');
const express = require('express');
const request = require('request');
const methodOverride = require('method-override');
const expressSession = require('express-session');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoStore = require('connect-mongo')(expressSession);
const path = require('path');
const passport = require('passport');
const flash = require('connect-flash');
// const loggerServices = require('../services/logger');
const { Cart } = require('../models');
const ShippingChart = require('../shipping');
const compression = require('compression');
const helmet = require('helmet');
const { symbolchart } = require('../config');
const notifications = [
     {
          no: 0,
          info:
               'Now Get Flat 25% off on all products by using coupon code "summer25".',
          seen: false
     }
];
let lastUrl;

const corsOptions = {
     origin: [
          'null',
          'https://row.babclothing.com',
          'http://localhost:3000',
          'https://*.babclothing.com',
          'https://in.babclothing.com',
          'https://www.babclothing.com'
     ],
     optionsSuccessStatus: 200
};

module.exports = {
     use: app => {
          app.use(cookieParser());
          app.use(
               expressSession({
                    secret: 'hello kitty',
                    resave: false,
                    cookie: {
                         path: '/',
                         httpOnly: true,
                         secure: false,
                         domain: process.env.Domain,
                         expires: false,
                         maxAge: 30 * 24 * 60 * 60 * 1000 //thirty day cookie
                    },
                    store: new mongoStore({
                         url: process.env.DATABASE,
                         ttl: 14 * 24 * 60 * 60 // = 14 days. Default
                    }),
                    saveUninitialized: false
               })
          );
          app.set('view engine', 'ejs');
          app.use(compression());
          app.use(helmet());
          app.use(cors(corsOptions));
          app.use(flash());
          app.use(passport.initialize());
          app.use(passport.session());
          app.use(express.static(__dirname + '/public/'));
          app.use(methodOverride('_method'));
          app.use(bodyParser.json({ limit: '50mb', parameterLimit: 1000000 }));
          app.use(
               bodyParser.urlencoded({
                    limit: '50mb',
                    extended: true,
                    parameterLimit: 1000000
               })
          );
          app.use((req, res, next) => {
               res.locals.env_type = process.env.TYPE;
               res.locals.symbolchart = symbolchart;
               res.locals.hostname = req.hostname;
               res.locals.currentUser = req.user;
               res.locals.session = req.session;
               res.locals.fullUrl = req.get('host') + req.originalUrl;
               res.locals.shipping = ShippingChart;
               res.locals.error = req.flash('error');
               res.locals.success = req.flash('success');
               res.locals.cart = new Cart(
                    req.session.cart ? req.session.cart : {}
               ).generateArray();
               res.locals.country = 'India';
               req.session.visited = Date.now();
               req.session.notification = req.session.notification
                    ? req.session.notification
                    : notifications;
               if (req.session.notification.length == 0) {
                    req.session.notification = notifications;
               }
               if (req.session.userlink) {
                    request.get(
                         `https://in.babclothing.com/attachname/${
                              req.session.userlink
                         }`,
                         () => {}
                    );
               }
               // loggerServices.addLog(req);
               next();
          });
          app.use((req, res, next) => {
               res.success = data => {
                    if (Array.isArray(data)) {
                         res.send({ isSuccess: true, items: data });
                    } else {
                         res.send({ isSuccess: true, data });
                    }
               };
               res.failure = error => {
                    res.send({ isSuccess: false, error });
               };
               next();
          });
          if (process.env.TYPE != 'local') {
               app.use((req, res, next) => {
                    if (req.originalUrl === '/select-region') {
                         next();
                    }
                    if (!req.xhr || req.path.indexOf('/api/') < 0) {
                         lastUrl = '/mycart';
                    }
                    req.session.userid = req.session.userid
                         ? req.session.userid
                         : 'none';
                    req.session.popup = req.session.popup
                         ? req.session.popup
                         : 'none';
                    req.session.selected_country = req.session.selected_country
                         ? req.session.selected_country
                         : 'none';
                    if (req.session.userlink) {
                         request.get(
                              `https://in.babclothing.com/attachname/${
                                   req.session.userlink
                              }/${req.session.userid}`,
                              err => {
                                   console.log(err);
                              }
                         );
                    }
                    const countryValue = req.session.selected_country;
                    const country = {
                         in: 'https://in.babclothing.com',
                         us: 'https://us.babclothing.com',
                         ae: 'https://ae.babclothing.com',
                         au: 'https://au.babclothing.com',
                         row: 'https://row.babclothing.com',
                         uk: 'https://uk.babclothing.com',
                         eu: 'https://eu.babclothing.com',
                         ca: 'https://ca.babclothing.com',
                         nz: 'https://nz.babclothing.com',
                         none: 'https://in.babclothing.com'
                    };
                    if (country[countryValue] != `https://${req.hostname}`) {
                         if (req.xhr == false) {
                              res.redirect(
                                   307,
                                   country[countryValue] + req.path
                              );
                         } else {
                              res.redirect(307, country[countryValue]);
                         }
                    } else if (country[countryValue] == undefined) {
                         res.redirect(307, 'https://in.babclothing.com');
                    } else {
                         next();
                    }
               });
          } else {
               app.use((req, res, next) => {
                    if (!req.xhr) {
                         lastUrl = req.originalUrl;
                    }
                    req.session.userid = req.session.userid
                         ? req.session.userid
                         : 'bab0eb48889df85369a09d9031fd56ea';
                    req.session.selected_country = req.session.selected_country
                         ? req.session.selected_country
                         : 'in';
                    req.session.popup = req.session.popup
                         ? req.session.popup
                         : 'none';
                    if (req.session.userlink) {
                         request.get(
                              `http://localhost:4000/attachname/${
                                   req.session.userlink
                              }/${req.session.userid}`,
                              err => {
                                   console.log(err);
                              }
                         );
                    }
                    next();
               });
          }
     }
};
