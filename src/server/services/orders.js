const moment = require('moment');
const { User, Mailgun } = require('../models');
const ccAvenueServices = require('./ccAvenue');
const { ccAvenueConfig } = require('../config');
const delhiveryServices = require('./delhivery');

const sortCouponShares = (couponId, orders, couponName) => {
     const couponUsers = [];
     const couponOrders = [];
     const couponData = {
          goldgym: {
               name: 'Gold Gym',
               withCouponShare: 0.15,
               withoutCouponShare: 0.3
          },
          neo: {
               name: 'Neo Fitness Gym',
               withCouponShare: 0.2,
               withoutCouponShare: 0.4
          },
          f360: {
               name: 'F360 Gym',
               withCouponShare: 0.15,
               withoutCouponShare: 0.3
          }
     };
     orders.sort((a, b) => new Date(a.orderdate) - new Date(b.orderdate));
     orders.forEach(singleOrder => {
          if (singleOrder.coupon && singleOrder.coupon._id === couponId) {
               if (couponUsers.indexOf(singleOrder.userid) === -1) {
                    couponUsers.push(singleOrder.userid);
               }
               couponOrders.push(singleOrder);
          } else if (couponUsers.indexOf(singleOrder.userid) > -1) {
               couponOrders.push(singleOrder);
          }
     });
     const currentCoupon = couponData[couponName];
     const returnData = {
          orders: [],
          totalSharePrice: 0,
          title: currentCoupon.name
     };
     couponOrders.map(singleOrder => {
          const {
               ordernumber,
               orderdate,
               persondetails,
               totalprice
          } = singleOrder;
          if (persondetails) {
               let sharePrice = 0;
               if (singleOrder.coupon) {
                    sharePrice = totalprice * currentCoupon.withCouponShare;
               } else {
                    sharePrice = totalprice * currentCoupon.withoutCouponShare;
               }
               returnData.totalSharePrice += sharePrice;
               returnData.orders.push({
                    orderNumber: ordernumber,
                    orderDate: moment(orderdate).format('DD/MM/YYYY'),
                    name: `${persondetails.fname} ${persondetails.lname}`,
                    sharePrice,
                    isCouponApplied: !!singleOrder.coupon,
                    totalprice
               });
          }
     });
     return returnData;
};
const findUserAndMakeOrder = async (req, res, savedOrder) => {
     try {
          const val = savedOrder.paymenttype;
          const foundUser = await User.findById(savedOrder.userid);
          foundUser.defaultaddress = savedOrder.delivery;
          foundUser.orders.push(savedOrder);
          await foundUser.save();
          switch (val) {
               case 'cod':
                    savedOrder.totalprice += 50;
                    makeOrder(req, res, savedOrder);
                    break;
               case 'paytm':
                    savedOrder.status = 'paytm';
                    makeOrder(req, res, savedOrder);
                    break;
               case 'card':
                    res.success({
                         url: ccAvenueConfig.serverAddress,
                         acc: ccAvenueConfig.accessCode,
                         enc: ccAvenueServices.createFromBody(savedOrder)
                    });
                    break;
               default:
                    res.send({
                         success: false,
                         message: 'Something went wrong.Please try again',
                         class: '.active'
                    });
          }
     } catch (error) {
          if (error) throw error;
     }
};
const makeOrder = async (req, res, savedOrder, paymentdetails) => {
     try {
          const {
               totalprice,
               persondetails,
               totalqty,
               paymenttype,
               deliveryaddress,
               ordernumber
          } = savedOrder;
          const fetchedWaybill = await delhiveryServices.waybill();
          const detail = {
               ordernumber,
               deliveryaddress: { ...deliveryaddress },
               shipping_mode: 'Express',
               waybill: fetchedWaybill,
               total_amount: totalprice,
               phone: persondetails.phone,
               quantity: totalqty,
               payment_type: paymenttype === 'cod' ? 'COD' : 'Prepaid',
               order_date: new Date(),
               cod_amount: totalprice,
               name: `${persondetails.fname} ${persondetails.lname}`
          };
          const packageData = await delhiveryServices.packagecreate(detail);
          if (packageData.success === false) {
               Mailgun.delivery_failed(null, savedOrder);
          }
          savedOrder.checked = true;
          savedOrder.waybill = fetchedWaybill;
          savedOrder.paymentdetails =
               savedOrder.paymenttype === 'cc' && paymentdetails
                    ? paymentdetails
                    : req.body;
          savedOrder.status = savedOrder.paymenttype !== 'paytm' ? 'OR' : 'PP';
          savedOrder.deliverydetails = packageData;
          if (savedOrder.coupon && savedOrder.coupon.length !== 0) {
               const foundUser = await User.findById(savedOrder.userid);
               foundUser.coupons.push(savedOrder.coupon._id);
               await foundUser.save();
          }
          const reSavedOrder = await savedOrder.save();
          Mailgun.order_recieved(
               { name: persondetails.fname, email: persondetails.eadd },
               savedOrder
          );
          if (savedOrder.status === 'PP') {
               return res.success({
                    order: reSavedOrder.getPublicFields()
               });
          }
          if (paymenttype === 'cod') {
               return res.success({
                    order: reSavedOrder.getPublicFields(),
                    invoice: ''
               });
          }
          req.flash('success', 'Thank you for shopping at Being Beast Store');
          res.redirect(`/order/${ordernumber}`);
     } catch (error) {
          if (error) throw error;
     }
};
const makeOrderPrepaid = async (res, savedOrder, paymentdetails) => {
     const fetchedWaybill = await delhiveryServices.waybill();
     const {
          totalprice,
          persondetails,
          totalqty,
          deliveryaddress,
          ordernumber
     } = savedOrder;
     const detail = {
          ordernumber,
          deliveryaddress: { ...deliveryaddress },
          shipping_mode: 'Express',
          waybill: fetchedWaybill,
          total_amount: totalprice,
          phone: persondetails.phone,
          quantity: totalqty,
          payment_type: 'Prepaid',
          order_date: new Date(),
          cod_amount: totalprice,
          name: `${persondetails.fname} ${persondetails.lname}`
     };
     const packageData = await delhiveryServices.packagecreate(detail);
     if (packageData.success === false) {
          Mailgun.delivery_failed(null, savedOrder);
     }
     savedOrder.checked = true;
     savedOrder.waybill = fetchedWaybill;
     savedOrder.paymentdetails = paymentdetails;
     savedOrder.status = 'OR';
     savedOrder.deliverydetails = packageData;
     if (savedOrder.coupon && savedOrder.coupon.length !== 0) {
          const foundUser = await User.findById(savedOrder.userid);
          foundUser.coupons.push(savedOrder.coupon._id);
          await foundUser.save();
     }
     await savedOrder.save();
     Mailgun.order_recieved(
          {
               name: persondetails.fname,
               email: persondetails.eadd
          },
          savedOrder
     );
     res.redirect(`/account/orders/${savedOrder.ordernumber}`);
};
module.exports = {
     sortCouponShares,
     findUserAndMakeOrder,
     makeOrder,
     makeOrderPrepaid
};
