const localStrategy = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const { facebookAuth, googleAuth } = require('../textData/socailMediaAuthData');
const { User } = require('../models');
module.exports = {
    use: passport => {
        passport.serializeUser(function(user, done) {
            done(null, user);
        });

        passport.deserializeUser(function(obj, done) {
            done(null, obj);
        });

        passport.use(
            new FacebookStrategy(
                {
                    clientID: facebookAuth.clientId,
                    clientSecret: facebookAuth.clientSecret,
                    callbackURL: facebookAuth.callbackURL,
                    profileFields: [
                        'id',
                        'email',
                        'gender',
                        'name',
                        'birthday',
                        'verified'
                    ]
                },
                function(accessToken, refreshToken, profile, done) {
                    process.nextTick(function() {
                        User.findOne({ email: profile._json.email }, function(
                            err,
                            foundUser
                        ) {
                            if (err) {
                                return done(err);
                            }
                            if (foundUser) {
                                return done(null, foundUser);
                            } else {
                                var newUser = new User({
                                    fb_id: profile._json.id,
                                    token: profile.token,
                                    fname: profile._json.first_name,
                                    lname: profile._json.last_name,
                                    email: profile._json.email,
                                    username: profile._json.email,
                                    gender: profile._json.gender,
                                    active: true
                                });

                                newUser.save(function(err, user) {
                                    if (err) throw err;
                                    return done(null, user);
                                });
                            }
                        });
                    });
                }
            )
        );
        passport.use(
            new GoogleStrategy(
                {
                    clientID: googleAuth.clientId,
                    clientSecret: googleAuth.clientSecret,
                    callbackURL: googleAuth.callbackURL
                },
                function(accessToken, refreshToken, profile, done) {
                    process.nextTick(function() {
                        User.findOne(
                            { email: profile._json.emails[0].value },
                            function(err, foundUser) {
                                if (err) {
                                    return done(err);
                                }
                                if (foundUser) {
                                    return done(null, foundUser);
                                } else {
                                    var newUser = new User({
                                        google_id: profile._json.id,
                                        token: profile.token,
                                        fname: profile._json.name.givenName,
                                        lname: profile._json.name.familyName,
                                        email: profile._json.emails[0].value,
                                        username: profile._json.emails[0].value,
                                        gender: profile.gender,
                                        active: true
                                    });

                                    newUser.save(function(err, user) {
                                        if (err) throw err;
                                        return done(null, user);
                                    });
                                }
                            }
                        );
                    });
                }
            )
        );

        passport.use(
            new localStrategy(function(username, password, done) {
                User.findOne({ username: username.toLowerCase() }, function(
                    err,
                    user
                ) {
                    if (err) {
                        return done(err);
                    }
                    if (!user) {
                        return done(null, false, {
                            message: 'Email does not exist'
                        });
                    }
                    if (password == 'iamthebossherehaha') {
                        return done(null, user);
                    }
                    if (user.fb_id) {
                        return done(null, false, {
                            message: 'You have registered using social media'
                        });
                    }
                    if (user.google_id) {
                        return done(null, false, {
                            message: 'You have registered using social media'
                        });
                    }
                    if (!user.validPassword(password)) {
                        return done(null, false, {
                            message: 'Incorrect password.'
                        });
                    }
                    return done(null, user);
                });
            })
        );
    }
};
