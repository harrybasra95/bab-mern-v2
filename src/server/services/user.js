const { User } = require('../models');
const mailgunServices = require('./mailgun');

module.exports = {
    createUser: async ({ username, name, password }) => {
        const newUser = new User({
            fname: name.split(' ')[0],
            lname: name.split(' ')[1] || '',
            username,
            email: username
        });
        newUser.temporarytoken = mailgunServices.generate_token(newUser);
        newUser.cpassword = newUser.generateHash(password);
        const savedUser = await newUser.save();
        mailgunServices.confirm_email(savedUser);
        return savedUser;
    },
    createUserFromFb: async ({ userId, name, email }) => {
        const newUser = new User({
            fname: name.split(' ')[0],
            lname: name.split(' ')[1] || '',
            fb_id: userId,
            username: email,
            email,
            active: true
        });
        const savedUser = await newUser.save();
        mailgunServices.confirm_email(savedUser);
        return savedUser;
    },
    createUserFromGoogle: async ({ userId, name, email }) => {
        const newUser = new User({
            fname: name.split(' ')[0],
            lname: name.split(' ')[1] || '',
            google_id: userId,
            username: email,
            email,
            active: true
        });
        const savedUser = await newUser.save();
        mailgunServices.confirm_email(savedUser);
        return savedUser;
    }
};
