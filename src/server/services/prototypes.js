Array.prototype.count = function (value) {
    return this.filter(x => x === value).length;
};

Array.prototype.getCountry = function (country) {
    return this.filter(
        value => value.c.toLowerCase() === country.toLowerCase().trim()
    );
};
