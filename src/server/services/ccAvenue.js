const crypto = require('crypto');
const { ccAvenueConfig } = require('../config');

const { merchantId, workingKey, serverIp, protocol } = ccAvenueConfig;

const createFromBody = ({ ordernumber, totalprice }) => {
    const body = `merchant_id=${merchantId}&order_id=${ordernumber}&currency=INR&amount=${totalprice}
         &redirect_url=${protocol}%3A%2F%2F${serverIp}%2Fapi%2Forder%2FccavResponseHandler
         &cancel_url=${protocol}%3A%2F%2F${serverIp}%2Fapi%2Forder%2FccavResponseHandler&language=en`;
    return encrypt(body);
};
const encrypt = plainText => {
    const m = crypto.createHash('md5');
    m.update(workingKey);
    const key = m.digest();
    const iv =
        '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f';
    const cipher = crypto.createCipheriv('aes-128-cbc', key, iv);
    let encoded = cipher.update(plainText, 'utf8', 'hex');
    encoded += cipher.final('hex');
    return encoded;
};

const decrypt = encText => {
    const m = crypto.createHash('md5');
    m.update(workingKey);
    const key = m.digest();
    const iv =
        '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f';
    const decipher = crypto.createDecipheriv('aes-128-cbc', key, iv);
    let decoded = decipher.update(encText, 'hex', 'utf8');
    decoded += decipher.final('utf8');
    return decoded;
};

const parse = data => {
    const array = data.split('&');
    const obj = {};
    array.forEach(key => {
        key = key.split('=');
        obj[key[0]] = key[1];
    });
    return obj;
};

module.exports = {
    createFromBody,
    encrypt,
    decrypt,
    parse
};
