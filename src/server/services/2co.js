const { currencyCode } = require('../config');

module.exports = {
    createFormBody: (
        { user, delivery, billing },
        { email, phone },
        { ordernumber, productdetails, shipping },
        selected_country
    ) => {
        const formBody = {
            sid: '103444918',
            mode: '2CO',
            card_holder_name: `${user.fname} ${user.lname}`,
            street_address: billing.street_no,
            street_address2: `${billing.street_no_2} ${billing.street_no_3}`,
            city: billing.city,
            state: billing.state,
            zip: billing.pincode,
            country: billing.country,
            email,
            phone,
            merchant_order_id: ordernumber,
            currency_code: currencyCode[selected_country],
            purchase_step: 'payment-method',
            ship_name: `${user.fname} ${user.lname}`,
            ship_street_address: delivery.street_no,
            ship_street_address2: `${delivery.street_no_2} ${
                delivery.street_no_3
            }`,
            ship_city: delivery.city,
            ship_state: delivery.state,
            ship_zip: delivery.pincode,
            ship_country: delivery.country,
            ship_phone: phone
        };
        let i = 0;
        productdetails.forEach(item => {
            formBody[`li_${i}_type`] = 'Product';
            formBody[`li_${i}_name`] = item.items.name;
            formBody[`li_${i}_price`] = item.price;
            i++;
        });
        formBody[`li_${i}_type`] = 'shipping';
        formBody[`li_${i}_name`] = 'Shipping';
        formBody[`li_${i}_price`] = shipping;
        return formBody;
    }
};
