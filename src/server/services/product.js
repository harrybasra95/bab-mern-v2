const { capWords } = require('../helpers');
const { currencyCode, symbolchart, sizeShort, serverIp } = require('../config');

module.exports = {
    sort: (productsObj, property, type) => {
        const products = [];
        for (const item in productsObj) {
            products.push(productsObj[item]);
        }
        products.sort((a, b) => {
            if (a[property] < b[property]) return -1;
            if (a[property] > b[property]) return 1;
            return 0;
        });
        if (!type || type === 'ascending') {
            return products;
        }
        return products.reverse();
    },
    addGalleryLabels: (products, { selected_country }) => {
        const combinedProducts = {};

        products.forEach(item => {
            if (item.uniqueid in combinedProducts) {
                const tempVar = combinedProducts[item.uniqueid].quantityChart;
                tempVar[item.size] = {
                    qty: item.quantity,
                    short: sizeShort[item.size],
                    class: item.quantity === 0 ? 'outOfStock' : ''
                };
                combinedProducts[item.uniqueid] = {
                    ...combinedProducts[item.uniqueid],
                    quantityChart: { ...tempVar },
                    totalQty:
                        combinedProducts[item.uniqueid].totalQty + item.quantity
                };
            } else {
                const quantityChart = sizeShort.getQuantityChart();
                const price = item.pricechart[selected_country];
                let salePrice;
                if (selected_country === 'in') {
                    salePrice = price - Math.ceil((price * item.sale) / 100);
                } else {
                    salePrice = price - ((price * item.sale) / 100).toFixed(2);
                }
                quantityChart[item.size] = {
                    qty: item.quantity,
                    short: sizeShort[item.size],
                    class: item.quantity === 0 ? 'outOfStock' : ''
                };
                combinedProducts[item.uniqueid] = {
                    ...item._doc,
                    quantityChart: {
                        ...quantityChart
                    },
                    totalQty: item.quantity,
                    showSale: item.sale === 0 ? 'hide' : 'show',
                    currencyCode: currencyCode[selected_country],
                    currencySymbol: symbolchart[selected_country],
                    price: item.sale === 0 ? price : salePrice,
                    originalPrice: item.pricechart[selected_country],
                    productTitle: item.name
                        .toUpperCase()
                        .replace('T SHIRT', 'T-SHIRT'),
                    colornameShow: item.colorname.replace(/-/g, ' '),
                    productLink: `/products/${item.type}/quicklook/${
                        item.uniqueid
                    }`,
                    collectionLink: `/collection/${item.series}`,
                    imageTitle: `${item.name} - ${capWords(
                        item.colorname.replace('-', ' ')
                    )} | Premium Sports Apparel | BAB`,
                    wishlistLink: `add_to_wishlist/${item.uniqueid}`
                };
            }
        });
        return combinedProducts;
    },
    addQuicklookLabels: (activeProducts, products, { selected_country }) => {
        console.log(selected_country);
        const item = activeProducts[0];
        const quantityChart = sizeShort.getQuantityChart();
        const pagelink = `${serverIp}/products/${item.type}/quicklook/${
            item.uniqueid
        }`;
        const colorsChart = {};
        let name = item.name;
        let salePrice;
        let totalQty = 0;
        if (item.type.indexOf('t-shirts') > -1) {
            name = name.toUpperCase().replace('T SHIRT', 'T-SHIRT');
        }
        const price = item.pricechart[selected_country];
        Object.keys(activeProducts).forEach(key => {
            totalQty += activeProducts[key].quantity;
            quantityChart[activeProducts[key].size] = {
                qty: activeProducts[key].quantity,
                short: sizeShort[activeProducts[key].size]
            };
        });
        Object.keys(products).forEach(key => {
            const tempItem = products[key];
            colorsChart[tempItem.colorname] = {
                colorName: tempItem.colorname,
                productLink: `/products/${tempItem.type}/quicklook/${
                    tempItem.uniqueid
                }`,
                colorCode: tempItem.color,
                secondcolor: tempItem.secondcolor,
                divTitle: tempItem.uniqueid.replace('-', ' ').toUpperCase()
            };
        });
        if (selected_country === 'in') {
            salePrice = price - Math.ceil((price * item.sale) / 100);
        } else {
            salePrice = price - ((price * item.sale) / 100).toFixed(2);
        }
        const returnData = {
            name,
            colorName: item.colorname.replace(/-/g, ' '),
            showSale: item.sale === 0 ? 'hide' : 'show',
            currencyCode: currencyCode[selected_country],
            currencySymbol: symbolchart[selected_country],
            price: item.sale === 0 ? price : salePrice,
            originalPrice: price,
            totalQty,
            quantityChart,
            colorsChart,
            fabric: item.productinfo.fabric[0],
            highlights: [...item.productinfo.highlights],
            washCare: [...item.productinfo.wcare],
            productDescription: item.description,
            showCaseImageUrl: `/img/showcase/${item.uniqueid}.jpg`,
            seriesName: `${item.series.toUpperCase()} SERIES`,
            seriesInfo: item.seriesinfo,
            pinterestShareLink: `"http://pinterest.com/pin/create/button/?url=${pagelink}&media=${serverIp}/${
                item.imgaddresses[0]
            }&description=${item.pagedescription}`,
            twitterShareLink: `https://twitter.com/home?status=${pagelink}`,
            facebookShareLink: `https://www.facebook.com/sharer/sharer.php?u=${pagelink}`,
            ...item._doc
        };
        return returnData;
    }
};
