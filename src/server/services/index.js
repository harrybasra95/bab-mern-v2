const productServices = require('./product');
const orderServices = require('./orders');
const expressServices = require('./express');
const passportServices = require('./passport');
const couponServices = require('./coupon');
const loggerServices = require('./logger');
const prototypeServices = require('./prototypes');
const ccAvenueServices = require('./ccAvenue');
const CardServices = require('./2co');
const delhiveryServices = require('./delhivery');
const intervalFunctions = require('./intervalFunctions');
const reactSession = require('./reactSession');
const cartServices = require('./cart');
const userServices = require('./user');
const mailgunServices = require('./mailgun');
const trackingServices = require('./tracking');

module.exports = {
     productServices,
     orderServices,
     expressServices,
     passportServices,
     couponServices,
     loggerServices,
     prototypeServices,
     ccAvenueServices,
     CardServices,
     delhiveryServices,
     intervalFunctions,
     reactSession,
     cartServices,
     userServices,
     mailgunServices,
     trackingServices
};
