const defaultError = 'Something went wrong.Please try again.';
const commonHelpers = require('./commonHelpers');
const authHelpers = require('./authHelpers');

module.exports = {
    commonHelpers,
    authHelpers,
    findDuplicates: arr =>
        arr.filter((item, index) => arr.indexOf(item) !== index),
    capWords: str =>
        str
            .split(' ')
            .map(val => val[0].toUpperCase() + val.substr(1).toLowerCase())
            .join(' '),
    errFlash: (req, res, redirectUrl) => {
        req.flash('error', 'Something went wrong.Please try again.');
        res.redirect(redirectUrl);
    },
    errAjax: (req, res, message = defaultError, className = '.active') => {
        res.send({ success: false, message, class: className });
    },

    alreadyActive: (req, res, next) => {
        if (req.user && req.user.active === true) {
            res.redirect('/');
            req.flash('error', 'Account has been already activated!');
        } else {
            return next();
        }
    },
    fieldIsNull: data => {
        if (!data || data.length === 0) {
            return true;
        }
        return false;
    }
};
