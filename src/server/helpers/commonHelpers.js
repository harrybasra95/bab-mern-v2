const defaultError = 'Something went wrong.Please try again.';
module.exports = {
    capWords: str => str
            .split(' ')
            .map((str) => str[0].toUpperCase() + str.substr(1).toLowerCase())
            .join(' '),
    errFlash: (req, res, redirectUrl) => {
        req.flash('error', 'Something went wrong.Please try again.');
        res.redirect(redirectUrl);
    },
    errAjax: (req, res, message = defaultError) => {
        res.send({ success: false, message });
    },
    alreadyActive: (req, res, next) => {
        if (req.user && req.user.active == true) {
            res.redirect('/');
            req.flash('error', 'Account has been already activated!');
        } else {
            return next();
        }
    },
    fieldIsNull: data => {
        if (!data || data.length == 0) {
            return true;
        } 
            return false;
    }
};
