const defaultError = 'Something went wrong.Please try again.';
module.exports = {
    userExists: (req, res, next) => {
        if (req.isAuthenticated()) {
            return next();
        }
        if (req.xhr) {
            return res.send({ success: false, defaultError });
        }
        req.flash('error', 'Please login first!');
        res.redirect('/login');
    },
    userNotExists: (req, res, next) => {
        if (!req.isAuthenticated()) {
            return next();
        }
        if (req.xhr) {
            return res.send({ success: false, defaultError });
        }
        req.flash('error', 'Already logged In');
        res.redirect('/');
    }
};
