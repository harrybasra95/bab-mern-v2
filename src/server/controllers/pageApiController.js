const { Product, Coupon, User, ReactSession, Order } = require('../models');
const axios = require('axios');
const { productServices, orderServices } = require('../services');
const { couponServices } = require('../services');
const { zohoConfig } = require('../config');

module.exports = {
     apparel: (req, res) => {
          res.send(true);
     },
     products: async (req, res) => {
          const { type, name } = req.query;
          if (!type) {
               return res.failure('Please enter a valid type');
          }
          if (!name) {
               return res.failure('Please enter a valid name');
          }
          const foundProducts = await Product.find({
               [type === 'category' ? 'type' : 'series']: name
          });
          const unSortedProducts = productServices.addGalleryLabels(
               foundProducts,
               req.session
          );
          const sortedProducts = productServices.sort(
               unSortedProducts,
               'priceindia',
               'ascending'
          );
          return res.success(sortedProducts);
     },
     allSizeProducts: async (req, res) => {
          try {
               const foundProducts = await Product.find();
               return res.success(foundProducts);
          } catch (error) {
               res.failure(error);
          }
     },
     allProducts: async (req, res) => {
          try {
               const foundProducts = await Product.find();
               const unSortedProducts = productServices.addGalleryLabels(
                    foundProducts,
                    req.session
               );
               const sortedProducts = productServices.sort(
                    unSortedProducts,
                    'priceindia',
                    'ascending'
               );
               return res.success(sortedProducts);
          } catch (error) {
               res.failure(error);
          }
     },
     checkProducts: async (req, res) => {
          const { data } = await axios.get(
               `https://inventory.zoho.in/api/v1/items?authtoken=${
                    zohoConfig.authToken
               }&organization_id=${zohoConfig.organization_id}`,
               {
                    headers: {
                         Authorization: `Zoho-authtoken ${zohoConfig.authToken}`
                    }
               }
          );
          data.items.forEach(singleItem => {
               Product.findOneAndUpdate(
                    { skunumber: Number(singleItem.sku) },
                    { quantity: Number(singleItem.stock_on_hand) },
                    { new: true },
                    (err, p) => {}
               );
          });
          res.send(true);
     },
     productData: async (req, res) => {
          try {
               const { type, id } = req.query;
               const selectedCountry = req.get('selected-country');
               const activeProducts = await Product.find({
                    type,
                    uniqueid: id
               });
               if (!activeProducts) {
                    return res.failure('Product not found');
               }
               const sameNameProducts = await Product.find({
                    name: activeProducts[0].name
               });
               const productData = productServices.addQuicklookLabels(
                    activeProducts,
                    sameNameProducts,
                    { selected_country: selectedCountry }
               );
               return res.success(productData);
          } catch (error) {
               res.failure(error);
          }
     },
     applyCoupon: async (req, res) => {
          const sessionToken = req.get('session-id');
          const selectedCountry = req.get('selected-country');
          const couponName = req.body.name;
          try {
               const foundSession = await ReactSession.findById(sessionToken);
               const foundCoupon = await Coupon.findOne({
                    name: couponName.toLowerCase()
               });
               const foundUser = await User.findById(
                    foundSession.user._id,
                    'email coupons'
               );
               if (!foundCoupon) {
                    return res.failure('Please enter a valid coupon name.');
               }
               const {
                    expiredate,
                    particularuser,
                    minamount,
                    peruser,
                    amounttype,
                    discount
               } = foundCoupon;
               const { totalPrice } = foundSession.cart;
               const isCouponApplicable = couponServices.isCouponApplicableOnProducts(
                    foundSession.cart,
                    foundCoupon,
                    selectedCountry
               );
               console.log(
                    foundUser.coupons.count(foundCoupon._id.toString()),
                    peruser
               );
               if (expiredate - Date.now() <= 0) {
                    return res.failure('Coupon has been expired.');
               }
               if (isCouponApplicable === false) {
                    return res.failure(
                         'Coupon is not available for some of your products.'
                    );
               }
               if (
                    particularuser.value === true &&
                    particularuser.id !== foundUser.email
               ) {
                    return res.failure('Coupon is not valid for you.');
               }
               if (minamount > totalPrice) {
                    return res.failure(
                         `Coupon is valid on min amount of INR ${minamount}`
                    );
               }
               if (
                    peruser > 0 &&
                    foundUser.coupons.count(foundCoupon._id.toString()) >=
                         peruser
               ) {
                    return res.failure(
                         `This coupon can be used only ${peruser} time(s)`
                    );
               }
               if (amounttype === true && selectedCountry !== 'in') {
                    return res.failure(
                         'This coupon is not applicable for this region'
                    );
               }
               if (amounttype === true) {
                    const couponDiscount = discount;
                    foundCoupon.couponDiscount = couponDiscount;
               } else if (selectedCountry === 'in') {
                    const couponDiscount = Math.ceil(
                         (totalPrice * foundCoupon.discount) / 100
                    );
                    foundCoupon.couponDiscount = couponDiscount;
               } else {
                    const couponDiscount = (
                         (totalPrice * foundCoupon.discount) /
                         100
                    ).toFixed(2);
                    foundCoupon.couponDiscount = couponDiscount;
               }
               const savedSession = await ReactSession.findByIdAndUpdate(
                    sessionToken,
                    {
                         coupon: {
                              gymAddress: foundCoupon.gymAddress,
                              type: foundCoupon.type,
                              name: foundCoupon.name,
                              cod: foundCoupon.cod,
                              couponDiscount: foundCoupon.couponDiscount
                         }
                    },
                    { new: true }
               );
               res.success(savedSession);
          } catch (error) {
               res.failure(error);
          }
     },
     removeCoupon: async (req, res) => {
          try {
               const sessionToken = req.get('session-id');
               const foundSession = await ReactSession.findByIdAndUpdate(
                    sessionToken,
                    { coupon: {} },
                    { new: true }
               );
               return res.success(foundSession);
          } catch (error) {
               return res.failure(error);
          }
     },
     fetchCouponOrders: async (req, res) => {
          try {
               const foundCoupon = await Coupon.findById(req.params.id);
               const foundOrders = await Order.find({
                    orderdate: { $gt: foundCoupon.initialdate },
                    ordernumber: { $gt: 1549 },
                    status: 'OR'
               });
               const sortedOrders = orderServices.sortCouponShares(
                    req.params.id,
                    foundOrders,
                    foundCoupon.name
               );
               sortedOrders.couponName = foundCoupon.name;
               res.success(sortedOrders);
          } catch (error) {
               res.send({ error: 'Something went wrong' });
          }
     }
};
