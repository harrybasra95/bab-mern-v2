const { Product, Cart } = require('../models');
const { reactSession } = require('../services');
const { productKeysList } = require('../config');

module.exports = {
    addToCartByUniqueId: async (req, res) => {
        const { id, size, quantity } = req.body;
        const sessionToken = req.get('session-id');
        const selectedCountry = req.get('selected-country');
        const foundProduct = await Product.findOne(
            { uniqueid: id, size },
            productKeysList
        );
        if (!foundProduct) {
            return res.failure('Product not found');
        }
        if (foundProduct.quantity > quantity) {
            return res.failure('Product quantity is not sufficient');
        }
        const savedSession = await reactSession.addProductToCart(
            foundProduct,
            quantity,
            sessionToken,
            selectedCountry
        );
        return res.success(savedSession);
    },
    removeFromCartByUniqueId: async (req, res) => {
        const { id } = req.body;
        const sessionToken = req.get('session-id');
        const savedSession = await reactSession.removeProductFromCart(
            id,
            sessionToken
        );
        return res.success(savedSession);
    },
    addToCart: (req, res) => {
        delete req.session.coupon;
        res.locals.session = req.session;
        const newCart = new Cart(req.session.cart ? req.session.cart : {});
        const uniqueid = req.body.uniqueid.trim();
        let size = req.body.size;
        const qty = req.body.qty;
        const x = ['1', '2', '3'];
        const y = ['S', 'M', 'L', 'XL'];
        if (y.includes(size) === false) {
            res.send({ success: false, message: 'Please select a valid size' });
        } else if (x.includes(qty) === false) {
            res.send({
                success: false,
                message: 'Please select a valid quantity'
            });
        } else {
            if (size === 'S') {
                size = 'Small';
            }
            if (size === 'M') {
                size = 'Medium';
            }
            if (size === 'L') {
                size = 'Large';
            }
            if (size === 'XL') {
                size = 'Extra Large';
            }
            Product.findOne(
                {
                    uniqueid,
                    size
                },
                (err, foundProduct) => {
                    if (
                        err ||
                        !foundProduct ||
                        foundProduct.length === 0 ||
                        foundProduct.quantity < 1
                    ) {
                        res.send({
                            success: false,
                            message: 'Something went wrong.Please try again'
                        });
                    } else if (
                        newCart.items[foundProduct._id] &&
                        Number(newCart.items[foundProduct._id].qty) +
                            Number(qty) >
                            3
                    ) {
                        res.send({
                            success: false,
                            message: 'Maximum quantity reached'
                        });
                    } else if (
                        newCart.items[foundProduct._id] &&
                        Number(newCart.items[foundProduct._id].qty) +
                            Number(qty) >=
                            foundProduct.quantity
                    ) {
                        res.send({
                            success: false,
                            message: 'Maximum quantity reached'
                        });
                    } else {
                        for (let i = 0; i < req.body.qty; i++) {
                            foundProduct.priceindia = foundProduct.toJSON().pricechart[
                                req.session.selected_country
                            ];
                            newCart.add(
                                foundProduct,
                                foundProduct._id,
                                req.session.selected_country
                            );
                        }
                        req.session.cart = newCart;
                        const obj = {
                            success: true,
                            message: 'Item added successfully',
                            cart: newCart.generateArray(),
                            product: foundProduct,
                            totalcart: newCart,
                            totalPrice: newCart.totalPrice,
                            totalQty: newCart.totalQty
                        };
                        res.json(obj);
                    }
                }
            );
        }
    },
    quicklookAddToCart: (req, res) => {
        delete req.session.coupon;
        res.locals.session = req.session;
        const uniqueid = req.params.uniqueid;
        const size = req.params.size;
        const newCart = new Cart(req.session.cart ? req.session.cart : {});
        Product.findOne(
            {
                uniqueid,
                size: 'Small'
            },
            (err, foundProduct) => {
                if (
                    err ||
                    !foundProduct ||
                    foundProduct.length == 0 ||
                    foundProduct.sizeInfo[size].quantity < 1
                ) {
                    res.send({
                        success: false,
                        message: 'This product is currently out of stock',
                        cart: newCart.generateArray(),
                        product: foundProduct,
                        totalQty: newCart.totalQty
                    });
                } else if (
                    newCart.items[foundProduct._id] &&
                    newCart.items[foundProduct._id].qty + 1 > 3
                ) {
                    res.send({
                        success: false,
                        message: 'Maximum quantity reached',
                        cart: newCart.generateArray(),
                        product: foundProduct,
                        totalQty: newCart.totalQty
                    });
                } else {
                    foundProduct.priceindia = foundProduct.toJSON().pricechart[
                        req.session.selected_country
                    ];
                    foundProduct.size = size;
                    newCart.add(
                        foundProduct,
                        foundProduct._id,
                        req.session.selected_country
                    );
                    req.session.cart = newCart;
                    const obj = {
                        success: true,
                        message: 'Item added to cart',
                        cart: newCart.generateArray(),
                        product: foundProduct,
                        totalQty: newCart.totalQty,
                        totalPrice: newCart.totalPrice
                    };
                    res.send(obj);
                }
            }
        );
    },
    buyNow: (req, res) => {
        delete req.session.coupon;
        res.locals.session = req.session;
        const newCart = new Cart(req.session.cart ? req.session.cart : {});
        const uniqueid = req.body.uniqueid.trim();
        let size = req.body.size;
        const qty = req.body.qty;
        const x = ['1', '2', '3'];
        const y = ['S', 'M', 'L', 'XL'];
        if (y.includes(size) == false) {
            res.send({ success: false, message: 'Please select a valid size' });
        } else if (x.includes(qty) == false) {
            res.send({
                success: false,
                message: 'Please select a valid quantity'
            });
        } else {
            if (size == 'S') {
                size = 'Small';
            }
            if (size == 'M') {
                size = 'Medium';
            }
            if (size == 'L') {
                size = 'Large';
            }
            if (size == 'XL') {
                size = 'Extra Large';
            }
            Product.findOne(
                {
                    uniqueid,
                    size
                },
                (err, product) => {
                    if (
                        err ||
                        !product ||
                        product.length == 0 ||
                        product.quantity < 1
                    ) {
                        res.send({
                            success: false,
                            message: 'Something went wrong.Please try again'
                        });
                    } else if (
                        newCart.items[product._id] &&
                        Number(newCart.items[product._id].qty) + Number(qty) > 3
                    ) {
                        res.send({
                            success: false,
                            message: 'Maximum quantity reached'
                        });
                    } else if (
                        newCart.items[product._id] &&
                        Number(newCart.items[product._id].qty) + Number(qty) >=
                            product.quantity
                    ) {
                        res.send({
                            success: false,
                            message: 'Maximum quantity reached'
                        });
                    } else {
                        for (let i = 0; i < req.body.qty; i++) {
                            product.priceindia = product.toJSON().pricechart[
                                req.session.selected_country
                            ];
                            newCart.add(
                                product,
                                product._id,
                                req.session.selected_country
                            );
                        }
                        req.session.cart = newCart;
                        const obj = {
                            success: true,
                            message: 'Item added successfully',
                            cart: newCart.generateArray(),
                            product,
                            totalcart: newCart,
                            totalPrice: newCart.totalPrice,
                            totalQty: newCart.totalQty
                        };
                        res.json(obj);
                    }
                }
            );
        }
    },
    changeProductQty: (req, res) => {
        const newCart = new Cart(req.session.cart ? req.session.cart : {});
        delete req.session.coupon;
        res.locals.session = req.session;
        Product.findById(req.params.id, (err, foundProduct) => {
            if (err) {
                throw err;
            } else if (!foundProduct) {
                res.send({
                    success: false,
                    message: 'Something went wrong.Please try again',
                    currency: 'USA',
                    cart: newCart.items[req.params.id],
                    totalcart: newCart
                });
                req.session.cart = newCart;
            } else if (newCart.items[req.params.id]) {
                if (req.params.val == 'increase') {
                    if (
                        newCart.items[req.params.id].qty ==
                            foundProduct.quantity ||
                        newCart.items[req.params.id].qty >= 3
                    ) {
                        req.session.cart = newCart;
                        res.send({
                            success: false,
                            message: 'Maximun quantity reached',
                            currency: 'USA',
                            cart: newCart.items[req.params.id],
                            totalcart: newCart
                        });
                    } else {
                        newCart.increase(
                            req.params.id,
                            req.session.selected_country
                        );
                        req.session.cart = newCart;
                        res.send({
                            success: true,
                            message: 'Quantity incresed',
                            currency: 'USA',
                            cart: newCart.items[req.params.id],
                            totalcart: newCart
                        });
                    }
                } else if (req.params.val == 'decrease') {
                    if (newCart.items[req.params.id].qty <= 1) {
                        req.session.cart = newCart;
                        res.send({
                            success: false,
                            message: 'Minimum quantity reached',
                            currency: 'USA',
                            cart: newCart.items[req.params.id],
                            totalcart: newCart
                        });
                    } else {
                        newCart.decrease(
                            req.params.id,
                            req.session.selected_country
                        );
                        req.session.cart = newCart;
                        res.send({
                            success: true,
                            message: 'Quantity decreased',
                            currency: 'USA',
                            cart: newCart.items[req.params.id],
                            totalcart: newCart
                        });
                    }
                } else if (req.params.val == 'remove') {
                    newCart.remove(req.params.id);
                    req.session.cart = newCart;
                    res.redirect('/mycart');
                } else if (req.params.val == 'remove_') {
                    newCart.remove(req.params.id);
                    req.session.cart = newCart;
                    res.send({
                        success: true,
                        message: 'Product removed',
                        currency: 'USA',
                        cart: newCart.generateArray(),
                        totalcart: newCart
                    });
                } else {
                    res.redirect('/');
                }
            }
        });
    },
    getCart: (req, res) => {
        const newCart = new Cart(req.session.cart ? req.session.cart : {});
        const obj = {
            cart: newCart.generateArray(),
            totalQty: newCart.totalQty,
            totalPrice: newCart.totalPrice
        };
        res.send(obj);
    }
};
