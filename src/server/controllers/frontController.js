'use strict';
const sha512 = require('js-sha512').sha512;
const {
    Product,
    User,
    Order,
    Coupon,
    Cart,
    Query,
    Mailgun
} = require('../models');
const helpers = require('../helpers');
const pageInfo = require('../textData/pageInfo');
const jwt = require('jsonwebtoken');
const { jwtSecret } = require('../config');
const validator = require('validator');
const { productServices, orderServices } = require('../services');

module.exports = {
    slash: (req, res) => {
        res.redirect('/apparel');
    },
    homepage: (req, res) => {
        res.render('index', { pageInfo: pageInfo.homepage() });
    },
    products: async (req, res) => {
        try {
            const foundProducts = await Product.find({});
            const unSortedProducts = productServices.addGalleryLabels(
                foundProducts,
                req.session
            );
            const sortedProducts = productServices.sort(
                unSortedProducts,
                'priceindia',
                'descending'
            );
            res.render('shop', {
                products: sortedProducts,
                pageInfo: pageInfo.products()
            });
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    wishlist: async (req, res) => {
        try {
            const foundUser = await User.findOne({ _id: req.user._id })
                .populate('wishlist')
                .exec();
            res.render('wishlist', {
                pageInfo: pageInfo.wishlist(),
                activeClass: ['', 'active', ''],
                user: foundUser
            });
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    termsAndConditions: (req, res) => {
        res.render('toc', { pageInfo: pageInfo.termsAndConditions() });
    },
    privacyPolicy: (req, res) => {
        res.render('privacypolicy', { pageInfo: pageInfo.privacyPolicy() });
    },
    stringers: async (req, res) => {
        try {
            const foundProducts = await Product.find({
                $or: [{ type: 'stringers' }, { type: 'tanks', series: 'elite' }]
            });
            const unSortedProducts = productServices.addGalleryLabels(
                foundProducts,
                req.session
            );
            const sortedProducts = productServices.sort(
                unSortedProducts,
                'priceindia',
                'descending'
            );
            res.render('shop', {
                pageInfo: pageInfo.stringers(),
                products: sortedProducts
            });
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    trackpants: async (req, res) => {
        try {
            const foundProducts = await Product.find({ type: 'trackpants' });
            const unSortedProducts = productServices.addGalleryLabels(
                foundProducts,
                req.session
            );
            const sortedProducts = productServices.sort(
                unSortedProducts,
                'priceindia',
                'descending'
            );
            res.render('shop', {
                pageInfo: pageInfo.trackpants(),
                products: sortedProducts
            });
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    tshirts: async (req, res) => {
        try {
            const foundProducts = await Product.find({ type: 't-shirts' });
            const unSortedProducts = productServices.addGalleryLabels(
                foundProducts,
                req.session
            );
            const sortedProducts = productServices.sort(
                unSortedProducts,
                'priceindia',
                'descending'
            );
            res.render('shop', {
                pageInfo: pageInfo.tshirts(),
                products: sortedProducts
            });
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    tanks: async (req, res) => {
        try {
            const foundProducts = await Product.find({ type: 'tanks' });
            const unSortedProducts = productServices.addGalleryLabels(
                foundProducts,
                req.session
            );
            const sortedProducts = productServices.sort(
                unSortedProducts,
                'priceindia',
                'descending'
            );
            res.render('shop', {
                pageInfo: pageInfo.tanks(),
                products: sortedProducts
            });
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    collections: async (req, res) => {
        try {
            const seriesName = req.params.id.toLowerCase();
            const foundProducts = await Product.find({ series: seriesName });
            const unSortedProducts = productServices.addGalleryLabels(
                foundProducts,
                req.session
            );
            const sortedProducts = productServices.sort(
                unSortedProducts,
                'priceindia',
                'descending'
            );
            res.render('shop', {
                pageInfo: pageInfo.collections(seriesName),
                products: sortedProducts,
                collection: true
            });
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    outlet: async (req, res) => {
        try {
            const foundProducts = await Product.find({ sale: { $gt: 0 } });
            const unSortedProducts = productServices.addGalleryLabels(
                foundProducts,
                req.session
            );
            const sortedProducts = productServices.sort(
                unSortedProducts,
                'priceindia',
                'descending'
            );
            res.render('shop', {
                pageInfo: pageInfo.outlet(),
                products: sortedProducts
            });
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    featured: async (req, res) => {
        try {
            const foundProducts = await Product.find({ featured: true });
            const unSortedProducts = productServices.addGalleryLabels(
                foundProducts,
                req.session
            );
            const sortedProducts = productServices.sort(
                unSortedProducts,
                'priceindia',
                'descending'
            );
            res.render('shop', {
                pageInfo: pageInfo.featured(),
                products: sortedProducts
            });
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    returns: (req, res) => {
        res.render('returns', { pageInfo: pageInfo.returns() });
    },
    faqs: (req, res) => {
        res.render('faqs', { pageInfo: pageInfo.faqs() });
    },
    contactUs: (req, res) => {
        res.render('contact_us', { pageInfo: pageInfo.contactUs() });
    },
    payWithPaytm: async (req, res) => {
        try {
            const foundOrder = await Order.findOne({
                ordernumber: req.params.id
            });
            const foundUser = await User.findById(req.user._id);
            if (
                foundOrder &&
                foundUser.orders.indexOf(foundOrder._id) > -1 &&
                foundOrder.status == 'PP'
            ) {
                res.render('paypaytm', {
                    pageInfo: pageInfo.payWithPaytm(),
                    order: foundOrder
                });
            } else {
                res.redirect('/apparel');
            }
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    myCart: (req, res) => {
        if (!req.session.cart) {
            res.render('mycart', {
                pageInfo: pageInfo.payWithPaytm(),
                cart: null,
                csrfToken: req.csrfToken()
            });
        } else {
            const newCart = new Cart(req.session.cart);
            const totalPrice = req.session.cart.totalPrice;
            res.render('mycart', {
                pageInfo: pageInfo.payWithPaytm(),
                cart: newCart.generateArray(),
                totalPrice,
                csrfToken: req.csrfToken()
            });
        }
    },
    login: (req, res) => {
        res.render('login', {
            pageInfo: pageInfo.login(),
            csrfToken: req.csrfToken()
        });
    },
    orders: async (req, res) => {
        try {
            const foundUser = await User.findOne({ _id: req.user._id })
                .populate('orders')
                .exec();
            res.render('orders', {
                pageInfo: pageInfo.orders(),
                user: foundUser,
                accountTitle: 'Orders',
                activeClass: ['active', '', '']
            });
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    singleOrder: async (req, res) => {
        try {
            const foundOrder = await Order.findOne({
                ordernumber: req.params.id
            })
                .populate('returndetails')
                .exec();
            const foundUser = await User.findById(req.user._id);
            if (foundUser.orders.indexOf(foundOrder._id) > -1) {
                const invoiceName = sha512(`${foundOrder.ordernumber}1`);
                res.render('orderdetails', {
                    pageInfo: pageInfo.singleOrder(req.params.id),
                    order: foundOrder,
                    invoice: invoiceName
                });
            } else {
                req.flash('error', 'Something went wrong.Please try again');
                res.redirect('/orders');
            }
        } catch (error) {
            req.flash('error', 'Something went wrong.Please try again');
            res.redirect('/orders');
        }
    },
    accountDetails: async (req, res) => {
        try {
            const foundUser = await User.findOne({ _id: req.user._id })
                .populate('address')
                .populate('wishlist')
                .exec();
            res.render('account_details', {
                pageInfo: pageInfo.accountDetails(),
                click: null,
                user: foundUser,
                csrfToken: req.csrfToken(),
                activeClass: ['', 'active', '']
            });
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    changePassword: async (req, res) => {
        try {
            const foundUser = await User.findOne({ _id: req.user._id })
                .populate('address')
                .populate('wishlist')
                .exec();
            res.render('account_details', {
                pageInfo: pageInfo.changePassword(),
                click: 'cp',
                user: foundUser,
                csrfToken: req.csrfToken(),
                activeClass: ['', 'active', '']
            });
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    signUp: (req, res) => {
        res.render('sign_up', {
            pageInfo: pageInfo.signUp(),
            csrfToken: req.csrfToken()
        });
    },
    forgotPassword: (req, res) => {
        res.render('forgetpassword', {
            pageInfo: pageInfo.forgotPassword(),
            csrfToken: req.csrfToken()
        });
    },
    couponShare: async (req, res) => {
        try {
            const foundCoupon = await Coupon.findById(req.params.id);
            const foundOrders = await Order.find({
                orderdate: { $gt: foundCoupon.initialdate },
                status: 'OR'
            });
            const sortedOrders = orderServices.sortCouponShares(
                req.params.id,
                foundOrders,
                foundCoupon.name
            );
            sortedOrders.couponName = foundCoupon.name;
            res.render('couponshare', {
                data: sortedOrders,
                pageInfo: pageInfo.default()
            });
        } catch (error) {
            res.send({ error: 'Something went wrong' });
        }
    },
    accountActivate: async (req, res) => {
        try {
            const foundUser = await User.findOne({
                temporarytoken: req.params.token
            });
            const token = req.params.token;
            jwt.verify(token, jwtSecret, err => {
                if (err) {
                    res.render('activate', {
                        success: false,
                        gtitle: 'SHOP',
                        pageInfo: pageInfo.accountActivate(),
                        message: 'Invalid token'
                    });
                } else if (!foundUser) {
                    res.render('activate', {
                        success: false,
                        gtitle: 'SHOP',
                        pageInfo: pageInfo.accountActivate(),
                        message: 'Invalid token'
                    });
                } else {
                    foundUser.temporarytoken = false;
                    foundUser.active = true;
                    if (req.user) {
                        req.user.active = true;
                    }
                    foundUser.save((err, savedUser) => {
                        if (err) {
                            console.log(err);
                        } else {
                            res.render('activate', {
                                success: true,
                                message: 'Account has been verified',
                                pageInfo: pageInfo.accountActivate()
                            });
                            Mailgun.email_confirmed(savedUser);
                        }
                    });
                }
            });
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    },
    forgetPasswordTokenCreation: async (req, res) => {
        try {
            const x = validator.isEmail(req.body.email);
            if (
                req.body.email == null ||
                req.body.email === '' ||
                x === false
            ) {
                res.send({
                    success: false,
                    message: 'Please provide a valid Email'
                });
            } else {
                const foundUser = await User.findOne({ email: req.body.email });
                if (!foundUser) {
                    res.send({
                        success: false,
                        message: 'Email id is not been registered'
                    });
                } else if (foundUser.active === false) {
                    res.send({
                        success: false,
                        message:
                            'Your account has not been activated.Please activate it'
                    });
                } else {
                    foundUser.resettoken = Mailgun.generate_token(foundUser);
                    foundUser.save((err, savedUser) => {
                        if (err) throw err;
                        else {
                            Mailgun.forgot_password(savedUser);
                            res.send({
                                success: true,
                                message: 'An mail has been sent to your Email'
                            });
                        }
                    });
                }
            }
        } catch (error) {
            res.send({ success: false, message: error });
        }
    },
    quicklookProduct: async (req, res) => {
        try {
            const activeProducts = await Product.find({
                type: req.params.id,
                uniqueid: req.params.uniqueid
            });
            const sameNameProducts = await Product.find({
                name: activeProducts[0].name
            });
            const productData = productServices.addQuicklookLabels(
                activeProducts,
                sameNameProducts,
                req.session
            );
            res.render('quicklook', {
                pageInfo: pageInfo.quicklook(activeProducts[0]),
                productData,
                product: sameNameProducts,
                activeproduct: activeProducts
            });
        } catch (error) {
            helpers.errFlash(req, res, '/products');
        }
    },
    updateUser: async (req, res) => {
        try {
            const parts2 = `${req.body.user.date}/${req.body.user.month}/${
                req.body.user.year
            }`;
            const parts = parts2.split('/');
            const mydate = new Date(parts[2], parts[0], parts[1], 0, 0, 0);
            req.body.user.dob = mydate;
            await User.findByIdAndUpdate(req.user._id, req.body.user);
            res.redirect('/account_details');
        } catch (error) {
            helpers.errFlash(req, res, '/products');
        }
    },
    checkout: (req, res) => {
        const newCart = new Cart(req.session.cart ? req.session.cart : {});
        if (req.user) {
            User.findById(req.user._id)
                .populate('address')
                .exec((err, foundUser) => {
                    if (err) throw err;
                    if (foundUser.address.length === 0) {
                        foundUser.address[0] = {};
                    }
                    res.render('checkout', {
                        pageInfo: pageInfo.checkout(),
                        gtitle: 'SALE',
                        totalcart: newCart,
                        user: foundUser,
                        csrfToken: req.csrfToken()
                    });
                });
        } else {
            const user2 = {
                address: {}
            };
            user2.address[0] = {};
            res.render('checkout', {
                pageInfo: pageInfo.checkout(),
                gtitle: 'SALE',
                totalcart: newCart,
                address: null,
                user: user2,
                csrfToken: req.csrfToken()
            });
        }
    },
    querySent: async (req, res) => {
        try {
            const x = validator.isAlpha(req.body.fname);
            const y = validator.isAlpha(req.body.lname);
            if (
                req.body.fname.length === 0 ||
                req.body.lname.length === 0 ||
                x === false ||
                y === false
            ) {
                res.send({
                    success: false,
                    message: 'Please enter a valid Name'
                });
            } else if (validator.isEmail(req.body.eadd) === false) {
                res.send({
                    success: false,
                    message: 'Please enter a valid Email'
                });
            } else if (req.body.message.length < 10) {
                res.send({
                    success: false,
                    message: 'Please enter a valid Query'
                });
            } else if (
                validator.isNumeric(req.body.phone) === false ||
                req.body.phone.length !== 10
            ) {
                res.send({
                    success: false,
                    message: 'Please enter a valid Phone Number'
                });
            } else {
                const totalNo = await Query.count();
                req.body.qnumber = totalNo;
                const savedQuery = await Query.create(req.query);
                Mailgun.query_recieved(savedQuery);
                res.send({
                    success: true,
                    message: 'Your query has been submitted'
                });
            }
        } catch (error) {
            throw error;
        }
    },
    selectRegion: (req, res) => {
        const country = {
            in: 'https://in.babclothing.com',
            us: 'https://us.babclothing.com',
            ae: 'https://ae.babclothing.com',
            au: 'https://au.babclothing.com',
            row: 'https://row.babclothing.com',
            uk: 'https://uk.babclothing.com',
            eu: 'https://eu.babclothing.com',
            ca: 'https://ca.babclothing.com',
            nz: 'https://nz.babclothing.com',
            none: 'https://in.babclothing.com'
        };
        if (country[req.body.region] != undefined) {
            delete req.session.cart;
            req.session.selected_country = req.body.region;
            res.send({
                success: true,
                add: country[req.body.region] + req.body.path
            });
        } else {
            res.send({
                success: false,
                add: `https://in.babclothing.com/${req.body.path}`
            });
        }
    },
    passwordResetTokenCheck: async (req, res) => {
        try {
            const foundUser = await User.findOne({
                resettoken: req.params.token
            });
            const token = req.params.token;
            jwt.verify(token, jwtSecret, err => {
                if (err) {
                    res.render('forgetpassword', {
                        success: false,
                        gtitle: 'SHOP',
                        csrfToken: req.csrfToken(),
                        error: 'Invalid token, Please try again'
                    });
                } else if (!foundUser) {
                    res.render('forgetpassword', {
                        success: false,
                        gtitle: 'SHOP',
                        csrfToken: req.csrfToken(),
                        error: 'Invalid token, Please try again'
                    });
                } else {
                    foundUser.resettoken = false;
                    foundUser.save((err, savedUser) => {
                        if (err) {
                            console.log(err);
                        } else {
                            res.render('newpassword', {
                                success: true,
                                message: 'Please enter your new password',
                                csrfToken: req.csrfToken(),
                                gtitle: 'SHOP',
                                user: savedUser
                            });
                        }
                    });
                }
            });
        } catch (error) {
            if (error) throw error;
        }
    },
    passwordResetCreateNew: async (req, res) => {
        try {
            const foundUser = await User.findByUsername(req.body.email);
            if (!foundUser) {
                res.send({
                    success: false,
                    message: 'user not found,Please try again'
                });
            } else if (
                req.body.password === null ||
                req.body.password === '' ||
                req.body.cpassword === null ||
                req.body.cpassword === ''
            ) {
                res.send({
                    success: false,
                    message: 'Please enter a valid password'
                });
            } else if (req.body.password !== req.body.cpassword) {
                res.send({
                    success: false,
                    message: "Passwords don't match"
                });
            } else {
                const newUser = new User({});
                foundUser.cpassword = newUser.generateHash(req.body.password);
                await foundUser.setPassword(req.body.password);
                await foundUser.save();
                res.send({
                    success: true,
                    message: 'Password changed successfullly',
                    url: '/login'
                });
            }
        } catch (error) {
            res.send({
                success: false,
                message: 'Invaid User,Please try again'
            });
        }
    },
    updatePassword: async (req, res) => {
        try {
            const foundUser = await User.findById(req.user._id);
            if (!foundUser) {
                req.flash('error', 'user does not exist');
                res.redirect('/account_details');
            } else {
                const a = foundUser.validPassword(req.body.oldpassword);
                if (a === false) {
                    req.flash('error', 'Old password does not match');
                    res.redirect('/account_details/changepass');
                } else if (
                    a === true &&
                    req.body.newpassword === req.body.oldpassword
                ) {
                    req.flash('error', 'No change in passwords');
                    res.redirect('/account_details/changepass');
                } else if (req.body.newpassword !== req.body.newcpassword) {
                    req.flash('error', 'New passwords does not match');
                    res.redirect('/account_details/changepass');
                } else {
                    await foundUser.setPassword(req.body.newpassword);
                    await foundUser.save();
                    req.flash('success', 'Password Changed Successfully.');
                    res.redirect('/account_details');
                }
            }
        } catch (error) {
            helpers.errFlash(req, res, '/apparel');
        }
    }
};
