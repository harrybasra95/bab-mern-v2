const { Subscribe } = require('../models');
const { emailValidation, phoneValidation } = require('../validations');

module.exports = {
    subscribe: async (req, res) => {
        try {
            const { email, phone } = req.body;
            const foundUser = await Subscribe.findOne({
                email: req.body.email
            });
            if (foundUser) {
                return res.failure('You are already subscribed!!!!');
            }
            if (!emailValidation(email)) {
                return res.failure('Please enter a valid E-mail');
            }
            if (phoneValidation(phone, req.session.selected_country)) {
                return res.failure('Please enter a valid Number');
            }
            await Subscribe.create(req.body);
            res.success({
                message: 'Thank you for subscribing'
            });
        } catch (error) {
            res.failure(error);
        }
    }
};
