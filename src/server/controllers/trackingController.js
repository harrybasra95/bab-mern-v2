const { TrackSession, ReactSession } = require('../models');
const { reactSession } = require('../services');

module.exports = {
     createSession: async (req, res) => {
          try {
               const { oldId, currentId } = req.body;
               const sessionId = req.get('session-id');
               if (!oldId) {
                    const foundReactSession = await ReactSession.findById(
                         sessionId
                    );
                    const newTrackSession = new TrackSession({
                         trackIds: [currentId],
                         sessionId,
                         userId: foundReactSession.isLoggedIn
                              ? foundReactSession.user._id
                              : null
                    });
                    await newTrackSession.save();
                    const savedReactSession = await reactSession.addTrackId(
                         currentId
                    );
                    return res.success(savedReactSession);
               }
               const foundTrackSession = await TrackSession.findOne({
                    trackIds: req
               });
          } catch (error) {
               res.failure(error);
          }
     }
};
