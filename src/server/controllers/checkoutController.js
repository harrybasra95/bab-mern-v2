const axios = require('axios');
const moment = require('moment');
const ShippingList = require('../shipping');
const {
    Order,
    Mailgun,
    User,
    Coupon,
    Product,
    Cart,
    ReturnOrder
} = require('../models');
const {
    orderServices,
    ccAvenueServices,
    delhiveryServices
} = require('../services');
const PaytmChecksum = require('../paytm/checksum');
const { paytmConfig, ccAvenueConfig } = require('../config');
const {
    nameValidation,
    phoneValidation,
    addressValidation
} = require('../validations');
const {
    countryFullForm,
    RowCountryList,
    EuCountryList,
    paymentTypeList
} = require('../config');
const PincodeList = require('../pincode.json');

module.exports = {
    createOrder: async (req, res) => {
        try {
            const { selected_country, coupon } = req.session;
            let { billing } = req.body;
            const {
                user,
                checkout_checkbox,
                delivery,
                delivery_type,
                payment_type
            } = req.body;
            const { fname, lname, phone } = user;
            const countryFromShippingList = ShippingList.getCountry(
                delivery.country.toLowerCase()
            );
            let shippingPrice = countryFromShippingList[0].r;
            const newCart = new Cart(req.session.cart ? req.session.cart : {});
            const { totalPrice, totalQty } = newCart;
            if (newCart.totalQty === 0) {
                return res.send({
                    success: false,
                    message: 'Please select add some products to your cart',
                    class: 'active',
                    redirecturl: '/'
                });
            }
            req.body.delivery_type = 'Express';
            billing =
                checkout_checkbox === 'true' ? { ...delivery } : { ...billing };
            delivery.street_no = `${delivery.street_no} ${
                delivery.street_no_2
            } ${delivery.street_no_3}`;
            if (!nameValidation(fname) || !nameValidation(lname)) {
                return res.send({
                    success: false,
                    message: 'Please enter a valid name between 3 to 20 char',
                    class: '.fullname'
                });
            }
            if (!phoneValidation(phone)) {
                return res.send({
                    success: false,
                    message: 'Please enter a valid Phone',
                    class: '.phone'
                });
            }
            if (
                !addressValidation(delivery) ||
                !addressValidation(billing) ||
                countryFromShippingList.length === 0
            ) {
                return res.send({
                    success: false,
                    message: 'Please enter a valid address ',
                    class: '.dcountry'
                });
            }
            if (selected_country !== 'in' && delivery_type === 'null') {
                return res.send({
                    success: false,
                    message: 'Please choose a delivery type',
                    class: '.delivery_type'
                });
            }
            if (req.user.active === false) {
                return res.send({
                    success: false,
                    message: 'Please activate your account',
                    class: '.active'
                });
            }
            if (
                payment_type === 'null' ||
                ['1', '3', '2', 'card', 'paypal'].includes(payment_type) ===
                    false
            ) {
                return res.send({
                    success: false,
                    message: 'Please select a payment method',
                    class: '.payment'
                });
            }
            if (coupon && coupon.cod === false && payment_type === '1') {
                return res.send({
                    success: false,
                    message: 'Something went wrong.Please try again',
                    class: '.active'
                });
            }
            if (
                selected_country === 'in' &&
                ['1', '2', '3'].includes(payment_type) === false
            ) {
                return res.send({
                    success: false,
                    message: 'Something went wrong.Please try again',
                    class: '.active'
                });
            }
            if (
                selected_country !== 'in' &&
                ['1', '2', '3'].includes(payment_type) === true
            ) {
                return res.send({
                    success: false,
                    message: 'Something went wrong.Please try again',
                    class: '.active'
                });
            }
            if (
                selected_country === 'row' &&
                RowCountryList.indexOf(delivery.country) < 0
            ) {
                return res.send({
                    success: false,
                    message: 'Please enter a valid address ',
                    class: '.dcountry'
                });
            }
            if (
                selected_country === 'eu' &&
                EuCountryList.indexOf(delivery.country) < 0
            ) {
                res.send({
                    success: false,
                    message: 'Please enter a valid address ',
                    class: '.dcountry'
                });
            }
            if (selected_country === 'in' || selected_country === 'none') {
                const output = PincodeList.filter(
                    value => value.Pincode === Number(delivery.pincode.trim())
                )[0];

                delivery.city = output.City;
                delivery.state = output.State;
                delivery.country = 'India';
                if (totalPrice >= 500) {
                    shippingPrice = 0;
                }
            } else {
                billing.country = delivery.country =
                    countryFullForm[selected_country];
            }
            delivery.fname = user.fname;
            delivery.lname = user.lname;
            delivery.phone = user.phone;
            const body = await delhiveryServices.pincode(delivery.pincode);
            if (!body.delivery_codes) {
                return res.send({
                    success: false,
                    message: 'Delivery is not possible at this address.'
                });
            }
            const totalOrdersCount = await Order.count();
            const newOrder = new Order({
                selected_country,
                persondetails: {
                    eadd: req.user.email,
                    fname,
                    lname,
                    phone
                },
                productdetails: [...newCart.generateArray()],
                deliveryaddress: { ...delivery },
                billingaddress: { ...billing },
                ordernumber: totalOrdersCount + 1005,
                status: 'PP',
                userid: req.user._id,
                subtotalprice: totalPrice,
                paymenttype: paymentTypeList[payment_type],
                temporarytoken: Mailgun.generate_token_1h(this),
                shipping: shippingPrice,
                tax: 0,
                totalqty: totalQty,
                type: delivery_type,
                discount: coupon ? coupon.value : 0,
                coupon: coupon || null,
                totalprice: totalPrice + shippingPrice
            });
            newOrder.totalprice -= newOrder.discount;
            if (req.session.coupon) {
                await User.findByIdAndUpdate(req.user._id, {
                    lastcoupon: coupon.name
                });
                if (req.session.coupon.particularuser === true) {
                    await Coupon.findByIdAndUpdate(coupon._id, {
                        expiredate: new Date()
                    });
                }
            }
            delete req.session.coupon;
            delete req.session.cart;
            res.locals.cart = req.session.cart;
            const savedOrder = await newOrder.save();
            const promisesArray = [];
            newCart.generateArray().forEach(singleProduct => {
                promisesArray.push(
                    new Promise((resolve, reject) => {
                        Product.findByIdAndUpdate(
                            singleProduct.items._id,
                            { $inc: { quantity: -1 } },
                            err => {
                                if (err) {
                                    reject();
                                }
                                resolve();
                            }
                        );
                    })
                );
            });
            await Promise.all(promisesArray);
            if (payment_type !== 'paypal') {
                orderServices.findUserAndMakeOrder(req, res, savedOrder);
            } else if (payment_type === 'paypal') {
                makeOrderPaypal(req, res, savedOrder);
            }
        } catch (error) {
            return res.send({
                success: false,
                message: 'Something went wrong.Please try again',
                class: '.active'
            });
        }
    },
    makeOrder2co: async (req, res) => {
        const foundOrder = await Order.findOne({
            ordernumber: req.params.id
        });
        orderServices.makeOrder(req, res, foundOrder, req.body);
    },
    makeOrderCc: async (req, res) => {
        const x = ccAvenueServices.decrypt(req.body.encResp);
        const array = x.split('&');
        const b = {};
        array.forEach(q => {
            q = q.split('=');
            b[q[0]] = q[1];
        });
        const pData = b;
        const foundOrder = await Order.findOne({ ordernumber: pData.order_id });
        if (foundOrder && foundOrder.checked === true) {
            req.flash('error', 'Something went wrong.Please try again .');
            res.redirect('/apparel');
        } else {
            const foundUser = await User.findById(req.user._id);
            if (foundUser.orders.indexOf(foundOrder._id) > -1) {
                if (!foundOrder) {
                    req.flash(
                        'error',
                        'Something went wrong.Please try again .'
                    );
                    res.redirect('/apparel');
                } else if (pData.order_status === 'Success') {
                    orderServices.makeOrderPrepaid(req, res, foundOrder, pData);
                } else {
                    req.body = pData;
                    foundOrder.checked = true;
                    foundOrder.status = 'F';
                    foundOrder.paymentdetails = pData;
                    foundOrder.save((err, savedOrder) => {
                        const elements = foundOrder.productdetails;
                        elements.forEach(item => {
                            Product.findById(
                                item.items._id,
                                (err, foundProduct) => {
                                    foundProduct.quantity += item.qty;
                                    foundProduct.save();
                                    if (
                                        savedOrder.coupon &&
                                        savedOrder.coupon.length !== 0 &&
                                        savedOrder.coupon.particularuser ===
                                            true
                                    ) {
                                        Coupon.findById(
                                            savedOrder.coupon._id,
                                            (err, foundCoupon) => {
                                                if (err) {
                                                    throw err;
                                                }
                                                foundCoupon.expiredate = new Date();
                                                foundCoupon.expiredate.setDate(
                                                    foundCoupon.expiredate.getDate() +
                                                        30
                                                );
                                                foundCoupon.save(err => {
                                                    if (err) {
                                                        throw err;
                                                    }
                                                    req.flash(
                                                        'error',
                                                        'Order failed.'
                                                    );
                                                    res.redirect('/orders');
                                                });
                                            }
                                        );
                                    } else {
                                        req.flash('error', 'Order failed.');
                                        res.redirect('/orders');
                                    }
                                }
                            );
                        });
                    });
                }
            } else {
                req.flash('error', 'Something went wrong.Please try again .');
                res.redirect('/apparel');
            }
        }
    },
    cancelOrderWithPayments: async (req, res) => {
        try {
            const foundOrder = await Order.findById(req.params.id);
            if (foundOrder.paymenttype === 'paytmyo') {
                const PAYTM_MERCHANT_KEY = paytmConfig.merchentKey;
                const orderCheckParamList = {
                    MID: paytmConfig.merchentId,
                    ORDER_ID: foundOrder.ordernumber
                };
                const orderCheckResult = await PaytmChecksum.genchecksum(
                    orderCheckParamList,
                    PAYTM_MERCHANT_KEY
                );

                orderCheckResult.CHECKSUMHASH = encodeURIComponent(
                    orderCheckResult.CHECKSUMHASH
                );
                const orderCheckFinalstring = `JsonData=${JSON.stringify(
                    orderCheckResult
                )}`;
                const orderCheckServer = `https://pguat.paytm.com/oltp/HANDLER_INTERNAL/getTxnStatus?${orderCheckFinalstring}`;
                const orderCheckBody = await axios.post(orderCheckServer);
                const orderCheckResponse = JSON.parse(orderCheckBody);
                const patymResponseStatus = orderCheckResponse.STATUS;
                if (patymResponseStatus !== 'TXN_SUCCESS') {
                    return res.send({
                        success: false,
                        message: 'Something went wrong.Please try again'
                    });
                }
                const orderRefundParamList = {
                    MID: paytmConfig.merchentId,
                    ORDER_ID: foundOrder.ordernumber,
                    TXNID: foundOrder.paymentdetails.TXNID,
                    REFUNDAMOUNT: foundOrder.totalPrice
                };
                const orderRefundResult = await PaytmChecksum.genchecksum(
                    orderRefundParamList,
                    PAYTM_MERCHANT_KEY
                );
                orderRefundResult.CHECKSUMHASH = encodeURIComponent(
                    orderRefundResult.CHECKSUMHASH
                );
                const orderRefundFinalstring = `JsonData=${JSON.stringify(
                    orderRefundResult
                )}`;
                const orderRefundServer = `https://pguat.paytm.com/oltp/HANDLER_INTERNAL/REFUND?${orderRefundFinalstring}`;
                const orderRefundBody = await axios.post(orderRefundServer);
                const orderRefundResponse = JSON.parse(orderRefundBody);
                foundOrder.status = 'C';
                foundOrder.canceldetails = orderRefundResponse;
                const { coupon } = await foundOrder.save();
                const purchasedProducts = foundOrder.productdetails;
                const ProductPromisesArray = [];
                purchasedProducts.forEach(item => {
                    ProductPromisesArray.push(
                        new Promise((resolve, reject) => {
                            Product.findByIdAndUpdate(
                                item.items._id,
                                {
                                    $inc: { quantity: item.qty }
                                },
                                err => {
                                    if (err) {
                                        return reject(err);
                                    }
                                    return resolve();
                                }
                            );
                        })
                    );
                });
                Promise.all(ProductPromisesArray).then(async () => {
                    if (coupon || coupon.particularuser === true) {
                        await Coupon.findByIdAndUpdate(coupon._id, {
                            expiredate: moment().add(30, 'days')
                        });
                    }
                    res.send({
                        success: true,
                        message:
                            'Your order has been cancelled.Your payment will be refunded within 72 hours.'
                    });
                });
            } else if (foundOrder.paymenttype === 'cc') {
                const workingKey = ccAvenueConfig.workingKey;
                const encrp = ccAvenueServices.encrypt(
                    JSON.stringify({ order_no: foundOrder.ordernumber }),
                    workingKey
                );
                const reqData = {
                    command: 'orderStatusTracker',
                    enc_request: encrp,
                    access_code: 'AVSV79FG49CK32VSKC',
                    request_type: 'json'
                };
                const headers = {
                    'content-type': 'application/x-www-form-urlencoded'
                };
                const body = await axios.post(
                    ccAvenueConfig.ccAvenueApiServer,
                    reqData,
                    { headers }
                );
                let response = body.split('&');
                let p = response[0].split('=');
                if (p[1] !== '0') {
                    return res.send({
                        success: false,
                        message: 'Something went wrong.Please try again'
                    });
                }
                p = response[1].split('=');
                response = ccAvenueServices.decrypt(p[1], workingKey);
                response = JSON.parse(response);
                const y = response.Order_Status_Result.order_status;
                if (y === 'Successful' || y === 'Shipped') {
                    const order_value = {
                        reference_no: response.Order_Status_Result.reference_no,
                        refund_amount:
                            foundOrder.subtotalprice - foundOrder.discount,
                        refund_ref_no: `REF${foundOrder.ordernumber}`
                    };
                    const encrp = ccAvenueServices.encrypt(
                        JSON.stringify(order_value),
                        workingKey
                    );
                    const options = {
                        method: 'POST',
                        url: ccAvenueConfig.ccAvenueApiServer,
                        headers: {
                            'content-type': 'application/x-www-form-urlencoded'
                        },
                        form: {
                            command: 'refundOrder',
                            enc_request: encrp,
                            access_code: 'AVSV79FG49CK32VSKC',
                            request_type: 'json'
                        }
                    };
                    request(options, (error, response, body) => {
                        if (error) {
                            res.send({
                                success: false,
                                message: 'Something went wrong.Please try again'
                            });
                        } else {
                            var response = body.split('&');
                            let p = response[0].split('=');
                            if (p[1] == '0') {
                                p = response[1].split('=');
                                response = ccav.decrypt(p[1], workingKey);
                                response = JSON.parse(response);
                                console.log(response);
                                if (
                                    response.Refund_Order_Result
                                        .refund_status == '0'
                                ) {
                                    foundOrder.status = 'C';
                                    foundOrder.canceldetails = response;
                                    foundOrder.save((err, savedOrder) => {
                                        const elements =
                                            foundOrder.productdetails;
                                        elements.forEach(item => {
                                            product.findById(
                                                item.items._id,
                                                (err, foundProduct) => {
                                                    foundProduct.quantity +=
                                                        item.qty;
                                                    foundProduct.save(
                                                        (
                                                            err,
                                                            savedProduct
                                                        ) => {}
                                                    );
                                                    if (
                                                        savedOrder.coupon &&
                                                        savedOrder.coupon
                                                            .length != 0 &&
                                                        savedOrder.coupon
                                                            .particularuser ==
                                                            true
                                                    ) {
                                                        coupon.findById(
                                                            savedOrder.coupon
                                                                ._id,
                                                            (
                                                                err,
                                                                foundCoupon
                                                            ) => {
                                                                if (err) {
                                                                    throw err;
                                                                }
                                                                foundCoupon.expiredate = new Date();
                                                                foundCoupon.expiredate.setDate(
                                                                    foundCoupon.expiredate.getDate() +
                                                                        30
                                                                );
                                                                foundCoupon.save(
                                                                    (
                                                                        err,
                                                                        savedCoupon
                                                                    ) => {
                                                                        if (
                                                                            err
                                                                        ) {
                                                                            throw err;
                                                                        }
                                                                        res.send(
                                                                            {
                                                                                success: true,
                                                                                message:
                                                                                    'Your order has been cancelled.Your payment will be refunded within 72 hours.'
                                                                            }
                                                                        );
                                                                    }
                                                                );
                                                            }
                                                        );
                                                    } else {
                                                        res.send({
                                                            success: true,
                                                            message:
                                                                'Your order has been cancelled.Your payment will be refunded within 72 hours.'
                                                        });
                                                    }
                                                }
                                            );
                                        });
                                    });
                                } else {
                                    res.send({
                                        success: false,
                                        message:
                                            'Refunding is available after two hours of order placing.Please try after sometime'
                                    });
                                }
                            } else {
                                res.send({
                                    success: false,
                                    message:
                                        'Something went wrong.Please try again'
                                });
                            }
                        }
                    });
                } else if (y == 'Awaited' || y == 'Initiated') {
                    res.send({
                        success: false,
                        message: 'Something went wrong.Please try again'
                    });
                } else {
                    res.send({
                        success: false,
                        message: 'Something went wrong.Please try again'
                    });
                }
            }
        } catch (error) {}
    },
    cancelOrderWithoutPayments: async (req, res) => {
        try {
            const orderStatusArray = ['OS', 'C', 'PP', 'R', 'S', 'D', 'RRR'];
            const foundOrder = await Order.findOne({
                ordernumber: req.params.id
            });
            const foundUser = await User.findById(req.user._id);
            if (
                !foundOrder ||
                !foundUser ||
                foundUser.orders.indexOf(foundOrder._id) < 0 ||
                orderStatusArray.indexOf(foundOrder.status) > -1
            ) {
                return res.send({
                    success: false,
                    message: 'Something went wrong.Please try again'
                });
            }
            const cancelStatus = await delhiveryServices.cancel(
                foundOrder.waybill
            );
            if (cancelStatus.status !== true) {
                return res.send({
                    success: false,
                    message: 'Something went wrong.Please try again'
                });
            }
            Mailgun.order_canceled(foundOrder);
            const productsList = foundOrder.productdetails;
            const promisesArray = [];
            productsList.forEach(singleProduct => {
                promisesArray.push(
                    new Promise((resolve, reject) => {
                        Product.findByIdAndUpdate(
                            singleProduct.items._id,
                            { $inc: { quantity: 1 } },
                            err => {
                                if (err) {
                                    reject();
                                }
                                resolve();
                            }
                        );
                    })
                );
            });
            await Promise.all(promisesArray);
            if (
                foundOrder.coupon &&
                foundOrder.coupon.particularuser === true
            ) {
                await Coupon.findByIdAndUpdate(foundOrder.coupon._id, {
                    expiredate: moment().add(30, 'days')
                });
            }
            foundOrder.selfcancel = true;
            foundOrder.status = 'C';
            await foundOrder.save();
            if (foundOrder.paymenttype === 'cod') {
                return res.send({
                    success: true,
                    message: 'Your order has been successfully canceled.'
                });
            }
            res.send({
                success: true,
                message:
                    'Your order has been successfully canceled.Your amount will be refunded in 3-4 days'
            });
        } catch (error) {
            console.log(error);
        }
    },
    returnOrder: async (req, res) => {
        const foundOrder = await Order.findOne({ ordernumber: req.params.id });
        const foundUser = await User.findById(req.user._id);
        const date = foundOrder.deliverydate;
        date.setDate(date.getDate() + 15);
        if (
            !foundUser ||
            !foundOrder ||
            foundUser.orders.indexOf(req.params.id) === -1 ||
            foundOrder.status !== 'D'
        ) {
            return res.send({
                success: false,
                message: 'Your return time period of 14 days has ended.'
            });
        }
        if (moment(foundOrder.deliverydate).add(15, 'days') - new Date() < 0) {
            foundOrder.status = 'OS';
            await foundOrder.save();
            return res.send({
                success: false,
                message: 'Your return time period of 14 days has ended.'
            });
        }
        const totalCount = await ReturnOrder.count();
        const newReturnOrder = new ReturnOrder({
            email: req.body.email,
            phone: req.body.phone,
            reason: req.body.reason,
            ticketno: totalCount,
            orderid: foundOrder._id,
            status: 1,
            note: 'You will be contacted soon by our help team.'
        });
        const savedReturn = await newReturnOrder.save();
        foundOrder.returndetails.push(savedReturn);
        foundOrder.status = 'RRR';
        await foundOrder.save();
        res.send({
            success: true,
            message: 'Return request has been raised',
            no: totalCount,
            note: savedReturn.note
        });
    }
};
