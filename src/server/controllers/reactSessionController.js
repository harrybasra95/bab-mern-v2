const { ReactSession } = require('../models');

module.exports = {
     create: async (req, res) => {
          try {
               const newReactSession = new ReactSession();
               newReactSession.id = newReactSession._id;
               newReactSession.selectedCountry = 'in';
               const savedSession = await newReactSession.save();
               return res.success(savedSession);
          } catch (error) {
               return res.failure(error);
          }
     },
     get: async (req, res) => {
          try {
               const foundSession = await ReactSession.findById(
                    req.params.token
               );
               if (!foundSession) {
                    return module.exports.create(req, res);
               }
               return res.success(foundSession);
          } catch (error) {
               return res.failure(error);
          }
     },
     selectCountry: async (req, res) => {
          try {
               const token = req.get('session-id');
               const foundSession = await ReactSession.findByIdAndUpdate(
                    token,
                    {
                         selectedCountry: req.body.country
                    },
                    { new: true }
               );
               if (!foundSession) {
                    res.failure('Session not found');
               }
               return res.success(foundSession);
          } catch (error) {
               res.failure(error);
          }
     }
};
