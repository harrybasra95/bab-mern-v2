const ShippingList = require('../shipping');
const moment = require('moment');
const sha512 = require('js-sha512').sha512;
const {
     Order,
     Mailgun,
     User,
     Coupon,
     Product,
     ReactSession
} = require('../models');
const {
     orderServices,
     delhiveryServices,
     cartServices,
     ccAvenueServices
} = require('../services');
const {
     nameValidation,
     phoneValidation,
     addressValidation
} = require('../validations');
const {
     userKeysList,
     singleOrderKeysList,
     indianStateCodesFullForm,
     allOrdersKeysList
} = require('../config');
const PincodeList = require('../pincode.json');
module.exports = {
     fetchAllOrders: async (req, res) => {
          try {
               const sessionToken = req.get('session-id');
               const foundSession = await ReactSession.findById(sessionToken);
               const { orders } = await User.findById(foundSession.user._id)
                    .populate('orders', allOrdersKeysList)
                    .exec();
               res.success({ orders });
          } catch (error) {
               res.failure({ error });
          }
     },
     fetchSingleOrder: async (req, res) => {
          try {
               const sessionToken = req.get('session-id');
               const foundSession = await ReactSession.findById(sessionToken);
               const foundOrder = await Order.findOne(
                    {
                         ordernumber: req.params.orderNumber
                    },
                    singleOrderKeysList
               )
                    .populate('returndetails')
                    .exec();
               const { orders } = await User.findById(
                    foundSession.user._id,
                    'orders'
               );
               if (orders.indexOf(foundOrder._id) > -1) {
                    const invoiceName = sha512(`${foundOrder.ordernumber}1`);
                    res.success({ invoice: invoiceName, order: foundOrder });
               } else {
                    res.failure('Something went wrong.Please try again');
               }
          } catch (error) {
               res.failure('Something went wrong.Please try again');
          }
     },
     pincodeCheck: async (req, res) => {
          const data = await delhiveryServices.pincode(req.query.pincode);
          if (data.delivery_codes.length === 0) {
               return res.failure('Pincode not available');
          }
          const pincodeData = data.delivery_codes[0].postal_code;
          return res.success({
               city: pincodeData.district,
               country: 'India',
               state: indianStateCodesFullForm[pincodeData.state_code]
          });
     },
     updateLastOrderData: async (req, res) => {
          try {
               const sessionToken = req.get('session-id');
               const foundSession = await ReactSession.findById(sessionToken);
               const foundUser = await User.findByIdAndUpdate(
                    foundSession.user._id.toString(),
                    { lastOrderDetails: req.body },
                    { new: true, fields: userKeysList }
               );
               const savedSession = await ReactSession.findByIdAndUpdate(
                    sessionToken,
                    {
                         user: foundUser
                    },
                    { new: true }
               );
               res.success(savedSession);
          } catch (error) {
               res.failure(error);
          }
     },
     create: async (req, res) => {
          try {
               let coupon = {};
               const sessionToken = req.get('session-id');
               const foundSession = await ReactSession.findById(sessionToken);
               const { selectedCountry, user } = foundSession;
               if (foundSession.coupon) {
                    coupon = await Coupon.findOne({
                         name: foundSession.coupon.name
                    });
               }
               const { paymentType } = req.body;
               const {
                    fname,
                    lname,
                    phone,
                    pincode,
                    streetNo,
                    country
               } = user.lastOrderDetails;
               const countryFromShippingList = ShippingList.getCountry(
                    country.toLowerCase()
               );
               let shippingPrice = countryFromShippingList[0].r;
               const newCart = foundSession.cart;
               const { totalPrice, totalQuantity } = newCart;
               req.body.delivery_type = 'Express';
               if (coupon && coupon.amounttype === true) {
                    coupon.couponDiscount = coupon.discount;
               } else if (coupon && selectedCountry === 'in') {
                    coupon.couponDiscount = Math.ceil(
                         (totalPrice * coupon.discount) / 100
                    );
               }
               if (totalQuantity === 0) {
                    return res.failure(
                         'Please select add some products to your cart'
                    );
               }
               if (!nameValidation(fname) || !nameValidation(lname)) {
                    return res.failure(
                         'Please enter a valid name between 3 to 20 char'
                    );
               }
               if (!phoneValidation(phone)) {
                    return res.failure('Please enter a valid Phone');
               }
               if (
                    !addressValidation({ streetNo, pincode }) ||
                    countryFromShippingList.length === 0
               ) {
                    return res.failure('Please enter a valid address ');
               }
               if (['card', 'paytm', 'cod'].includes(paymentType) === false) {
                    return res.failure('Please select a payment method');
               }
               if (coupon && coupon.cod === false && paymentType === 'cod') {
                    return res.failure('Something went wrong.Please try again');
               }
               const delivery = {};
               if (selectedCountry === 'in' || selectedCountry === 'none') {
                    const output = PincodeList.filter(
                         value => value.Pincode === Number(pincode.trim())
                    )[0];
                    delivery.pincode = output.Pincode;
                    delivery.city = output.City;
                    delivery.state = output.State;
                    delivery.country = 'India';
                    const discontedPrice = coupon
                         ? totalPrice - coupon.couponDiscount
                         : totalPrice;
                    if (discontedPrice >= 499) {
                         shippingPrice = 0;
                    }
               }
               delivery.street_no = streetNo;
               delivery.fname = fname;
               delivery.lname = lname;
               delivery.phone = phone;
               const body = await delhiveryServices.pincode(pincode);
               if (!body.delivery_codes) {
                    return res.failure(
                         'Delivery is not possible at this address.'
                    );
               }
               const totalOrdersCount = await Order.count();
               const newOrder = new Order({
                    selectedCountry,
                    persondetails: {
                         eadd: user.email,
                         fname,
                         lname,
                         phone
                    },
                    productdetails: [...cartServices.generateArray(newCart)],
                    deliveryaddress: { ...delivery },
                    ordernumber: totalOrdersCount + 1005,
                    status: 'PP',
                    userid: user._id.toString(),
                    subtotalprice: totalPrice,
                    paymenttype: paymentType,
                    shipping: shippingPrice,
                    tax: 0,
                    totalqty: totalQuantity,
                    discount: coupon ? coupon.couponDiscount : 0,
                    coupon: coupon || null,
                    totalprice: totalPrice + shippingPrice
               });
               newOrder.temporarytoken = Mailgun.generate_token_1h(newOrder);
               newOrder.totalprice -= newOrder.discount;
               if (coupon) {
                    await User.findByIdAndUpdate(user._id, {
                         lastcoupon: coupon.name
                    });
                    if (coupon.particularuser === true) {
                         await Coupon.findByIdAndUpdate(coupon._id, {
                              expiredate: new Date()
                         });
                    }
               }
               // await ReactSession.findByIdAndUpdate(sessionToken, {
               //      cart: {
               //           totalPrice: 0,
               //           totalQuantity: 0
               //      },
               //      coupon: {}
               // });
               const savedOrder = await newOrder.save();
               const promisesArray = [];
               Object.keys(newCart.products).forEach(key => {
                    const { product, productTotalQuantity } = newCart.products[
                         key
                    ];
                    promisesArray.push(
                         new Promise((resolve, reject) => {
                              Product.findByIdAndUpdate(
                                   product._id,
                                   {
                                        $inc: {
                                             quantity: -productTotalQuantity
                                        }
                                   },
                                   err => {
                                        if (err) {
                                             reject();
                                        }
                                        resolve();
                                   }
                              );
                         })
                    );
               });
               await Promise.all(promisesArray);
               orderServices.findUserAndMakeOrder(req, res, savedOrder);
          } catch (error) {
               return res.failure('Something went wrong.Please try again');
          }
     },
     makeOrderCc: async (req, res) => {
          const decryptedData = ccAvenueServices.decrypt(req.body.encResp);
          const paymentData = ccAvenueServices.parse(decryptedData);
          const foundOrder = await Order.findOne({
               ordernumber: paymentData.order_id
          });
          const foundUser = await User.findById(foundOrder.userid);
          if (
               !foundOrder ||
               foundOrder.checked === true ||
               foundUser.orders.indexOf(foundOrder._id) === -1
          ) {
               return res.failure();
          }
          if (paymentData.order_status === 'Success') {
               return orderServices.makeOrderPrepaid(
                    res,
                    foundOrder,
                    paymentData
               );
          }
          req.body = paymentData;
          foundOrder.checked = true;
          foundOrder.status = 'F';
          foundOrder.paymentdetails = paymentData;
          const savedOrder = await foundOrder.save();
          const promisesArray = [];
          savedOrder.productdetails.forEach(
               ({ product, productTotalQuantity }) => {
                    promisesArray.push(
                         new Promise((resolve, reject) => {
                              Product.findByIdAndUpdate(
                                   product._id,
                                   {
                                        $inc: {
                                             quantity: productTotalQuantity
                                        }
                                   },
                                   err => {
                                        if (err) {
                                             reject();
                                        }
                                        resolve();
                                   }
                              );
                         })
                    );
               }
          );
          await Promise.all(promisesArray);
          if (
               savedOrder.coupon &&
               savedOrder.coupon.length !== 0 &&
               savedOrder.coupon.particularuser === true
          ) {
               await Coupon.findByIdAndUpdate(savedOrder.coupon._id, {
                    expiredate: moment().add(30, 'days')
               });
          }
          res.redirect('/account/orders');
     }
};
