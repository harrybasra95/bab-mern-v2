const { Cart, Coupon, User } = require('../models');
const shippingChart = require('../shipping');
const { couponServices } = require('../services');
const { errAjax } = require('../helpers');

module.exports = {
    applyCoupon: async (req, res) => {
        const newCart = new Cart(req.session.cart);
        let shippingprice;
        const foundShipping = shippingChart.filter(
            value => value.c.toLowerCase() === req.body.country.toLowerCase()
        );
        if (foundShipping.length > 0) {
            shippingprice = foundShipping[0].r;
        } else if (
            newCart.totalPrice >= 500 &&
            req.body.country.toLowerCase() === 'india'
        ) {
            shippingprice = 0;
        } else {
            shippingprice = null;
        }
        try {
            const foundCoupon = await Coupon.findOne({
                name: req.body.name.toLowerCase()
            });
            const foundUser = await User.findById(req.user._id);
            if (!foundCoupon) {
                res.send({
                    success: false,
                    message: 'Please enter a valid coupon name.'
                });
            } else {
                const {
                    expiredate,
                    particularuser,
                    minamount,
                    peruser,
                    amounttype,
                    discount
                } = foundCoupon;
                const { selected_country } = req.session;
                const { totalPrice } = newCart;
                const isCouponApplicable = couponServices.isCouponApplicableOnProducts(
                    newCart,
                    foundCoupon,
                    selected_country
                );
                if (expiredate - Date.now() <= 0) {
                    res.send({
                        success: false,
                        message: 'Coupon has been expired.'
                    });
                } else if (isCouponApplicable === false) {
                    res.send({
                        success: false,
                        message:
                            'Coupon is not available for some of your products.'
                    });
                } else if (
                    particularuser.value === true &&
                    particularuser.id !== foundUser.email
                ) {
                    res.send({
                        success: false,
                        message: 'Coupon is not valid for you.'
                    });
                } else if (minamount > totalPrice) {
                    res.send({
                        success: false,
                        message: `Coupon is valid on min amount of INR ${minamount}`
                    });
                } else if (shippingprice === null) {
                    res.send({
                        success: false,
                        message: 'Please select a valid country'
                    });
                } else if (
                    peruser > 0 &&
                    foundUser.coupons.count(foundCoupon._id.toString()) >=
                        peruser
                ) {
                    res.send({
                        success: false,
                        message: `This coupon can be used only ${peruser} time(s)`
                    });
                } else if (amounttype === true && selected_country !== 'in') {
                    res.send({
                        success: false,
                        message: 'This coupon is not applicable for this region'
                    });
                } else if (amounttype === true) {
                    const couponDiscount = discount;
                    let afterDiscountTotal = totalPrice - couponDiscount;
                    foundCoupon.value = couponDiscount;
                    foundCoupon.couponDiscount = couponDiscount;
                    req.session.coupon = foundCoupon;
                    afterDiscountTotal += shippingprice;
                    res.send({
                        success: true,
                        message: 'Coupon successfully applied.',
                        total: afterDiscountTotal,
                        value: foundCoupon.discount,
                        cod: foundCoupon.cod
                    });
                } else if (req.session.selected_country === 'in') {
                    const couponDiscount = Math.ceil(
                        (totalPrice * foundCoupon.discount) / 100
                    );
                    let afterDiscountTotal = totalPrice - couponDiscount;
                    afterDiscountTotal += shippingprice;
                    foundCoupon.value = couponDiscount;
                    foundCoupon.couponDiscount = couponDiscount;
                    req.session.coupon = foundCoupon;
                    res.send({
                        success: true,
                        message: 'Coupon successfully applied.',
                        total: afterDiscountTotal,
                        value: couponDiscount,
                        cod: foundCoupon.cod
                    });
                } else {
                    const couponDiscount = (
                        (totalPrice * foundCoupon.discount) /
                        100
                    ).toFixed(2);
                    let afterDiscountTotal = totalPrice - couponDiscount;
                    foundCoupon.value = couponDiscount;
                    foundCoupon.couponDiscount = couponDiscount;
                    afterDiscountTotal += shippingprice;
                    req.session.coupon = foundCoupon;
                    res.send({
                        success: true,
                        message: 'Coupon successfully applied.',
                        total: afterDiscountTotal,
                        value: couponDiscount,
                        cod: foundCoupon.cod
                    });
                }
            }
        } catch (error) {
            errAjax();
        }
    },
    removeCoupon: (req, res) => {
        if (!req.session.coupon) {
            res.send({
                success: false,
                message: 'Something went wrong.Please try again.'
            });
        } else {
            delete req.session.coupon;
            const newCart = new Cart(req.session.cart);
            res.send({ success: true, total: newCart.totalPrice });
        }
    }
};
