const { User, Address } = require('../models');
const { loggerServices } = require('../services');
const { errAjax } = require('../helpers');
const Pincode = require('../pincode.json');
const { addressValidation } = require('../validations');

const { addErrorLog } = loggerServices;

module.exports = {
    maxAddressCheck: async (req, res) => {
        try {
            const foundUser = await User.findById(req.user._id);
            if (foundUser.address.length >= 5) {
                res.send({ success: false, message: 'Maximum limit reached' });
            } else {
                res.send({
                    success: true,
                    message: 'Please fill in the details'
                });
            }
        } catch (error) {
            errAjax(req, res);
            addErrorLog(req, error);
        }
    },
    delete: async (req, res) => {
        try {
            await Address.findByIdAndRemove(req.params.id);
            const foundUser = await User.findById(req.user._id)
                .populate('address')
                .exec();
            foundUser.address_no -= 1;
            foundUser.address.remove(req.params.id);
            await foundUser.save();
            res.send({
                success: true,
                message: 'Address deleted succesfully',
                address: foundUser.address
            });
        } catch (error) {
            errAjax(req, res);
            addErrorLog(req, error);
        }
    },
    edit: async (req, res) => {
        try {
            const foundAddress = await Address.findById(req.body.id);
            res.send({ success: true, address: foundAddress });
        } catch (error) {
            errAjax(req, res, 'Please try again!!!');
            addErrorLog(req, error);
        }
    },
    get: async (req, res) => {
        try {
            const foundUser = await User.findById(req.user._id)
                .populate('address')
                .exec();
            res.send({ success: true, user: foundUser });
        } catch (error) {
            errAjax(req, res, 'Please try again!!!');
            addErrorLog(req, error);
        }
    },
    create: async (req, res) => {
        try {
            if (addressValidation(req.body)) {
                res.send({
                    success: false,
                    message: 'Please enter valid details'
                });
            } else {
                const output = Pincode.filter(
                    value => value.Pincode === req.body.pincode
                );
                req.body.city = output[0].City;
                req.body.state = output[0].State;
                const {
                    street_no,
                    pincode,
                    city,
                    state,
                    fname,
                    lname,
                    phone
                } = req.body;
                const newAdd = new Address({
                    street_no,
                    country: 'India',
                    pincode,
                    city,
                    state,
                    fname,
                    lname,
                    phone
                });
                const foundUser = await User.findById(req.user._id).populate(
                    'address'
                );
                if (foundUser.address.length >= 5) {
                    return res.send({
                        success: false,
                        message: 'You can only add atmost 5 addresses',
                        address: foundUser.address
                    });
                }
                const savedAddress = await newAdd.save();
                foundUser.address.push(savedAddress);
                foundUser.address_no += 1;
                await foundUser.save();
                res.send({
                    success: true,
                    message: 'Address saved succesfully',
                    address: [...foundUser.address, savedAddress]
                });
            }
        } catch (error) {
            errAjax(req, res, 'Please try again!!!');
            addErrorLog(req, error);
        }
    }
};
