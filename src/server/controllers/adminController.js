const { Order, User } = require('../models');

module.exports = {
    homepage: (req, res) => {
        const pageInfo = {
            css: 'index.css'
        };
        res.render('admin/index', { pageInfo });
    },
    users: (req, res) => {
        const pageInfo = {
            css: 'users.css'
        };
        let page = 1;
        if (req.query.page) {
            page = parseInt(req.query.page);
        }
        let nextPage = page + 1;
        let lastPage = page - 1;
        User.count((err, count) => {
            if (page > parseInt(count / 20)) {
                nextPage = parseInt(count / 20) + 1;
            }
            if (page <= 1) {
                lastPage = 1;
            }
            User.find({}, (err, foundUsers) => {
                res.render('admin/users', {
                    pageInfo,
                    users: foundUsers,
                    total: count,
                    nextPage,
                    lastPage
                });
            })
                .skip((page - 1) * 20)
                .limit(20);
        });
    },
    confirmOrder: async (req, res) => {
        await Order.findByIdAndUpdate(req.params.id, { status: 'OR' });
        res.send('Order Confirmed');
    }
};
