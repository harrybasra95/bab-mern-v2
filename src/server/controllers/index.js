const addressController = require('./addressController');
const adminController = require('./adminController');
const apiController = require('./apiController');
const cartController = require('./cartController');
const checkoutController = require('./checkoutController');
const frontController = require('./frontController');
const pageApiController = require('./pageApiController');
const couponController = require('./couponController');
const reactSessionController = require('./reactSessionController');
const authApiController = require('./authApiController');
const orderController = require('./orderController');
const userController = require('./userController');
const trackingController = require('./trackingController');

module.exports = {
     addressController,
     adminController,
     apiController,
     cartController,
     checkoutController,
     frontController,
     pageApiController,
     couponController,
     reactSessionController,
     authApiController,
     orderController,
     userController,
     trackingController
};
