const { User, Product, Cart, Subscribe, Mailgun } = require('../models');
const Pincode = require('../pincode.json');
const { emailValidation, phoneValidation } = require('../validations');
const { errAjax, fieldIsNull } = require('../helpers');
const { loggerServices } = require('../services');
const ShippingList = require('../shipping');

const { addErrorLog } = loggerServices;

module.exports = {
     wishlistAdd: async (req, res) => {
          try {
               const foundProduct = await Product.findOne({
                    uniqueid: req.params.id
               });
               const foundUser = await User.findById(req.user._id);
               if (foundUser.wishlist.indexOf(foundProduct._id) !== -1) {
                    res.send({
                         success: true,
                         message: 'Already added to your wishlist'
                    });
               } else {
                    foundUser.wishlist.push(foundProduct);
                    foundUser.save(() => {
                         res.send({
                              success: true,
                              message: 'Added to your wishlist'
                         });
                    });
               }
          } catch (error) {
               errAjax(req, res);
               addErrorLog(req, error);
          }
          if (fieldIsNull(req.params.id)) {
               errAjax(req, res, 'Please enter a valid product');
          } else {
          }
     },
     wishlistRemove: async (req, res) => {
          try {
               const foundUser = await User.findById(req.user._id);
               foundUser.wishlist.remove(req.params.id);
               await foundUser.save();
               res.send({
                    success: true,
                    message: 'Product removed',
                    length: foundUser.wishlist.length
               });
          } catch (error) {
               errAjax(req, res);
               addErrorLog(req, error);
          }
     },
     codOptions: (req, res) => {
          let cart = new Cart(req.session.cart ? req.session.cart : {});
          cart = cart.generateArray();
          let cod = true;
          cart.forEach(item => {
               if (item.items.series === 'bdedicated') {
                    cod = false;
               }
          });
          res.send(cod);
     },
     subscribe: async (req, res) => {
          try {
               const { email, phone } = req.body;
               const foundUser = await Subscribe.findOne({
                    email: req.body.email
               });
               if (foundUser) {
                    return res.send({
                         success: false,
                         message: 'You are already subscribed!!!!'
                    });
               }
               if (!emailValidation(email)) {
                    return res.send({
                         success: false,
                         message: 'Please enter a valid E-mail'
                    });
               }
               if (phoneValidation(phone, req.session.selected_country)) {
                    return res.send({
                         success: false,
                         message: 'Please enter a valid Number'
                    });
               }
               await Subscribe.create(req.body);
               res.send({
                    success: true,
                    message: 'Thank you for subscribing'
               });
          } catch (error) {
               errAjax(req, res);
               addErrorLog(req, error);
          }
     },
     checkEmail: async (req, res) => {
          try {
               const { email } = req.body;
               if (!emailValidation(email)) {
                    return res.send({
                         success: false,
                         message: 'Please enter a valid email'
                    });
               }
               const foundUser = await User.findByUsername(email);
               if (foundUser) {
                    return res.send({
                         success: false,
                         message: 'Email already taken'
                    });
               }
               res.send({
                    success: true,
                    message: 'Email is available'
               });
          } catch (error) {
               errAjax(req, res);
               addErrorLog(req, error);
          }
     },
     checkPincode: (req, res) => {
          if (req.params.id === 'pincode' && req.body.pincode.length === 6) {
               for (let i = 0; i < Pincode.length; i++) {
                    if (Pincode[i].Pincode === req.body.pincode) {
                         res.send({ success: true, pincode: Pincode[i] });
                    }
               }
          } else {
               res.send({
                    success: false,
                    message: 'Please enter a valid pincode'
               });
          }
     },
     checkShipping: (req, res) => {
          const output = ShippingList.filter(
               value => value.c.toLowerCase() === req.params.id.toLowerCase()
          );
          if (output.length > 0) {
               res.send({
                    success: true,
                    shipping: output[0].r,
                    total: req.session.cart.totalPrice
               });
          } else {
               res.send({ success: false, total: req.session.cart.totalPrice });
          }
     },
     sendActivationMail: async (req, res) => {
          try {
               const foundUser = await User.findOne({ _id: req.user._id });
               if (foundUser.active === true) {
                    req.user.active = true;
                    return res.send({
                         success: false,
                         message: 'User already active'
                    });
               }
               foundUser.temporarytoken = Mailgun.generate_token(foundUser);
               await foundUser.save();
               Mailgun.confirm_email(foundUser);
               res.send({ success: true, email: foundUser.email });
          } catch (error) {
               errAjax(req, res);
               addErrorLog(req, error);
          }
     },
     mailDelivererd: (req, res) => {
          const data = req.body;
          const username = data['event-data'].recipient;
          const mail_id = data['event-data']['user-variables'].mail_id;
          User.findOne({ username }, (err, foundUser) => {
               if (foundUser && foundUser.mails.length > mail_id) {
                    const element = foundUser.mails[mail_id];
                    element.delivered = true;
                    const insertAtIndex = mail_id;
                    const stringToBeInserted = element;
                    foundUser.mails.splice(
                         insertAtIndex - 1,
                         1,
                         stringToBeInserted
                    );
                    foundUser.save();
               }
          });
          res.send(true);
     },
     mailOpened: (req, res) => {
          User.findOne({ username: req.params.username }, (err, foundUser) => {
               if (foundUser && foundUser.mails.length > req.params.mail_id) {
                    const element = foundUser.mails[req.params.mail_id];
                    element.opened = true;
                    const insertAtIndex = req.params.mail_id;
                    const stringToBeInserted = element;
                    foundUser.mails.splice(
                         insertAtIndex - 1,
                         1,
                         stringToBeInserted
                    );
                    foundUser.save(() => {
                         console.log('mail-opened');
                    });
               }
          });
          res.sendFile(`${__dirname}/public/img/logo.PNG`);
     },
     mailClicked: (req, res) => {
          User.findOne({ username: req.query.username }, (err, foundUser) => {
               if (foundUser && foundUser.mails.length > req.query.mail_id) {
                    const element = foundUser.mails[req.query.mail_id];
                    element.link_clicked = true;
                    const insertAtIndex = req.query.mail_id;
                    const stringToBeInserted = element;
                    foundUser.mails.splice(
                         insertAtIndex - 1,
                         1,
                         stringToBeInserted
                    );
                    foundUser.save(() => {
                         console.log('mail-clicked');
                    });
               }
          });
          res.redirect(req.query.url);
     }
};
