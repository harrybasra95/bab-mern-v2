const Validator = require('validator');

module.exports = {
    emailValidation: email => Validator.isEmail(email),
    nameValidation: name => Validator.isAlpha(name.trim()),
    phoneValidation: (phone, selectedCountry) => {
        if (selectedCountry !== 'in') {
            return true;
        }
        return Validator.isNumeric(phone) === false || phone.length !== 10;
    },
    addressValidation: address => {
        const isFalseArray = Object.keys(address).filter(key => {
            if (key === 'street_no_2' || key === 'street_no_3') {
                return true;
            }
            if (
                !address[key] ||
                address[key] === null ||
                address[key].length === 0
            ) {
                return false;
            }
            return true;
        });
        return isFalseArray.length === Object.keys(address).length;
    }
};
