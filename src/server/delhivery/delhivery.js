const request = require('request');
const axios = require('axios');
const { delhiveryToken } = require('../config');

const cancel = function (data, cb) {
    const options = {
        method: 'POST',
        url: 'https://track.delhivery.com/api/p/edit',
        headers: {
            Authorization: 'Token b66d81bf1c79e745eacbf6938e43d521bb299d0e',
            'Content-type': 'application/json'
        },
        body: `{"waybill":"${data}","cancellation": "true"}`
    };
    request(options, cb);
};

const waybill = async () => {
    const { data } = await axios.get(
        `https://track.delhivery.com/waybill/api/fetch/json/?cl=BEINGBEAST&token=${delhiveryToken}`
    );
    return data;
};

const track = async waybill => {
    const headers = {
        Authorization: 'Token b66d81bf1c79e745eacbf6938e43d521bb299d0e',
        'Content-type': 'application/json'
    };
    const { data } = await axios.get(
        `https://track.delhivery.com/api/packages/json/?token=${delhiveryToken}&format=json&waybill=${waybill}`,
        { headers }
    );
    return data;
};

const packing = async waybill => {
    const headers = {
        Authorization: 'Token b66d81bf1c79e745eacbf6938e43d521bb299d0e',
        'Content-type': 'application/json'
    };
    const { data } = await axios.get(
        `https://track.delhivery.com/api/p/packing_slip?wbns=${waybill}`,
        { headers }
    );
    return data;
};
const pickup = function (data, cb) {
    const options = {
        method: 'POST',
        url: 'https://track.delhivery.com/fm/request/new/',
        headers: {
            Authorization: 'Token b66d81bf1c79e745eacbf6938e43d521bb299d0e',
            'Content-type': 'application/json'
        },
        body:
            '{"pickup_location": "BEINGBEAST",\
			    "pickup_time": "09:43:42",\
			    "pickup_date": "20181230",\
			    "expected_package_count": "1"} '
    };
    request(options, cb);
};
const pincode = async pincode => {
    const { data } = await axios.get(
        `https://track.delhivery.com/c/api/pin-codes/json/?token=${delhiveryToken}&filter_codes=${pincode}`
    );
    return data;
};
const packagecreate = function (data, cb) {
    const options = {
        method: 'POST',
        url: 'https://track.delhivery.com/cmu/push/json/',
        qs: { token: 'b66d81bf1c79e745eacbf6938e43d521bb299d0e' },
        headers: {
            'postman-token': '206cf886-0fa4-1102-c1bc-b70ef3145832',
            'cache-control': 'no-cache'
        },
        body: `format=json&status=&data={"shipments":[{"city":"${
            data.deliveryaddress.city
        }","waybill":"${data.waybill}","total_amount":"${
            data.total_amount
        }","seller_cst":"03092214174","name":"${
            data.name
        }","weight":${data.quantity *
            0.24},"extra_parameters":{"return_reason":"Item: 1,WRONG_PRODUCT,Item:2,Wrong Size","encryptedShipmentID":null},"country":"${
            data.deliveryaddress.country
        }","quantity":${data.quantity},"seller_tin":"03092214174","state":"${
            data.deliveryaddress.state
        }","shipping_mode":"Express","phone":"${data.phone}","add":"${
            data.deliveryaddress.street_no
        }","cod_amount":"${data.cod_amount}","order_date":"${
            data.order_date
        }","pin":"${
            data.deliveryaddress.pincode
        }","products_desc":"Gymwear Apparel","payment_mode":"${
            data.payment_type
        }","order":"${
            data.ordernumber
        }"}],"pickup_location":{"city":"JALANDHAR","state":"Punjab","name":"BEINGBEAST","pin":"144002"}}`

        // '"shipments": \
        // 			[{"city":"'+data.deliveryaddress.city+'",\
        // 	"add": "'+data.deliveryaddress.street_no+'",\
        // 	"country": "'+data.deliveryaddress.country+'",\
        // 	"state": "'+data.deliveryaddress.state+'",\
        // 	"pin": "'+data.deliveryaddress.pincode+'",\
        // 			"total_amount": "'+data.total_amount+'",\
        // 			"seller_cst": "03092214174",\
        // 			"name": "BEINGBEAST",\
        // 			"weight": '+data.quantity * 240.0+' gm'+',\
        // 			"waybill":"'+data.waybill+'",\
        // 			"extra_parameters": {\
        // 				"return_reason": "Item: 1,WRONG_PRODUCT,Item:2,Wrong Size",\
        // 		"encryptedShipmentID": null},\
        // 	"seller_tin": "03092214174",\
        // 	"phone": "'+data.phone+'",\
        // 	"shipment_length": 19.2,\
        // 	"shipment_height": 2.79,\
        // 	"shipment_width": 10.21\
        // 	"order_date":"'+data.order_date+'",\
        // 	"shipping_mode": "'+data.shipping_mode+'",\
        // 	"products_desc": "Readymade Garments",\
        // 	"quantity":'+data.quantity+',\
        // 	"order": "'+data.ordernumber+'",\
        // 	"payment_mode": "'+data.payment_type+'",\
        // 	"cod_amount": "'+data.cod_amount+'"}],\
        // "pickup_location": {"add": " W.P. 181/A BASTI SHEIKH","city": "JALANDHAR","country": "India","name": "BEINGBEAST","phone": "8569078629","pin": "144002"}}'
    };
    console.log(options);
    request(options, cb);
};

module.exports.waybill = waybill;
module.exports.track = track;
module.exports.cancel = cancel;
module.exports.packing = packing;
module.exports.pickup = pickup;
module.exports.pincode = pincode;
module.exports.packagecreate = packagecreate;
