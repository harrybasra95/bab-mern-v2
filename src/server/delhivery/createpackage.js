const request = require('request');

function createPackage(data, cb) {
    const options = {
        method: 'POST',
        url: 'https://test.delhivery.com/cmu/push/json/',
        qs: { token: '4edc429626aae80b5232896545a8461da8742946' },
        headers: {
            'postman-token': '206cf886-0fa4-1102-c1bc-b70ef3145832',
            'cache-control': 'no-cache'
        },
        body:
            'format=json&status=&data={\
			"shipments": \
  				[{"city": "Mundhwa",\
				"add": "B1 flat no 704 prestige panorama,Keshav nagar",\
				"country": "India",\
				"state": "Maharashtra",\
				"pin": "411036",\
  				"total_amount": "760.95",\
  				"seller_cst": "",\
  				"name": "BEINGBEAST",\
  				"weight": 0.5,\
  				"waybill": "99238610010146",\
  				"extra_parameters": {\
  					"return_reason": "Item: 1,WRONG_PRODUCT,Item:2,Wrong Size",\
					"encryptedShipmentID": null},\
				"seller_tin": "",\
				"phone": "0000000000000000",\
				"order_date":"2017-05-05 12:30:30",\
				"shipping_mode": "Express",\
				"products_desc": "",\
				"quantity":10,\
				"order": "282-109-0987",\
				"payment_mode": "COD",\
				"cod_amount": "10.00",\
				"shipment_length": 19.2,\
				"shipment_height": 2.79,\
				"shipment_width": 10.21}],\
			"pickup_location": {\
				"add": " W.P. 181/A BASTI SHEIKH",\
				"city": "JALANDHAR",\
				 "country": "India",\
				 "name": "BEINGBEAST",\
				 "phone": "8569078629",\
				 "pin": "144002"}}'
    };

    request(options, cb);
}

createPackage();
