
var request = require("request");

module.exports = function pickup(data,cb){
  var options = { 
      method: 'POST',
      url: "https://test.delhivery.com/fm/request/new/",
      headers:{ 
        'Authorization':"Token 4edc429626aae80b5232896545a8461da8742946",
        'Content-type':"application/json",
      },
      body: '{"pickup_location": "BEINGBEAST",\
            "pickup_time": "09:43:42",\
            "pickup_date": "20181230",\
            "expected_package_count": "1"} '
    };

    request(options, cb);
}
