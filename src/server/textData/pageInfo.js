const commonHelpers = require('../helpers/commonHelpers');

const uptoProductBreadcrumbs = [
    {
        name: 'home',
        url: '/'
    },
    {
        name: 'products',
        url: '/products'
    }
];

const defaultDesc =
    'BAB is a active wear fitness clothing range for men and women that include gym apparel and lifestyle wear. Our range of workout clothes includes seamless t-shirts and tank tops, stringers made from ultra-soft premium quality fabric. Buy Now at BAB Store. Redefined Yourself.';
module.exports = {
    slash: () => ({
        gtitle: 'BEST SELLER',
        title: 'BAB | Gym Wear and Fitness Clothing | Workout CLothes',
        desc: defaultDesc,
        cssFile: 'index.css'
    }),
    homepage: () => ({
        gtitle: 'shop',
        title: 'BAB | Premium Athletic Wear | Workout CLothes',
        desc: defaultDesc,
        cssFile: 'index.css'
    }),
    products: () => ({
        gtitle: 'shop',
        title: 'BAB | Premium Athletic Wear | Workout CLothes',
        desc: defaultDesc,
        cssFile: 'products.css',
        breadcrumbs: [...uptoProductBreadcrumbs]
    }),
    wishlist: () => ({
        gtitle: 'shop',
        title: 'Wishlist | Being Beast Apparel | BAB',
        desc: defaultDesc,
        breadcrumbs: [
            {
                name: 'home',
                url: '/'
            },
            {
                name: 'wishlist',
                url: '/wishlist'
            }
        ]
    }),
    termsAndConditions: () => ({
        gtitle: 'shop',
        title: 'Terms and Conditions | Being Beast Apparel | BAB',
        desc: defaultDesc,
        breadcrumbs: [
            {
                name: 'home',
                url: '/'
            },
            {
                name: 'terms and conditions',
                url: '/terms-and-conditions'
            }
        ]
    }),
    privacyPolicy: () => ({
        gtitle: 'shop',
        title: 'Privacy Policy | Being Beast Apparel | BAB',
        desc: defaultDesc
    }),
    stringers: () => ({
        gtitle: 'stringers',
        title: "Shop Men's Stringers | Premium Athletic Wear | BAB",
        desc:
            'We have a wide range of stringers for superior comfort during intense workouts.Their Super Stretchable Design and Parallel fit to make them a unique breed of stringers for unique breed of athletes.',
        cssFile: 'products.css',
        breadcrumbs: [
            ...uptoProductBreadcrumbs,
            { name: 'stringers', url: '/stringers' }
        ]
    }),
    trackpants: () => ({
        gtitle: 'Trackpants',
        title: "Shop Men's Trackpants | Premium Athletic Wear | BAB",
        desc:
            'BAB Trackpants is our complete workout bottoms with simple design but unique fabric which can be worn during winters as well as summers. Buy Now at BAB Store. Redefined Yourself.',
        cssFile: 'products.css',
        breadcrumbs: [
            ...uptoProductBreadcrumbs,
            { name: 'Trackpants', url: '/trackpants' }
        ]
    }),
    tshirts: () => ({
        gtitle: 'T-Shirts',
        title: "Shop Men's T-Shirts | Premium Athletic Wear | BAB",
        desc:
            'Perfect workout wardrobes with simple yet catching designs BAB T-shirts are made with most superior fabrics to provide consistent comfort and breathability.Be caution it may cause envy.',
        cssFile: 'products.css',
        breadcrumbs: [
            ...uptoProductBreadcrumbs,
            { name: 'T-Shirts', url: '/t-shirts' }
        ]
    }),
    tanks: () => ({
        gtitle: 'Tanks',
        title: "Shop Men's Tanks | Premium Athletic Wear | BAB",
        desc:
            'Made from light weighted fabric BAB Tanks provide you with a physic hugging design to stay a step ahead of the rest.These iconic tanks are Engineered with your comfort in mind.',
        cssFile: 'products.css',
        breadcrumbs: [
            ...uptoProductBreadcrumbs,
            { name: 'Tanks', url: '/tanks' }
        ]
    }),
    collections: name => ({
        gtitle: `${commonHelpers.capWords(name)} Series`,
        title: `Shop ${commonHelpers.capWords(
            name
        )} Series | Premium Athletic Wear | BAB`,
        desc: collectionDesc[name],
        cssFile: 'products.css',
        breadcrumbs: [
            ...uptoProductBreadcrumbs,
            { name: `${name} series`, url: `/collection/${name}` }
        ]
    }),
    outlet: () => ({
        gtitle: 'outlet',
        title: 'BAB | Premium Athletic Wear | Workout CLothes',
        desc: defaultDesc,
        cssFile: 'products.css',
        breadcrumbs: [
            ...uptoProductBreadcrumbs,
            { name: 'outlet', url: '/outlet' }
        ]
    }),
    featured: () => ({
        gtitle: 'featured',
        title: 'BAB | Premium Athletic Wear | Workout CLothes',
        desc: defaultDesc,
        cssFile: 'products.css',
        breadcrumbs: [
            ...uptoProductBreadcrumbs,
            { name: 'featured', url: '/featured' }
        ]
    }),
    returns: () => ({
        gtitle: 'returns',
        title: 'Returns | Being Beast Apparel | BAB',
        desc: defaultDesc,
        breadcrumbs: [
            {
                name: 'home',
                url: '/'
            },
            {
                name: 'return policy',
                url: '/return'
            }
        ]
    }),
    faqs: () => ({
        gtitle: "faq's",
        title: "FAQ's | Being Beast Apparel | BAB",
        desc: defaultDesc,
        breadcrumbs: [
            {
                name: 'home',
                url: '/'
            },
            {
                name: 'faqs',
                url: '/faqs'
            }
        ]
    }),
    contactUs: () => ({
        gtitle: 'Contact Us',
        title: 'Contact Us | Being Beast Apparel | BAB',
        desc: defaultDesc,
        breadcrumbs: [
            {
                name: 'home',
                url: '/'
            },
            {
                name: 'contact us',
                url: '/contact-us'
            }
        ]
    }),
    payWithPaytm: () => ({
        gtitle: 'pay-with-paytm',
        title: 'Pay with Paytm | Being Beast Apparel | BAB',
        desc: defaultDesc
    }),
    myCart: () => ({
        gtitle: 'My Cart',
        title: 'Cart | Being Beast Apparel | BAB',
        desc: defaultDesc,
        breadcrumbs: [
            ...uptoProductBreadcrumbs,
            {
                name: 'my cart',
                url: '/myCart'
            }
        ]
    }),
    login: () => ({
        gtitle: 'Login',
        title: 'Login | Being Beast Apparel | BAB',
        desc: defaultDesc,
        breadcrumbs: [
            {
                name: 'home',
                url: '/'
            },
            {
                name: 'login',
                url: '/login'
            }
        ]
    }),
    orders: () => ({
        gtitle: 'Orders',
        title: 'My Orders | Being Beast Apparel | BAB',
        desc: defaultDesc,
        breadcrumbs: [
            {
                name: 'home',
                url: '/'
            },
            {
                name: 'my account',
                url: '/account-details'
            },
            {
                name: 'orders',
                url: '/orders'
            }
        ]
    }),
    singleOrder: () => ({
        gtitle: 'Single Order',
        title: 'Order Details | Being Beast Apparel | BAB',
        desc: defaultDesc,
        breadcrumbs: [
            {
                name: 'home',
                url: '/'
            },
            {
                name: 'my account',
                url: '/account-details'
            },
            {
                name: 'orders',
                url: '/orders'
            }
        ]
    }),
    accountDetails: () => ({
        gtitle: 'Account Details',
        title: 'Account Details | Being Beast Apparel | BAB',
        desc: defaultDesc,
        breadcrumbs: [
            {
                name: 'home',
                url: '/'
            },
            {
                name: 'my account',
                url: '/account-details'
            }
        ]
    }),
    changePassword: () => ({
        gtitle: 'Account Details',
        title: 'Account Details | Being Beast Apparel | BAB',
        desc: defaultDesc,
        breadcrumbs: [
            {
                name: 'home',
                url: '/'
            },
            {
                name: 'my account',
                url: '/account-details'
            }
        ]
    }),
    signUp: () => ({
        gtitle: 'Create Account',
        title: 'Create Account | Being Beast Apparel | BAB',
        desc: defaultDesc,
        breadcrumbs: [
            {
                name: 'home',
                url: '/'
            },
            {
                name: 'login',
                url: '/login'
            },
            {
                name: 'sign up',
                url: '/login/sign_up'
            }
        ]
    }),
    forgotPassword: () => ({
        gtitle: 'Forgot Password',
        title: 'Forget Password | Being Beast Apparel | BAB',
        desc: defaultDesc,
        breadcrumbs: [
            {
                name: 'home',
                url: '/'
            },
            {
                name: 'forget password',
                url: '/password/forget'
            }
        ]
    }),
    accountActivate: () => ({
        gtitle: 'Account Activation',
        title: 'Account Activation | Being Beast Apparel | BAB',
        desc: defaultDesc
    }),
    checkout: () => ({
        gtitle: 'Checkout',
        title: 'Checkout | Being Beast Apparel | BAB',
        desc: defaultDesc
    }),
    quicklook: product => ({
        gtitle: 'Quicklook',
        title: product.pagetitle,
        desc: product.pagedescription,
        cssFile: 'quicklook.css'
    }),
    default: () => ({
        gtitle: 'BAB',
        title: 'BAB | Premium Athletic Wear | Workout Clothes',
        desc: defaultDesc
    })
};

const collectionDesc = {
    elite:
        'Presenting you our BAB Elite Seamless Series designed to accentuate your physique with the most unrivalled tapered fits and endless strech to push your limits.',
    perform:
        'Our Perform Series is specifically designed for those heated and intense workouts with innovative three piece design to keep you cool and comfortable and well aerated at all times.',
    contour:
        'Be it on streets or during workout BAB Contour Series will give you a flexible and dynamic fit. It comes with stuning colors and minimal branding to help you to stand out. Buy Now at BAB Store. Redefined Yourself.',
    signature:
        'BAB Signature is a cotton based clothing series best suited to wear in summers during workout as well as outside. Due to cotton fabric it gives you a cozy and sweat free feel. Buy Now at BAB Store. Redefined Yourself.',
    fit:
        'BAB Trackpants is our complete workout bottoms with simple design but unique fabric which can be worn during winters as well as summers. Buy Now at BAB Store. Redefined Yourself.'
};
