var mongoose 	= require("mongoose");


var newPerson = new mongoose.Schema({
	availabilty:{
		type:Boolean,
		default:false,
	},
	name:{
		type:String,
		unique:true
	},
	messages:{
		type:Array
	},
	done:{
		type:Boolean
	},
});

module.exports = mongoose.model("chatUser",newPerson);