var mongoose = require("mongoose");



var newReturnOrder = new mongoose.Schema({
	email:String,
	phone:String,
	reason:String,
	orderdate:{type:Date,default:Date.now()},
	ticketno:String,
	orderid:String,
	status:String,
	note:String,
	checked:{type:Boolean,default:false},

});
	
module.exports = mongoose.model("returnOrder",newReturnOrder)