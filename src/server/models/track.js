var mongoose = require('mongoose');

var newTrackSession = new mongoose.Schema({
     trackIds: Array,
     sessionId: String,
     userId: String
});

module.exports = mongoose.model('trackSession', newTrackSession);
