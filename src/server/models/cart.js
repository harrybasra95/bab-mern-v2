module.exports = function Cart(oldCart) {
    this.items = oldCart.items || {};
    this.totalQty = oldCart.totalQty || 0;
    this.totalPrice = oldCart.totalPrice || 0;

    this.add = function(item, id, country) {
        var storedItem = this.items[id];
        if (!storedItem) {
            storedItem = this.items[id] = { items: item, qty: 0, price: 0 };
        }
        storedItem.qty++;
        if (item.sale != 0) {
            if (country == 'in') {
                var x =
                    item.priceindia -
                    Math.ceil((item.priceindia * item.sale) / 100);
            } else {
                var x =
                    item.priceindia -
                    ((item.priceindia * item.sale) / 100).toFixed(2);
            }
        } else {
            var x = item.priceindia;
        }
        storedItem.price += x;
        this.totalQty++;
        this.totalPrice += x;
    };
    this.generateArray = function() {
        var arr = [];
        for (var id in this.items) {
            arr.push(this.items[id]);
        }
        return arr;
    };
    this.getUniqueIds = function() {
        var arr = [];
        for (var id in this.items) {
            arr.push(this.items[id].items.uniqueid);
        }
        return arr;
    };
    this.decrease = function(id, country) {
        if (this.items[id].items.sale != 0) {
            if (country == 'in') {
                var x =
                    this.items[id].items.priceindia -
                    Math.ceil(
                        (this.items[id].items.priceindia *
                            this.items[id].items.sale) /
                            100
                    );
            } else {
                var x =
                    this.items[id].items.priceindia -
                    (
                        (this.items[id].items.priceindia *
                            this.items[id].items.sale) /
                        100
                    ).toFixed(2);
            }
        } else {
            var x = this.items[id].items.priceindia;
        }
        this.items[id].qty = this.items[id].qty - 1;
        this.items[id].price -= x;
        this.totalQty = this.totalQty - 1;
        this.totalPrice -= x;
    };
    this.increase = function(id, country) {
        if (this.items[id].items.sale != 0) {
            if (country == 'in') {
                var x =
                    this.items[id].items.priceindia -
                    Math.ceil(
                        (this.items[id].items.priceindia *
                            this.items[id].items.sale) /
                            100
                    );
            } else {
                var x =
                    this.items[id].items.priceindia -
                    (
                        (this.items[id].items.priceindia *
                            this.items[id].items.sale) /
                        100
                    ).toFixed(2);
            }
        } else {
            var x = this.items[id].items.priceindia;
        }
        this.items[id].qty++;
        this.items[id].price += x;
        this.totalQty++;
        this.totalPrice += x;
    };
    this.remove = function(id) {
        this.totalQty = this.totalQty - this.items[id].qty;
        this.totalPrice = this.totalPrice - this.items[id].price;
        delete this.items[id];
    };
};
