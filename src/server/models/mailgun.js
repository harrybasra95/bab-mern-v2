const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const mg = require('nodemailer-mailgun-transport');
const {
    activemail,
    forgetmail,
    userOrderRecieveMail,
    orderShippedMail,
    subscriptionConfirmMail,
    queryRecivedMail
} = require('../textData/mails');

const api_key = 'key-19d182daacf52086cb13e25a4cbd9c3d';
const domain = 'babclothing.com';
const secret = 'one sweet kitten';

const auth = {
    auth: {
        api_key,
        domain
    }
};
const nodemailerMailgun = nodemailer.createTransport(mg(auth));
module.exports = {
    generate_token: user => {
        return jwt.sign(
            {
                email: user.email
            },
            secret,
            {
                expiresIn: '24h'
            }
        );
    },
    generate_token_1h: user => {
        return jwt.sign(
            {
                email: user.ordernumber
            },
            secret,
            {
                expiresIn: '1h'
            }
        );
    },
    confirm_email: user => {
        sendFunction({
            from: 'BAB Store<no-reply@babclothing.com>',
            to: user.email, // An array if you have multiple recipients.
            subject: `Hi, ${
                user.fname
            }  Just one more step needed to complete your registration,.`,
            html: activemail(user)
        });
    },
    email_confirmed: user => {
        sendFunction({
            from: 'Excited User <me@samples.beingbeast.org>',
            to: user.email,
            subject: 'User activated',
            text: 'User activated',
            html: 'User avtivated'
        });
    },
    forgot_password: user => {
        sendFunction({
            from: 'BAB Store<no-reply@babclothing.com>',
            to: user.email, // An array if you have multiple recipients.
            subject: `Hi, ${
                user.fname
            } Please verify you account to change your password.`,
            html: forgetmail(user)
        });
    },
    delivery_failed: (user, order) => {
        sendFunction({
            from: 'BAB Store<no-reply@babclothing.com>',
            to: 'order@babclothing.com', // An array if you have multiple recipients.
            subject: `!!!!!!!!!!Package has not been created for  ${
                order.ordernumber
            }`,
            html: `<h2>!!!!!!!!!!Package has not been created for  ${
                order.ordernumber
            }</h2>`
        });
    },
    order_canceled: order => {
        sendFunction({
            from: 'BAB Store<no-reply@babclothing.com>',
            to: 'order@babclothing.com',
            subject: `Order no ${order.ordernumber} has been canceled`,
            html: `<h2>Order no ${order.ordernumber} has been canceled</h2>`
        });
    },
    order_recieved: (user, order) => {
        let p = '';
        order.productdetails.forEach(y => {
            p += `<li>${y.product.uniqueid} -  ${y.product.size}</li>`;
        });
        let c;
        if (order.coupon) {
            c = order.coupon.name;
        } else {
            c = 'No coupon';
        }

        const mail = `<li>Order number :${order.ordernumber}</li>\
						<li>Waybill : ${order.waybill}</li>\
						<li>Name : ${order.persondetails.fname} ${order.persondetails.lname}</li>\
						<li>Total price :${order.totalprice}</li>\
						<li>Payment Type :${order.paymenttype}</li>\
						<li>Phone no. :${order.persondetails.phone}</li>\
						<li>\
							Products :\
							<ol>${p}</ol>\
						 :</li>\
						<li>Coupon :${c}</li>\
						<li>Address :${order.deliveryaddress.street_no}<br>${
            order.deliveryaddress.city
        }<br>${order.deliveryaddress.state}<br>${
            order.deliveryaddress.pincode
        }</li>`;
        sendFunction({
            from: 'BAB Store<no-reply@babclothing.com>',
            to: 'order@babclothing.com',
            subject: `Hi One order recieved.Order number ${order.ordernumber}`,
            html: mail
        });
        sendFunction({
            from: 'BAB Store<no-reply@babclothing.com>',
            to: user.email, // An array if you have multiple recipients.
            subject: `Hi ${user.name} ,Your order has been recieved`,
            html: userOrderRecieveMail()
        });
    },

    order_shipped: user => {
        sendFunction({
            from: 'BAB Store<no-reply@babclothing.com>',
            to: user.email, // An array if you have multiple recipients.
            subject: `Hi ${user.name}, Order confirmed.`,
            html: orderShippedMail(user)
        });
    },
    subs_confirm_email: user => {
        sendFunction({
            from: 'BAB<no-reply@babclothing.com>',
            to: user.email, // An array if you have multiple recipients.
            subject: 'Welcome to BAB Family ',
            html: subscriptionConfirmMail()
        });
    },
    query_recieved: query => {
        const mail = `<li>Name : ${query.fname} ${query.lname}</li>\
					<li>Query Number : ${query.qnumber}</li>\
					<li>Email :${query.eadd}</li>\
					<li>Phone :${query.phone}</li>\
                    <li>Message :${query.message}</li>`;
        sendFunction({
            from: 'BAB Store<no-reply@babclothing.com>',
            to: 'order@babclothing.com',
            subject: 'Hi One Query recieved.',
            html: mail
        });
        sendFunction({
            from: 'BAB Store<no-reply@babclothing.com>',
            to: query.eadd,
            subject: `Hi ${query.fname} ,Your query has been submitted`,
            html: queryRecivedMail(query)
        });
    }
};

const sendFunction = data => {
    nodemailerMailgun.sendMail(
        {
            ...data
        },
        (err, info) => {
            if (err) {
                console.log(err);
            } else {
                console.log(JSON.stringify(info));
            }
        }
    );
};
