var mongoose = require('mongoose');

var newLink = new mongoose.Schema({
    name: String,
    date: Date,
    page: String,
    for: String,
    linkid: String,
    users: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'trackrecord'
        }
    ]
});

// module.exports = mongoose.model("link",newLink);
