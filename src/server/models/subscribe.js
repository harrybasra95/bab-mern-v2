var mongoose = require("mongoose");

var newSubscribe = new mongoose.Schema({
	email:String,
	phone:String,
	dob:Date,
	gender:String,
	joindate:{type:Date,default:Date.now},
	name:String,
	login:String,
	trackid:String,
});

module.exports = mongoose.model("subscribe",newSubscribe);