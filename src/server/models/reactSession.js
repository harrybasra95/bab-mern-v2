const mongoose = require('mongoose');

const newReactSession = new mongoose.Schema(
    {
        id: String,
        cart: {
            type: Object,
            default: {
                products: {},
                totalPrice: 0,
                totalQuantity: 0
            }
        },
        user: { type: Object, default: {} },
        coupon: { type: Object, default: {} },
        isLoggedIn: { type: Boolean, default: false },
        selectedCountry: { type: String, default: null }
    },
    { minimize: false }
);

module.exports = mongoose.model('reactSession', newReactSession);
