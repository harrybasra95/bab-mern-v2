const mongoose = require('mongoose');

const rechecktime = new Date();
rechecktime.setMinutes(rechecktime.getMinutes() + 5);

const newOrder = new mongoose.Schema({
    productdetails: Array,
    userid: String,
    ordernumber: Number,
    waybill: String,
    type: String,
    paymenttype: String,
    selected_country: String,
    temporarytoken: { type: String },
    orderdate: { type: Date, default: Date.now },
    status: { type: String, default: 'PP' },
    statusreason: { type: String, default: 'Payment pending' },
    shipping: Number,
    tax: Number,
    return: { type: Boolean, default: false },
    totalprice: Number,
    discount: { type: Number, default: 0 },
    subtotalprice: Number,
    totalqty: Number,
    invoiceno: Number,
    deliverydate: Date,
    rechecktime: { type: Date, default: rechecktime },
    checked: { type: Boolean, default: false },
    paymentdetails: Object,
    persondetails: Object,
    deliveryaddress: Object,
    billingaddress: Object,
    deliverydetails: Object,
    canceldetails: Object,
    refunddetails: Object,
    paypaldata: Object,
    paypaltoken: String,
    printed: { type: Boolean, default: false },
    coupon: Object,
    lastcoupon: Object,
    selfcancel: false,
    returndetails: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'returnOrder'
        }
    ]
});

newOrder.methods.getPublicFields = function() {
    const returnObject = {
        status: this.status,
        waybill: this.waybill,
        returndetails: this.returndetails,
        productdetails: this.productdetails,
        ordernumber: this.ordernumber,
        persondetails: this.persondetails,
        paymenttype: this.paymenttype,
        subtotalprice: this.subtotalprice,
        shipping: this.shipping,
        coupon: this.coupon,
        discount: this.discount,
        totalprice: this.totalprice,
        deliveryaddress: this.deliveryaddress
    };
    return returnObject;
};

module.exports = mongoose.model('orders', newOrder);
