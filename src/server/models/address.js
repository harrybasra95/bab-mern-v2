const mongoose = require('mongoose');
const titlize = require('mongoose-title-case');

const newAddress = new mongoose.Schema({
    fname: { type: String, trim: true },
    lname: { type: String, trim: true },
    phone: { type: String, trim: true },
    street_no: { type: String, trim: true },
    city: { type: String, trim: true },
    state: { type: String, trim: true },
    country: { type: String, trim: true },
    pincode: { type: Number, trim: true }
});
newAddress.plugin(titlize, {
    paths: ['street_no', 'city', 'state', 'country']
});

module.exports = mongoose.model('address', newAddress);
