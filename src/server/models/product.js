const mongoose = require('mongoose');

const newProduct = new mongoose.Schema({
    type: String,
    name: String,
    foldername: String,
    size: String,
    uniqueid: String,
    wcare: String,
    colorname: String,
    secondcolorname: String,
    color: String,
    secondcolor: String,
    skunumber: Number,
    quantity: Number,
    description: String,
    series: String,
    priceindia: Number,
    priceout: Number,
    featured: { type: Boolean, default: false },
    date: Date,
    display: String,
    title: String,
    imgalt: String,
    imgno: Number,
    imgaddresses: Array,
    pagedescription: String,
    pagetitle: String,
    pricechart: Object,
    keywords: Object,
    productinfo: Object,
    seriesinfo: String,
    sale: { type: Number, default: 0 },
    sizeInfo: Object,
    brand: String,
    totalQuantity: Number
});

newProduct.pre('save', function (next) {
    if (this.size === 'Small' && this.brand === 'bab') {
        this.totalQuantity = 0;
        Object.keys(this.sizeInfo).forEach(key => {
            this.totalQuantity += this.sizeInfo[key].quantity;
        });
    }
    next();
});

module.exports = mongoose.model('product', newProduct);
