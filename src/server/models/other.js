var mongoose 	= require("mongoose");

var other = new mongoose.Schema({
	name:String,
	invoiceno:Number,
	fromWhatsapp:{type:Number,default:0},
	fromFacebook:{type:Number,default:0},
	trackdata:String,
})

module.exports = mongoose.model("other",other)
