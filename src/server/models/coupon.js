const mongoose = require('mongoose');

const newCoupon = new mongoose.Schema({
     type: String,
     gymAddress: String,
     name: { type: String },
     discription: String,
     cod: { type: Boolean, default: false },
     amounttype: { type: Boolean },
     percenttype: { type: Boolean },
     initialdate: { type: Date, default: Date.now() },
     expiredate: { type: Date },
     notonproducts: { type: Array },
     particularuser: { type: Object },
     peruser: { type: Number, default: 0 },
     users: { type: Array },
     minamount: { type: Number },
     discount: Number,
     value: Number
});

module.exports = mongoose.model('coupon', newCoupon);
