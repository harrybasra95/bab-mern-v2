var mongoose 	= require("mongoose");


var newQuery = new mongoose.Schema({
	fname:{type:String,required:true},
	lname:{type:String,required:true},
	eadd:{type:String,required:true},
	message:{type:String,required:true},
	qnumber:{type:Number,required:true},
	phone:{type:Number,required:true},
	reply:{type:Boolean,default:false},
	
})
module.exports = mongoose.model("query",newQuery)
