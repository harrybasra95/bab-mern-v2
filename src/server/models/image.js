let mongoose = require('mongoose'),
    titlize = require('mongoose-title-case');

const newImage = new mongoose.Schema({
    image: Object
});

module.exports = mongoose.model('image', newImage);
