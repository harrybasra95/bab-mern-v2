const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const bcrypt = require('bcrypt-nodejs');
const titlize = require('mongoose-title-case');

const newUser = new mongoose.Schema({
    fname: { type: String, trim: true },
    lname: { type: String, trim: true },
    username: { type: String, required: true, trim: true },
    email: { type: String, required: true, trim: true },
    dob: { type: Date, trim: true },
    gender: { type: String, trim: true },
    joindate: { type: Date, default: Date.now, trim: true },
    phone: { type: Number, trim: true },
    cpassword: { type: String, trim: true },
    address_no: { type: Number, default: 0, trim: true },
    password: { type: String, trim: true },
    active: { type: Boolean, required: true, default: false, trim: true },
    temporarytoken: { type: String, trim: true },
    resettoken: { type: String, trim: true },
    coupons: { type: Array, trim: true },
    mails: { type: Array, trim: true },
    fb_id: { type: String, trim: true },
    role: { type: String, enum: ['user', 'admin'], trim: true },
    lastcoupon: { type: String, trim: true },
    google_id: { type: String, trim: true },
    lastOrderDetails: { type: Object, trim: true },
    orders: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'orders'
        }
    ],
    tempOrders: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'tempOrders'
        }
    ],
    wishlist: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'product'
        }
    ],
    address: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'address'
        }
    ],
    track_id: Array,
    defaultaddress: { type: Object, default: false },
    cart: Object
});

newUser.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

newUser.plugin(titlize, {
    paths: ['fname', 'lname']
});

newUser.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.cpassword);
};

newUser.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', newUser);
