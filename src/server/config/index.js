module.exports = {
     allOrdersKeysList: 'productdetails ordernumber totalprice status',
     singleOrderKeysList:
          'status waybill returndetails productdetails ordernumber persondetails paymenttype subtotalprice shipping coupon discount totalprice deliveryaddress',
     userKeysList:
          'fname lname email dob gender role phone active fb_id google_id lastOrderDetails',
     productKeysList:
          'type name size uniqueid colorname secondcolorname imgaddresses pricechart sale',
     delhiveryToken: '',
     serverIp: 'https://www.babclothing.com',
     inServerIp: 'in.babclothing.com',
     jwtSecret: '',
     zohoConfig: {
          authToken: '',
          organization_id: ''
     },
     paypalConfig: {
          mode: 'live',
          client_id:
               '',
          client_secret:
               ''
     },
     paytmConfig: {
          merchentKey: '',
          merchentId: ''
     },
     ccAvenueConfig: {
          protocol: 'https',
          serverIp: 'in.babclothing.com',
          serverAddress: 'https://secure.ccavenue.com',
          testServerAddress: 'https://test.ccavenue.com',
          merchantId: '',
          workingKey: '',
          accessCode: '',
          ccAvenueApiServer: 'https://api.ccavenue.com/apis/servlet/DoWebTrans',
          ccAvenueTestServer: ''
     },
     countryFullForm: {
          us: 'United States of America',
          ca: 'Canada',
          uk: 'United Kingdom',
          au: 'Australia',
          nw: 'New Zealand'
     },
     couponData: {},
     currencyCode: {
          in: 'INR',
          us: 'USD',
          ca: 'CAD',
          au: 'AUD',
          row: 'USD',
          eu: 'EUR',
          uk: 'GBP',
          nz: 'NZD',
          ae: 'AED',
          none: 'INR'
     },
     symbolchart: {
          in: 'Rs.',
          us: '&#x24;',
          ca: '&#x24;',
          au: '&#x24;',
          row: '&#x24;',
          eu: '&#8364;',
          uk: '&#163;',
          nz: '&#x24;',
          ae: '&#x62f;&#x2e;&#x625;',
          none: 'Rs.'
     },
     sizeShort: {
          Small: 's',
          Medium: 'm',
          Large: 'l',
          'Extra Large': 'xl',
          getQuantityChart() {
               const returnData = {};
               Object.keys(this).forEach((key, i) => {
                    if (i < Object.keys(this).length - 1) {
                         returnData[key] = {};
                    }
               });
               return returnData;
          }
     },
     RowCountryList: [
          'Bahrain',
          'Bangladesh',
          'Barbados',
          'Belarus',
          'Bermuda',
          'Bhutan',
          'Botswana',
          'Brunei Darussalam',
          'Cambodia',
          'Cayman Island',
          "China (People's Rep)",
          'Cuba',
          'Egypt',
          'Eritrea',
          'Ethiopia',
          'Fiji',
          'Georgia',
          'Ghana',
          'Hong Kong',
          'Iceland',
          'Israel',
          'Japan',
          'Jordan',
          'Kenya',
          'Korea (Republic of)',
          'Macao (China)',
          'Malawi',
          'Malaysia',
          'Mauritius',
          'Mongolia',
          'Morocco',
          'Namibia',
          'Nauru',
          'Nepal',
          'Norway',
          'Oman',
          'Pakistan',
          'Philippines',
          'Qatar',
          'Russia',
          'Senegal',
          'Singapore',
          'South Africa',
          'Sudan',
          'Switzerland',
          'Taiwan',
          'Tanzania',
          'Thailand',
          'Turkey',
          'Uganda',
          'Ukraine',
          'United States of America',
          'Vietnam'
     ],
     EuCountryList: [
          'Austria',
          'Bulgaria (Rep)',
          'Cyprus',
          'Estonia',
          'Finland',
          'France',
          'Germany',
          'Greece',
          'Hungary',
          'Ireland',
          'Italy',
          'Latvia',
          'Netherlands',
          'Poland',
          'Portugal',
          'Romania',
          'Spain'
     ],
     paymentTypeList: {
          1: 'cod',
          2: 'paytm',
          3: 'cc',
          card: '2co',
          paypal: 'paypal'
     },
     indianStateCodesFullForm: {
          AP: 'Andhra Pradesh',
          AR: 'Arunachal Pradesh',
          AS: 'Assam',
          BR: 'Bihar',
          CH: 'Chandigarh',
          CT: 'Chhattisgarh',
          DL: 'Delhi',
          GA: 'Goa',
          GJ: 'Gujarat',
          HR: 'Haryana',
          HP: 'Himachal Pradesh',
          JK: 'Jammu and Kashmir',
          JH: 'Jharkhand',
          KA: 'Karnataka',
          KL: 'Kerala',
          MP: 'Madhya Pradesh',
          MH: 'Maharashtra',
          MN: 'Manipur',
          ML: 'Meghalaya',
          MZ: 'Mizoram',
          NL: 'Nagaland',
          OR: 'Odisha (Orissa)',
          PB: 'Punjab',
          RJ: 'Rajasthan',
          SK: 'Sikkim',
          TN: 'Tamil Nadu',
          TR: 'Tripura',
          UP: 'Uttar Pradesh',
          UT: 'Uttarakhand',
          WB: 'West Bengal'
     }
};
