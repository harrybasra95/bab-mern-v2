module.exports = [
    {
        r: 13,
        c: 'Australia',
        f: 'au'
    },
    {
        r: 18,
        c: 'Austria',
        f: 'at'
    },
    {
        r: 13,
        c: 'Bahrain',
        f: 'bh'
    },
    {
        r: 10,
        c: 'Bangladesh',
        f: 'bd'
    },
    {
        r: 13,
        c: 'Barbados',
        f: 'bb'
    },
    {
        r: 18,
        c: 'Belarus',
        f: 'by'
    },
    {
        r: 15,
        c: 'Bermuda',
        f: 'bm'
    },
    {
        r: 10,
        c: 'Bhutan',
        f: 'bt'
    },
    {
        r: 19,
        c: 'Botswana',
        f: 'bw'
    },
    {
        r: 10,
        c: 'Brunei Darussalam',
        f: 'bn'
    },
    {
        r: 13,
        c: 'Bulgaria (Rep)',
        f: 'bg'
    },
    {
        r: 10,
        c: 'Cambodia',
        f: 'kh'
    },
    {
        r: 19,
        c: 'Canada',
        f: 'ca'
    },
    {
        r: 16,
        c: 'Cayman Island',
        f: 'ky'
    },
    {
        r: 10,
        c: "China (People's Rep)",
        f: 'cn'
    },
    {
        r: 18,
        c: 'Cuba',
        f: 'cu'
    },
    {
        r: 15,
        c: 'Cyprus',
        f: 'cy'
    },
    {
        r: 14,
        c: 'Egypt',
        f: 'eg'
    },
    {
        r: 15,
        c: 'Eritrea',
        f: 'er'
    },
    {
        r: 15,
        c: 'Estonia',
        f: 'ee'
    },
    {
        r: 19,
        c: 'Ethiopia',
        f: 'et'
    },
    {
        r: 13,
        c: 'Fiji',
        f: 'fj'
    },
    {
        r: 25,
        c: 'Finland',
        f: 'fi'
    },
    {
        r: 20,
        c: 'France',
        f: 'fr'
    },
    {
        r: 17,
        c: 'Georgia',
        f: 'gr'
    },
    {
        r: 21,
        c: 'Germany',
        f: 'de'
    },
    {
        r: 12,
        c: 'Ghana',
        f: 'gh'
    },
    {
        r: 18,
        c: 'Greece',
        f: 'gr'
    },
    {
        r: 15,
        c: 'Hong Kong',
        f: 'hk'
    },
    {
        r: 18,
        c: 'Hungary',
        f: 'hu'
    },
    {
        r: 19,
        c: 'Iceland',
        f: 'is'
    },
    {
        r: 50,
        c: 'India',
        f: 'in'
    },
    {
        r: 20,
        c: 'Ireland',
        f: 'ie'
    },
    {
        r: 15,
        c: 'Israel',
        f: 'il'
    },
    {
        r: 19,
        c: 'Italy',
        f: 'it'
    },
    {
        r: 11,
        c: 'Japan',
        f: 'jp'
    },
    {
        r: 15,
        c: 'Jordan',
        f: 'jo'
    },
    {
        r: 18,
        c: 'Kenya',
        f: 'ke'
    },
    {
        r: 11,
        c: 'Korea (Republic of)',
        f: 'kr'
    },
    {
        r: 14,
        c: 'Latvia',
        f: 'lv'
    },
    {
        r: 10,
        c: 'Macao (China)',
        f: 'mo'
    },
    {
        r: 14,
        c: 'Malawi',
        f: 'mw'
    },
    {
        r: 17,
        c: 'Malaysia',
        f: 'my'
    },
    {
        r: 15,
        c: 'Mauritius',
        f: 'mu'
    },
    {
        r: 13,
        c: 'Mongolia',
        f: 'mn'
    },
    {
        r: 16,
        c: 'Morocco',
        f: 'ma'
    },
    {
        r: 13,
        c: 'Namibia',
        f: 'na'
    },
    {
        r: 12,
        c: 'Nauru',
        f: 'nr'
    },
    {
        r: 10,
        c: 'Nepal',
        f: 'np'
    },
    {
        r: 20,
        c: 'Netherlands',
        f: 'nl'
    },
    {
        r: 12,
        c: 'New Zealand',
        f: 'nz'
    },
    {
        r: 25,
        c: 'Norway',
        f: 'no'
    },
    {
        r: 13,
        c: 'Oman',
        f: 'om'
    },
    {
        r: 10,
        c: 'Pakistan',
        f: 'pk'
    },
    {
        r: 13,
        c: 'Philippines',
        f: 'ph'
    },
    {
        r: 15,
        c: 'Poland',
        f: 'pl'
    },
    {
        r: 15,
        c: 'Portugal',
        f: 'pt'
    },
    {
        r: 14,
        c: 'Qatar',
        f: 'qa'
    },
    {
        r: 19,
        c: 'Romania',
        f: 'ro'
    },
    {
        r: 25,
        c: 'Russia',
        f: 'ru'
    },
    {
        r: 17,
        c: 'Senegal',
        f: 'sn'
    },
    {
        r: 13,
        c: 'Singapore',
        f: 'sg'
    },
    {
        r: 17,
        c: 'South Africa',
        f: 'za'
    },
    {
        r: 16,
        c: 'Spain',
        f: 'es'
    },
    {
        r: 18,
        c: 'Sudan',
        f: 'sd'
    },
    {
        r: 23,
        c: 'Switzerland',
        f: 'ch'
    },
    {
        r: 11,
        c: 'Taiwan',
        f: 'tw'
    },
    {
        r: 15,
        c: 'Tanzania',
        f: 'tz'
    },
    {
        r: 10,
        c: 'Thailand',
        f: 'th'
    },
    {
        r: 13,
        c: 'Turkey',
        f: 'tr'
    },
    {
        r: 17,
        c: 'United Arab Emirates',
        f: 'ae'
    },
    {
        r: 18,
        c: 'Uganda',
        f: 'ug'
    },
    {
        r: 19,
        c: 'United Kingdom',
        f: 'uk'
    },
    {
        r: 18,
        c: 'Ukraine',
        f: 'ua'
    },
    {
        r: 13,
        c: 'United States of America',
        f: 'us'
    },
    {
        r: 10,
        c: 'Vietnam',
        f: 'vn'
    }
];
