var express 	= require("express");
var router 		= express.Router({mergeParams:true});
var user 		= require("../models/user");



router.get("/",function(req,res){

	user.find({},function(err,user){
			if (err) {
				console.log("error in finding");
				console.log(err);}
				else{
				res.render("database",{user:user});
					}
	});	
});





router.get("/new",function(req,res){
	res.render("newUser");
	
});


router.post("/",function(req,res){
		user.create(req.body.user,function(err,user){
						res.redirect("/users");
					})
});


router.get("/:id",function(req,res){

	var id = req.params.id;
	user.find({_id:id},function(err,user){
		if(err){console.log("error in finding");}
		else{
			res.render("userdetails",{user:user[0]})
		}
	})

});






router.get("/:id/edit",function(req,res){


	user.find({_id:req.params.id},function(err,user){
			if (err) {
				console.log("error in getting user");
				console.log(err);}
				else{
					
				res.render("updateUser",{user:user});
					}
	});
	
});


router.put("/:id",function(req,res){
	user.findByIdAndUpdate(req.params.id,req.body.user,function(err,user){
		if (err) {console.log("error in updating");
					console.log(err);}
		else{
				res.redirect("/users/"+req.params.id)
					}
	})
});

router.delete("/:id",function(req,res){
	user.findByIdAndRemove(req.params.id,function(err,user){
		res.redirect("/users")
	});
});

module.exports = router;