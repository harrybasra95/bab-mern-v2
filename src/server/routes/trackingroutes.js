const express = require('express');
const router = express.Router({ mergeParams: true });
const { trackingController } = require('../controllers');

router.post('/create', trackingController.createSession);

module.exports = router;
