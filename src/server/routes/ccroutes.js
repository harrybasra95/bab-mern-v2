var express 		= require("express"),
	router 			= express.Router({mergeParams:true});
	var http = require('http'),
	order  			= require("../models/orders"),
    fs = require('fs'),
    qs = require('querystring');
    ccav = require('../ccavenue/ccavutil.js'),
    ccavReqHandler = require('../ccavenue/ccavRequestHandler.js'),
    ccavResHandler=require('../ccavenue/ccavResponseHandler.js');




router.get('/about', function (req, res){
    	res.render('payment');
});


router.post('/payment/cc/:id', function (req, res){
	order.findOne({temporarytoken:req.params.id},function(err,foundOrder){
		if (err){
				req.flash("error","Please try again")
				res.redirect("/apparel");
			}else{
				var token = req.params.id;
				jwt.verify(token,secret,function(err,decoded){
					if (err || !foundOrder) {
						req.flash("error","Please try again");
						res.redirect("/apparel");
					}else{
						var request = req;
						var response = res;
						// console.log(foundOrder);
						var body = 'merchant_id=145324&order_id='+foundOrder.ordernumber+'&currency=INR&amount='+foundOrder.totalprice+'&redirect_url=https%3A%2F%2Fin.babclothing.com%2FccavResponseHandler&cancel_url=https%3A%2F%2Fin.babclothing.com%2FccavResponseHandler&language=EN',
						workingKey = '725E9E182739397DF1D199CDDE6AC049',	//Put in the 32-Bit key shared by CCAvenues.
						accessCode = 'AVKE73EI30CA74EKAC',			//Put in the Access Code shared by CCAvenues.
						encRequest = '',
						formbody = '';
					    request.on('data', function (data) {
					    body += data;
						encRequest = ccav.encrypt(body,workingKey); 
						formbody = '<form id="nonseamless" method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"/> <input type="hidden" id="encRequest" name="encRequest" value="' + encRequest + '"><input type="hidden" name="access_code" id="access_code" value="' + accessCode + '"><script language="javascript">document.redirect.submit();</script></form>';
					    });
									
					    request.on('end', function () {
					        response.writeHeader(200, {"Content-Type": "text/html"});
						response.write(formbody);
						// console.log(formbody);
						response.end();
					    });
					   return response; 
					}
				})
			}
	});
});






module.exports = router;
