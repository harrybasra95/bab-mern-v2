const express = require('express');
const router = express.Router({ mergeParams: true });
const middlewares = require('../middlewares');
const { apiController } = require('../controllers');

router.get(
     '/add_to_wishlist/:id',
     middlewares.userExists,
     apiController.wishlistAdd
);
router.get(
     '/wishlist/remove/:id',
     middlewares.userExists,
     apiController.wishlistRemove
);
router.get('/cod-option', apiController.codOptions);
router.post('/subscribe', apiController.subscribe);
router.post('/check/pincode', apiController.checkPincode);
router.post('/check/email', apiController.checkEmail);
router.post('/users/activation_mail/send', apiController.sendActivationMail);
router.post('/mail/mail-delivered', apiController.mailDelivererd);
router.get('/get-logo/:username/:mail_id', apiController.mailOpened);
router.get('/mail/mail-clicked', apiController.mailClicked);
router.get('/check-shipping/:id', apiController.checkShipping);

module.exports = router;
