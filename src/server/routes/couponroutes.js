const express = require('express');
const router = express.Router({ mergeParams: true });
const { authHelpers } = require('../helpers');
const { couponController } = require('../controllers');

router.post('/coupon', authHelpers.userExists, couponController.applyCoupon);
router.get(
    '/coupon/remove',
    authHelpers.userExists,
    couponController.removeCoupon
);


module.exports = router;
