const addressRoutes = require('./addressroutes');
const adminRoutes = require('./adminroutes');
const apiRoutes = require('./apiroutes');
const appRoutes = require('./approutes');
const authRoutes = require('./authroutes');
const backRoutes = require('./backroutes');
const cartRoutes = require('./cartRoutes');
// const ccRoutes = require('./ccroutes');
const checkoutRoutes = require('./checkoutroutes');
const frontRoutes = require('./frontroutes');
const pageApiRoutes = require('./pageApiRoutes');
const testRoutes = require('./test');
const trackingRoutes = require('./trackingroutes');
const passportRoutes = require('./passportroutes');
const couponRoutes = require('./couponroutes');
const reactSessionRoutes = require('./reactSessionRoutes');
const authApiRoutes = require('./authApiRoutes');
const orderRoutes = require('./orderRoutes');
const userRoutes = require('./userRoutes');

module.exports = {
     addressRoutes,
     adminRoutes,
     apiRoutes,
     appRoutes,
     authRoutes,
     backRoutes,
     cartRoutes,
     checkoutRoutes,
     frontRoutes,
     pageApiRoutes,
     testRoutes,
     trackingRoutes,
     passportRoutes,
     couponRoutes,
     reactSessionRoutes,
     authApiRoutes,
     orderRoutes,
     userRoutes
};
