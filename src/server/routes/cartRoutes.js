const express = require('express');
const { cartController } = require('../controllers');

const router = express.Router({ mergeParams: true });

router.post('/add_to_cart/', cartController.addToCart);
router.get('/add_to_cart/:uniqueid/:size', cartController.quicklookAddToCart);
router.post('/buy-now', cartController.buyNow);
router.get('/qty/:val/:id', cartController.changeProductQty);
router.get('/getcart', cartController.getCart);

router.post(
    '/api/cart/add-to-cart-by-unique-id',
    cartController.addToCartByUniqueId
);

router.post(
    '/api/cart/remove-from-cart-by-unique-id',
    cartController.removeFromCartByUniqueId
);

module.exports = router;
