const express = require('express');
const { userController } = require('../controllers');

const router = express.Router({ mergeParams: true });

router.post('/subscribe', userController.subscribe);

module.exports = router;
