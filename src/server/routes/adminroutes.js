const express = require('express');
const app = express();
const router = express.Router({ mergeParams: true });
const { adminController } = require('../controllers');

router.get('/', adminController.homepage);
router.get('/users', adminController.users);
router.get('/admin/order/confirm/:id', adminController.confirmOrder);

module.exports = router;
