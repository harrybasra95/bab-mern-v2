const express = require('express');
const { checkoutController } = require('../controllers');

const router = express.Router({ mergeParams: true });

router.post('/checkout', checkoutController.createOrder);
router.get('/2co/makeorder/:id', checkoutController.makeOrder2co);
router.get('/cancelmyorder/:id', checkoutController.cancelOrderWithPayments);
router.post('/order/cancel/:id', checkoutController.cancelOrderWithoutPayments);
router.post('/order/return/:id', checkoutController.returnOrder);

module.exports = router;
