const express = require('express');
const router = express.Router({ mergeParams: true });
const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const User = require('../models/user');
const cart = require('../models/cart');
const mailgun = require('../models/mailgun');
const csrf = require('csurf');
const validator = require('validator');
const request = require('request');
const csrfProtection = csrf();

passport.use(
    'local',
    new localStrategy(function(username, password, done) {
        User.findOne({ username: username }, function(err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false);
            }
            if (!user.verifyPassword(password)) {
                return done(null, false);
            }
            return done(null, user);
        });
    })
);

router.post('/sign_up', function(req, res) {
    var parts2 =
        req.body.user.date +
        '/' +
        req.body.user.month +
        '/' +
        req.body.user.year;
    var parts = parts2.split('/');
    var mydate = new Date(parts[2], parts[0] - 1, parts[1], 0, 0, 0);

    var x = validator.isAlpha(req.body.user.fname);
    var y = validator.isAlpha(req.body.user.lname);
    var a = validator.isLength(req.body.user.fname, { min: 3, max: 20 });
    var b = validator.isLength(req.body.user.lname, { min: 3, max: 20 });
    req.body.username = req.body.username.toLowerCase();

    if (x == false || y == false || a == false || b == false) {
        res.send({
            success: false,
            message: 'Please enter a valid name between 3 to 20 char'
        });
    } else if (validator.isEmail(req.body.username) == false) {
        res.send({ success: false, message: 'Please enter a valid Email' });
    } else if (
        validator.isNumeric(req.body.user.phone) == false ||
        req.body.user.phone.length != 10 ||
        req.body.user.phone.length == null
    ) {
        res.send({ success: false, message: 'Please enter a valid Phone' });
    } else if (req.body.password != req.body.cpassword) {
        res.send({
            success: false,
            message: "Password and confirm password don't match"
        });
    } else {
        var newUser = new User({
            username: req.body.username,
            fname: req.body.user.fname,
            lname: req.body.user.lname,
            phone: req.body.user.phone,
            email: req.body.username,
            dob: mydate
        });
        newUser.temporarytoken = mailgun.generate_token(newUser);
        newUser.cpassword = newUser.generateHash(req.body.password);
        User.register(newUser, req.body.password, function(err, foundUser) {
            if (err) {
                console.log(err);
                res.send({ success: false, message: 'Email already taken' });
            } else
                passport.authenticate('local')(req, res, function() {
                    res.send({
                        success: true,
                        message: 'Signed up successfully',
                        url: '/apparel'
                    });
                    mailgun.confirm_email(foundUser);
                    request.get(
                        'https://in.babclothing.com/attachtrackid/' +
                            foundUser.username +
                            '/' +
                            req.session.userid,
                        function(err) {
                            console.log(err);
                        }
                    );
                });
        });
    }
});

router.post('/signup_checkout', function(req, res) {
    req.body.username = req.body.username.toLowerCase();
    if (validator.isEmail(req.body.username) == false) {
        res.send({ success: false, message: 'Please enter a valid Email' });
    } else if (req.body.password != req.body.cpassword) {
        res.send({
            success: false,
            message: "Password and confirm password don't match"
        });
    } else {
        var newUser = new User({
            username: req.body.username,
            email: req.body.username
        });
        newUser.temporarytoken = mailgun.generate_token(newUser);
        newUser.cpassword = newUser.generateHash(req.body.password);
        User.register(newUser, req.body.password, function(err, foundUser) {
            if (err) {
                console.log(err);
                res.send({ success: false, message: 'Email already taken' });
            } else
                passport.authenticate('local')(req, res, function() {
                    res.send({
                        success: true,
                        message: 'Signed up successfully',
                        url: '/checkout'
                    });
                    foundUser.fname = 'User';
                    request.get(
                        'https://in.babclothing.com/attachtrackid/' +
                            foundUser.username +
                            '/' +
                            req.session.userid,
                        function(err) {
                            console.log(err);
                        }
                    );
                    mailgun.confirm_email(foundUser);
                });
        });
    }
});

router.use(csrfProtection);

router.get('/logout', function(req, res) {
    var Cart = new cart(req.session.cart ? req.session.cart : {});
    if (req.user) {
        User.findById(req.user._id, function(err, foundUser) {
            if (err) {
                throw err;
            }
            foundUser.cart = Cart;
            foundUser.save(function(err) {
                if (err) {
                    throw err;
                }
                req.logout();
                res.redirect('/redirect/callback');
            });
        });
    } else {
        res.redirect('/apparel');
    }
});

router.post(
    '/login',
    passport.authenticate('local', {
        failureRedirect: '/login',
        failureFlash: true
    }),
    function(req, res) {
        var request = require('request');
        request.get(
            'https://in.babclothing.com/attachtrackid/' +
                req.user.username +
                '/' +
                req.session.userid,
            function(err) {
                console.log(err);
            }
        );
        req.session.coupon = null;
        res.locals.session = req.session;
        req.flash('success', 'Login successfully');
        if (req.session.returnTo && req.session.returnTo.length != 0) {
            var x = req.session.returnTo;
            req.session.returnTo = null;
            res.locals.session = req.session;
            res.redirect('/' + x);
        } else {
            res.redirect('/checkout');
        }
    }
);

function cartCheck(req, res, next) {
    var Cart = new cart(req.session.cart ? req.session.cart : {});
    if (Cart.totalQty == 0) {
        req.flash('error', 'Please add some products.');
        res.redirect('/');
    } else {
        next();
    }
}

function loggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}

module.exports = router;
