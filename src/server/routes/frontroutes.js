const express = require('express');
const router = express.Router({ mergeParams: true });
const controller = require('../controllers/frontController');
const middlewares = require('../middlewares');
const csrf = require('csurf');
const user = require('../models/user');
const jwt = require('jsonwebtoken');
const secret = 'one sweet kitten';
const csrfProtection = csrf();
Array.prototype.remove = require('array-remove-by-value');

// var arr = [];

router.use(csrfProtection);
router.get('/', controller.slash);
router.get('/apparel', controller.homepage);
router.get('/products', controller.products);
router.get('/wishlist', middlewares.userExists, controller.wishlist);
router.get('/terms-and-conditions', controller.termsAndConditions);
router.get('/privacy-policy', controller.privacyPolicy);
router.get('/stringers', controller.stringers);
router.get('/trackpants', controller.trackpants);
router.get('/collection/:id', controller.collections);
router.get('/t-shirts', controller.tshirts);
router.get('/outlet', controller.outlet);
router.get('/featured', controller.featured);
router.get('/tanks', controller.tanks);
router.get('/return', controller.returns);
router.get('/faqs', controller.faqs);
router.get('/contact-us', controller.contactUs);
router.get('/mycart', controller.myCart);
router.get('/login', middlewares.userNotExists, controller.login);
router.get('/orders', middlewares.userExists, controller.orders);
router.get('/order/:id', middlewares.userExists, controller.singleOrder);
router.post('/updateUser', controller.updateUser);
router.get('/products/:id/quicklook/:uniqueid', controller.quicklookProduct);
router.post('/query', controller.querySent);
router.get('/password/forget', controller.forgotPassword);
router.post('/password/forget', controller.forgetPasswordTokenCreation);
router.get('/couponshares/:id/:name', controller.couponShare);
router.get('/login/sign-up', controller.signUp);
router.post('/select-region', controller.selectRegion);
router.get('/password/reset/:token', controller.passwordResetTokenCheck);
router.post('/password/reset', controller.passwordResetCreateNew);
router.post('/updatePass', middlewares.userExists, controller.updatePassword);
router.get(
    '/pay-with-paytm/:id',
    middlewares.userExists,
    controller.payWithPaytm
);
router.get(
    '/checkout',
    middlewares.cartCheck,
    middlewares.userExists,
    controller.checkout
);
router.get(
    '/account-details',
    middlewares.userExists,
    controller.accountDetails
);
router.get(
    '/account-details/wishlist',
    middlewares.userExists,
    controller.wishlist
);
router.get(
    '/account-details/change-password',
    middlewares.userExists,
    controller.changePassword
);
router.get(
    '/activate/:token',
    middlewares.alreadyActive,
    controller.accountActivate
);

router.get('/login/:url', middlewares.userNotExists, function(req, res) {
    req.session.returnTo = req.params.url;
    res.render('login', { gtitle: 'SHOP', csrfToken: req.csrfToken() });
});

module.exports = router;
