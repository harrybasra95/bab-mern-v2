var express = require('express'),
    router = express.Router({ mergeParams: true }),
    product = require('../models/product');

router.get('/products/:id', (req, res) => {
    if (req.params.id == 'SJGnseOhXt') {
        product.find({}, (err, foundProducts) => {
            res.send(foundProducts);
        });
    } else {
        res.send('Authentication Failed');
    }
});

router.post('/product/id/:id', (req, res) => {
    if (req.body.cart && req.body.cart.length > 0) {
        var foundProducts = [];
        var x = false;
        req.body.cart.forEach(item => {
            product.findById(item, (err, foundProduct) => {
                if (err || !foundProduct) {
                    x = true;
                } else {
                    foundProducts.push(foundProduct);
                }
            });
        });
        if (x == false && foundProducts.length > 0) {
            res.send({ success: true, cart: foundProducts });
        } else {
            res.send({ success: false });
        }
    }
});

router.get('/user/id/:id', (req, res) => {
    user.findById(req.params.id)
        .populate('orders')
        .populate('wishlist')
        .populate('address')
        .exec((err, foundUser) => {
            if (err || !foundUser) {
                res.send({ success: false });
            } else {
                res.send({ success: true, user: foundUser });
            }
        });
});

router.get('/category/:id/:cate', (req, res) => {
    if (req.params.id == 'SJGnseOhXt') {
        product.find({ type: req.params.cate }, (err, foundProducts) => {
            res.send(foundProducts);
        });
    } else {
        res.send('Authentication Failed');
    }
});

router.get('/collection/:id/:cate', (req, res) => {
    if (req.params.id == 'SJGnseOhXt') {
        product.find({ series: req.params.cate }, (err, foundProducts) => {
            res.send(foundProducts);
        });
    } else {
        res.send('Authentication Failed');
    }
});

router.post('/password/forget', function(req, res) {
    var x = validator.isEmail(req.body.email);
    // console.log(req.body);
    if (req.body.email == null || req.body.email == '' || x == false) {
        res.send({ success: false, message: 'Please provide a valid Email' });
    } else {
        user.findOne({ email: req.body.email }, function(err, foundUser) {
            if (err) {
                res.send({ success: false, message: err });
            } else {
                if (!foundUser) {
                    res.send({
                        success: false,
                        message: 'Email id is not been registered'
                    });
                } else {
                    if (foundUser.active == false) {
                        res.send({
                            success: false,
                            message:
                                'Your account has not been activated.Please activate it'
                        });
                    } else {
                        foundUser.resettoken = mailgun.generate_token(
                            foundUser
                        );
                        foundUser.save(function(err, savedUser) {
                            if (err) throw err;
                            else {
                                mailgun.forgot_password(savedUser);
                                res.send({
                                    success: true,
                                    message:
                                        'An mail has been sent to your Email'
                                });
                            }
                        });
                    }
                }
            }
        });
    }
});

router.get('/login/:val', (req, res) => {
    user.findOne({ username: req.body.email }, (err, foundUser) => {
        if (err) {
            res.send({ success: false, message: 'Something went wrong.' });
        } else if (foundUser) {
            res.send({
                success: true,
                message: 'User exists',
                user: foundUser
            });
        } else if (!foundUser) {
            var newUser = new user({
                token: req.body.token,
                fname: req.body.first_name,
                lname: req.body.last_name,
                email: req.body.email,
                username: req.body.email,
                gender: req.body.gender,
                active: true
            });
            if (req.params.val == 'google') {
                newUser.google_id = req.body.id;
            } else if (req.params.val == 'fb') {
                newUser.fb_id = req.body.id;
            }
            newUser.save(function(err, user) {
                if (err) throw err;
                res.send({
                    success: true,
                    message: 'User created',
                    user: user
                });
            });
        }
    });
});

router.post('/sign_up', function(req, res) {
    // var parts2 = req.body.user.date+"/"+req.body.user.month+"/"+req.body.user.year;
    var parts = req.body.user.date.split('/');
    var mydate = new Date(parts[2], parts[0] - 1, parts[1], 0, 0, 0);

    var x = validator.isAlpha(req.body.user.fname);
    var y = validator.isAlpha(req.body.user.lname);
    var a = validator.isLength(req.body.user.fname, { min: 3, max: 20 });
    var b = validator.isLength(req.body.user.lname, { min: 3, max: 20 });
    req.body.username = req.body.username.toLowerCase();

    if (x == false || y == false || a == false || b == false) {
        res.send({
            success: false,
            message: 'Please enter a valid name between 3 to 20 char'
        });
    } else if (validator.isEmail(req.body.username) == false) {
        res.send({ success: false, message: 'Please enter a valid Email' });
    } else if (
        validator.isNumeric(req.body.user.phone) == false ||
        req.body.user.phone.length != 10 ||
        req.body.user.phone.length == null
    ) {
        res.send({ success: false, message: 'Please enter a valid Phone' });
    } else if (req.body.password != req.body.cpassword) {
        res.send({
            success: false,
            message: "Password and confirm password don't match"
        });
    } else {
        var newUser = new user({
            username: req.body.username,
            fname: req.body.user.fname,
            lname: req.body.user.lname,
            phone: req.body.user.phone,
            email: req.body.username,
            dob: mydate
        });
        newUser.temporarytoken = mailgun.generate_token(newUser);
        newUser.cpassword = newUser.generateHash(req.body.password);
        user.register(newUser, req.body.password, function(err, foundUser) {
            if (err) {
                console.log(err);
                res.send({ success: false, message: 'Email already taken' });
            } else
                passport.authenticate('local')(req, res, function() {
                    res.send({
                        success: true,
                        message: 'Signed up successfully',
                        url: '/apparel'
                    });
                    mailgun.confirm_email(foundUser);
                });
        });
    }
});

router.post('/signup_checkout', function(req, res) {
    req.body.username = req.body.username.toLowerCase();
    if (validator.isEmail(req.body.username) == false) {
        res.send({ success: false, message: 'Please enter a valid Email' });
    } else if (req.body.password != req.body.cpassword) {
        res.send({
            success: false,
            message: "Password and confirm password don't match"
        });
    } else {
        var newUser = new user({
            username: req.body.username,
            email: req.body.username
        });
        newUser.temporarytoken = mailgun.generate_token(newUser);
        newUser.cpassword = newUser.generateHash(req.body.password);
        user.register(newUser, req.body.password, function(err, foundUser) {
            if (err) {
                console.log(err);
                res.send({ success: false, message: 'Email already taken' });
            } else
                passport.authenticate('local')(req, res, function() {
                    res.send({
                        success: true,
                        message: 'Signed up successfully',
                        url: '/checkout'
                    });
                    foundUser.fname = 'User';
                    mailgun.confirm_email(foundUser);
                });
        });
    }
});

router.post('/login', function(req, res) {
    if (validator.isEmail(req.body.username) == false) {
        res.send({ success: false, message: 'Please enter a valid email' });
    } else {
        user.findOne({ username: req.body.username }, (err, foundUser) => {
            if (err) {
                res.send({
                    success: false,
                    message: 'Something went wrong. Please Try again'
                });
            } else if (!foundUser) {
                res.send({ success: false, message: "Email don't exists" });
            } else {
                res.send({
                    success: true,
                    message: 'Login successfully.',
                    user: foundUser
                });
            }
        });
    }
});

router.get('/categories', (req, res) => {
    var cate = [
        { name: 'stringers', displayname: 'Stringers' },
        { name: 'tanks', displayname: 'tanks' },
        { name: 't-shirts', displayname: 't-shirts' },
        { name: 'track pants', displayname: 'track-pants' }
    ];
    res.send(cate);
});
router.get('/collections', (req, res) => {
    var cate = [
        { name: 'elite series', displayname: 'elite series' },
        { name: 'perform series', displayname: 'perform series' },
        { name: 'contour series', displayname: 'contour series' },
        { name: 'signature series', displayname: 'signature series' },
        { name: 'fit series', displayname: 'fit series' }
    ];
    res.send(cate);
});

router.post('/query', function(req, res) {
    var x = validator.isAlpha(req.body.fname);
    var y = validator.isAlpha(req.body.lname);
    if (
        req.body.fname.length == 0 ||
        req.body.lname.length == 0 ||
        x == false ||
        y == false
    ) {
        res.send({ success: false, message: 'Please enter a valid Name' });
    } else if (validator.isEmail(req.body.eadd) == false) {
        res.send({ success: false, message: 'Please enter a valid Email' });
    } else if (req.body.message.length < 10) {
        res.send({ success: false, message: 'Please enter a valid Query' });
    } else {
        query.count(function(err, totalNo) {
            req.body.qnumber = totalNo;
            query.create(req.body, function(err, savedQuery) {
                if (err) {
                    throw err;
                } else {
                    mailgun.query_recieved(savedQuery);
                    res.send({
                        success: true,
                        message: 'Your query has been submitted'
                    });
                }
            });
        });
    }
});

module.exports = router;
