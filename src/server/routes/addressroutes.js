const express = require('express');
const controller = require('../controllers/addressController');
const middlewares = require('../middlewares');

const router = express.Router({ mergeParams: true });

router.get(
    '/address/check',
    middlewares.userExists,
    controller.maxAddressCheck
);
router.get('/address/delete/:id', middlewares.userExists, controller.delete);
router.post('/address/edit', middlewares.userExists, controller.edit);
router.get('/address/get', middlewares.userExists, controller.get);
router.post('/address', middlewares.userExists, controller.create);
module.exports = router;
