const express = require('express');
const { authApiController } = require('../controllers');

const router = express.Router({ mergeParams: true });

router.post('/login', authApiController.login);
router.post('/create-user', authApiController.createUser);
router.post('/login/facebook', authApiController.loginViaFacebook);
router.post('/login/google', authApiController.loginViaGoogle);
router.get('/logout', authApiController.logout);

module.exports = router;
