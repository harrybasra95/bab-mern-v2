const express = require('express');
const { reactSessionController } = require('../controllers');

const router = express.Router({ mergeParams: true });

router.post('/', reactSessionController.create);
router.get('/:token', reactSessionController.get);
router.post('/select-country', reactSessionController.selectCountry);

module.exports = router;
