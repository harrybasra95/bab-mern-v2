const express = require('express');
const { pageApiController } = require('../controllers');

const router = express.Router({ mergeParams: true });

router.get('/apparel', pageApiController.apparel);
router.get('/products', pageApiController.products);
router.get('/products/all', pageApiController.allProducts);
router.get('/products/all-sizes', pageApiController.allSizeProducts);
router.get('/products/product-data', pageApiController.productData);
router.get('/products/check', pageApiController.checkProducts);

router.post('/coupon/apply', pageApiController.applyCoupon);
router.get('/coupon/remove', pageApiController.removeCoupon);
router.get('/coupon/orders/:id', pageApiController.fetchCouponOrders);

module.exports = router;
