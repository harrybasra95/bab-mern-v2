const express = require('express');
const { orderController } = require('../controllers');

const router = express.Router({ mergeParams: true });

router.post('/create', orderController.create);
router.post('/ccavResponseHandler', orderController.makeOrderCc);
router.get('/all', orderController.fetchAllOrders);
router.get('/single-order/:orderNumber', orderController.fetchSingleOrder);
router.put('/last-order-data', orderController.updateLastOrderData);
router.get('/pincode-check', orderController.pincodeCheck);

module.exports = router;
