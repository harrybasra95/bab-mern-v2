const express = require('express');
const passport = require('passport');

const router = express.Router({ mergeParams: true });

router.get(
    '/auth/facebook',
    passport.authenticate('facebook', { scope: ['email'] })
);

router.get(
    '/facebook/auth',
    passport.authenticate('facebook', {
        successRedirect: '/redirect/callback',
        failureRedirect: '/login'
    })
);

router.get(
    '/auth/google',
    passport.authenticate('google', { scope: ['profile', 'email'] })
);

router.get(
    '/google/auth',
    passport.authenticate('google', {
        successRedirect: '/redirect/callback',
        failureRedirect: '/login'
    })
);

module.exports = router;
