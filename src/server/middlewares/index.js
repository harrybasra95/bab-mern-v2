const { Cart } = require('../models');

const defaultError = 'Something went wrong.Please try again.';

module.exports = {
    userExists: (req, res, next) => {
        if (req.isAuthenticated()) {
            return next();
        }
        if (req.xhr) {
            return res.send({ success: false, defaultError });
        }
        req.flash('error', 'Please login first!');
        res.redirect('/login');
    },
    userNotExists: (req, res, next) => {
        if (!req.isAuthenticated()) {
            return next();
        }
        if (req.xhr) {
            return res.send({ success: false, defaultError });
        }
        req.flash('error', 'Already logged In');
        res.redirect('/');
    },
    cartCheck: (req, res, next) => {
        const cart = new Cart(req.session.cart ? req.session.cart : {});
        if (cart.totalQty === 0) {
            req.flash('error', 'Please add some products.');
            res.redirect('/');
        } else {
            next();
        }
    },
    alreadyActive: (req, res, next) => {
        if (req.user && req.user.active == true) {
            res.redirect('/');
            req.flash('error', 'Account has been already activated!');
        } else {
            return next();
        }
    }
};
