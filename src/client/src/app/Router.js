import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';
import { connect } from 'react-redux';
import { sessionActions, userActions } from './actions';
import { AuthRequiredComponent, LoaderComponent } from './components';
import { sessionServices, tokenServices } from './services';
import './neutral.styl';
const { getToken, setToken, setInterceptor } = tokenServices;

const CouponSharePage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "CouponSharePage" */ './pages/CouponSharePage'
          ),
     loading: LoaderComponent
});

const Homepage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "Homepage" */ './pages/Homepage'
          ),
     loading: LoaderComponent
});

const LoginPage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "LoginPage" */ './pages/LoginPage'
          ),
     loading: LoaderComponent
});
const ProductGalleryPage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "ProductGalleryPage" */ './pages/ProductGalleryPage'
          ),
     loading: LoaderComponent
});

const QuicklookPage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "QuicklookPage" */ './pages/QuicklookPage'
          ),
     loading: LoaderComponent
});

const MyCartPage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "MyCartPage" */ './pages/MyCartPage'
          ),
     loading: LoaderComponent
});

const CheckoutPage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "CheckoutPage" */ './pages/CheckoutPage'
          ),
     loading: LoaderComponent
});

const OrderDetailsPage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "OrderDetailsPage" */ './pages/OrderDetailsPage'
          ),
     loading: LoaderComponent
});

const MyOrdersPage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "MyOrdersPage" */ './pages/MyOrdersPage'
          ),
     loading: LoaderComponent
});

const SignUpPage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "SignUpPage" */ './pages/SignUpPage'
          ),
     loading: LoaderComponent
});

const PayViaPaytmPage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "PayViaPaytmPage" */ './pages/PayViaPaytmPage'
          ),
     loading: LoaderComponent
});

const ContactUsPage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "ContactUsPage" */ './pages/ContactUsPage'
          ),
     loading: LoaderComponent
});

const ReturnPolicyPage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "ReturnPolicyPage" */ './pages/ReturnPolicyPage'
          ),
     loading: LoaderComponent
});

const TermsAndConditionsPage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "TermsAndConditionsPage" */ './pages/TermsAndConditionsPage'
          ),
     loading: LoaderComponent
});

const PrivacyPolicyPage = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "PrivacyPolicyPage" */ './pages/PrivacyPolicyPage'
          ),
     loading: LoaderComponent
});

class Router extends Component {
     constructor(props) {
          super(props);

          this.state = { loading: true };
     }

     async componentDidMount() {
          const {
               setSessionData,
               loadingState,
               setIsMobile,
               setDeviceHeight,
               webpEnabled
          } = this.props;
          Modernizr.on('webp', function(result) {
               if (result) {
                    webpEnabled(true);
               } else {
                    webpEnabled(false);
               }
          });
          setIsMobile(window.innerWidth <= 768);
          setDeviceHeight(window.innerHeight);
          const token = getToken();
          if (!token) {
               const {
                    isSuccess,
                    data
               } = await sessionServices.createSession();
               if (isSuccess) {
                    setToken(data.id);
                    setInterceptor(data.id, data.selectedCountry);
                    setSessionData(data);
               }
               loadingState(false);
               this.setState({ loading: false });
               return null;
          }
          const { isSuccess, data } = await sessionServices.getSession(token);
          if (isSuccess) {
               setSessionData(data);
               setToken(data.id);
               setInterceptor(data.id, data.selectedCountry);
          }
          loadingState(false);
          this.setState({ loading: false });
     }

     render() {
          const { loading } = this.state;
          if (!loading) {
               return (
                    <Switch>
                         <Route exact path="/" component={Homepage} />
                         <Route
                              exact
                              path="/couponshares/:couponId/:name"
                              component={CouponSharePage}
                         />
                         <Route
                              exact
                              path="/privacy-policy"
                              component={PrivacyPolicyPage}
                         />
                         <Route
                              exact
                              path="/terms-and-conditions"
                              component={TermsAndConditionsPage}
                         />
                         <Route
                              exact
                              path="/contact-us"
                              component={ContactUsPage}
                         />
                         <Route
                              exact
                              path="/return"
                              component={ReturnPolicyPage}
                         />
                         <Route
                              exact
                              path="/login"
                              render={() => (
                                   <AuthRequiredComponent
                                        authType="logOutReq"
                                        orRender={<LoginPage />}
                                   />
                              )}
                         />
                         <Route
                              exact
                              path="/create-account"
                              render={() => (
                                   <AuthRequiredComponent
                                        authType="logOutReq"
                                        orRender={<SignUpPage />}
                                   />
                              )}
                         />
                         <Route
                              exact
                              path="/products/all"
                              render={props => (
                                   <ProductGalleryPage {...props} />
                              )}
                         />
                         <Route
                              exact
                              path="/products/:type/:name"
                              render={props => (
                                   <ProductGalleryPage
                                        key={props.match.params.name}
                                        {...props}
                                   />
                              )}
                         />
                         <Route
                              exact
                              path="/products/:type/quicklook/:id"
                              render={props => (
                                   <QuicklookPage
                                        key={props.match.params.id}
                                        {...props}
                                   />
                              )}
                         />
                         <Route exact path="/mycart" component={MyCartPage} />
                         <Route
                              exact
                              path="/checkout/:section"
                              render={props => (
                                   <AuthRequiredComponent
                                        authType="checkout"
                                        orRender={
                                             <CheckoutPage
                                                  key={
                                                       props.match.params
                                                            .section
                                                  }
                                                  {...props}
                                             />
                                        }
                                   />
                              )}
                         />
                         <Route
                              exact
                              path="/orders/:orderNumber/pay-via-paytm"
                              render={props => (
                                   <AuthRequiredComponent
                                        authType="logInReq"
                                        orRender={
                                             <PayViaPaytmPage {...props} />
                                        }
                                   />
                              )}
                         />
                         <Route
                              exact
                              path="/account/orders/:orderNumber"
                              render={props => (
                                   <AuthRequiredComponent
                                        authType="logInReq"
                                        orRender={
                                             <OrderDetailsPage {...props} />
                                        }
                                   />
                              )}
                         />
                         <Route
                              exact
                              path="/account/orders"
                              render={() => (
                                   <AuthRequiredComponent
                                        authType="logInReq"
                                        orRender={<MyOrdersPage />}
                                   />
                              )}
                         />
                    </Switch>
               );
          } else {
               return <LoaderComponent />;
          }
     }
}

const mapStateToProps = state => {
     const { loading } = state.userReducer;
     return { loading };
};

export default connect(
     mapStateToProps,
     { ...sessionActions, ...userActions }
)(Router);
