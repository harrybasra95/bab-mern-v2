import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { headerSidebarComponentData } from '../../staticData';
import cn from 'classnames';
import './style.styl';
import { connect } from 'react-redux';
import { userActions, sessionActions } from '../../actions';
import { userServices } from '../../services';

const { sideHeaderList } = headerSidebarComponentData;

class HeaderSidebar extends Component {
     constructor(props) {
          super(props);
          this.state = {
               classChanges: {}
          };
     }

     changeClassType(i) {
          const { classChanges } = this.state;
          const newClassChanges = classChanges;
          newClassChanges[i] = !newClassChanges[i];
          this.setState({ classChanges: { ...newClassChanges } });
     }

     async logOut() {
          const {
               loadingState,
               showMessage,
               setSessionData,
               setBlackOverflow
          } = this.props;
          loadingState(true);
          const data = await userServices.logoutUser();
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          setBlackOverflow(false);
          setSessionData(data.data);
     }

     renderList() {
          const { isLoggedIn } = this.props;
          const { classChanges } = this.state;
          const returnArray = [];
          sideHeaderList.forEach((item, i) => {
               if (item.subLabels) {
                    if (item.logInReq && !isLoggedIn) {
                         return;
                    }
                    if (item.logOutReq && isLoggedIn) {
                         return;
                    }
                    returnArray.push(
                         <div
                              className={cn(item.className, {
                                   [item.activeClass]: classChanges[i]
                              })}
                         >
                              <Link onClick={() => this.changeClassType(i)}>
                                   <span>
                                        {item.label}
                                        <i
                                             className={cn(
                                                  'fa',
                                                  {
                                                       'fa-plus': !classChanges[
                                                            i
                                                       ]
                                                  },
                                                  {
                                                       'fa-minus':
                                                            classChanges[i]
                                                  }
                                             )}
                                             aria-hidden="true"
                                        />
                                   </span>
                              </Link>

                              {item.subLabels.map(subItem => (
                                   <Link
                                        key={subItem.label}
                                        className={cn('submenu', {
                                             forward: classChanges[i]
                                        })}
                                        to={subItem.link}
                                   >
                                        <span>{subItem.label}</span>
                                   </Link>
                              ))}
                         </div>
                    );
               } else {
                    if (item.logInReq && !isLoggedIn) {
                         return;
                    }
                    if (item.logOutReq && isLoggedIn) {
                         return;
                    }
                    if (item.link === '/logout') {
                         returnArray.push(
                              <a onClick={() => this.logOut()}>
                                   <span>{item.label}</span>
                              </a>
                         );
                    } else {
                         returnArray.push(
                              <Link to={item.link}>
                                   <span>{item.label}</span>
                              </Link>
                         );
                    }
               }
          });
          return returnArray;
     }

     render() {
          const { isHeaderShowing } = this.props;
          return (
               <div
                    className={cn('header_sidebar', {
                         header_sidebar_open: isHeaderShowing
                    })}
               >
                    {this.renderList()}
               </div>
          );
     }
}

const mapStateToProps = state => {
     const { isHeaderShowing, isLoggedIn } = state.userReducer;

     return { isHeaderShowing, isLoggedIn };
};
export default connect(
     mapStateToProps,
     { ...userActions, ...sessionActions }
)(HeaderSidebar);
