import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './style.styl';
import { cartActions } from '../../actions';

class Footer extends Component {
     constructor(props) {
          super(props);
          this.state = { headerOpen: false };
     }

     openHeader() {
          const { headerOpen } = this.state;
          this.setState({ headerOpen: !headerOpen });
     }

     render() {
          return (
               <div>
                    <div className="footer hidden-xs">
                         <div className="container">
                              <div className="row">
                                   <div className="col-md-3 col-sm-3 inner-footer">
                                        <Link to="/">
                                             <img
                                                  className="footer-logo"
                                                  src="/img/logo_white.png"
                                             />
                                        </Link>
                                        <Link to="/">
                                             <img
                                                  className="footer-logo footer-logo-bottom"
                                                  src="/img/bottom_logo_white.svg"
                                             />
                                        </Link>
                                   </div>
                                   <div className="col-md-3 col-sm-3 inner-footer">
                                        <ul>
                                             <h3>Links</h3>
                                             <div className="line" />
                                             <li>&nbsp;</li>
                                             <li>
                                                  <span
                                                       className="glyphicon glyphicon-menu-right"
                                                       aria-hidden="true"
                                                  />
                                                  &nbsp;<a href="/">Home</a>
                                             </li>
                                             <li>
                                                  <span
                                                       className="glyphicon glyphicon-menu-right"
                                                       aria-hidden="true"
                                                  />
                                                  &nbsp;
                                                  <a href="/products">Shop</a>
                                             </li>
                                             <li>
                                                  <span
                                                       className="glyphicon glyphicon-menu-right"
                                                       aria-hidden="true"
                                                  />
                                                  &nbsp;
                                                  <a href="/contact_us">
                                                       Contact Us
                                                  </a>
                                             </li>
                                             <li>
                                                  <span
                                                       className="glyphicon glyphicon-menu-right"
                                                       aria-hidden="true"
                                                  />
                                                  &nbsp;
                                                  <a
                                                       href="#"
                                                       className="change-country"
                                                  >
                                                       Change Country
                                                  </a>
                                             </li>
                                        </ul>
                                   </div>
                                   <div className="col-md-3 col-sm-3 inner-footer">
                                        <ul>
                                             <h3>Policies</h3>
                                             <div className="line" />
                                             <li>&nbsp;</li>
                                             <li>
                                                  <span
                                                       className="glyphicon glyphicon-menu-right"
                                                       aria-hidden="true"
                                                  />
                                                  &nbsp;
                                                  <a href="/privacy-policy">
                                                       Privacy policy
                                                  </a>
                                             </li>
                                             <li>
                                                  <span
                                                       className="glyphicon glyphicon-menu-right"
                                                       aria-hidden="true"
                                                  />
                                                  &nbsp;
                                                  <a href="/return">
                                                       Return and Refund
                                                  </a>
                                             </li>
                                        </ul>
                                   </div>
                                   <div className="col-md-3 col-sm-3 inner-footer">
                                        <ul>
                                             <h3>Follow us</h3>
                                             <div className="line" />
                                             <li>&nbsp;</li>
                                             <li>
                                                  <a href="https://www.facebook.com/theofficialbab/">
                                                       <i className="fa fa-facebook" />
                                                       &nbsp;facebook
                                                  </a>
                                             </li>
                                             <li>
                                                  <a href="https://twitter.com/Bab_store">
                                                       <i className="fa fa-tumblr" />
                                                       &nbsp;twitter
                                                  </a>
                                             </li>
                                             <li>
                                                  <a href="https://www.instagram.com/theofficialbab/">
                                                       <i className="fa fa-instagram" />
                                                       &nbsp;instagram
                                                  </a>
                                             </li>
                                        </ul>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div className="footer hidden-sm hidden-md hidden-lg">
                         <div>
                              <a href="/">
                                   <img
                                        className="footer-logo"
                                        src="/img/logo_white.png"
                                   />
                              </a>
                              <a href="/">
                                   <img
                                        className="footer-logo"
                                        src="/img/bottom_logo_white.svg"
                                   />
                              </a>
                         </div>
                         <div>
                              <a href="/faqs">
                                   <p>FAQ&apos;s</p>
                              </a>
                              <a href="/contact_us">
                                   <p>CONTACT US</p>
                              </a>
                              <a href="/return">
                                   <p>RETURNS</p>
                              </a>
                         </div>
                    </div>

                    <div className="end-footer">
                         <div>
                              <a href="https://www.instagram.com/theofficialbab/">
                                   <img
                                        alt="bab being beast apparel instagram "
                                        title="BAB Being Beast Bpparel Instagram"
                                        src="/img/insta.gif"
                                   />
                              </a>
                              <a href="https://twitter.com/Bab_store">
                                   <img
                                        alt="bab being beast apparel twitter"
                                        title="BAB Being Beast Apparel Twitter"
                                        src="/img/tw.png"
                                   />
                              </a>
                              <a href="https://www.facebook.com/theofficialbab/">
                                   <img
                                        alt="bab being beast apparel facebook"
                                        title="BAB Being Beast Apparel Facebook"
                                        src="/img/fb.png"
                                   />
                              </a>
                         </div>
                         <p className="hidden-xs">
                              <h5 className="hidden-xs">
                                   © 2017 BAB Store | All Rights Reserved.
                                   <a href="/terms-and-conditions">
                                        Terms & Conditions
                                   </a>{' '}
                                   &nbsp;|&nbsp;
                                   <a href="/privacy-policy"> Privacy Policy</a>
                              </h5>
                         </p>
                         <p className="hidden-sm hidden-md hidden-lg">
                              <h5 className="hidden-sm hidden-md hidden-lg">
                                   © 2017 BAB Store | All Rights Reserved.
                              </h5>
                              <h5 className="hidden-sm hidden-md hidden-lg">
                                   {' '}
                                   <a href="/terms-and-conditions">
                                        Terms & Conditions
                                   </a>{' '}
                                   &nbsp;|&nbsp;
                                   <a href="/privacy-policy"> Privacy Policy</a>
                              </h5>
                         </p>
                    </div>
               </div>
          );
     }
}

const mapStateToProps = state => {
     const { cart } = state.userReducer;
     return { cart };
};

export default connect(
     mapStateToProps,
     cartActions
)(Footer);
