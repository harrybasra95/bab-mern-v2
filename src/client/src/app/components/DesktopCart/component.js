import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames';
import { connect } from 'react-redux';
import { symbolchart } from '../../config';
import { cartServices } from '../../services';
import { userActions, cartActions, sessionActions } from '../../actions';
import './style.styl';
class DesktopCart extends Component {
     constructor(props) {
          super(props);

          this.state = {
               windowHeight: window.innerHeight
          };
     }

     async removeFromCart(id) {
          const {
               loadingState,
               setSessionData,
               showMessage,
               showCart
          } = this.props;
          loadingState(true);
          const data = await cartServices.removeFromCartByUniqueId(id);
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          setSessionData(data.data);
          return showCart(true);
     }

     renderProducts() {
          const { cart, selectedCountry } = this.props;
          const returnData = [];
          if (cart.products) {
               Object.keys(cart.products).forEach(key => {
                    const {
                         product,
                         productTotalPrice,
                         productTotalQuantity
                    } = cart.products[key];
                    returnData.push(
                         <div className="sidecart-content">
                              <h4>
                                   <span
                                        onClick={() =>
                                             this.removeFromCart(product._id)
                                        }
                                        className="remove-product glyphicon								glyphicon-remove"
                                   />
                              </h4>
                              <Link
                                   to={`/products/${product.type}/quicklook/${
                                        product.uniqueid
                                   }`}
                              >
                                   <img src={product.imgaddresses[0]} />
                              </Link>
                              <Link
                                   to={`/products/${product.type}/quicklook/${
                                        product.uniqueid
                                   }`}
                              >
                                   <h4 style={{ textTransform: 'capitalize' }}>
                                        {`${product.name} ${product.colorname}`}
                                   </h4>
                              </Link>
                              <h5>Quantity : {productTotalQuantity}</h5>
                              <h5>Size : {product.size}</h5>
                              <h5>
                                   {symbolchart[selectedCountry]}{' '}
                                   {productTotalPrice}
                              </h5>
                         </div>
                    );
               });
          }
          return returnData;
     }

     render() {
          const { windowHeight } = this.state;
          const { cart, selectedCountry, isCartShowing } = this.props;
          return (
               <div
                    className={cn('sidecart', { sidecart_view: isCartShowing })}
               >
                    <div className="heading">
                         <Link to="/mycart">
                              <h3>My Cart</h3>
                         </Link>
                    </div>
                    <div
                         className="sidecart-content-box"
                         style={{ height: windowHeight - 150 }}
                    >
                         {this.renderProducts()}
                    </div>
                    <div className="sidecart-total">
                         <div className="heading">
                              <h3>Total</h3>
                              <h3>
                                   <span>
                                        {symbolchart[selectedCountry]}{' '}
                                        {cart.totalPrice}
                                   </span>
                              </h3>
                         </div>
                         <Link to="/mycart">
                              <div className="heading">
                                   <h3>Buy Now</h3>
                              </div>
                         </Link>
                    </div>
               </div>
          );
     }
}

const mapStateToProps = state => {
     const { cart, selectedCountry, isCartShowing } = state.userReducer;
     return { cart, selectedCountry, isCartShowing };
};

export default connect(
     mapStateToProps,
     { ...userActions, ...sessionActions, ...cartActions }
)(DesktopCart);
