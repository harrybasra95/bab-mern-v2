import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { headerComponentData } from '../../staticData';
import './style.styl';
import { FaBars } from 'react-icons/fa';
import { staticData } from '../../config';
import { HeaderSidebar, TopInfoBar } from '../../components';
import { cartActions, userActions } from '../../actions';
const { accountOptionList } = headerComponentData;

class Header extends Component {
     constructor(props) {
          super(props);
     }

     openHeader() {
          const { showHeader, isHeaderShowing } = this.props;
          showHeader(!isHeaderShowing);
     }

     renderAdminButton() {
          const { isLoggedIn, user } = this.props;
          if (isLoggedIn && user.role === 'admin') {
               return (
                    <li>
                         <a href="/admin">
                              <p>ADMIN</p>
                         </a>
                    </li>
               );
          }
     }

     renderLoginButton() {
          const { isLoggedIn } = this.props;
          if (isLoggedIn) {
               return (
                    <li className="dropdown" id="myAccountButton">
                         <a className="pointer">
                              <span>{accountOptionList.title}</span>
                         </a>
                         <ul className="accountList">
                              {accountOptionList.sublist.map(
                                   ({ title, url }) => (
                                        <li key={title}>
                                             <a href={url}>
                                                  <div />
                                                  {title}
                                             </a>
                                        </li>
                                   )
                              )}
                         </ul>
                    </li>
               );
          }
          return (
               <li id="indexLoginButton">
                    <Link to="/login">
                         <p>LOGIN</p>
                    </Link>
               </li>
          );
     }

     render() {
          const { cart, showCart } = this.props;
          return (
               <div>
                    <HeaderSidebar />
                    <div className="header">
                         <nav
                              id="navbar-default"
                              className="navbar navbar-default navbar-fixed-top topnav"
                              role="navigation"
                         >
                              <TopInfoBar />
                              <div id="topbar_right" className="topbar">
                                   <div className="helpbar container">
                                        <ul>
                                             <li>
                                                  Email us :{' '}
                                                  {staticData.supportEmail}
                                             </li>
                                             <li>
                                                  <Link to="/faqs">
                                                       Need help?
                                                  </Link>
                                             </li>
                                             <li>
                                                  <Link to="/contact-us">
                                                       Contact Us
                                                  </Link>
                                             </li>
                                        </ul>
                                   </div>
                              </div>
                              <div
                                   id="navbar_right"
                                   className="container topnav"
                              >
                                   <div className="row">
                                        <div
                                             onClick={this.openHeader.bind(
                                                  this
                                             )}
                                             className="toggle_button"
                                        >
                                             <FaBars className="toggle_button_icon" />
                                        </div>
                                        <div className="navbar-header">
                                             <Link
                                                  className="text-center navbar-brand topnav"
                                                  to="/"
                                             >
                                                  <img
                                                       alt="logo"
                                                       src="/img/logo.PNG"
                                                       className="logo col-lg-pull-0 col-md-pull-0 col-sm-1 col-sm-pull-3"
                                                  />
                                             </Link>
                                        </div>
                                        <div
                                             id="sidebar_cart"
                                             onClick={() => showCart(true)}
                                        >
                                             <span className="cart_number">
                                                  {cart.totalQuantity}
                                             </span>
                                        </div>
                                        <div
                                             className="collapse navbar-collapse"
                                             id="bs-example-navbar-collapse-1"
                                        >
                                             <ul className="nav-list nav navbar-nav navbar-right">
                                                  <li className="dropdown">
                                                       <a className="pointer">
                                                            <span>SHOP</span>
                                                       </a>
                                                       <ul>
                                                            <li>
                                                                 <Link to="/products/category/stringers">
                                                                      <div />
                                                                      Stringers
                                                                 </Link>
                                                            </li>
                                                            <li>
                                                                 <Link to="/products/category/t-shirts">
                                                                      <div />
                                                                      T-Shirts
                                                                 </Link>
                                                            </li>
                                                            <li>
                                                                 <Link to="/products/category/tanks">
                                                                      <div />
                                                                      Tanks
                                                                 </Link>
                                                            </li>
                                                            <li>
                                                                 <Link to="/products/category/trackpants">
                                                                      <div />
                                                                      Track
                                                                      Pants
                                                                 </Link>
                                                            </li>
                                                       </ul>
                                                  </li>
                                                  <div className="sublist">
                                                       <ul>
                                                            <li className="sublist-items">
                                                                 <Link to="/products/category/stringers">
                                                                      <p>
                                                                           Stringers
                                                                      </p>
                                                                 </Link>
                                                            </li>
                                                            <li className="sublist-items">
                                                                 <Link to="/products/category/t-shirts">
                                                                      T-Shirts
                                                                 </Link>
                                                            </li>
                                                            <li className="sublist-items">
                                                                 <Link to="/products/category/tanks">
                                                                      Tanks
                                                                 </Link>
                                                            </li>
                                                            <li className="sublist-items">
                                                                 <Link to="/products/category/trackpants">
                                                                      Track
                                                                      Pants
                                                                 </Link>
                                                            </li>
                                                       </ul>
                                                  </div>
                                                  <li>
                                                       <Link to="/products">
                                                            <p>FEATURED</p>
                                                       </Link>
                                                  </li>
                                                  {this.renderAdminButton()}
                                                  {this.renderLoginButton()}
                                                  <li>
                                                       <a
                                                            onClick={() =>
                                                                 showCart(true)
                                                            }
                                                            className="fetch_cart"
                                                       >
                                                            <div className="bag_icon" />
                                                            <span className="cart_number">
                                                                 {
                                                                      cart.totalQuantity
                                                                 }
                                                            </span>
                                                       </a>
                                                  </li>
                                             </ul>
                                        </div>
                                   </div>
                              </div>
                         </nav>
                         <div className="space" />
                    </div>
               </div>
          );
     }
}

const mapStateToProps = state => {
     const {
          cart,
          isLoggedIn,
          user,
          message,
          messageType,
          isHeaderShowing
     } = state.userReducer;
     return { cart, isLoggedIn, user, message, messageType, isHeaderShowing };
};

export default connect(
     mapStateToProps,
     { ...cartActions, ...userActions }
)(Header);
