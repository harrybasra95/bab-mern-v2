import React, { Component } from 'react';
import './style.styl';
import { userActions } from '../../actions';
import { selectCountryComponent } from '../../staticData';
import { sessionServices, tokenServices } from '../../services';
import { connect } from 'react-redux';

class SelectCountry extends Component {
     renderCountries() {
          const returnData = [];
          const { countriesList } = selectCountryComponent;
          countriesList.forEach(({ title, imgUrl, val }, i) => {
               if (i % 2 === 0) {
                    returnData.push(
                         <div key={val}>
                              <div
                                   onClick={() => this.selectCountry(val)}
                                   className="select-country"
                              >
                                   <img src={imgUrl} />
                                   <h3>{title}</h3>
                              </div>
                              <div
                                   onClick={() =>
                                        this.selectCountry(
                                             countriesList[i + 1].val
                                        )
                                   }
                                   className="select-country"
                              >
                                   <h3>{countriesList[i + 1].title}</h3>
                                   <img src={countriesList[i + 1].imgUrl} />
                              </div>
                         </div>
                    );
               }
          });
          return returnData;
     }

     async selectCountry(val) {
          const { selectCountry, loadingState, showMessage } = this.props;
          loadingState(true);
          const data = await sessionServices.selectCountry(val);
          loadingState(false);
          if (!data.isSuccess) {
               showMessage(data);
          }
          selectCountry(val);
          tokenServices.setInterceptor(null, val);
     }

     render() {
          return (
               <div className="region-select-outer">
                    <div className="region-select text-center">
                         <span
                              onClick={() => this.selectCountry('in')}
                              className="glyphicon glyphicon-remove"
                         />
                         <div>
                              <img
                                   className="worldmap"
                                   src="/img/worldmap.jpg"
                              />
                              <p>Choose your store</p>
                              <span className="inner-box">
                                   {this.renderCountries()}
                              </span>
                         </div>
                    </div>
               </div>
          );
     }
}

const mapStateToProps = state => state;

export default connect(
     mapStateToProps,
     userActions
)(SelectCountry);
