import React, { Component } from 'react';
import './style.styl';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import ReactBodymovin from 'react-bodymovin';
import animation from './loader.json';
import cn from 'classnames';
import { googleAnalyticsId } from '../../config';
import { connect } from 'react-redux';
import { allPageInfo } from '../../staticData';
import Loadable from 'react-loadable';
import LoaderComponent from '../LoaderComponent';
import { trackingServices } from '../../services';
import { userActions, cartActions } from '../../actions';
import ReactGA from 'react-ga';

let lastUrl = '';

const bodymovinOptions = {
     loop: true,
     autoplay: true,
     prerender: true,
     animationData: animation
};

const BottomInfoBar = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "BottomInfoBar" */ '../BottomInfoBar'
          ),
     loading: LoaderComponent
});

const Header = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "Header" */ '../Header'
          ),
     loading: LoaderComponent
});
const Footer = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "Footer" */ '../Footer'
          ),
     loading: LoaderComponent
});
const Subscribe = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "Subscribe" */ '../Subscribe'
          ),
     loading: LoaderComponent
});
const SelectCountry = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "SelectCountry" */ '../SelectCountry'
          ),
     loading: LoaderComponent
});
const DesktopCart = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "DesktopCart" */ '../DesktopCart'
          ),
     loading: LoaderComponent
});
const MobileCart = Loadable({
     loader: () =>
          import(
               /* webpackPrefetch: true,webpackChunkName: "MobileCart" */ '../MobileCart'
          ),
     loading: LoaderComponent
});

class ContainerComponent extends Component {
     constructor(props) {
          super(props);

          this.state = {
               lastLocation: ''
          };
     }

     componentDidMount() {
          const { trackId, location, ChangeBottomBarStatus } = this.props;
          const ref = location.search;
          if (ref && ref === '?ref=i') {
               ChangeBottomBarStatus(true);
          }
          trackingServices.startSession(trackId);
     }

     renderSelectCountry() {
          const { selectedCountry } = this.props;
          if (selectedCountry === null) {
               return <SelectCountry />;
          }
     }
     isLoading() {
          const { loading } = this.props;
          if (loading) {
               return (
                    <div className="container-loader">
                         <div className="container-loader-div">
                              <ReactBodymovin options={bodymovinOptions} />
                         </div>
                    </div>
               );
          }
          return null;
     }
     showMessage() {
          const { message, messageType, showMessage } = this.props;
          if (message) {
               setTimeout(() => {
                    showMessage({ message: null });
               }, 5000);
               return (
                    <div className="container alert_div from-up-animation ">
                         <div
                              className={cn('alert ', {
                                   'alert-danger': messageType === 'error',
                                   'alert-success': messageType === 'success'
                              })}
                         >
                              {message}
                         </div>
                    </div>
               );
          }
     }

     onRouteChanged() {
          const { setBlackOverflow } = this.props;
          window.scrollTo(0, 0);
          setBlackOverflow(false);
          if (process.env.TYPE === 'online') {
               ReactGA.initialize(googleAnalyticsId);
               ReactGA.pageview(
                    window.location.pathname + window.location.search
               );
          }
     }

     render() {
          const {
               children,
               pageTitle,
               blackOverflow,
               setBlackOverflow,
               subscribeHidden,
               location,
               checkoutPage,
               showBottomBar,
               history
          } = this.props;
          let pageInfo;
          if (pageTitle) {
               pageInfo = allPageInfo[pageTitle];
          }
          if (
               process.env.TYPE !== 'local' &&
               window.location.hostname === 'babclothing.com'
          ) {
               return history.push(
                    `https://in.babclothin.com${window.location.pathname}`
               );
          }
          if (lastUrl != location.pathname) {
               this.onRouteChanged();
               lastUrl = location.pathname;
          }
          return (
               <div
                    className={cn({
                         'container-overflow-hidden': blackOverflow,
                         'bottom-bar-padding': showBottomBar
                    })}
               >
                    {this.renderSelectCountry()}
                    {this.isLoading()}
                    {this.showMessage()}
                    {pageInfo ? (
                         <Helmet>
                              <title>{pageInfo().title}</title>
                              <meta
                                   name="description"
                                   content={pageInfo().desc}
                              />
                         </Helmet>
                    ) : null}
                    {!checkoutPage ? <DesktopCart /> : null}
                    {!checkoutPage ? <MobileCart /> : null}
                    {blackOverflow ? (
                         <div
                              className="black-overflow"
                              onClick={() => setBlackOverflow(false)}
                         />
                    ) : null}
                    {!checkoutPage ? <Header /> : null}
                    {children}
                    {!subscribeHidden ? <Subscribe /> : null}
                    {!checkoutPage ? <Footer /> : null}
                    {
                         <BottomInfoBar
                              showCouponText={!checkoutPage && showBottomBar}
                              showBar={!checkoutPage}
                         />
                    }
               </div>
          );
     }
}

ContainerComponent.propTypes = {
     pageTitle: PropTypes.string
};

const mapStateToProps = state => {
     const {
          message,
          messageType,
          loading,
          selectedCountry,
          blackOverflow,
          trackId,
          showBottomBar
     } = state.userReducer;
     return {
          message,
          messageType,
          loading,
          selectedCountry,
          blackOverflow,
          trackId,
          showBottomBar
     };
};

export default withRouter(
     connect(
          mapStateToProps,
          { ...userActions, ...cartActions }
     )(ContainerComponent)
);
