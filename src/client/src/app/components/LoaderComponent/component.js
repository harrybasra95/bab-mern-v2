import React, { Component } from 'react';
import './style.styl';
import ReactBodymovin from 'react-bodymovin';
import animation from './loader.json';
const bodymovinOptions = {
     loop: true,
     autoplay: true,
     prerender: true,
     animationData: animation
};

export default class LoaderComponent extends Component {
     render() {
          return (
               <div className="container-loader">
                    <div className="container-loader-div">
                         <ReactBodymovin options={bodymovinOptions} />
                    </div>
               </div>
          );
     }
}
