import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    TextField,
    Radio,
    FormControlLabel,
    FormHelperText,
    FormControl
} from '@material-ui/core';
import { colors } from '../../config';

const CssTextField = makeStyles(theme => ({
    redTheme: {
        '& label': {
            color: colors.redColor,
            fontSize: '16px'
        },
        '& label.Mui-focused': {
            color: colors.redColor
        },
        '& .MuiFormLabel-filled': {
            color: colors.redColor
        },
        '& .MuiInput-root': {
            '&:before': {
                borderBottomColor: 'white'
            },
            '& .MuiInput-input': {
                color: 'white',
                fontSize: '16px'
            }
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: 'white'
        },
        '& .MuiOutlinedInput-root': {
            '&.Mui-focused fieldset': {
                borderWidth: '1px'
            }
        }
    },
    normal: {}
}));

export default function Input({
    label,
    type,
    value,
    onChange,
    defaultValue,
    disabled,
    name,
    checked,
    inputType,
    error,
    errorMessage,
    ref,
    theme
}) {
    const classes = CssTextField();
    const inputVariant = inputType || 'outlined';
    if (type !== 'radio') {
        return (
            <FormControl style={{ width: '100%' }} error={error}>
                <TextField
                    classes={{
                        root:
                            theme === 'redTheme'
                                ? classes.redTheme
                                : classes.normal
                    }}
                    ref={ref}
                    error={error}
                    defaultValue={defaultValue}
                    label={label}
                    type={type}
                    value={value}
                    onChange={onChange}
                    margin="normal"
                    variant={inputVariant}
                    disabled={disabled}
                />
                {error ? (
                    <FormHelperText id="component-error-text">
                        {errorMessage}
                    </FormHelperText>
                ) : null}
            </FormControl>
        );
    } else {
        return (
            <FormControlLabel
                value={value}
                onChange={onChange}
                control={<Radio name={name} />}
                label={label}
                checked={checked}
            />
        );
    }
}
