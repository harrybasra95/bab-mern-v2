import React, { Component } from 'react';
import './style.styl';

const whatsapp = (
     <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://wa.me/919899823278"
     >
          {' '}
          <p>
               For Any help / inquiries{'     '}
               <picture>
                    <source
                         srcSet="/img/whatsapp-icon.webp"
                         type="image/webp"
                    />
                    <source srcSet="/img/whatsapp-icon.png" type="image/png" />
                    <img
                         title="whatsapp icon"
                         className="whatsapp-icon"
                         src="/img/whatsapp-icon.png"
                         alt="whatsapp icon"
                    />
               </picture>{' '}
               98998-23278
          </p>
     </a>
);

const bottomInfoBarData = [whatsapp, 'Use Coupon Code "my50" to get 50% off'];

export default class BottomInfoBar extends Component {
     constructor(props) {
          super(props);

          this.state = {
               currentPoint: 0,
               start: true
          };
     }

     componentDidMount() {
          setInterval(() => {
               this.startTimer();
          }, 5000);
     }

     startTimer() {
          const { showCouponText } = this.props;
          let { currentPoint } = this.state;
          if (currentPoint === bottomInfoBarData.length - 1) {
               currentPoint = 0;
          } else {
               currentPoint += 1;
          }
          if (showCouponText) {
               this.setState({
                    currentPoint,
                    start: false
               });
          } else {
               this.setState({
                    currentPoint: 0,
                    start: true
               });
          }
     }

     handleClick() {
          const { currentPoint } = this.state;
          if (currentPoint === 0) {
               var win = window.open('https://wa.me/919899823278', '_blank');
               win.focus();
          }
     }

     renderPoint() {
          const { currentPoint, start } = this.state;
          const pointsLength = bottomInfoBarData.length;
          return (
               <div onClick={() => this.handleClick()}>
                    <p key={currentPoint} className="message fadeInAnimation">
                         {bottomInfoBarData[currentPoint]}
                    </p>
                    {!start ? (
                         <p
                              key={
                                   bottomInfoBarData[
                                        currentPoint === 0
                                             ? pointsLength - 1
                                             : currentPoint - 1
                                   ]
                              }
                              className="message fadeOutAnimation"
                         >
                              {
                                   bottomInfoBarData[
                                        currentPoint === 0
                                             ? pointsLength - 1
                                             : currentPoint - 1
                                   ]
                              }
                         </p>
                    ) : null}
               </div>
          );
     }

     render() {
          const { showBar } = this.props;
          if (!showBar) {
               return null;
          }
          return <div className="bottom-bar-text">{this.renderPoint()}</div>;
     }
}
