import React from 'react';
import cn from 'classnames';
import './style.styl';

function Button({
     onClick,
     label,
     backgroundColor,
     borderRadius,
     type,
     icon,
     selected,
     fontSize
}) {
     if (type && type === 'checkout') {
          return (
               <button
                    style={{ backgroundColor, borderRadius }}
                    onClick={onClick}
                    type="button"
                    className={cn('payment-option', { selected: selected })}
               >
                    <img src={`/img/payment-options/${icon}.png`} alt="" />
                    <p className="button-text">{label}</p>
                    {selected ? (
                         <img
                              className="checked-image"
                              src="/img/checked.png"
                         />
                    ) : (
                         <img
                              style={{ opacity: 0 }}
                              className="checked-image"
                              src="/img/checked.png"
                         />
                    )}
               </button>
          );
     }
     return (
          <button
               style={{ backgroundColor, borderRadius }}
               onClick={onClick}
               type="button"
          >
               <p className="button-text" style={{ fontSize }}>
                    {label}
               </p>
          </button>
     );
}

export default Button;
