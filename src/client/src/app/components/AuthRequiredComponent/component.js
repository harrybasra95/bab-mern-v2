import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../../actions';

export class AuthRequiredComponent extends Component {
     constructor(props) {
          super(props);
     }
     render() {
          const { authType, isLoggedIn, cart, showMessage } = this.props;
          if (authType === 'checkout') {
               if (!isLoggedIn) {
                    showMessage('Please log in to continue');
                    return <Redirect to="/login" />;
               }
               if (cart.totalQuantity === 0) {
                    showMessage('Please add some products in your cart');
                    return <Redirect to="/" />;
               }
          }
          if (authType === 'logInReq' && !isLoggedIn) {
               showMessage('Please log in to continue');
               return <Redirect to="/login" />;
          }
          if (authType === 'logOutReq' && isLoggedIn) {
               showMessage('Please log out to continue');
               return <Redirect to="/" />;
          }
          return this.props.orRender;
     }
}

const mapStateToProps = state => {
     const { isLoggedIn, cart, showMessage } = state.userReducer;
     return { isLoggedIn, cart, showMessage };
};
export default connect(
     mapStateToProps,
     userActions
)(AuthRequiredComponent);
