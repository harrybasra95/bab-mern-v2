import React, { Component } from 'react';
import cn from 'classnames';
import './style.styl';
import { connect } from 'react-redux';
import { userActions } from '../../actions';
import { userServices } from '../../services';
import { userValidations } from '../../validations';

class Subscribe extends Component {
     constructor(props) {
          super(props);
          this.state = {
               showText: false,
               email: '',
               phone: ''
          };
     }
     componentDidMount() {
          window.addEventListener('scroll', this.handleScroll.bind(this));
     }

     componentWillUnmount() {
          window.removeEventListener('scroll', this.handleScroll.bind(this));
     }
     handleScroll() {
          if (this.subscribeElement) {
               const top = this.subscribeElement.getBoundingClientRect().top;
               if (top < window.innerHeight) {
                    this.setState({ showText: true });
                    window.removeEventListener(
                         'scroll',
                         this.handleScroll.bind(this)
                    );
               }
          }
     }

     async sendRequest() {
          const { emailValidation, phoneValidation } = userValidations;
          const { loadingState, showMessage } = this.props;
          const { email, phone } = this.state;
          if (!emailValidation(email).isSuccess) {
               return showMessage(emailValidation(email).message);
          }
          if (!phoneValidation(phone).isSuccess) {
               return showMessage(phoneValidation(phone).message);
          }
          loadingState(true);
          const data = await userServices.subscribeUser({ email, phone });
          loadingState(false);
          console.log(data);
          if (!data.isSuccess) {
               showMessage(data);
          }
     }

     render() {
          const { showText, phone, email } = this.state;
          return (
               <div>
                    <div
                         ref={ref => (this.subscribeElement = ref)}
                         className="subscribe desktop_subscribe "
                    >
                         <div className="subscribe-inner">
                              <div>
                                   <div className="hero-heading ">
                                        <h1
                                             className={cn({
                                                  'animate-hero-text': showText
                                             })}
                                        >
                                             Subscribe For exciting offers
                                             <span className="moving-line" />
                                        </h1>
                                   </div>
                                   <h4>
                                        Subscribe to our Newsletter to get
                                        additional benefits. Don&apos;t Worry we
                                        don&apos;t like spam too.
                                   </h4>
                              </div>

                              <div className="subscribe-form">
                                   <div className="email-box">
                                        <input
                                             type="text"
                                             name="email"
                                             value={email}
                                             id="email"
                                             required
                                             onChange={({ target }) =>
                                                  this.setState({
                                                       email: target.value
                                                  })
                                             }
                                        />
                                        <label htmlFor="email">Email</label>
                                   </div>
                                   <div className="phone-box">
                                        <input
                                             required
                                             type="phone"
                                             name="phone"
                                             value={phone}
                                             id="phone"
                                             onChange={({ target }) =>
                                                  this.setState({
                                                       phone: target.value
                                                  })
                                             }
                                        />
                                        <label htmlFor="phone">phone</label>
                                   </div>
                                   <button onClick={() => this.sendRequest()}>
                                        Send my discount
                                   </button>
                                   <div className="subscribe-message">
                                        <p className="move-up error" />
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div className="thank-you">
                         <div className="thank-you-inner">
                              <div className="info-box">
                                   <h1>Thank You!!</h1>
                                   <h4>
                                        For Subscribing to our newsletter.Your
                                        coupon code has been sent to your email.
                                   </h4>
                                   <button>Use my coupon</button>
                                   <p>( Apply it to your current cart. )</p>
                                   <p>No, I want to pay more.</p>
                              </div>
                              <div className="share-box">
                                   <h3>Tell your friends</h3>
                                   <div>
                                        <a
                                             target="_blank"
                                             rel="noopener noreferrer"
                                             href="http://pinterest.com/pin/create/button/?url=https://in.babclothing.com&media=https://in.babclothing.com/img/bg1.jpg"
                                             className="pin-it-button"
                                             count-layout="horizontal"
                                        >
                                             <img
                                                  src="/img/pinterest.png"
                                                  title="Pin It"
                                             />
                                        </a>
                                        <a
                                             target="_blank"
                                             rel="noopener noreferrer"
                                             href="https://twitter.com/home?status=https://in.babclothing.com"
                                        >
                                             <img src="/img/tw.png" />
                                        </a>
                                        <a
                                             target="_blank"
                                             rel="noopener noreferrer"
                                             href="https://www.facebook.com/sharer/sharer.php?u=https://in.babclothing.com"
                                        >
                                             <img src="/img/fb.png" />
                                        </a>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          );
     }
}

const mapStateToProps = state => state;

export default connect(
     mapStateToProps,
     userActions
)(Subscribe);
