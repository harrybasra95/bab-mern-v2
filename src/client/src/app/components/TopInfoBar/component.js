import React, { Component } from 'react';
import { topInfoBarData } from '../../staticData';
import './style.styl';

export default class TopInfoBar extends Component {
     constructor(props) {
          super(props);

          this.state = {
               currentPoint: 0,
               start: true
          };
     }

     componentDidMount() {
          setInterval(() => {
               this.startTimer();
          }, 5000);
     }

     startTimer() {
          let { currentPoint } = this.state;
          if (currentPoint === topInfoBarData.offerPoints.length - 1) {
               currentPoint = 0;
          } else {
               currentPoint += 1;
          }
          this.setState({ currentPoint, start: false });
     }

     renderPoint() {
          const { currentPoint, start } = this.state;
          const pointsLength = topInfoBarData.offerPoints.length;
          return (
               <div>
                    <p key={currentPoint} className="message fadeInAnimation">
                         {topInfoBarData.offerPoints[currentPoint]}
                    </p>
                    {!start ? (
                         <p
                              key={
                                   topInfoBarData.offerPoints[
                                        currentPoint === 0
                                             ? pointsLength - 1
                                             : currentPoint - 1
                                   ]
                              }
                              className="message fadeOutAnimation"
                         >
                              {
                                   topInfoBarData.offerPoints[
                                        currentPoint === 0
                                             ? pointsLength - 1
                                             : currentPoint - 1
                                   ]
                              }
                         </p>
                    ) : null}
               </div>
          );
     }

     render() {
          return <div className="header-message-box">{this.renderPoint()}</div>;
     }
}
