import React, { Component } from 'react';
import './style.styl';
import { connect } from 'react-redux';
import {FaTimes} from 'react-icons/fa';
import { Link } from 'react-router-dom';
import cn from 'classnames';
import { userActions, cartActions, sessionActions } from '../../actions';
import { cartServices } from '../../services';
import { symbolchart } from '../../config';

class MobileCart extends Component {
     constructor(props) {
          super(props);

          this.state = {
               windowHeight: window.innerHeight
          };
     }

     async removeFromCart(id) {
          const {
               loadingState,
               setSessionData,
               showMessage,
               showCart
          } = this.props;
          loadingState(true);
          const data = await cartServices.removeFromCartByUniqueId(id);
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          setSessionData(data.data);
          return showCart(true);
     }
     renderCart() {
          const { cart, selectedCountry } = this.props;
          const returnData = [];
          if (cart.products) {
               Object.keys(cart.products).forEach(key => {
                    const {
                         product,
                         productTotalPrice,
                         productTotalQuantity
                    } = cart.products[key];
                    returnData.push(
                         <div className="sidecart-content">
                              <FaTimes
                                   onClick={() =>
                                        this.removeFromCart(
                                             product._id
                                        )
                                   }
                                   className="remove-product glyphicon glyphicon-remove"
                              />
                              <Link
                                   to={`/products/${
                                        product.type
                                   }/quicklook/${
                                        product.uniqueid
                                   }`}
                              >
                                   <img
                                        src={
                                             product
                                                  .imgaddresses[0]
                                        }
                                   />
                              </Link>
                              <Link
                                   to={`/products/${
                                        product.type
                                   }/quicklook/${
                                        product.uniqueid
                                   }`}
                              >
                                   <h4
                                        style={{
                                             textTransform:
                                                  'capitalize'
                                        }}
                                   >
                                        {`${product.name} ${
                                             product.colorname
                                        }`}{' '}
                                        - {product.size}
                                   </h4>
                              </Link>
                              <h5>
                                   Qty :{' '}
                                   {productTotalQuantity}
                              </h5>
                              <h5>
                                   {
                                        symbolchart[
                                             selectedCountry
                                        ]
                                   }{' '}
                                   {productTotalPrice}
                              </h5>
                         </div>
                    );
               });
          }
          return returnData;
     }
     render() {
          const { windowHeight } = this.state;
          const { isCartShowing, cart, selectedCountry } = this.props;
          return (
               <div
                    animation="false"
                    className={cn('sidecart_mobile', {
                         sidecart_mobile_right: isCartShowing
                    })}
               >
                    <div className="heading">
                         <Link to="/mycart">
                              <h3>CART</h3>
                         </Link>
                    </div>
                    <div
                         className="sidecart-content-box-mobile"
                         style={{ maxHeight: windowHeight - 150 }}
                    >
                         {this.renderCart()}
                    </div>
                    <div className="sidecart-total">
                         <div className="heading">
                              <h4>Total</h4>
                              <h4>
                                   <span>
                                        {symbolchart[selectedCountry]}{' '}
                                        {cart.totalPrice}
                                   </span>
                              </h4>
                         </div>
                         <div className="heading m_checkout_button">
                              <Link to="/mycart">
                                   <h3>Buy Now</h3>
                              </Link>
                         </div>
                    </div>
               </div>
          );
     }
}

const mapStateToProps = state => {
     const { cart, isCartShowing, selectedCountry } = state.userReducer;
     return { cart, isCartShowing, selectedCountry };
};

export default connect(
     mapStateToProps,
     { ...userActions, ...cartActions, ...sessionActions }
)(MobileCart);
