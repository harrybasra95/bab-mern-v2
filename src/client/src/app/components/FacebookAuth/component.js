import React, { Component } from 'react';
import { facebookAppId } from '../../config';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';

export default class FacebookAuth extends Component {
    render() {
        const { render, callback } = this.props;
        return (
            <FacebookLogin
                render={render}
                appId={facebookAppId}
                fields="name,email,picture"
                isMobile={true}
                callback={callback}
            />
        );
    }
}
