import React, { Component } from 'react';
import { googleClientId } from '../../config';
import GoogleLogin from 'react-google-login';

export default class GoogleAuth extends Component {
    render() {
        const { render, onSuccess } = this.props;
        return (
            <GoogleLogin
                render={render}
                clientId={googleClientId}
                fields="name email picture"
                isMobile={true}
                onSuccess={onSuccess}
            />
        );
    }
}
