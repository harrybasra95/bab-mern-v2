module.exports = {
    countriesList: [
        { title: 'India', imgUrl: '/flags/in.png', val: 'in' },
        { title: 'Europe', imgUrl: '/flags/eu.png', val: 'eu' },
        { title: 'Australia', imgUrl: '/flags/au.png', val: 'au' },
        { title: 'UAE', imgUrl: '/flags/ae.png', val: 'ae' },
        { title: 'New Zealand', imgUrl: '/flags/nz.png', val: 'nz' },
        { title: 'USA', imgUrl: '/flags/us.png', val: 'us' },
        { title: 'Canada', imgUrl: '/flags/ca.png', val: 'ca' },
        { title: 'Other', imgUrl: '/flags/mn.png', val: 'row' }
    ]
};
