module.exports = {
    accountOptionList: {
        title: 'MY ACCOUNT',
        sublist: [
            {
                title: 'My Orders',
                url: '/orders'
            },
            {
                title: 'Account Details',
                url: '/account-details'
            },
            {
                title: 'Edit Password',
                url: '/account-details/change-password'
            },
            {
                title: 'My Wishlist',
                url: '/wishlist'
            },
            {
                title: 'Logout',
                url: '/logout'
            }
        ]
    }
};
