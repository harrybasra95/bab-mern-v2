import homepageData from './homepageData';
import allPageInfo from './allPageInfo';
import headerComponentData from './headerComponentData';
import selectCountryComponent from './selectCountryComponent';
import checkoutPageData from './checkoutPageData';
import headerSidebarComponentData from './headerSidebarComponentData';
import orderDetailsData from './orderDetailsData';
import myOrdersData from './myOrdersData';
import topInfoBarData from './topInfoBarData';

export {
     topInfoBarData,
     homepageData,
     allPageInfo,
     headerComponentData,
     selectCountryComponent,
     checkoutPageData,
     headerSidebarComponentData,
     orderDetailsData,
     myOrdersData
};
