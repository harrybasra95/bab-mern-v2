module.exports = {
     sideHeaderList: [
          {
               label: 'categories',
               link: null,
               className: 'shop_by_type',
               activeClass: 'height_auto_type',
               subLabels: [
                    {
                         label: 'stringers',
                         link: '/products/category/stringers'
                    },
                    { label: 'tanks', link: '/products/category/tanks' },
                    { label: 't-shirts', link: '/products/category/t-shirts' },
                    {
                         label: 'trackpants',
                         link: '/products/category/trackpants'
                    }
               ]
          },
          {
               label: 'collections',
               link: null,
               className: 'shop_by_collection',
               activeClass: 'height_auto_collection',
               subLabels: [
                    {
                         label: 'elite series',
                         link: '/products/collection/elite'
                    },
                    {
                         label: 'perform series',
                         link: '/products/collection/perform'
                    },
                    {
                         label: 'contour series',
                         link: '/products/collection/contour'
                    },
                    {
                         label: 'signature series',
                         link: '/products/collection/signature'
                    },
                    { label: 'fit series', link: '/products/collection/fit' }
               ]
          },
          { label: 'featured', link: '/products/all' },
          { label: 'login', link: '/login', logOutReq: true },
          {
               label: 'my account',
               link: null,
               logInReq: true,
               className: 'sidebar_accounts',
               activeClass: 'height_auto',
               subLabels: [
                    { label: 'my orders', link: '/account/orders' },
                    { label: 'account details', link: '/account' },
                    { label: 'my wishlist', link: '/account/wishlist' }
               ]
          },
          { label: 'contact us', link: '/contact-us' },
          { label: 'return/refund policy', link: '/return' },
          { label: 'logout', link: '/logout', logInReq: true }
     ]
};
