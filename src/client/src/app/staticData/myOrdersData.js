module.exports = {
    statusFullForm: {
        OR: 'Order Recieved',
        PP: 'Payment Pending',
        C: 'Order Canceled',
        F: 'Order Failed',
        RRR: 'Return Requested',
        R: 'Returned',
        S: 'Shipped',
        D: 'Delivered',
        OS: 'Delivered'
    }
};
