module.exports = {
    splashPageData: [
        {
            title: 'Tanks',
            url: '/products/category/tanks',
            buttonText: 'Shop Now',
            imgUrl: '/img/cp3.jpeg',
            side: 'right'
        },
        {
            title: 'Stringers',
            url: '/products/category/stringers',
            buttonText: 'Shop Now',
            imgUrl: '/img/cp2.jpeg',
            side: 'left'
        },
        {
            title: 'T-shirts',
            url: '/products/category/t-shirts',
            buttonText: 'Shop Now',
            imgUrl: '/img/cp4.jpeg',
            side: 'right'
        },
        {
            title: 'Track-Pants',
            url: '/products/category/trackpants',
            buttonText: 'Shop Now',
            imgUrl: '/img/cp1.jpeg',
            side: 'left'
        }
    ],
    categoryDivData: [
        {
            title: 'Stringers',
            imgUrl: '/img/c1.jpg',
            url: '/products/category/stringers'
        },
        {
            title: 'T-shirts',
            imgUrl: '/img/c2.jpg',
            url: '/products/category/t-shirts'
        },
        {
            title: 'Tanks',
            imgUrl: '/img/c3.jpg',
            url: '/products/category/tanks'
        }
    ]
};
