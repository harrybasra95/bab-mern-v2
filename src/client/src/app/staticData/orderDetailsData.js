module.exports = {
    orderStatusDef: {
        or: 'Thank you. Your order has been recieved.',
        s:
            'Your order has been shipped.Please click track to find its location.',
        pp:
            'Confirmation on your payment has yet being pending.Please wait or contact us',
        f:
            'We are unabled to confirm payment from you.Thus we have cancled your order',
        c: 'Your order has been successfully canceled',
        d: 'Your order has been successfully delivered',
        os: 'Your order has been successfully delivered',
        rrr: 'Return request has been generated.Please check its status',
        r: 'Your order has been successfully returned'
    }
};
