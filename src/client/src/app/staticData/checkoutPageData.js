import { userValidations } from '../validations';

const checkoutPageData = {
     getSections: (user, coupon, checkoutInfo) => {
          return [
               {
                    number: 1,
                    label: 'Personal Details',
                    buttonFunction: 'sendRequest',
                    obj: user,
                    inputs: [
                         {
                              label: 'First Name',
                              value: user.fname,
                              fieldName: 'fname',
                              validation: userValidations.nameValidation
                         },
                         {
                              label: 'Last Name',
                              value: user.lname,
                              fieldName: 'lname',
                              validation: userValidations.nameValidation
                         },
                         {
                              label: 'Phone',
                              value: user.phone,
                              type: 'number',
                              fieldName: 'phone',
                              validation: userValidations.phoneValidation
                         }
                    ]
               },
               {
                    number: 2,
                    label: 'Apply Coupon',
                    buttonFunction: 'sendRequest',
                    obj: coupon,
                    inputs: [
                         {
                              label: 'Name',
                              value: coupon.name,
                              fieldName: 'couponName',
                              disabled: !!coupon.name
                         }
                    ],
                    buttons: [
                         {
                              label: coupon.name
                                   ? 'Remove Coupon'
                                   : 'Apply Coupon',
                              backgroundColor: coupon.name ? '#C63F3F' : null,
                              onClick: coupon.name
                                   ? 'removeCoupon'
                                   : 'applyCoupon'
                         }
                    ],
                    radios: [
                         {
                              startLabel: 'Delivery Type',
                              show: coupon.name && coupon.type === 'gym',
                              label: 'Deliver to my gym.',
                              type: 'radio',
                              name: 'deliverTo',
                              value: 'myGym',
                              selected: true,
                              fieldName: 'deliverTo',
                              address: coupon.gymAddress
                         },
                         {
                              show: coupon.name && coupon.type === 'gym',
                              label: 'Deliver to my home.',
                              type: 'radio',
                              name: 'deliverTo',
                              value: 'myHome',
                              selected: true,
                              fieldName: 'deliverTo'
                         }
                    ]
               },
               {
                    number: 3,
                    label: 'Delivery Details',
                    buttonFunction: 'sendRequest',
                    obj: user.lastOrderDetails,
                    inputs: [
                         {
                              label: 'Street No.',
                              validation: userValidations.streetNoValidation,
                              value: user.lastOrderDetails
                                   ? user.lastOrderDetails.streetNo
                                   : null,
                              fieldName: 'streetNo'
                         },
                         {
                              label: 'Pincode',
                              validation: userValidations.pincodeValidation,
                              value: user.lastOrderDetails
                                   ? user.lastOrderDetails.pincode
                                   : null,
                              fieldName: 'pincode',
                              type: 'number'
                         },
                         {
                              show: !!checkoutInfo.city,
                              label: 'City',
                              value: checkoutInfo.city,
                              fieldName: 'city',
                              disabled: true
                         },
                         {
                              show: !!checkoutInfo.state,
                              label: 'State',
                              value: checkoutInfo.state,
                              fieldName: 'state',
                              disabled: true
                         },
                         {
                              show: !!checkoutInfo.country,
                              label: 'Country',
                              value: checkoutInfo.country,
                              fieldName: 'country',
                              disabled: true
                         }
                    ]
               },
               {
                    number: 4,
                    label: 'Payment Info',
                    buttonFunction: 'createOrder',
                    lines: ['Your Grand Total is '],
                    buttons: [
                         {
                              type: 'checkout',
                              label: 'Credit/Debit Card',
                              value: 'card',
                              icon: 'card',
                              onClick: 'selectPaymentOption'
                         },
                         {
                              type: 'checkout',
                              label: 'Paytm',
                              value: 'paytm',
                              icon: 'paytm',
                              onClick: 'selectPaymentOption'
                         },
                         {
                              show: coupon && coupon.name ? false : true,
                              type: 'checkout',
                              label: 'Cash On Delivery (Rs. 50 extra)',
                              value: 'cod',
                              icon: 'cod',
                              onClick: 'selectPaymentOption'
                         }
                    ]
               }
          ];
     }
};

export default checkoutPageData;
