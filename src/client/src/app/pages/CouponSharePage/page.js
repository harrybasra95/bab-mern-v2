import React, { Component } from 'react';
import './style.styl';
import { connect } from 'react-redux';
import { userActions } from '../../actions';
import { couponServices } from '../../services';

class CouponSharePage extends Component {
     constructor(props) {
          super(props);

          this.state = {
               data: {
                    orders: []
               }
          };
     }
     async componentDidMount() {
          const { showMessage, loadingState } = this.props;
          const { couponId } = this.props.match.params;
          loadingState(true);
          const data = await couponServices.fetchCouponOrders(couponId);
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          return this.setState({ data: data.data });
     }
     renderOrders() {
          const returnArray = [];
          const { data } = this.state;
          data.orders.forEach((item, i) => {
               returnArray.push(
                    <tr>
                         <td>{i}</td>
                         <td>{item.orderNumber}</td>
                         <td>{item.name}</td>
                         <td>{item.orderDate}</td>
                         <td>{item.totalprice}</td>
                         <td>{item.isCouponApplied ? 'Yes' : 'No'}</td>
                         <td>{item.sharePrice}</td>
                    </tr>
               );
          });
     }

     render() {
          const { data } = this.state;
          return (
               <div className="container">
                    <div className="couponshare">
                         <h1 className="text-center">Profit Share</h1>
                         <h3 className="text-center">{data.title}</h3>
                         <content>
                              <table border="1px">
                                   <tr>
                                        <th>No.</th>
                                        <th>Order No</th>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Total Price</th>
                                        <th>Coupon</th>
                                        <th>Share Price</th>
                                   </tr>
                                   {this.renderOrders()}

                                   <tr>
                                        <td />
                                        <td />
                                        <td />
                                        <td />
                                        <td />
                                        <td>Grand Total</td>
                                        <td>{data.totalSharePrice}</td>
                                   </tr>
                              </table>
                         </content>
                    </div>
               </div>
          );
     }
}

const mapStateToProps = state => state;

export default connect(
     mapStateToProps,
     userActions
)(CouponSharePage);
