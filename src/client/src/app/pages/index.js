import Homepage from './Homepage';
import LoginPage from './LoginPage';
import ProductGalleryPage from './ProductGalleryPage';
import QuicklookPage from './QuicklookPage';
import MyCartPage from './MyCartPage';
import CheckoutPage from './CheckoutPage';
import OrderDetailsPage from './OrderDetailsPage';
import MyOrdersPage from './MyOrdersPage';
import SignUpPage from './SignUpPage';
import PayViaPaytmPage from './PayViaPaytmPage';
import ContactUsPage from './ContactUsPage';
import ReturnPolicyPage from './ReturnPolicyPage';
import TermsAndConditionsPage from './TermsAndConditionsPage';
import PrivacyPolicyPage from './PrivacyPolicyPage';

export {
     Homepage,
     LoginPage,
     ProductGalleryPage,
     QuicklookPage,
     MyCartPage,
     CheckoutPage,
     OrderDetailsPage,
     MyOrdersPage,
     SignUpPage,
     PayViaPaytmPage,
     ContactUsPage,
     ReturnPolicyPage,
     TermsAndConditionsPage,
     PrivacyPolicyPage
};
