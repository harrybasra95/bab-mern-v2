import React, { Component } from 'react';
import './style.css';
import { connect } from 'react-redux';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

import { couponServices } from '../../services';
import { userActions, sessionActions } from '../../actions';

class CheckoutPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            couponName: ''
        };
    }

    async applyCoupon() {
        const { loadingState, showMessage, setSessionData } = this.props;
        const { couponName } = this.state;
        loadingState(true);
        const data = await couponServices.applyCoupon(couponName);
        loadingState(false);
        if (!data.isSuccess) {
            return showMessage(data);
        }
        setSessionData(data.data);
    }

    async removeCoupon() {
        console.log('thissihaidssa');
        const { loadingState, showMessage, setSessionData } = this.props;
        loadingState(true);
        const data = await couponServices.removeCoupon();
        loadingState(false);
        if (!data.isSuccess) {
            return showMessage(data);
        }
        setSessionData(data.data);
    }

    renderCouponBox() {
        const { coupon } = this.props;
        return (
            <div>
                <h3>Apply Coupon</h3>
                <div className="apply_coupon_box">
                    <div className="coupon">
                        <input
                            type="text"
                            id="coupon"
                            onChange={event =>
                                this.setState({
                                    couponName: event.target.value
                                })
                            }
                            value={coupon.name}
                            disabled={!!coupon.name}
                        />
                        {coupon.name ? (
                            <span
                                onClick={() => this.removeCoupon()}
                                className="remove_coupon glyphicon glyphicon-remove"
                            />
                        ) : (
                            <button
                                onClick={() => this.applyCoupon()}
                                type="button"
                                id="coupon_apply"
                            >
                                <h5>Apply</h5>
                            </button>
                        )}
                    </div>
                </div>
            </div>
        );
    }
    renderPaymentBox() {
        return (
            <div>
                <h3>Payment Method</h3>
                <div className="payment">
                    <div className="p_box">
                        <div className="p_method_heading" />
                        <div className="p_method_box">
                            <div val="1" className="p_method cod_method <%=x%>">
                                Cash on delivery<span>(Rs50 Extra )</span>
                            </div>
                            <div val="2" className="p_method">
                                <img
                                    style={{ height: '80%', marginTop: '5px' }}
                                    src="/img/paytm.png"
                                />
                            </div>
                            <div val="3" className="p_method <%=z%>">
                                Credit/Debit/Net Banking
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    renderActivePopUp() {
        const { user } = this.props;
        if (user.active === false) {
            return (
                <div id="email_verify" className="email_verify">
                    <p>Your account has not been verified.please verify it.</p>
                    <button id="resend_mail">Resend Mail</button>
                    <span className="close_email_verify glyphicon glyphicon-remove" />
                </div>
            );
        }
        return null;
    }
    render() {
        const { user, coupon } = this.props;
        if (!user.defaultaddress) {
            user.defaultaddress = {};
        }
        return (
            <div style={{ backgroundColor: '#f5f8f8' }}>
                <div className="checkout-page">
                    <div className="container">
                        <h1 className="text-center checkout_heading">
                            Checkout
                        </h1>
                        <br />
                        <TextField
                            id="outlined-name"
                            label="Name"
                            margin="normal"
                            variant="outlined"
                        />
                        <div className="row">
                            <form
                                method="post"
                                onSubmit="return billing_form(this)"
                                id="billing_form"
                            >
                                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                    <div className="billing-details">
                                        <div className="billing-form">
                                            <h3 className="text-center">
                                                Customer Info
                                            </h3>
                                            <div className="outer-box fullname">
                                                <div className="outer-box name">
                                                    <input
                                                        type="text"
                                                        required
                                                        value={user.fname}
                                                        id="fname"
                                                    />
                                                    <label htmlFor="fname">
                                                        First Name
                                                    </label>
                                                </div>
                                                <div className="outer-box name">
                                                    <input
                                                        type="text"
                                                        required
                                                        value={user.lname}
                                                        id="lname"
                                                    />
                                                    <label htmlFor="lname">
                                                        Last Name
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="outer-box phone">
                                                <input
                                                    value={
                                                        user.defaultaddress
                                                            .phone
                                                    }
                                                    type="text"
                                                    required
                                                    id="phone"
                                                />
                                                <label htmlFor="phone">
                                                    Phone
                                                </label>
                                            </div>
                                            <h3 className="text-center">
                                                Delivery Info
                                            </h3>
                                            <div className="outer-box">
                                                <input
                                                    type="text"
                                                    value={
                                                        user.defaultaddress
                                                            .street_no
                                                    }
                                                    required
                                                    id="street_no"
                                                />
                                                <label htmlFor="street_no">
                                                    Address line 1
                                                </label>
                                            </div>
                                            <div className="outer-box">
                                                <input
                                                    type="text"
                                                    value=""
                                                    id="street_no"
                                                />
                                                <label htmlFor="street_no">
                                                    Address line 2 (optional)
                                                </label>
                                            </div>
                                            <div className="outer-box pincode">
                                                <input
                                                    value={
                                                        user.defaultaddress
                                                            .pincode
                                                    }
                                                    type="text"
                                                    required
                                                    id="pincode"
                                                />
                                                <label htmlFor="pincode">
                                                    Pincode
                                                </label>
                                            </div>
                                            <div className="outer-box">
                                                <input
                                                    type="text"
                                                    value={
                                                        user.defaultaddress.city
                                                    }
                                                    required
                                                    id="city"
                                                />
                                                <label htmlFor="city">
                                                    City
                                                </label>
                                            </div>

                                            <div className=" dcountry">
                                                <div className="outer-box">
                                                    <input
                                                        type="text"
                                                        value={
                                                            user.defaultaddress
                                                                .state
                                                        }
                                                        required=""
                                                        id="state"
                                                    />
                                                    <label htmlFor="state">
                                                        State
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="outer-box">
                                                <input
                                                    type="text"
                                                    value="India"
                                                    required
                                                    id="country"
                                                />
                                                <label htmlFor="country">
                                                    Country
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12  text-center">
                                    {this.renderCouponBox()}
                                    {this.renderPaymentBox()}
                                    <h3 className="text-center o_heading">
                                        Order Details
                                    </h3>
                                    <div className="cart-total">
                                        <div className="subtotal_price">
                                            <h4>Subtotal</h4>
                                            <h4 id="subtotal_price" />
                                        </div>
                                        <div className="shipping_price">
                                            <h4>Shipping</h4>
                                            <h4 id="shipping_price" />
                                            <h4 id="shipping_price" />
                                        </div>
                                        {coupon ? (
                                            <div className="coupon_price">
                                                <h4>Coupon Discount</h4>
                                                <h4 id="coupon_price">-</h4>
                                            </div>
                                        ) : null}
                                        <div className="total_price">
                                            <h4>Total</h4>
                                            <h4 id="total_price" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="row">
                            <div className="text-center	 col-lg-12">
                                <div className="outer-box p_to_p_box">
                                    <button
                                        val="p_to_p"
                                        id="p_to_p"
                                        type="submit"
                                    >
                                        <h4>Proceed to Pay</h4>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { user, coupon, selectedCountry, cart } = state.userReducer;
    return { user, coupon, selectedCountry, cart };
};
export default connect(
    mapStateToProps,
    { ...userActions, ...sessionActions }
)(CheckoutPage);
