import React from 'react';
import { checkoutPageData } from '../../../staticData';
import { Input, Button } from '../../../components';

const renderSection = () => {
    const { user, coupon } = this.props;
    const { currentPoint, checkoutInfo, buttonFunction } = this.state;
    const returnData = [];
    const radioData = [];
    const sectionPoint = checkoutPageData.getSections(user, coupon)[
        currentPoint - 1
    ];
    if (buttonFunction !== sectionPoint.buttonFunction) {
        this.setState({ buttonFunction: sectionPoint.buttonFunction });
    }
    const { inputs, buttons, radios } = sectionPoint;
    if (inputs) {
        inputs.forEach(
            ({ label, value, type, fieldName, disabled, maxLength }) => {
                returnData.push(
                    <Input
                        maxLength={maxLength || null}
                        key={label}
                        defaultValue={value || null}
                        disabled={disabled}
                        label={label}
                        value={checkoutInfo[fieldName] || null}
                        type={type || 'text'}
                        onChange={({ target }) =>
                            this.handleChanges(fieldName, target.value)
                        }
                    />
                );
            }
        );
    }
    if (buttons) {
        buttons.forEach(({ label, onClick, backgroundColor }) => {
            returnData.push(
                <div className="side-button">
                    <Button
                        label={label}
                        onClick={() => this[onClick]()}
                        backgroundColor={backgroundColor}
                    />
                </div>
            );
        });
    }
    if (radios) {
        radios.forEach(
            ({
                label,
                type,
                name,
                fieldName,
                value,
                show,
                startLabel,
                address
            }) => {
                if (show) {
                    if (startLabel) {
                        radioData.push(
                            <h4 className="delivery-type-heading">
                                {startLabel}
                            </h4>
                        );
                    }
                    radioData.push(
                        <div>
                            <Input
                                label={label}
                                name={name}
                                value={value}
                                type={type}
                                checked={checkoutInfo[fieldName] === value}
                                onChange={({ target }) =>
                                    this.handleChanges(fieldName, target.value)
                                }
                            />
                            {checkoutInfo[fieldName] === value ? (
                                <p className="deliver-to-address">{address}</p>
                            ) : null}
                        </div>
                    );
                }
            }
        );
    }
    return (
        <div className="checkout-middle">
            <p className="current-step-heading">{sectionPoint.label}</p>
            <div>
                <form action="">
                    {returnData}
                    <div className="delivery-type-box">{radioData}</div>
                </form>
            </div>
        </div>
    );
};

export default renderSection;
