import React, { Component } from 'react';
import './style.styl';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Button, ContainerComponent, Input } from '../../components';
import cn from 'classnames';
import { couponServices, orderServices } from '../../services';
import { checkoutPageData } from '../../staticData';
import { symbolchart } from '../../config';
import { FaCheck, FaAngleDown } from 'react-icons/fa';
import { userActions, sessionActions } from '../../actions';

const sectionArray = [
     'personal-details',
     'apply-coupon',
     'delivery-details',
     'payment-info'
];

class CheckoutPage extends Component {
     constructor(props) {
          super(props);

          this.state = {
               currentPoint: 1,
               checkoutInfo: {},
               pincodeCheck: false,
               errors: {},
               paymentType: null,
               orderSummaryOpen: false
          };
     }

     componentDidMount() {
          const { user, coupon, cart } = this.props;
          let discount = 0;
          if (coupon && coupon.couponDiscount) {
               discount = coupon.couponDiscount;
          }
          this.checkSectionPoint();
          this.setState({
               pincodeCheck: user.lastOrderDetails
                    ? !!user.lastOrderDetails.pincode
                    : false,
               checkoutInfo: {
                    fname: user.lastOrderDetails
                         ? user.lastOrderDetails.fname
                         : user.fname,
                    lname: user.lastOrderDetails
                         ? user.lastOrderDetails.lname
                         : user.lname,
                    phone: user.lastOrderDetails
                         ? user.lastOrderDetails.phone
                         : user.phone,
                    deliverTo: 'myGym',
                    ...user.lastOrderDetails,
                    shipping: cart.totalPrice - discount >= 499 ? 0 : 50
               } || {
                    deliverTo: 'myGym',
                    shipping: 0
               }
          });
     }

     checkSectionPoint() {
          let { section } = this.props.match.params;
          section = sectionArray.indexOf(section) + 1;
          const { user, coupon } = this.props;
          const { lastOrderDetails } = user;
          if (section === 0) {
               return this.props.history.push('/');
          }
          if (!lastOrderDetails) {
               return this.setState({ currentPoint: 1 });
          }
          const {
               fname,
               lname,
               phone,
               deliverTo,
               streetNo,
               pincode
          } = lastOrderDetails;
          if (
               (section === 2 && fname && lname && phone) ||
               (section === 3 && fname && lname && phone)
          ) {
               return this.setState({
                    currentPoint: Number(section)
               });
          }
          if (
               section === 4 &&
               fname &&
               lname &&
               phone &&
               coupon.name &&
               deliverTo === 'myGym'
          ) {
               return this.setState({
                    currentPoint: Number(section)
               });
          }
          if (section === 4 && fname && lname && phone && streetNo && pincode) {
               return this.setState({
                    currentPoint: Number(section)
               });
          }
     }

     async sendRequest() {
          const {
               setSessionData,
               showMessage,
               loadingState,
               history,
               coupon
          } = this.props;
          const { checkoutInfo, currentPoint, pincodeCheck } = this.state;
          const { fname, lname, phone, streetNo } = checkoutInfo;
          if (
               !fname ||
               !lname ||
               !phone ||
               (currentPoint === 3 && !streetNo) ||
               (currentPoint === 3 && !pincodeCheck)
          ) {
               return showMessage('Please enter all fields');
          }

          loadingState(true);
          const data = await orderServices.updateLastOrderData(checkoutInfo);
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          setSessionData(data.data);
          if (
               currentPoint === 2 &&
               coupon.name &&
               coupon.type === 'gym' &&
               checkoutInfo.deliverTo === 'myGym'
          ) {
               return history.push(
                    `/checkout/${sectionArray[currentPoint + 1]}`
               );
          }
          return history.push(`/checkout/${sectionArray[currentPoint]}`);
     }

     async createOrder() {
          const { loadingState, showMessage, history } = this.props;
          const { paymentType } = this.state;
          if (paymentType === null) {
               return showMessage('Please select a payment method');
          }
          loadingState(true);
          const data = await orderServices.createOrder({
               paymentType
          });
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          if (paymentType === 'cod') {
               return history.push(
                    `/account/orders/${data.data.order.ordernumber}`,
                    {
                         ...data.data
                    }
               );
          }
          if (paymentType === 'card') {
               this.setState({
                    access_code: data.data.acc,
                    encRequest: data.data.enc,
                    url: data.data.url
               });
               return this.ccForm.submit();
          }
          if (paymentType === 'paytm') {
               return history.push(
                    `/orders/${data.data.order.ordernumber}/pay-via-paytm`,
                    {
                         ...data.data
                    }
               );
          }
     }

     async applyCoupon() {
          const { loadingState, showMessage, setSessionData } = this.props;
          const { checkoutInfo } = this.state;
          loadingState(true);
          const data = await couponServices.applyCoupon(
               checkoutInfo.couponName
          );
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          setSessionData(data.data);
     }

     async removeCoupon() {
          const {
               loadingState,
               showMessage,
               setSessionData,
               coupon
          } = this.props;
          loadingState(true);
          const data = await couponServices.removeCoupon();
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          this.setState({
               checkoutInfo: {
                    ...data.data.user.lastOrderDetails,
                    couponName: coupon.name
               }
          });
          setSessionData(data.data);
     }

     goToSectionLink(number, condition) {
          const { history } = this.props;
          if (condition) {
               return history.push(`/checkout/${sectionArray[number - 1]}`);
          }
     }

     renderSectionPoints() {
          const { user, coupon } = this.props;
          const { currentPoint, checkoutInfo } = this.state;
          const returnData = [];
          const sectionPoints = checkoutPageData.getSections(
               user,
               coupon,
               checkoutInfo
          );
          sectionPoints.forEach(({ label, number }) => {
               returnData.push(
                    <div
                         onClick={() =>
                              this.goToSectionLink(
                                   number,
                                   number < currentPoint
                              )
                         }
                         className="singlepoint-outer"
                    >
                         <div
                              className={cn('singlepoint', {
                                   'singlepoint-done': number < currentPoint,
                                   'singlepoint-current':
                                        number === currentPoint
                              })}
                         >
                              {number < currentPoint ? (
                                   <FaCheck color="white" />
                              ) : (
                                   <p className="singlepoint-number">
                                        {number}
                                   </p>
                              )}
                         </div>
                         <p className="singlepoint-label">{label}</p>
                    </div>
               );
          });

          return <div className="steppoints-outer">{returnData}</div>;
     }

     renderCCAvenueForm() {
          const { access_code, encRequest, url } = this.state;
          return (
               <div style={{ display: 'hidden' }}>
                    <form
                         ref={ref => (this.ccForm = ref)}
                         id="nonseamless"
                         method="post"
                         name="redirect"
                         action={`${url}/transaction/transaction.do?command=initiateTransaction`}
                    >
                         <input
                              type="hidden"
                              id="encRequest"
                              name="encRequest"
                              value={encRequest}
                         />
                         <input
                              type="hidden"
                              name="access_code"
                              id="access_code"
                              value={access_code}
                         />
                    </form>
               </div>
          );
     }

     renderOrderSummaryBox() {
          const { checkoutInfo, orderSummaryOpen } = this.state;
          const { selectedCountry, cart, coupon } = this.props;
          let discount = 0;
          if (coupon && coupon.couponDiscount) {
               discount = coupon.couponDiscount;
          }
          const summaryObj = {
               'Order Subtotal': `${symbolchart[selectedCountry]} ${cart.totalPrice}`,
               Discount: ` - ${symbolchart[selectedCountry]} ${discount}`,
               Shipping: checkoutInfo.shipping,
               'Order Total': `${
                    symbolchart[selectedCountry]
               } ${cart.totalPrice + checkoutInfo.shipping - discount}`
          };
          const data = [];
          Object.keys(summaryObj).forEach(key => {
               data.push(
                    <div className="order-summary-detail-single">
                         <p>{key}</p>
                         <p>{summaryObj[key]}</p>
                    </div>
               );
          });
          return (
               <div className="order-summary-box">
                    <div
                         onClick={() =>
                              this.setState({
                                   orderSummaryOpen: !orderSummaryOpen
                              })
                         }
                         className="order-summary-box-title"
                    >
                         <p className="order-summary-box-title-text">
                              Order Summary
                         </p>
                         <FaAngleDown color="black" size={18} />
                    </div>
                    <div
                         className={cn('order-summary-details-box', {
                              'order-summary-details-box-open': orderSummaryOpen
                         })}
                    >
                         {data}
                    </div>
               </div>
          );
     }

     renderSection() {
          const { user, coupon, cart, selectedCountry } = this.props;
          let discount = 0;
          if (coupon && coupon.couponDiscount) {
               discount = coupon.couponDiscount;
          }
          const {
               currentPoint,
               checkoutInfo,
               buttonFunction,
               errors,
               paymentType
          } = this.state;
          const returnData = [];
          const radioData = [];
          const sectionPoint = checkoutPageData.getSections(
               user,
               coupon,
               checkoutInfo
          )[currentPoint - 1];
          if (buttonFunction !== sectionPoint.buttonFunction) {
               this.setState({ buttonFunction: sectionPoint.buttonFunction });
          }
          const { inputs, buttons, radios, lines } = sectionPoint;
          if (inputs) {
               inputs.forEach(
                    ({
                         label,
                         value,
                         type,
                         fieldName,
                         disabled,
                         maxLength,
                         validation,
                         show
                    }) => {
                         if (typeof show === 'undefined' || show) {
                              returnData.push(
                                   <Input
                                        error={!!errors[fieldName]}
                                        errorMessage={errors[fieldName]}
                                        maxLength={maxLength || null}
                                        key={label}
                                        defaultValue={value || null}
                                        disabled={disabled}
                                        label={label}
                                        value={checkoutInfo[fieldName] || null}
                                        type={type || 'text'}
                                        onChange={({ target }) =>
                                             this.handleChanges(
                                                  fieldName,
                                                  target.value,
                                                  validation
                                             )
                                        }
                                   />
                              );
                         }
                    }
               );
          }
          if (buttons) {
               buttons.forEach(
                    ({
                         label,
                         onClick,
                         backgroundColor,
                         type,
                         icon,
                         value,
                         show
                    }) => {
                         if (typeof show === 'undefined' || show) {
                              returnData.push(
                                   <div
                                        className={cn({ 'side-button': !type })}
                                   >
                                        <Button
                                             selected={
                                                  value && paymentType === value
                                             }
                                             label={label}
                                             icon={icon}
                                             type={type}
                                             onClick={() =>
                                                  this[onClick](value)
                                             }
                                             backgroundColor={backgroundColor}
                                        />
                                   </div>
                              );
                         }
                    }
               );
          }
          if (radios) {
               radios.forEach(
                    ({
                         label,
                         type,
                         name,
                         fieldName,
                         value,
                         show,
                         startLabel,
                         address
                    }) => {
                         if (show) {
                              if (startLabel) {
                                   radioData.push(
                                        <h4 className="delivery-type-heading">
                                             {startLabel}
                                        </h4>
                                   );
                              }
                              radioData.push(
                                   <div>
                                        <Input
                                             label={label}
                                             name={name}
                                             value={value}
                                             type={type}
                                             checked={
                                                  checkoutInfo[fieldName] ===
                                                  value
                                             }
                                             onChange={({ target }) =>
                                                  this.handleChanges(
                                                       fieldName,
                                                       target.value
                                                  )
                                             }
                                        />
                                        {checkoutInfo[fieldName] === value ? (
                                             <p className="deliver-to-address">
                                                  {address}
                                             </p>
                                        ) : null}
                                   </div>
                              );
                         }
                    }
               );
          }
          return (
               <div className="checkout-middle">
                    {lines ? (
                         <div className="order-total-box">
                              <p className="current-step-heading">
                                   Order Total
                              </p>
                              <p className="order-total-price">
                                   Your grand total is{' '}
                                   {symbolchart[selectedCountry]}
                                   {cart.totalPrice +
                                        checkoutInfo.shipping -
                                        discount}
                              </p>
                              {this.renderOrderSummaryBox()}
                         </div>
                    ) : null}
                    <p className="current-step-heading">{sectionPoint.label}</p>
                    <div>
                         <form action="">
                              {returnData}
                              <div className="delivery-type-box">
                                   {radioData}
                              </div>
                         </form>
                    </div>
               </div>
          );
     }

     handleChanges(name, value, validation) {
          const { checkoutInfo, errors, pincodeCheck } = this.state;
          checkoutInfo[name] = value;
          if (validation && !validation(value).isSuccess) {
               errors[name] = validation(value).message;
          } else {
               delete errors[name];
          }
          if (name === 'pincode' && !pincodeCheck) {
               errors[name] = validation(value).message;
          }
          if (name === 'pincode' && value.length === 6) {
               this.pincodeCheck(value);
               return this.setState({
                    checkoutInfo: { ...checkoutInfo },
                    errors: { ...errors }
               });
          }
          return this.setState({
               checkoutInfo: { ...checkoutInfo },
               errors: { ...errors },
               pincodeCheck: false
          });
     }

     async pincodeCheck(pincode) {
          const { loadingState, showMessage, cart, coupon } = this.props;
          const { pincodeCheck, checkoutInfo, errors } = this.state;
          let discount = 0;
          if (coupon && coupon.couponDiscount) {
               discount = coupon.couponDiscount;
          }
          if (!pincodeCheck) {
               loadingState(true);
               delete checkoutInfo.city;
               delete checkoutInfo.state;
               delete checkoutInfo.country;
               this.setState({ checkoutInfo: { ...checkoutInfo } });
               const data = await orderServices.pincodeCheck(pincode);
               loadingState(false);
               if (!data.isSuccess) {
                    this.setState({
                         pincodeCheck: false,
                         errors: {
                              ...errors,
                              pincode: 'Please enter a valid pincode'
                         }
                    });
                    return showMessage(data);
               }
               delete errors.pincode;
               data.data.shipping = cart.totalPrice - discount >= 499 ? 0 : 50;
               return this.setState({
                    pincodeCheck: true,
                    checkoutInfo: { ...checkoutInfo, ...data.data },
                    errors: { ...errors }
               });
          }
     }

     selectPaymentOption(value) {
          this.setState({ paymentType: value });
     }

     render() {
          const { deviceHeight } = this.props;
          const { buttonFunction, errors, currentPoint } = this.state;
          return (
               <ContainerComponent checkoutPage subscribeHidden>
                    <div
                         className="checkout-page"
                         style={{ minHeight: deviceHeight }}
                    >
                         <div className="checkout-upper">
                              <p className="text-center checkout_heading">
                                   Checkout
                              </p>
                              {this.renderSectionPoints()}
                         </div>
                         {this.renderSection()}
                         {this.renderCCAvenueForm()}
                         <div className="checkout-lower">
                              <Button
                                   backgroundColor={
                                        Object.keys(errors).length === 0
                                             ? null
                                             : 'grey'
                                   }
                                   label={
                                        currentPoint === 4
                                             ? 'Create Order'
                                             : 'Next'
                                   }
                                   onClick={() => {
                                        return Object.keys(errors).length === 0
                                             ? this[buttonFunction]()
                                             : null;
                                   }}
                              />
                         </div>
                    </div>
               </ContainerComponent>
          );
     }
}

const mapStateToProps = state => {
     const {
          user,
          coupon,
          selectedCountry,
          cart,
          deviceHeight
     } = state.userReducer;
     return { user, coupon, selectedCountry, cart, deviceHeight };
};
export default withRouter(
     connect(
          mapStateToProps,
          {
               ...userActions,
               ...sessionActions
          }
     )(CheckoutPage)
);
