import React, { Component } from 'react';
import './style.styl';
import { ContainerComponent } from '../../components';

class ContactUsPage extends Component {
     render() {
          return (
               <ContainerComponent>
                    <div className="contact_us">
                         <div className="container">
                              <div className="row">
                                   <h1 className="text-center">Contact Us</h1>
                                   <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 form_box">
                                        <h3>Please write your issue</h3>
                                        <form>
                                             <div className="outer-box name">
                                                  <input
                                                       type="text"
                                                       required=""
                                                       id="fname"
                                                       name="fname"
                                                  />
                                                  <label for="fname">
                                                       First name
                                                  </label>
                                             </div>
                                             <div className="outer-box name">
                                                  <input
                                                       type="text"
                                                       required=""
                                                       id="lname"
                                                       name="lname"
                                                  />
                                                  <label for="lname">
                                                       Last name
                                                  </label>
                                             </div>
                                             <div className="outer-box">
                                                  <input
                                                       type="text"
                                                       required=""
                                                       id="eadd"
                                                       name="username"
                                                  />
                                                  <label for="eadd">
                                                       Email
                                                  </label>
                                             </div>
                                             <div className="outer-box">
                                                  <input
                                                       type="text"
                                                       required=""
                                                       id="phone"
                                                       name="phone"
                                                  />
                                                  <label for="phone">
                                                       Phone
                                                  </label>
                                             </div>
                                             <div className="outer-box">
                                                  <textarea
                                                       id="message"
                                                       name="message"
                                                       placeholder="Write your query"
                                                  />
                                             </div>
                                             <div className="outer-box">
                                                  <button
                                                       id="query_submit"
                                                       type="button"
                                                  >
                                                       Submit
                                                  </button>
                                             </div>
                                        </form>
                                   </div>
                                   <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                                        <h3>
                                             You can also write to us on our
                                             address.
                                        </h3>
                                        <h4>
                                             We'll be happy to hear from you.
                                        </h4>
                                        <div className="own_address">
                                             <h4>
                                                  <span>Address:</span>
                                                  BAB Headquaters
                                             </h4>
                                             <h4>
                                                  <span /> W P 181 / A Basti
                                                  Sheikh
                                             </h4>
                                             <h4>
                                                  <span>City:</span> Jalandhar
                                             </h4>
                                             <h4>
                                                  <span>State:</span> Punjab
                                             </h4>
                                             <h4>
                                                  <span>Pincode:</span> 144002
                                             </h4>
                                             <h4>
                                                  <span>Phone:</span> 9899823278
                                                  (Whatsapp only)
                                             </h4>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </ContainerComponent>
          );
     }
}

export default ContactUsPage;
