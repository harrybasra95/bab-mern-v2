import React, { Component } from 'react';
import './style.css';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { userServices } from '../../services';
import { userValidations } from '../../validations';
import { userActions, sessionActions } from '../../actions';
import { ContainerComponent } from '../../components';
const { loginUser } = userServices;
const { emailValidation, passwordValidation } = userValidations;

class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    async sendRequest() {
        const {
            showMessage,
            loadingState,
            setSessionData,
            history
        } = this.props;
        const { username, password } = this.state;
        if (!emailValidation(username).isSuccess) {
            return showMessage(emailValidation(username));
        }
        if (!passwordValidation(password).isSuccess) {
            return showMessage(passwordValidation(password));
        }
        loadingState(true);
        const data = await loginUser(username, password);
        loadingState(false);
        if (!data.isSuccess) {
            return showMessage(data);
        }
        history.push('/');
        return setSessionData(data.data);
    }

    render() {
        const { username, password } = this.state;

        return (
            <ContainerComponent pageTitle="login">
                <div className="login-box">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="login">
                                    <h1 className="hidden-xs">Login</h1>
                                    <div className="row">
                                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div className="login1 text-center">
                                                <div className="outer-box">
                                                    <input
                                                        onChange={event => {
                                                            this.setState({
                                                                username:
                                                                    event.target
                                                                        .value
                                                            });
                                                        }}
                                                        value={username}
                                                        type="text"
                                                        required=""
                                                        id="username"
                                                        name="username"
                                                    />
                                                    <label htmlFor="username">
                                                        Email
                                                    </label>
                                                </div>
                                                <div className="outer-box">
                                                    <input
                                                        onChange={event => {
                                                            this.setState({
                                                                password:
                                                                    event.target
                                                                        .value
                                                            });
                                                        }}
                                                        value={password}
                                                        type="password"
                                                        id="lpassword"
                                                        required=""
                                                        name="password"
                                                    />
                                                    <label htmlFor="lpassword">
                                                        Password
                                                    </label>
                                                </div>
                                                <input
                                                    type="hidden"
                                                    name="_csrf"
                                                    value="<%=csrfToken%>"
                                                />
                                                <Link to="/password/forget">
                                                    <p
                                                        id="forgot_pass"
                                                        className="text-right"
                                                    >
                                                        Forgot password?
                                                    </p>
                                                </Link>
                                                <button
                                                    onClick={() =>
                                                        this.sendRequest()
                                                    }
                                                    type="submit"
                                                    className="login-button btn btn-default"
                                                >
                                                    Log-in
                                                </button>
                                                <a href="/login/sign_up">
                                                    <button className="sign-up-button btn btn-default">
                                                        Sign Up
                                                    </button>
                                                </a>
                                                <div
                                                    id="borderLeft"
                                                    className="hidden-xs"
                                                />
                                            </div>
                                        </div>
                                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div className="login2">
                                                <a href="/auth/facebook">
                                                    <div className="login-btn-fb">
                                                        <span>
                                                            <i className="fa fa-facebook" />
                                                        </span>
                                                        <p>
                                                            Sign in using
                                                            Facebook
                                                        </p>
                                                    </div>
                                                </a>
                                                <a href="/auth/google">
                                                    <div className="login-btn-g">
                                                        <span>
                                                            <i className="fa fa-google-plus" />
                                                        </span>
                                                        <p>
                                                            Sign in using
                                                            Google+
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContainerComponent>
        );
    }
}

const mapStateToProps = state => state;

export default withRouter(
    connect(
        mapStateToProps,
        { ...userActions, ...sessionActions }
    )(LoginPage)
);
