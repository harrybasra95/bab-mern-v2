import React, { Component } from 'react';
import './style.styl';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userServices } from '../../services';
import { userValidations } from '../../validations';
import { userActions, sessionActions } from '../../actions';
import { colors } from '../../config';
import {
     ContainerComponent,
     Input,
     Button,
     FacebookAuth,
     GoogleAuth
} from '../../components';
const { loginUser, loginViaFacebook, loginViaGoogle } = userServices;
const { emailValidation, passwordValidation } = userValidations;

class LoginPage extends Component {
     constructor(props) {
          super(props);
          this.state = {
               username: '',
               password: ''
          };
     }

     async sendRequest() {
          const {
               showMessage,
               loadingState,
               setSessionData,
               history
          } = this.props;
          const { username, password } = this.state;
          if (!emailValidation(username).isSuccess) {
               return showMessage(emailValidation(username));
          }
          if (!passwordValidation(password).isSuccess) {
               return showMessage(passwordValidation(password));
          }
          loadingState(true);
          const data = await loginUser(username, password);
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          history.push('/');
          return setSessionData(data.data);
     }

     async facebookSuccess(response) {
          const {
               showMessage,
               loadingState,
               setSessionData,
               history
          } = this.props;
          loadingState(true);
          const data = await loginViaFacebook({ data: response });
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          history.push('/');
          return setSessionData(data.data);
     }
     async googleSuccess(response) {
          const {
               showMessage,
               loadingState,
               setSessionData,
               history
          } = this.props;
          loadingState(true);
          const data = await loginViaGoogle({ data: response });
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          history.push('/');
          return setSessionData(data.data);
     }

     render() {
          const { deviceHeight } = this.props;
          const { username, password } = this.state;
          return (
               <ContainerComponent checkoutPage subscribeHidden>
                    <div
                         className="login-box"
                         style={{ minHeight: deviceHeight }}
                    >
                         <div className="login-header-box f2">
                              <h1 className="header-text">Login</h1>
                              <div className="underline" />
                         </div>
                         <div className="login-form-box f2">
                              <form action="">
                                   <div className="login-email-box">
                                        <Input
                                             key="Email Address"
                                             theme="redTheme"
                                             value={username}
                                             onChange={({ target }) =>
                                                  this.setState({
                                                       username: target.value
                                                  })
                                             }
                                             inputType="standard"
                                             label="Email Address"
                                             labelColorFocused={colors.redColor}
                                        />
                                   </div>
                                   <div className="login-password-box">
                                        <Input
                                             key="Password"
                                             type="password"
                                             inputType="standard"
                                             theme="redTheme"
                                             defaultValue={password || null}
                                             onChange={({ target }) =>
                                                  this.setState({
                                                       password: target.value
                                                  })
                                             }
                                             label="Password"
                                             labelColorFocused={colors.redColor}
                                        />
                                        <div className="forget-password-box">
                                             <Link className="forget-password-text">
                                                  Forget Password?
                                             </Link>
                                        </div>
                                   </div>
                              </form>
                         </div>
                         <div className="login-button-box f1">
                              <Button
                                   onClick={() => this.sendRequest()}
                                   label="LOGIN"
                                   backgroundColor={colors.redColor}
                                   borderRadius="50px"
                              />
                         </div>
                         <div className="social-login-box f1">
                              <p className="login-with-text">or login with</p>
                              <div className="social-links-box">
                                   <GoogleAuth
                                        onSuccess={data =>
                                             this.googleSuccess(data)
                                        }
                                        render={props => (
                                             <div
                                                  onClick={props.onClick}
                                                  className="social-link"
                                             >
                                                  <img
                                                       src="/img/social-icons/google.png"
                                                       alt="google-social-sign-in"
                                                  />
                                             </div>
                                        )}
                                   />

                                   <FacebookAuth
                                        callback={data =>
                                             this.facebookSuccess(data)
                                        }
                                        render={props => (
                                             <div
                                                  onClick={props.onClick}
                                                  className="social-link"
                                             >
                                                  <img
                                                       src="/img/social-icons/facebook.png"
                                                       alt="facebook-social-sign-in"
                                                  />
                                             </div>
                                        )}
                                   />
                              </div>
                         </div>
                         <div className="sign-up-text-box f1">
                              <p className="sign-up-text">
                                   Don&apos;t Have an account?
                                   <Link to="/create-account"> Sign Up</Link>
                              </p>
                         </div>
                    </div>
               </ContainerComponent>
          );
     }
}

const mapStateToProps = state => {
     const { deviceHeight } = state.userReducer;
     return { deviceHeight };
};

export default withRouter(
     connect(
          mapStateToProps,
          { ...userActions, ...sessionActions }
     )(LoginPage)
);
