import React, { Component } from 'react';
import './style.styl';
import { orderDetailsData } from '../../staticData';
import { symbolchart } from '../../config';
import { orderServices } from '../../services';
import { connect } from 'react-redux';
import { userActions } from '../../actions';
import { ContainerComponent } from '../../components';

class OrderDetailsPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            order: {
                status: 'PP',
                persondetails: {},
                productdetails: [],
                deliveryaddress: {}
            }
        };
    }

    async componentDidMount() {
        const { loadingState, showMessage, match, location } = this.props;
        if (location.state) {
            return this.setState({
                order: { ...location.state.order },
                invoice: location.state.invoice
            });
        }
        const { orderNumber } = match.params;
        loadingState(true);
        const data = await orderServices.fetchSingleOrder(orderNumber);
        loadingState(false);
        if (!data.isSuccess) {
            return showMessage(data);
        }
        return this.setState({
            order: { ...data.data.order },
            invoice: data.data.invoice
        });
    }

    renderTrackBox() {
        const { order } = this.state;
        if (order.status === 'S' || order.status === 'D') {
            return (
                <div className="trackbox">
                    <div>
                        <span className="glyphicon glyphicon-remove" />
                        <h3>Tracking details</h3>
                        <table>
                            <tr>
                                <td className="d">Current Status</td>
                                <td className="b" id="track_status">
                                    -
                                </td>
                            </tr>
                            <tr>
                                <td className="d">Current Location</td>
                                <td className="b" id="currentloc">
                                    -
                                </td>
                            </tr>
                            <tr>
                                <td className="d">Exp. Delivery Date</td>
                                <td className="b" id="expdate">
                                    -
                                </td>
                            </tr>
                            <tr>
                                <td className="d">Waybill no.</td>
                                <td className="b">{order.waybill}</td>
                            </tr>
                        </table>
                        <div className="detailedtrack" />
                    </div>
                </div>
            );
        }
    }

    renderReturnBox() {
        const { order } = this.state;
        if (order.status == 'RRR') {
            var a = [
                'Yo',
                'Ticket Generated',
                'Item Recieved',
                'Inspection',
                'Refund Confirmed'
            ];
            return (
                <div className="return_status_box">
                    <div>
                        <span className="glyphicon glyphicon-remove" />
                        <h3 className="text-center">Return Status</h3>
                        <ol className="progress-track" />
                        <h5>
                            <span>Ticket No:</span>{' '}
                            {order.returndetails[0].ticketno}
                        </h5>
                        <h5>
                            <span>Comments:</span> {order.returndetails[0].note}
                        </h5>
                    </div>
                </div>
            );
        }
    }

    renderProducts() {
        const { order } = this.state;
        const { selectedCountry } = this.props;
        const returnArray = [];
        order.productdetails.forEach(
            ({
                items,
                product,
                productTotalQuantity,
                qty,
                productTotalPrice,
                price
            }) => {
                let singleProduct;
                if (items) {
                    singleProduct = {
                        ...items,
                        totalQuantity: qty,
                        totalPrice: price
                    };
                } else {
                    singleProduct = {
                        ...product,
                        totalQuantity: productTotalQuantity,
                        totalPrice: productTotalPrice
                    };
                }
                const quicklookLink = `/products/${
                    singleProduct.type
                }/quicklook/${singleProduct.uniqueid}`;
                returnArray.push(
                    <div className="cart-item">
                        <a href={quicklookLink}>
                            <img
                                title={singleProduct.name}
                                alt={`${singleProduct.type.toLowerCase()} ${singleProduct.name.toLowerCase()} ${
                                    singleProduct.colorname
                                } BAB`}
                                src={singleProduct.imgaddresses[0]}
                            />
                        </a>
                        <div className="item-details">
                            <a href={quicklookLink}>
                                <h5>
                                    {singleProduct.name} -{' '}
                                    {singleProduct.colorname.replace(/-/g, ' ')}{' '}
                                    - {singleProduct.size}
                                </h5>
                            </a>
                            <h5>Qty : {singleProduct.totalQuantity} </h5>
                            <h5>
                                {symbolchart[selectedCountry]}{' '}
                                {singleProduct.totalPrice}
                            </h5>
                        </div>
                    </div>
                );
            }
        );
        return returnArray;
    }

    render() {
        const {selectedCountry} = this.props;
        const { order, invoice, } = this.state;
        return (
            <ContainerComponent subscribeHidden>
                <div className="order_detail_box ">
                    <div className="container">
                        <div className="row">
                            <h2 className="text-center">Order Details</h2>
                            <div className="col-lg-12 text-left">
                                <div className="row">
                                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                        <div className="order_detail">
                                            <h5>
                                                {
                                                    orderDetailsData
                                                        .orderStatusDef[
                                                        order.status.toLowerCase()
                                                    ]
                                                }
                                            </h5>
                                        </div>
                                        <div className="order_detail">
                                            <h5>
                                                Order No. {order.ordernumber}
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className=" col-lg-12 col-md-12 col-sm-12 col-xs-12  ">
                                        <div className="product_details">
                                            <div className="track_and_invoice">
                                                {order.status == 'S' ||
                                                order.status == 'D' ? (
                                                    <a>
                                                        <h5
                                                            value={
                                                                order.waybill
                                                            }
                                                            id="trackOrder"
                                                        >
                                                            Track
                                                        </h5>
                                                    </a>
                                                ) : null}
                                                {this.renderTrackBox()}
                                                {order.status === 'D' ||
                                                order.status === 'S' ||
                                                order.status === 'RRR' ||
                                                order.status === 'OS' ? (
                                                    <a
                                                        href={`/invoices/${invoice}.jpeg`}
                                                        id="invoice"
                                                        download={`${
                                                            order.ordernumber
                                                        }-invoice`}
                                                    >
                                                        <h5>Invoice</h5>
                                                    </a>
                                                ) : null}
                                                {order.status === 'D' ? (
                                                    <a>
                                                        <h5 className="cancel_now">
                                                            Return
                                                        </h5>
                                                    </a>
                                                ) : null}
                                                {order.status === 'RRR' ? (
                                                    <a>
                                                        <h5
                                                            val="/order/returnstatus"
                                                            className="return_status"
                                                        >
                                                            Return Status
                                                        </h5>
                                                    </a>
                                                ) : null}
                                                {this.renderReturnBox()}
                                                {order.status == 'OR' ||
                                                order.status == 'PP' ? (
                                                    <a>
                                                        <h5 className="cancel_now">
                                                            Cancel
                                                        </h5>
                                                    </a>
                                                ) : null}
                                                <a>
                                                    <h5 id="contact">
                                                        Contact Us
                                                    </h5>
                                                </a>
                                                <div className="contactUs">
                                                    <div>
                                                        <span className="glyphicon glyphicon-remove" />
                                                        <h3>Write to us</h3>
                                                        <h5>
                                                            Name :{' '}
                                                            {
                                                                order
                                                                    .persondetails
                                                                    .fname
                                                            }{' '}
                                                            {
                                                                order
                                                                    .persondetails
                                                                    .lname
                                                            }
                                                        </h5>
                                                        <h5>
                                                            Order No :{' '}
                                                            {order.ordernumber}
                                                        </h5>
                                                        <textarea
                                                            name="issue"
                                                            placeholder="Issue details"
                                                        />
                                                        <button>Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <h5>Product details</h5>
                                            <div className="cart-info">
                                                <div className="products">
                                                    {this.renderProducts()}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-3 col-md-3 col-sm-4 col-xs-6  buyer_info">
                                        <div className="buyer_info_box">
                                            <h5>Buyer&apos;s information</h5>
                                            <table>
                                                <tr>
                                                    <td>
                                                        {
                                                            order.persondetails
                                                                .fname
                                                        }{' '}
                                                        {
                                                            order.persondetails
                                                                .lname
                                                        }
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        {
                                                            order.persondetails
                                                                .phone
                                                        }
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        {
                                                            order.persondetails
                                                                .eadd
                                                        }
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 col-md-3 col-sm-4 col-xs-6  d_info">
                                        <div className="d_info_box">
                                            <h5>Delivery Address</h5>
                                            <table>
                                                <tr>
                                                    <td>
                                                        {
                                                            order
                                                                .deliveryaddress
                                                                .street_no
                                                        }
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        {
                                                            order
                                                                .deliveryaddress
                                                                .city
                                                        }
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        {
                                                            order
                                                                .deliveryaddress
                                                                .state
                                                        }
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        {
                                                            order
                                                                .deliveryaddress
                                                                .country
                                                        }
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        {
                                                            order
                                                                .deliveryaddress
                                                                .pincode
                                                        }
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div className=" col-lg-3 col-md-3 col-sm-4 col-xs-6  p_info">
                                        <div className="p_info_box">
                                            <h5>Payment details</h5>
                                            <table>
                                                <tr>
                                                    <td>Payment Type</td>
                                                    {order.paymenttype ==
                                                    'cod' ? (
                                                        <td>COD</td>
                                                    ) : (
                                                        <td>Prepaid</td>
                                                    )}
                                                </tr>
                                                <tr>
                                                    <td>Subtotal </td>
                                                    <td>
                                                        {
                                                            symbolchart[
                                                                selectedCountry
                                                            ]
                                                        }{' '}
                                                        {order.subtotalprice}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Shipping</td>
                                                    <td>
                                                        {order.shipping !== 0
                                                            ? `${
                                                                  symbolchart[
                                                                      selectedCountry
                                                                  ]
                                                              } ${
                                                                  order.shipping
                                                              }`
                                                            : 'FREE'}
                                                    </td>
                                                </tr>
                                                {order.coupon ? (
                                                    <tr>
                                                        <td>Discount</td>
                                                        <td>
                                                            -
                                                            {
                                                                symbolchart[
                                                                    selectedCountry
                                                                ]
                                                            }{' '}
                                                            {order.discount}
                                                        </td>
                                                    </tr>
                                                ) : null}
                                                {order.paymenttype == 'cod' ? (
                                                    <tr>
                                                        <td>
                                                            Extra Charges(COD)
                                                        </td>
                                                        <td>
                                                            {
                                                                symbolchart[
                                                                    selectedCountry
                                                                ]
                                                            }{' '}
                                                            50
                                                        </td>
                                                    </tr>
                                                ) : null}
                                                <tr className="totalprice_row">
                                                    <td>Total</td>
                                                    <td>
                                                        {
                                                            symbolchart[
                                                                selectedCountry
                                                            ]
                                                        }{' '}
                                                        {order.totalprice}
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContainerComponent>
        );
    }
}

const mapStateToProps = state => {
    const { selectedCountry } = state.userReducer;
    return { selectedCountry };
};

export default connect(
    mapStateToProps,
    userActions
)(OrderDetailsPage);
