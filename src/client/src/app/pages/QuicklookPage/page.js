import React, { Component } from 'react';
import './style.styl';
import { userActions, sessionActions, cartActions } from '../../actions';
import { Link } from 'react-router-dom';
import cn from 'classnames';
import {FaHeart,FaCheck} from 'react-icons/fa';
import ReactImageMagnify from 'react-image-magnify';
import { connect } from 'react-redux';
import  'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';
import { ContainerComponent } from '../../components';
import { productServices, cartServices } from '../../services';

class QuicklookPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showDetails: 'productInfo',
            showSizeGuide: false,
            selectedSize: null,
            currentShowCaseImage: '',
            pd: {
                imgaddresses: [],
                highlights: [],
                washCare: [],
                series: ''
            }
        };
    }

    async componentDidMount() {
        const { loadingState, showMessage } = this.props;
        const { type, id } = this.props.match.params;
        loadingState(true);
        const data = await productServices.fetchProductData(type, id);
        loadingState(false);
        if (!data.isSuccess) {
            return showMessage(data);
        }
        const pd = data.data;
        this.setState({
             pd: { ...pd },
             currentShowCaseImage: pd.imgaddresses[0].replace(
                  '.jpg',
                  '.jpg'
             )
        });
    }

    loadImage(src){
        const {webpEnabled} = this.props;
        if(webpEnabled){
            return src.replace('.jpg','.webp');
        }
        return src;
    }

    renderProductName() {
        const { pd } = this.state;
        return (
            <div>
                <h1 className="text-center p_heading">
                    <span property="name">
                        {pd.name}
                        <br />
                        <span className="colorname">{pd.colorName}</span>
                    </span>
                </h1>
            </div>
        );
    }

    renderImageShowcase() {
        const { currentShowCaseImage, pd } = this.state;
        return (
             <div>
                  <div
                       val={pd.imgno}
                       className="col-lg-2 col-md-2 hidden-sm  hidden-xs sample_images"
                  >
                       {pd.imgaddresses.map(src => (
                            <img
                                 onClick={() =>
                                      this.setState({
                                           currentShowCaseImage: this.loadImage(
                                                src
                                           )
                                      })
                                 }
                                 key={src}
                                 property="image"
                                 src={this.loadImage(src)}
                                 className="sample"
                            />
                       ))}
                  </div>
                  <div className="qboxleft col-lg-6 col-md-6 col-sm-7 text-center">
                       <div className="image-slideshow-desktop">
                            <ReactImageMagnify
                                 enlargedImagePosition="over"
                                 {...{
                                      smallImage: {
                                           isFluidWidth: true,
                                           src: this.loadImage(
                                                currentShowCaseImage
                                           )
                                      },
                                      largeImage: {
                                           src: this.loadImage(
                                                currentShowCaseImage
                                           ),
                                           width: 800,
                                           height: 800
                                      }
                                 }}
                            />
                       </div>
                       <div className="image-slideshow-mobile">
                            <Carousel
                                 showArrows={false}
                                 showStatus={false}
                                 infiniteLoop={true}
                            >
                                 {pd.imgaddresses.map(src => (
                                      <div key={src}>
                                           <img
                                                src={this.loadImage(
                                                     src
                                                )}
                                           />
                                      </div>
                                 ))}
                            </Carousel>
                       </div>
                  </div>
             </div>
        );
    }

    renderProuctPrice() {
        const { pd } = this.state;
        if (pd.sale === 0) {
            return (
                <h3 className="price">
                    <span property="price">
                        {pd.currencySymbol} {pd.price}
                    </span>
                </h3>
            );
        } else {
            return (
                <h3 className="price ">
                    <span className="cutted-price">
                        {pd.currencySymbol} {pd.originalPrice}
                    </span>
                    <span property="price">
                        {pd.currencySymbol} {pd.price}
                    </span>
                </h3>
            );
        }
    }

    renderProductColors() {
        const { pd } = this.state;
        const returnData = [];
        if (pd.colorsChart) {
            Object.keys(pd.colorsChart).forEach(key => {
                const {
                    productLink,
                    divTitle,
                    colorCode,
                    secondcolor
                } = pd.colorsChart[key];
                returnData.push(
                    <Link className="color-link" to={productLink}>
                        <div
                            title={divTitle}
                            style={{ backgroundColor: colorCode }}
                            className="color"
                        >
                            {secondcolor ? (
                                <div style={{ backgroundColor: secondcolor }} />
                            ) : null}
                        </div>
                    </Link>
                );
            });
        }
        return returnData;
    }

    renderProductSizes() {
        const { pd, selectedSize } = this.state;
        const returnData = [];
        if (pd.quantityChart) {
            Object.keys(pd.quantityChart).forEach(key => {
                const { qty, short } = pd.quantityChart[key];
                returnData.push(
                    <div
                        onClick={() => this.setState({ selectedSize: key })}
                        className={cn(
                            { outOfStock: qty < 1 },
                            { selected: selectedSize === key }
                        )}
                    >
                        <p>{short.toUpperCase()}</p>
                        <span />
                    </div>
                );
            });
        }
        return (
            <div className={cn({ 'total-stockout': pd.totalQty < 1 })}>
                {returnData}
            </div>
        );
    }

    renderSizeGuide() {
        const { pd } = this.state;
        const sizeGuideObj = {
            other: {
                title: ['Chest Measurement', 'Size'],
                width: '50%',
                data: [
                    { c1: '34-36', c2: 'S' },
                    { c1: '37-39', c2: 'M' },
                    { c1: '40-42', c2: 'L' },
                    { c1: '43-45', c2: 'XL' }
                ],
                note:
                    'If you’re on the borderline between two sizes, order the smaller size for a tighter fit or the larger size for a looser fit'
            },
            fit: {
                title: ['Waist Measurement', 'Hip Measurement', 'Size'],
                width: '33.34%',
                data: [
                    { c1: '32', c2: '39', c3: 'S' },
                    { c1: '34', c2: '41', c3: 'M' },
                    { c1: '36', c2: '43', c3: 'L' },
                    { c1: '38', c2: '45', c3: 'XL' }
                ],
                note:
                    'If your measurements for hips and waist correspond to two different suggested sizes , order the one indicated by your hip measurement .'
            }
        };
        const currentSizeGuide =
            sizeGuideObj[pd.series === 'fit' ? 'fit' : 'other'];
        return (
            <div className="size-guide show">
                <div>
                    <span
                        onClick={() => this.setState({ showSizeGuide: false })}
                        className="glyphicon glyphicon-remove"
                    />
                    <h3>Size Guide</h3>
                    <table>
                        <tr>
                            {currentSizeGuide.title.map(item => (
                                <th
                                    key={item}
                                    width={currentSizeGuide.width}
                                    className="text-center border-right"
                                >
                                    {item}
                                </th>
                            ))}
                        </tr>
                        {currentSizeGuide.data.map(item => (
                            <tr key={item.c1}>
                                {Object.keys(item).map(key => (
                                    <td
                                        key={key}
                                        width={currentSizeGuide.width}
                                        className="text-center border-bottom"
                                    >
                                        {item[key]}
                                    </td>
                                ))}
                            </tr>
                        ))}
                    </table>
                    <p>{currentSizeGuide.note}</p>
                </div>
            </div>
        );
    }

    renderProductDetails() {
        const { pd, showDetails } = this.state;
        return (
             <div
                  onMouseLeave={() =>
                       this.setState({ showDetails: null })
                  }
                  className="mobile-details  hidden-sm hidden-lg hidden-md"
             >
                  <div
                       onClick={() =>
                            this.setState({
                                 showDetails: 'features'
                            })
                       }
                  >
                       <p>FEATURES</p>
                       <span className="glyphicon glyphicon-plus" />
                       <div
                            className={cn('content', {
                                 'max-height':
                                      showDetails === 'features'
                            })}
                       >
                            <p>{pd.pagedescription}</p>
                       </div>
                  </div>
                  <div
                       onClick={() =>
                            this.setState({
                                 showDetails: 'details'
                            })
                       }
                  >
                       <p>DETAILS</p>
                       <span className="glyphicon glyphicon-plus" />
                       <div
                            className={cn('content', {
                                 'max-height': showDetails === 'details'
                            })}
                       >
                            <p>
                                 <span>Fabric</span> :{pd.fabric}
                            </p>
                            <p>
                                 <span>Highlights</span> :{' '}
                            </p>
                            <ul>
                                 {pd.highlights.map(item => (
                                      <li key={item}>
                                           <FaCheck />
                                           <p>{item}.</p>
                                      </li>
                                 ))}
                            </ul>
                       </div>
                  </div>
                  <div
                       onClick={() =>
                            this.setState({
                                 showDetails: 'care'
                            })
                       }
                  >
                       <p>FABRIC & CARE</p>
                       <span className="glyphicon glyphicon-plus" />
                       <div
                            className={cn('content', {
                                 'max-height': showDetails === 'care'
                            })}
                       >
                            <p>
                                 <span>Wash Care</span> :{' '}
                            </p>
                            <ul>
                                 {pd.washCare.map(item => (
                                      <li key={item}>
                                           <FaCheck />
                                           <p>{item}</p>
                                      </li>
                                 ))}
                            </ul>
                       </div>
                  </div>
             </div>
        );
    }

    async addToCart(id) {
        const { selectedSize } = this.state;
        const {
            loadingState,
            setSessionData,
            showMessage,
            showCart
        } = this.props;
        if (selectedSize === null) {
            return showMessage('Please select a valid size');
        }
        loadingState(true);
        const data = await cartServices.addToCartByUniqueId(
            id,
            selectedSize,
            1
        );
        loadingState(false);
        if (!data.isSuccess) {
            return showMessage(data);
        }
        setSessionData(data.data);
        return showCart(true);
    }

    render() {
        const { pd, showSizeGuide, showDetails } = this.state;
        const { isMobile } = this.props;
        return (
            <ContainerComponent>
                <div
                    vocab="http://schema.org/"
                    typeof="Product"
                    className="quicklook"
                >
                    <div className="container">
                        <div className="row">
                            <div className="inner-box">
                                {!isMobile ? this.renderProductName() : null}
                                {this.renderImageShowcase()}
                                {isMobile ? this.renderProductName() : null}
                                {isMobile ? this.renderProuctPrice() : null}

                                <div className="qboxright col-lg-4 col-md-4 col-sm-5 text-left">
                                    <div className="hidden-xs">
                                        <br />
                                    </div>
                                    <span property="offers" typeof="Offer">
                                        <meta
                                            property="priceCurrency"
                                            content={pd.currenyCode}
                                        />
                                        {!isMobile
                                            ? this.renderProuctPrice()
                                            : null}
                                        {pd.totalQty > 0 ? (
                                            <h3 className="stock-info hidden-xs">
                                                <link
                                                    property="availability"
                                                    href="http://schema.org/InStock"
                                                />
                                                In stock
                                            </h3>
                                        ) : (
                                            <h3 className="stock-info hidden-xs">
                                                Out of stock
                                            </h3>
                                        )}
                                    </span>
                                    <div className="border hidden-xs" />
                                    <div className="col-left ">
                                        <div className="color_div">
                                            <h4>Choose Color &nbsp;</h4>
                                            <div className="text-center">
                                                {this.renderProductColors()}
                                            </div>
                                        </div>
                                        <div className="share_icons hidden-xs">
                                            <h4>Share it: </h4>
                                            <a
                                                href={`https://www.facebook.com/sharer/sharer.php?u=https://in.bab.store/products/${
                                                    pd.type
                                                }/quicklook/${pd.uniqueid}`}
                                            >
                                                <i className="fa fa-facebook" />
                                            </a>
                                            <a
                                                href={`https://twitter.com/home?status=https://in.bab.store/products/${
                                                    pd.type
                                                }/quicklook/${pd.uniqueid}`}
                                            >
                                                <i className="fa fa-twitter" />
                                            </a>
                                        </div>
                                    </div>
                                    <div
                                        id="select_coloumn"
                                        className="col-right"
                                    >
                                        <div className="select-size ">
                                            <h3>Choose Size</h3>
                                            <h4
                                                onClick={() =>
                                                    this.setState({
                                                        showSizeGuide: true
                                                    })
                                                }
                                                className="size-guide-button"
                                            >
                                                Size Guide
                                            </h4>
                                            {this.renderProductSizes()}
                                        </div>
                                        {showSizeGuide
                                            ? this.renderSizeGuide()
                                            : null}
                                    </div>
                                    <p className="articles-left" />
                                    <div className="price-outer">
                                        <div
                                            onClick={() =>
                                                this.addToCart(pd.uniqueid)
                                            }
                                            id="quicklook_cart_button"
                                            className="text-center placeorder invalid_input"
                                        >
                                            <h3>Add To Cart</h3>
                                        </div>
                                        <div className="add-to-wishlist">
                                            <FaHeart></FaHeart>
                                        </div>
                                    </div>
                                    <div className="points">
                                        <div>
                                            <FaCheck></FaCheck>
                                            <p>
                                                Free Shipping Over Rs. 500 (For
                                                India Only).
                                            </p>
                                        </div>
                                        <div>
                                            <FaCheck></FaCheck>
                                            <p>
                                                Easy Returns / Exchanges. (
                                                <a href="/return">
                                                    Return Policy
                                                </a>
                                                )
                                            </p>
                                        </div>
                                        <div>
                                            <FaCheck></FaCheck>
                                            <p>
                                                14 Days Returns. (
                                                <a href="/return">
                                                    Return Policy
                                                </a>
                                                )
                                            </p>
                                        </div>
                                    </div>
                                    <div className="mobile-share hidden-sm hidden-lg hidden-md">
                                        <p>Share</p>
                                        <div>
                                            <a
                                                href={pd.pinterestShareLink}
                                                className="pin-it-button"
                                                count-layout="horizontal"
                                            >
                                                <img
                                                    src="/img/pinterest.png"
                                                    title="Pin It"
                                                />
                                            </a>
                                            <a href={pd.twitterShareLink}>
                                                <img src="/img/tw.png" />
                                            </a>
                                            <a href={pd.facebookShareLink}>
                                                <img src="/img/fb.png" />
                                            </a>
                                        </div>
                                    </div>
                                    {this.renderProductDetails()}
                                    <div className="row text-center hidden-xs">
                                        <div className="product-info">
                                            <div className="tabs">
                                                <div
                                                    onClick={() =>
                                                        this.setState({
                                                            showDetails:
                                                                'productInfo'
                                                        })
                                                    }
                                                    className={cn({
                                                        on:
                                                            showDetails ===
                                                            'productInfo'
                                                    })}
                                                >
                                                    Product info
                                                    <span />
                                                </div>
                                                <div
                                                    onClick={() =>
                                                        this.setState({
                                                            showDetails:
                                                                'washCare'
                                                        })
                                                    }
                                                    className={cn({
                                                        on:
                                                            showDetails ===
                                                            'washCare'
                                                    })}
                                                >
                                                    Wash Care
                                                    <span />
                                                </div>
                                            </div>
                                            <div className="content">
                                                <div
                                                    className={cn('a1', {
                                                        hidden:
                                                            showDetails !==
                                                            'productInfo'
                                                    })}
                                                >
                                                    <span
                                                        property="description"
                                                        dangerouslySetInnerHTML={{
                                                            __html:
                                                                pd.description
                                                        }}
                                                    />
                                                </div>
                                                <div
                                                    className={cn('a2', {
                                                        hidden:
                                                            showDetails !==
                                                            'washCare'
                                                    })}
                                                    dangerouslySetInnerHTML={{
                                                        __html: pd.washCare
                                                    }}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="about-us-quicklook">
                        <div
                            style={{ overflow: 'hidden' }}
                            className="about-us-quicklook-outer text-center"
                        >
                            <img className="" src={pd.showCaseImageUrl} />
                        </div>
                        <div className="">
                            <div className="about-us-quicklook-inner">
                                <h2>{pd.series.toUpperCase()} SERIES</h2>

                                <div>
                                    <h4>{pd.seriesinfo}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContainerComponent>
        );
    }
}

const mapStateToProps = state => {
    const {isMobile,webpEnabled} = state.userReducer;
    return { isMobile,webpEnabled };
};

export default connect(
    mapStateToProps,
    { ...userActions, ...sessionActions, ...cartActions }
)(QuicklookPage);
