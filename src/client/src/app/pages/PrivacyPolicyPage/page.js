import React, { Component } from 'react';
import './style.styl';
import { ContainerComponent } from '../../components';

class PrivacyPolicyPage extends Component {
     render() {
          return (
               <ContainerComponent>
                    <div class="container">
                         <div class="privacypolicy">
                              <h1 class="text-center">PRIVACY POLICY</h1>
                              <h4>WHAT INFORMATION WE COLLECT</h4>
                              <h4>
                                   WE DO NOT STORE CREDIT/DEBIT CARD DETAILS NOR
                                   DO WE SHARE CUSTOMER DETAILS WITH ANY 3RD
                                   PARTIES
                              </h4>
                              <p>
                                   The information we collect via this website
                                   may include:
                              </p>
                              <ol>
                                   <li>
                                        Any personal details you type in and
                                        submit, such as name, address, email
                                        address, etc.
                                   </li>
                                   <li>
                                        Data which allows us to recognize you,
                                        your preferences and how you use this
                                        website. This saves you from re-entering
                                        information when you return to the site.
                                        This data is collected by cookies from
                                        your navigation around the site. A
                                        cookie is a small amount of data which
                                        we send to your computer. The data is
                                        then stored on your browser or hard
                                        disk. Most web browsers can be set to
                                        prevent you from receiving new cookies,
                                        notify you before accepting cookies or
                                        disable cookies altogether. Details
                                        about this can normally be found in the
                                        Help facility provided with your
                                        browser. If you disable cookies, you
                                        will not be able to receive personalized
                                        facilities and your progress to check
                                        out may take longer. The cookies store
                                        information only for as long as you are
                                        viewing the website.
                                   </li>
                                   <li>
                                        Your IP address (this is your computer’s
                                        individual identification number) which
                                        is automatically logged by our web
                                        server. This is used to note your
                                        interest in our website.
                                   </li>
                                   <li>
                                        Your preferences and use of email
                                        updates, recorded by emails we send you
                                        (if you select to receive email updates
                                        on products and offers).
                                   </li>
                              </ol>
                              <h4>WHAT WE DO WITH YOUR INFORMATION</h4>
                              <p>
                                   Any personal information we collect from you
                                   will be used in accordance with the Right to
                                   Information Act,2005 and other applicable
                                   laws. The details we collect will be used:-
                              </p>
                              <ol>
                                   <li>
                                        To process your order, to maintain
                                        guarantee records and to provide
                                        after-sales service (we may pass your
                                        details to another organization to
                                        supply/deliver products or services you
                                        have purchased and/or to provide
                                        after-sales service).
                                   </li>
                                   <li>
                                        To carry out security checks (this may
                                        involve passing your details to credit
                                        reference agencies, who will check
                                        details we give them against public and
                                        private databases and may keep a record
                                        of that check to use in future security
                                        checks - this helps to protect you and
                                        us from fraudulent transactions)
                                   </li>
                                   <li>
                                        To comply with legal requirements.
                                        <br />
                                        We may use third parties to carry out
                                        certain activities, such as processing
                                        and sorting data, monitoring how
                                        customers use our site and issuing our
                                        emails for us.
                                   </li>
                                   <li />
                              </ol>
                              <p>
                                   We would also like to inform you of various
                                   promotions, goods and services that may be of
                                   interest to you. You may be contacted by
                                   post, email, telephone, SMS or such other
                                   means as we regard as appropriate, including
                                   new technology. If you wish to receive these
                                   communications, please tick the "Yes" box
                                   when entering your personal details. You may
                                   unsubscribe at a later date, if you wish -
                                   see "Your rights" section below.{' '}
                              </p>
                              <h4>YOUR RIGHTS</h4>
                              <ol>
                                   <li>
                                        You can ask us to update your personal
                                        information by emailing precise details
                                        of your request to us support@bab.store.
                                        Please ensure you type "update" in the
                                        subject line of your email.
                                   </li>
                                   <li>
                                        You can ask us not to send you future
                                        marketing communications. To do this,
                                        simply email us support@bab.store. Type
                                        "No Marketing" in the subject line and
                                        then the following in the main text:-
                                        <br />
                                        your postal address, including postcode,
                                        your email address
                                        <br />
                                        We will then suppress your data from all
                                        direct marketing, although we will still
                                        keep it for processing your order and
                                        fulfilling customer service and after
                                        sale obligations.
                                        <br />
                                        Alternatively, you can post your request
                                        to the address in (3) below.
                                   </li>
                                   <li>
                                        You also have the right to request a
                                        copy of any personal information we hold
                                        about you. To do this, simply write to
                                        us at the following address:
                                        <br />
                                        BAB HEADQUARTERS
                                        <br />
                                        HARRYTEJ ENTERPRISES
                                        <br />
                                        W.P. 181/A BASTI SHEIKH,
                                        <br />
                                        JALANDHAR,PUNJAB-144002
                                        <br />
                                        INDIA
                                   </li>
                              </ol>
                         </div>
                    </div>
               </ContainerComponent>
          );
     }
}

export default PrivacyPolicyPage;
