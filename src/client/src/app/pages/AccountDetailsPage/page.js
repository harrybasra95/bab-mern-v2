import React, { Component } from 'react';
import { Header, Footer } from '../../components';

export default class AccountDetailsPage extends Component {
    renderAddresses() {
        const returnArray = [];
        user.address.forEach(singleAddress => {
            returnArray.push(
                <div className="address-details">
                    <b>
                        {singleAddress.fname}
                        {singleAddress.lname}
                    </b>
                    ,<br />
                    {singleAddress.phone},<br />
                    {singleAddress.street_no},<br />
                    {singleAddress.city},<br />
                    {singleAddress.state},<br />
                    {singleAddress.pincode},<br />
                    {singleAddress.country}
                    <br />
                    <button value={singleAddress._id} id="delete_address">
                        <span className="glyphicon glyphicon-remove" />
                    </button>
                </div>
            );
        });
        return returnArray;
    }
    render() {
        return (
            <div>
                <Header />
                <div className="account-details">
                    <div className="container">
                        <div className="breadcrumbs">
                            <a href="/">Home</a>
                            <span>/</span>
                            <a href="/account_details">Accounts</a>
                        </div>
                        <div className="account-section">
                            <h2 className="text-center">Account Details</h2>
                            <ul className="header-list">
                                <li
                                    value="1"
                                    className="account_details p_details"
                                >
                                    Personal Details
                                </li>
                                <li
                                    value="2"
                                    className="account_details addresses"
                                >
                                    Addresses
                                </li>
                                <li
                                    value="3"
                                    className="account_details change_pass"
                                >
                                    Change Password
                                </li>
                            </ul>
                            <div className="accounts_box">
                                <div
                                    id="account-info-box"
                                    className="accounts account-info-box col-lg-12 col-md-12 col-sm-12"
                                >
                                    <div className="account-info">
                                        <table>
                                            <tr>
                                                <td className="i_feilds">
                                                    <h5>Name </h5>
                                                </td>
                                                <td>
                                                    <h5>
                                                        {user.fname +
                                                            ' ' +
                                                            user.lname}
                                                    </h5>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td className="i_feilds">
                                                    <h5>Phone Number</h5>
                                                </td>
                                                <td>
                                                    <h5>{user.phone}</h5>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td className="i_feilds">
                                                    <h5>Email</h5>
                                                </td>
                                                <td>
                                                    <h5>{user.email}></h5>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td className="i_feilds">
                                                    <h5>Date Of Birth </h5>
                                                </td>
                                                <td>
                                                    <h5>
                                                        {user.dob
                                                            .toDateString()
                                                            .slice(4)}
                                                    </h5>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td className="i_feilds">
                                                    <h5>Gender</h5>
                                                </td>
                                                <td>
                                                    <h5>{user.gender}</h5>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div
                                    id="addresses"
                                    className="accounts addresses col-lg-12 col-md-12 col-sm-12"
                                >
                                    <div className="row address-details-box ">
                                        {this.renderAddresses()}
                                        <div className="add_new_address_box">
                                            <div>
                                                <button id="add_new_address">
                                                    <span className="glyphicon glyphicon-plus" />
                                                </button>
                                                <h5>Add New Address</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div
                                    id="c_password"
                                    style="z-index: 600"
                                    className="accounts c_password col-lg-12 col-md-12 col-sm-12"
                                >
                                    <div className="changepass">
                                        <form
                                            method="post"
                                            action="/updatePass"
                                        >
                                            <div className="pass-change-form">
                                                <div className="row">
                                                    <div className="col-xs-12">
                                                        <div className="outer-box name">
                                                            <input
                                                                type="password"
                                                                required=""
                                                                id="oldpassword"
                                                                name="oldpassword"
                                                            />
                                                            <label htmlFor="oldpassword">
                                                                Old Password
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div className="col-xs-12">
                                                        <div className="outer-box name">
                                                            <input
                                                                type="password"
                                                                required=""
                                                                id="newpassword"
                                                                name="newpassword"
                                                            />
                                                            <label htmlFor="newpassword">
                                                                New Password
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div className="col-xs-12">
                                                        <div className="outer-box name">
                                                            <input
                                                                type="password"
                                                                required=""
                                                                id="newcpassword"
                                                                name="newcpassword"
                                                            />
                                                            <label htmlFor="newcpassword">
                                                                Confirm Password
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div className="col-xs-12">
                                                        <input
                                                            type="hidden"
                                                            name="_csrf"
                                                            value={csrfToken}
                                                        />
                                                        <button>Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}
