import React, { Component } from 'react';
import './style.styl';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userServices } from '../../services';
import { userValidations } from '../../validations';
import { userActions, sessionActions } from '../../actions';
import { colors } from '../../config';
import { ContainerComponent, Input, Button } from '../../components';
const { createUser } = userServices;
const { emailValidation, passwordValidation, nameValidation } = userValidations;

class SignUpPage extends Component {
     constructor(props) {
          super(props);
          this.state = {
               username: '',
               password: ''
          };
     }

     async sendRequest() {
          const {
               showMessage,
               loadingState,
               setSessionData,
               history
          } = this.props;
          const { username, password, name } = this.state;
          if (!nameValidation(name).isSuccess) {
               return showMessage(nameValidation(name));
          }
          if (!emailValidation(username).isSuccess) {
               return showMessage(emailValidation(username));
          }
          if (!passwordValidation(password).isSuccess) {
               return showMessage(passwordValidation(password));
          }
          loadingState(true);
          const data = await createUser({ username, password, name });
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          history.push('/');
          return setSessionData(data.data);
     }

     render() {
          const { deviceHeight } = this.props;
          const { username, password, name } = this.state;
          return (
               <ContainerComponent checkoutPage subscribeHidden>
                    <div
                         className="sign-up-box"
                         style={{ minHeight: deviceHeight }}
                    >
                         <div className="sign-up-header-box f2">
                              <h1 className="header-text">Create Account</h1>
                              <div className="underline" />
                         </div>
                         <div className="sign-up-form-box f2">
                              <form action="">
                                   <div className="sign-up-email-box">
                                        <Input
                                             key="Name"
                                             theme="redTheme"
                                             value={name}
                                             onChange={({ target }) =>
                                                  this.setState({
                                                       name: target.value
                                                  })
                                             }
                                             inputType="standard"
                                             label="Name"
                                             labelColorFocused={colors.redColor}
                                        />
                                   </div>

                                   <div className="sign-up-email-box">
                                        <Input
                                             key="Email Address"
                                             theme="redTheme"
                                             value={username}
                                             onChange={({ target }) =>
                                                  this.setState({
                                                       username: target.value
                                                  })
                                             }
                                             inputType="standard"
                                             label="Email Address"
                                             labelColorFocused={colors.redColor}
                                        />
                                   </div>
                                   <div className="sign-up-password-box">
                                        <Input
                                             key="Password"
                                             type="password"
                                             inputType="standard"
                                             theme="redTheme"
                                             defaultValue={password || null}
                                             onChange={({ target }) =>
                                                  this.setState({
                                                       password: target.value
                                                  })
                                             }
                                             label="Password"
                                             labelColorFocused={colors.redColor}
                                        />
                                   </div>
                              </form>
                         </div>
                         <div className="sign-up-button-box f1">
                              <Button
                                   onClick={() => this.sendRequest()}
                                   label="CREATE ACCOUNT"
                                   backgroundColor={colors.redColor}
                                   borderRadius="50px"
                              />
                         </div>
                         <div className="login-text-box f1">
                              <p className="login-text">
                                   Already Have an account?
                                   <Link to="/login"> Login</Link>
                              </p>
                         </div>
                    </div>
               </ContainerComponent>
          );
     }
}

const mapStateToProps = state => {
     const { deviceHeight } = state.userReducer;
     return { deviceHeight };
};

export default withRouter(
     connect(
          mapStateToProps,
          { ...userActions, ...sessionActions }
     )(SignUpPage)
);
