import React, { Component } from 'react';
import './style.styl';
import { ContainerComponent } from '../../components';

export default class ReturnPolicyPage extends Component {
     render() {
          return (
               <ContainerComponent>
                    <div class="container">
                         <div class="returns_refunds text-center">
                              <h1 class="text-center">RETURN POLICY</h1>
                              <h4>
                                   You can <a href="/contact_us">click here</a>{' '}
                                   or write to us at{' '}
                                   <span
                                        style={{
                                             'text-transform': 'lowercase'
                                        }}
                                   >
                                        support@babclothing.com
                                   </span>{' '}
                                   returning an item.
                              </h4>
                              <h4>
                                   We guarantee returns and replacement for any
                                   unused/sealed products till 14 business days
                                   of the order.
                              </h4>
                              <h4>
                                   Our return policy lasts 14 days. If 14 days
                                   have gone by since your purchase,
                                   unfortunately we can’t offer you a refund or
                                   exchange.
                              </h4>
                              <p>
                                   To be eligible for a return, your item must
                                   be unused and in the same condition that you
                                   received it. It must also be in the original
                                   packaging and tags. To complete your return,
                                   we require a receipt or proof of purchase.
                              </p>
                              <h4>1. Refunds (if applicable)</h4>
                              <p>
                                   Once your return is received and inspected,
                                   we will send you an email to notify you that
                                   we have received your returned item. We will
                                   also notify you of the approval or rejection
                                   of your refund. If you are approved, then
                                   your refund will be processed, and a credit
                                   will automatically be applied to your credit
                                   card or original method of payment, within a
                                   certain amount of days.
                              </p>
                              <p>
                                   <b>
                                        <u>
                                             Note : Shipping cost of the return
                                             has to be beared by you.
                                        </u>
                                   </b>
                              </p>
                              <h4>
                                   2. Late or missing refunds (if applicable)
                              </h4>
                              <p>
                                   If you haven’t received a refund yet, first
                                   check your bank account again. Then contact
                                   your credit card company, it may take some
                                   time before your refund is officially posted.
                                   Next contact your bank. There is often some
                                   processing time before a refund is posted. If
                                   you’ve done all of this and you still have
                                   not received your refund yet, please contact
                                   us at support@babclothing.com.
                              </p>
                              <h4>3. Exchanges (if applicable)</h4>
                              <p>
                                   We only replace items if you have ordered a
                                   wrong size. If you need to exchange it for
                                   the same item, send us an email at
                                   support@babclothing.com.
                              </p>
                              <h1 class="text-center">International Returns</h1>
                              <p>
                                   We understand that an item may be unsuitable
                                   from time to time, you're welcome to return
                                   your item back to us.
                              </p>
                              <p>
                                   Write to us at support@babclothing.com
                                   returning an item.
                              </p>
                              <p>
                                   You are liable for the cost of returning your
                                   item. We recommend returning your order via
                                   your local postal service on a standard
                                   service. Please keep all postage receipts,
                                   just in case your order is lost in transit.{' '}
                              </p>
                              <p>
                                   Please remember to mark your package as
                                   'returned goods' along with your contact
                                   details.
                              </p>
                              <p>
                                   The parcel is the customers responsibility up
                                   until BAB has acknowledged that the package
                                   has been received into our warehouse.
                              </p>
                              <p>
                                   Once we've received your return back in our
                                   warehouse and it has been processed, you'll
                                   receive an email confirming what action has
                                   been taken.{' '}
                              </p>
                              <h4
                                   style={{
                                        'word-spacing': '2px'
                                   }}
                              >
                                   Returns Address:
                              </h4>
                              <p>
                                   BAB HEADQUARTERS
                                   <br />
                                   HARRYTEJ ENTERPRISES
                                   <br />
                                   W.P. 181/A BASTI SHEIKH,
                                   <br />
                                   JALANDHAR,PUNJAB-144002
                                   <br />
                                   INDIA
                              </p>
                         </div>
                    </div>
               </ContainerComponent>
          );
     }
}
