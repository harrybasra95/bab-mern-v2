import React, { Component } from 'react';
import './style.styl';
import {ContainerComponent} from '../../components';

class TermsAndConditionsPage extends Component {
     render() {
          return (
               <ContainerComponent>
                    <div class="container">
                         <div class="toc">
                              <h1 class="text-center">
                                   TERMS AND CONDITIONS
                              </h1>
                              <h3 class="text-center">
                                   BAB is a registered consumer brand
                                   of HARRYTEJ ENTERPRISES.
                              </h3>
                              <h4>1. PLACING AN ORDER.</h4>
                              <ul>
                                   <li>
                                        Find the product(s) you want
                                        on the WWW.BAB.STORE website.
                                   </li>
                                   <li>
                                        Select the size you require
                                        and press ‘Add to shopping
                                        cart’.
                                   </li>
                                   <li>
                                        Once you’ve added all of the
                                        items you require to your
                                        basket, select checkout
                                        option.
                                   </li>
                                   <li>
                                        You will be asked to input
                                        your shipping & billing
                                        details.
                                   </li>
                                   <li>
                                        Review your order to ensure
                                        all items & details are
                                        correct.
                                   </li>
                                   <li>Press ‘Place Order’.</li>
                                   <li>
                                        You will then receive an order
                                        confirmation via email to the
                                        email address provided at the
                                        time of ordering.
                                   </li>
                              </ul>
                              <p>
                                   In the unlikely circumstance we’d
                                   be unable to fulfill your order for
                                   whatever reason, an email will be
                                   sent to the address supplied by
                                   yourself when you placed the order
                                   explaining the issue that may have
                                   arose. Remember to check your
                                   basket and delivery information to
                                   ensure your order is correct before
                                   pressing the ‘Place Your Order’
                                   button, as we may not be able to
                                   amend your order once it has been
                                   submitted.
                              </p>
                              <h4>
                                   1.1 HARRYTEJ ENTERPRISES RESERVES
                                   THE RIGHT TO REJECT OR CANCEL YOUR
                                   ORDER FULFILLMENT OF ALL ORDERS ON
                                   THE WWW.BAB.STORE WEBSITE. WE
                                   EXPLICITLY RESERVE THE RIGHT NOT TO
                                   ACCEPT YOUR ORDER FOR ANY OF THE
                                   FOLLOWING REASONS:
                              </h4>
                              <ul>
                                   <li>
                                        The product is not available /
                                        in stock
                                   </li>
                                   <li>
                                        Your billing information is
                                        not correct or not verifiable
                                   </li>
                                   <li>
                                        Your order has been suspected
                                        of fraudulent activity
                                   </li>
                                   <li>
                                        We could not deliver to the
                                        address provided by yourself
                                   </li>
                                   <li>Force Majeure</li>
                                   <li>
                                        In the event of misspelling,
                                        pricing or other errors or
                                        mistakes in the website
                                        information
                                   </li>
                              </ul>
                              <h4>1.2 DATA CHECK</h4>
                              <p>
                                   We may run some checks on your
                                   details before we ship your order.
                                   These checks may include verifying
                                   your address and payment details
                                   linked to your order. Any orders
                                   found to be made under fraudulent
                                   pretenses will be followed up with
                                   an investigation.
                              </p>
                              <h4>1.3 USER CONTENT</h4>
                              <p>
                                   When you transmit, upload, post,
                                   e-mail, share, distribute,
                                   reproduce or otherwise make
                                   available suggestions, ideas,
                                   inquiries, feedback, data, text,
                                   software, music, sound,
                                   photographs, graphics, images,
                                   videos, messages or other materials
                                   ("User Content") on the Site, you
                                   are entirely responsible for such
                                   User Content. You hereby grant to
                                   us a perpetual, worldwide,
                                   irrevocable, unrestricted,
                                   non-exclusive, royalty-free license
                                   to use, copy, license, sublicense,
                                   adapt, distribute, display,
                                   publicly perform, reproduce,
                                   transmit, modify, edit, and
                                   otherwise exploit such User Content
                                   throughout the world, in all media
                                   now known or hereafter developed,
                                   for any purpose whatsoever,
                                   including without limitation,
                                   developing, manufacturing,
                                   distributing and marketing
                                   products.
                              </p>
                              <p>
                                   You represent and warrant that you
                                   own or otherwise control the rights
                                   to your User Content. You agree not
                                   to engage in or assist or encourage
                                   others to engage in transmitting,
                                   uploading, posting, e-mailing,
                                   sharing, distributing, reproducing,
                                   or otherwise making available User
                                   Content that (a) is unlawful,
                                   harmful, threatening, abusive,
                                   harassing, tortious, defamatory,
                                   vulgar, obscene, pornographic,
                                   libelous, invasive of another's
                                   privacy, hateful, or racially,
                                   ethnically or otherwise
                                   objectionable; (b) you do not have
                                   a right to make available under any
                                   law or under contractual or
                                   fiduciary relationships; (c) is
                                   known by you to be false,
                                   fraudulent, inaccurate or
                                   misleading; (d) you were
                                   compensated for or granted any
                                   consideration by any third party;
                                   or (e) infringes any patent,
                                   trademark, trade secret, copyright
                                   or other proprietary rights of any
                                   party.
                              </p>
                              <p>
                                   We are in no way responsible for
                                   examining or evaluating User
                                   Content, nor do we assume any
                                   responsibility or liability for the
                                   User Content. We do not endorse or
                                   control the User Content
                                   transmitted or posted on the Site
                                   and therefore, we do not guarantee
                                   the accuracy, integrity or quality
                                   of User Content. You understand
                                   that by using the Site, you may be
                                   exposed to User Content that is
                                   offensive, indecent or
                                   objectionable to you. Under no
                                   circumstances will we be liable in
                                   any way for any User Content,
                                   including without limitation, for
                                   any errors or omissions in any User
                                   Content, or for any loss or damage
                                   of any kind incurred by you as a
                                   result of the use of any User
                                   Content transmitted, uploaded,
                                   posted, e-mailed or otherwise made
                                   available via the Site. You hereby
                                   waive all rights to any claims
                                   against us for any alleged or
                                   actual infringements of any
                                   proprietary rights, rights of
                                   privacy and publicity, moral
                                   rights, and rights of attribution
                                   in connection with User Content.
                              </p>
                              <p>
                                   You acknowledge that we have the
                                   right (but not the obligation) in
                                   our sole discretion to refuse to
                                   post or remove any User Content and
                                   we reserve the right to change,
                                   condense, or delete any User
                                   Content. Without limiting the
                                   generality of the foregoing or any
                                   other provision of these Terms and
                                   Conditions, we have the right to
                                   remove any User Content that
                                   violates these Terms and Conditions
                                   or is otherwise objectionable and
                                   we reserve the right to refuse
                                   service and/or terminate accounts
                                   without prior notice for any users
                                   who violate these Terms and
                                   Conditions or infringe the rights
                                   of others.
                              </p>
                              <h4>1.4 DELETION OF USER CONTENT</h4>
                              <p>
                                   If you wish to delete your user
                                   content on our website or in
                                   connection with our mobile
                                   applications, please contact us by
                                   email at support@bab.store and
                                   include the following information
                                   in your deletion request: first
                                   name, user name/screen name (if
                                   applicable), email address
                                   associated with our website and/or
                                   mobile applications, your reason
                                   for deleting the posting, and
                                   date(s) of posting(s) you wish to
                                   delete (if you have it). We may not
                                   be able to process your deletion
                                   request if you are unable to
                                   provide such information to us.
                                   Please allow up to 10 business days
                                   to process your deletion request.
                              </p>
                              <h4>2. PRICES/CURRENCY</h4>
                              <p>
                                   The product prices displayed on the
                                   website are inclusive of Goods and
                                   Services Tax. Shipping rates are
                                   applied per order. The exact
                                   shipping rates depend on the
                                   country where your order is being
                                   delivered.
                              </p>
                              <h4>
                                   2.1 PRICES MAY VARY ACROSS STORES.
                              </h4>
                              <p>Conversion rates may apply. </p>
                              <h4>2.2 PRICE CHANGES</h4>
                              <p>
                                   HARRYTEJ ENTERPRISES reserves the
                                   right to change the price of a
                                   product at any time without any
                                   forewarning. Any orders placed
                                   prior to this change cannot be
                                   amended to the revised price.
                              </p>
                              <h4>3. SHIPPING & DELIVERY</h4>
                              <p>
                                   All orders made on the
                                   WWW.BAB.STORE website are
                                   dispatched from the INDIA. We aim
                                   to get all orders out on the same
                                   day; however, due to volumes this
                                   is not always possible. Tracking
                                   numbers, if applicable, are
                                   included in dispatch confirmation
                                   emails. HARRYTEJ ENTERPRISES cannot
                                   be held accountable for parcels
                                   delayed to reasons beyond our
                                   control, including but not limited
                                   to the following: Customs, service
                                   strikes, civil commotion, riot,
                                   invasion, terrorist attack or
                                   threat of terrorist attack,
                                   weather, natural disasters, fire,
                                   epidemics or failure of public or
                                   private telecommunications
                                   networks.{' '}
                              </p>
                              <h4>CUSTOMS</h4>
                              <p>
                                   Countries other than INDIA may be
                                   subject to additional duties to be
                                   paid.
                              </p>
                              <p>
                                   As the recipient, you are liable
                                   for all import duties, customs and
                                   local sales taxes levied by the
                                   country you reside in; payment of
                                   these is necessary to release your
                                   order from customs on arrival.
                              </p>
                              <p>
                                   The final value of your order does
                                   not include any additional duties
                                   required. The carrier, prior to
                                   delivery, will inform you of the
                                   additional duties.
                              </p>
                              <p>
                                   If you refuse to pay the duties to
                                   release your order, the order will
                                   subsequently be returned back to
                                   the BAB warehouse and refunded. A
                                   shipping & handling fee may be
                                   deducted from your refund.
                              </p>
                              <p>
                                   By law, HARRYTEJ ENTERPRISES is
                                   required to state the correct
                                   amount paid for any outgoing
                                   international package. Any requests
                                   to change this information will be
                                   denied.
                              </p>
                              <h4>FAQ</h4>
                              Also on the FAQ page: I can’t track my
                              order:- Once you’ve received your
                              shipping confirmation, your tracking
                              details can take up to 24 hours to
                              update. If your tracking details have
                              not updated after 24 hours from the
                              moment it was dispatched, please contact
                              us at support@bab.store.
                              <h4>4. PROMOTIONS / OFFERS</h4>
                              <h4>4.1 GIFT CARDS</h4>
                              Once a gift card has been issued, the
                              card becomes that customers’ property.
                              HARRYTEJ ENTERPRISES reserves the right
                              to refuse or cancel a gift card without
                              any further discussion. Gift cards have
                              a validity of 12 months, and don’t need
                              to be spent all at once. Gift cards are
                              only valid on the site in which they
                              have been purchased from. I.e.
                              WWW.BAB.STORE gift card purchases can
                              only be utilized on the online store.
                              <h4>4.2 DISCOUNT CODES</h4>
                              Only one discount code can be used per
                              order. HARRYTEJ ENTERPRISES reserves the
                              right to refuse or cancel the use of a
                              discount code without any further
                              discussion. Usage of discount codes are
                              subject to stock availability.
                              <h4>4.3 PROMOTIONAL OFFERS</h4>
                              Promotions may vary across stores
                              <h4>5.1 LIABILITY </h4>
                              <p>
                                   User agrees that neither HARRYTEJ
                                   ENTERPRISES nor its group
                                   companies, directors, officers or
                                   employee shall be liable for any
                                   direct or/and indirect or/and
                                   incidental or/and special or/and
                                   consequential or/and exemplary
                                   damages, resulting from the use
                                   or/and the inability to use the
                                   service or/and for cost of
                                   procurement of substitute goods
                                   or/and services or resulting from
                                   any goods or/and data or/and
                                   information or/and services
                                   purchased or/and obtained or/and
                                   messages received or/and
                                   transactions entered into through
                                   or/and from the service or/and
                                   resulting from unauthorized access
                                   to or/and alteration of user's
                                   transmissions or/and data or/and
                                   arising from any other matter
                                   relating to the service, including
                                   but not limited to, damages for
                                   loss of profits or/and use or/and
                                   data or other intangible, even if
                                   HARRYTEJ ENTERPRISES has been
                                   advised of the possibility of such
                                   damages. User further agrees that
                                   HARRYTEJ ENTERPRISES shall not be
                                   liable for any damages arising from
                                   interruption, suspension or
                                   termination of service, including
                                   but not limited to direct or/and
                                   indirect or/and incidental or/and
                                   special consequential or/and
                                   exemplary damages, whether such
                                   interruption or/and suspension
                                   or/and termination was justified or
                                   not, negligent or intentional,
                                   inadvertent or advertent.{' '}
                              </p>
                              <p>
                                   User agrees that HARRYTEJ
                                   ENTERPRISES shall not be
                                   responsible or liable to user, or
                                   anyone, for the statements or
                                   conduct of any third party of the
                                   service. In sum, in no event shall
                                   Company's total liability to the
                                   User for all damages or/and losses
                                   or/and causes of action exceed the
                                   amount paid by the User to HARRYTEJ
                                   ENTERPRISES, if any, that is
                                   related to the cause of action.
                              </p>
                              <h4>
                                   5.2 DISCLAIMER OF CONSEQUENTIAL
                                   DAMAGES
                              </h4>
                              <p>
                                   In no event shall HARRYTEJ
                                   ENTERPRISES or any parties,
                                   organizations or entities
                                   associated with the corporate brand
                                   name us or otherwise, mentioned at
                                   this Website be liable for any
                                   damages whatsoever (including,
                                   without limitations, incidental and
                                   consequential damages, lost
                                   profits, or damage to computer
                                   hardware or loss of data
                                   information or business
                                   interruption) resulting from the
                                   use or inability to use the Website
                                   and the Website material, whether
                                   based on warranty, contract, tort,
                                   or any other legal theory, and
                                   whether or not, such organization
                                   or entities were advised of the
                                   possibility of such damages.
                              </p>
                              <h4>6.1 USE OF CONTENT</h4>
                              <p>
                                   All logos, brands, marks headings,
                                   labels, names, signatures,
                                   numerals, shapes or any
                                   combinations thereof, appearing in
                                   this site, except as otherwise
                                   noted, are properties either owned,
                                   or used under license, by the
                                   business and / or its associate
                                   entities who feature on this
                                   Website. The use of these
                                   properties or any other content on
                                   this site, except as provided in
                                   these terms and conditions or in
                                   the site content, is strictly
                                   prohibited.
                              </p>
                              <p>
                                   You may not sell or modify the
                                   content of this Website or
                                   reproduce, display, publicly
                                   perform, distribute, or otherwise
                                   use the materials in any way for
                                   any public or commercial purpose
                                   without the respective
                                   organization’s or entity’s written
                                   permission.
                              </p>
                              <h4>
                                   6.2 INTELLECTUAL PROPERTY RIGHTS
                              </h4>
                              <p>
                                   HARRYTEJ ENTERPRISES owns all
                                   intellectual property rights in the
                                   website and in the material
                                   published on it. These works are
                                   protected by copyright laws
                                   worldwide.
                              </p>
                              <p>
                                   Permission is granted to
                                   temporarily download one copy of
                                   the materials (information or
                                   software) on WWW.BAB.STORE web site
                                   for personal, non-commercial
                                   transitory viewing only. This is
                                   the grant of a license, not a
                                   transfer of title, and under this
                                   license you may not: modify or copy
                                   the materials, use the materials
                                   for any commercial purpose, or for
                                   any public display (commercial or
                                   non-commercial); attempt to
                                   decompile or reverse engineer any
                                   software contained on WWW.BAB.STORE
                                   web site; remove any copyright or
                                   other proprietary notations from
                                   the materials; or transfer the
                                   materials to another person or
                                   "mirror" the materials on any other
                                   server. This license shall
                                   automatically terminate if you
                                   violate any of these restrictions
                                   and may be terminated by HARRYTEJ
                                   ENTERPRISES at any time. Upon
                                   terminating your viewing of these
                                   materials or upon the termination
                                   of this license, you must destroy
                                   any downloaded materials in your
                                   possession whether in electronic or
                                   printed format.
                              </p>
                              <h4>BAB HEADQUARTERS</h4>
                              <h5>HARRYTEJ ENTERPRISES</h5>
                              <h5>W.P. 181/A BASTI SHEIKH,</h5>
                              <h5>JALANDHAR,PUNJAB-144002</h5>
                              <h5>INDIA</h5>
                              <h5>GSTIN: 03COEPB7184L1ZV</h5>
                              <h5>Email: support@bab.store</h5>
                         </div>
                    </div>
               </ContainerComponent>
          );
     }
}


export default TermsAndConditionsPage;