import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import './style.styl';
import { symbolchart } from '../../config';
import { ContainerComponent } from '../../components';
import { orderServices } from '../../services';
import { userActions } from '../../actions';
import { myOrdersData } from '../../staticData';

class MyOrdersPage extends Component {
     constructor(props) {
          super(props);

          this.state = { orders: [] };
     }

     async componentDidMount() {
          const { loadingState, showMessage } = this.props;
          loadingState(true);
          const data = await orderServices.fetchAllOrders();
          loadingState(false);
          if (!data.isSuccess) {
               showMessage(data);
          }
          this.setState({ orders: [...data.data.orders] });
     }

     renderOrders() {
          const { orders } = this.state;
          const { selectedCountry, history } = this.props;
          const returnArray = [];
          orders
               .reverse()
               .forEach(
                    ({
                         _id,
                         status,
                         productdetails,
                         ordernumber,
                         totalprice
                    }) => {
                         var x = '';
                         if (productdetails.length > 1) {
                              var y = Number(productdetails.length) - 1;
                              x = ',+' + y;
                         }
                         let product;
                         if (productdetails[0].product) {
                              product = {
                                   ...productdetails[0].product,
                                   totalQuantity:
                                        productdetails[0].productTotalQuantity
                              };
                         } else {
                              product = {
                                   ...productdetails[0].items,
                                   totalQuantity: productdetails[0].qty
                              };
                         }
                         returnArray.push(
                              <tr
                                   key={_id}
                                   onClick={() =>
                                        history.push(
                                             `/account/orders/${ordernumber}`
                                        )
                                   }
                              >
                                   <td>
                                        <div className="order-detail text-left">
                                             <div className="order-image-box">
                                                  <img
                                                       className="order-image"
                                                       title={product.name}
                                                       alt={`${product.type.toLowerCase()} ${product.name.toLowerCase()} ${
                                                            product.colorname
                                                       }   BAB`}
                                                       src={
                                                            product
                                                                 .imgaddresses[0]
                                                       }
                                                  />
                                             </div>
                                             <h5>
                                                  {product.name}{' '}
                                                  {product.colorname.replace(
                                                       /-/g,
                                                       ' '
                                                  )}
                                                  {x}
                                             </h5>
                                             <h6>
                                                  Qty:{product.totalQuantity}
                                             </h6>
                                             <h6>Size: {product.size}</h6>
                                        </div>
                                   </td>
                                   <td>
                                        {myOrdersData.statusFullForm[status]}
                                   </td>
                                   <td className="hidden-xs">{ordernumber}</td>
                                   <td className="hidden-xs">
                                        {symbolchart[selectedCountry]}{' '}
                                        {totalprice}
                                   </td>
                              </tr>
                         );
                    }
               );
          return returnArray;
     }

     render() {
          const { orders } = this.state;
          return (
               <ContainerComponent subscribeHidden>
                    <div className="orders">
                         <div className="container">
                              <div className="row">
                                   <div className="col-lg-12 	text-center">
                                        <h1>My Orders</h1>
                                   </div>
                              </div>

                              <div className="row">
                                   <div className="col-lg-12">
                                        {orders.length === 0 ? (
                                             <h3 className="">
                                                  No previous orders.
                                             </h3>
                                        ) : null}

                                        <div className="orders-info ">
                                             <table>
                                                  <tr id="legend_row">
                                                       <th>
                                                            Product Description
                                                       </th>
                                                       <th>Status</th>
                                                       <th className="hidden-xs">
                                                            Order No.
                                                       </th>
                                                       <th className="hidden-xs">
                                                            Order Total
                                                       </th>
                                                  </tr>
                                                  {this.renderOrders()}
                                             </table>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </ContainerComponent>
          );
     }
}

const mapStateToProps = state => {
     const { selectedCountry } = state.userReducer;
     return { selectedCountry };
};

export default withRouter(
     connect(
          mapStateToProps,
          userActions
     )(MyOrdersPage)
);
