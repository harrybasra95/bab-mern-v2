import React, { Component } from 'react';
import { ContainerComponent } from '../../components';
import './style.styl';
import { orderServices } from '../../services';
import { connect } from 'react-redux';
import { userActions } from '../../actions';

class PayViaPaytmPage extends Component {
     constructor(props) {
          super(props);

          this.state = { order: {} };
     }

     async componentDidMount() {
          const { loadingState, showMessage, match, location } = this.props;
          if (location.state) {
               return this.setState({
                    order: {
                         ...location.state.order
                    }
               });
          }
          const { orderNumber } = match.params;
          loadingState(true);
          const data = await orderServices.fetchSingleOrder(orderNumber);
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          return this.setState({
               order: { ...data.data.order }
          });
     }

     copyToClipboard() {
          var textField = document.createElement('textarea');
          textField.innerText = '8569078629';
          document.body.appendChild(textField);
          textField.select();
          document.execCommand('copy');
          textField.remove();
          this.setState({ changeText: true });
     }
     render() {
          const { order, changeText } = this.state;
          return (
               <ContainerComponent subscribeHidden>
                    <div className="container text-center pay-with-paytm ">
                         <h3>
                              Pay With
                              <img src="/img/paytm.png" />
                         </h3>
                         <h4>
                              Please Transfer{' '}
                              <span style={{ fontWeight: 'bold' }}>
                                   Rs. {order.totalprice}
                              </span>{' '}
                              to confirm your order.
                         </h4>
                         <div className="number">
                              <p>85690-78629</p>
                         </div>
                         <div
                              onClick={() => this.copyToClipboard()}
                              className="copy"
                         >
                              <h6>
                                   {changeText ? 'Copied' : 'Copy This Number'}
                              </h6>
                         </div>
                    </div>
               </ContainerComponent>
          );
     }
}

const mapStateToProps = state => state;

export default connect(
     mapStateToProps,
     userActions
)(PayViaPaytmPage);
