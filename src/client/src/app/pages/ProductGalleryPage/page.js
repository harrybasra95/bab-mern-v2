import React, { Component } from 'react';
import cn from 'classnames';
import './style.styl';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import { allPageInfo } from '../../staticData';
import { ContainerComponent } from '../../components';
import { productServices, cartServices } from '../../services';
import { userActions, sessionActions, cartActions } from '../../actions';

class ProductGalleryPage extends Component {
     constructor(props) {
          super(props);
          this.state = {
               products: [],
               redirectToHome: false,
               addToCartId: null
          };
     }

     async componentDidMount() {
          const { showMessage, loadingState } = this.props;
          const { type, name } = this.props.match.params;
          if (!type || !name) {
               loadingState(true);
               const data = await productServices.fetchAllProducts();
               loadingState(false);
               if (!data.isSuccess) {
                    return showMessage(data);
               }
               return this.setState({ products: data.items });
          }
          if (type !== 'collection' && type !== 'category') {
               return this.setState({ redirectToHome: true });
          }
          loadingState(true);
          const data = await productServices.fetchProducts(type, name);
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          this.setState({ products: data.items });
     }

     async addToCart(id, size, quantity) {
          const {
               loadingState,
               setSessionData,
               showMessage,
               showCart
          } = this.props;
          loadingState(true);
          const data = await cartServices.addToCartByUniqueId(
               id,
               size,
               quantity
          );
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          setSessionData(data.data);
          return showCart(true);
     }

     renderProducts() {
          const { products, addToCartId } = this.state;
          const returnData = [];
          products.forEach(
               (
                    {
                         productLink,
                         totalQty,
                         sale,
                         quantityChart,
                         uniqueid,
                         productTitle,
                         colornameShow,
                         currenyCode,
                         showSale,
                         currencySymbol,
                         originalPrice,
                         price,
                         imageTitle,
                         imgaddresses,
                         wishlistLink
                    },
                    i
               ) => {
                    returnData.push(
                         <div
                              key={uniqueid}
                              itemScope
                              itemType="http://schema.org/Product"
                              className="col-lg-4 col-sm-6 col-xs-6 tiles"
                         >
                              <div
                                   onMouseLeave={() =>
                                        this.setState({
                                             addToCartId: null
                                        })
                                   }
                                   className="u-tile"
                              >
                                   <Link to={productLink}>
                                        <div
                                             className={cn(
                                                  'out-of-stock-cover',
                                                  {
                                                       hide: totalQty > 0,
                                                       show: totalQty === 0
                                                  }
                                             )}
                                        >
                                             <div className="line-through">
                                                  <p>Out Of Stock</p>
                                             </div>
                                        </div>
                                        <div
                                             className={`sale_info ${showSale}`}
                                        >
                                             <p>{sale}% off</p>
                                        </div>
                                        <picture>
                                             <source
                                                  srcSet={imgaddresses[0].replace(
                                                       '.jpg',
                                                       '.webp'
                                                  )}
                                                  type="image/webp"
                                             />
                                             <source
                                                  srcSet={imgaddresses[0]}
                                                  type="image/jpeg"
                                             />
                                             <img
                                                  itemProp="image"
                                                  title={imageTitle}
                                                  className="index_image"
                                                  src={imgaddresses[0]}
                                                  alt={imageTitle}
                                             />
                                        </picture>
                                   </Link>
                                   <div className="add_to_cart_bar">
                                        <div
                                             className={cn('select_size_bar', {
                                                  select_size_bar_selected:
                                                       addToCartId === i
                                             })}
                                        >
                                             {Object.keys(quantityChart).map(
                                                  size => (
                                                       <div
                                                            onClick={() => {
                                                                 if (
                                                                      quantityChart[
                                                                           size
                                                                      ].qty > 0
                                                                 ) {
                                                                      this.addToCart(
                                                                           uniqueid,
                                                                           size,
                                                                           1
                                                                      );
                                                                 }
                                                            }}
                                                            key={`${uniqueid}-${
                                                                 quantityChart[
                                                                      size
                                                                 ].short
                                                            }`}
                                                            className={`add_to_cart ${
                                                                 quantityChart[
                                                                      size
                                                                 ].class
                                                            }`}
                                                            id={uniqueid}
                                                       >
                                                            {quantityChart[
                                                                 size
                                                            ].short.toUpperCase()}
                                                       </div>
                                                  )
                                             )}
                                        </div>
                                        <div
                                             onClick={() =>
                                                  this.setState({
                                                       addToCartId: i
                                                  })
                                             }
                                             className={cn(
                                                  'add_to_cart_button',
                                                  {
                                                       add_to_cart_button_left:
                                                            addToCartId === i
                                                  }
                                             )}
                                        >
                                             <p>Add to cart</p>
                                        </div>
                                        <div
                                             className={cn(
                                                  'add_to_wishlist_button',
                                                  {
                                                       add_to_wishlist_button_left:
                                                            addToCartId === i
                                                  }
                                             )}
                                             id={wishlistLink}
                                        >
                                             <span
                                                  className="glyphicon glyphicon-heart"
                                                  aria-hidden="true"
                                             />
                                        </div>
                                        <div className="add_to_wishlist_bar">
                                             <p>Added to wishlist</p>
                                        </div>
                                   </div>
                              </div>
                              <p>
                                   <Link to={productLink}>
                                        <h4
                                             style={{
                                                  textTransform: 'capitalize'
                                             }}
                                        >
                                             <span itemProp="name">
                                                  {productTitle}
                                                  <br />{' '}
                                                  <span className="colorname">
                                                       {colornameShow}
                                                  </span>
                                             </span>
                                        </h4>
                                   </Link>
                              </p>
                              <span
                                   itemProp="offers"
                                   itemScope
                                   itemType="http://schema.org/Offer"
                              >
                                   <meta
                                        itemProp="priceCurrency"
                                        content={currenyCode}
                                   />
                                   <p>
                                        <h5>
                                             <span
                                                  className={`cutted_price ${showSale}`}
                                             >
                                                  {currencySymbol}{' '}
                                                  {originalPrice}
                                             </span>
                                             <span itemProp="price">
                                                  {currencySymbol} {price}
                                             </span>
                                        </h5>
                                   </p>
                              </span>
                         </div>
                    );
               }
          );
          return returnData;
     }

     render() {
          const { redirectToHome } = this.state;
          const { type, name } = this.props.match.params;
          let pageInfo;
          if (!type && !name) {
               pageInfo = allPageInfo.products();
          } else if (type === 'category') {
               pageInfo = allPageInfo[name]();
          } else {
               pageInfo = allPageInfo.collections(name);
          }
          return (
               <ContainerComponent pageTitle={name || 'products'}>
                    {redirectToHome ? <Redirect to="/" /> : null}
                    <div className="gallery-logo">
                         <div className="container">
                              <p>
                                   <h1 className="cat_heading">
                                        {pageInfo.gtitle.toUpperCase()}
                                   </h1>
                              </p>
                         </div>
                    </div>
                    <div className="best-seller-main">
                         <div className="container">
                              <div className="row">
                                   <div className="gallery-main">
                                        {this.renderProducts()}
                                   </div>
                              </div>
                         </div>
                    </div>
               </ContainerComponent>
          );
     }
}

const mapStateToProps = state => state;

export default connect(
     mapStateToProps,
     { ...userActions, ...sessionActions, ...cartActions }
)(ProductGalleryPage);
