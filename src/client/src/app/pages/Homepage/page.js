import React, { Component } from 'react';
import './style.styl';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { addAnimationClass } from '../../helpers';
import { homepageData } from '../../staticData';
import { sessionActions } from '../../actions';
import { ContainerComponent } from '../../components';
const { splashPageData, categoryDivData } = homepageData;

class Homepage extends Component {
     constructor(props) {
          super(props);
          this.state = {};
     }

     renderSplashDivs() {
          const returnData = [];
          splashPageData.forEach(({ title, url, buttonText, imgUrl }) => {
               returnData.push(
                    <div>
                         <div className="category-gallery-detail-box">
                              <h1
                                   ref={ref => {
                                        addAnimationClass(
                                             ref,
                                             'from-bottom-animation',
                                             1
                                        );
                                   }}
                              >
                                   {title}
                              </h1>
                              <Link to={url}>
                                   <button className="fade-in-animation">
                                        {buttonText}
                                   </button>
                              </Link>
                         </div>
                         <picture>
                              <source
                                   srcSet={imgUrl.replace('.jpeg', '.webp')}
                                   type="image/webp"
                              />
                              <source srcSet={imgUrl} type="image/jpeg" />
                              <img
                                   className="index_image "
                                   src={imgUrl}
                                   alt="Alt Text!"
                              />
                         </picture>
                    </div>
               );
          });
          return returnData;
     }
     renderCategoryDivs() {
          const returnData = [];
          categoryDivData.forEach(({ title, imgUrl, url }) => {
               returnData.push(
                    <div
                         style={{
                              backgroundImage: `url(${imgUrl})`
                         }}
                         className="div"
                    >
                         <Link to={url}>
                              <picture>
                                   <source
                                        srcSet={imgUrl.replace('.jpg', '.webp')}
                                        type="image/webp"
                                   />
                                   <source srcSet={imgUrl} type="image/jpeg" />
                                   <img
                                        className="index_image"
                                        src={imgUrl}
                                        alt="Alt Text!"
                                   />
                              </picture>
                              <div className="" />
                              <h1>{title}</h1>
                         </Link>
                    </div>
               );
          });
          return returnData;
     }

     render() {
          return (
               <ContainerComponent pageTitle="homepage">
                    <div className="home">
                         <div className="category-gallery">
                              {this.renderSplashDivs()}
                         </div>
                    </div>
                    <div className="hidden-xs">
                         <div className="categories2">
                              {this.renderCategoryDivs()}
                         </div>
                    </div>
               </ContainerComponent>
          );
     }
}

const mapStateToProps = state => {
     return state;
};

export default connect(
     mapStateToProps,
     sessionActions
)(Homepage);
