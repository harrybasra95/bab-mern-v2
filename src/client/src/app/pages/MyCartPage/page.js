import React, { Component } from 'react';
import './style.styl';
import { ContainerComponent } from '../../components';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { cartServices } from '../../services';
import { symbolchart, currencyCode } from '../../config';
import { userActions, sessionActions } from '../../actions';

class MyCartPage extends Component {
     constructor(props) {
          super(props);
          this.state = {};
     }

     async removeFromCart(id) {
          console.log('Asdasd');
          const { loadingState, setSessionData, showMessage } = this.props;
          loadingState(true);
          const data = await cartServices.removeFromCartByUniqueId(id);
          loadingState(false);
          if (!data.isSuccess) {
               return showMessage(data);
          }
          return setSessionData(data.data);
     }

     renderCart() {
          const { cart, selectedCountry } = this.props;
          const returnData = [];
          if (cart.products) {
               Object.keys(cart.products).forEach(key => {
                    const {
                         product,
                         productTotalPrice,
                         productTotalQuantity
                    } = cart.products[key];
                    const {
                         name,
                         colorname,
                         type,
                         imgaddresses,
                         size
                    } = product;
                    returnData.push(
                         <div className="row">
                              <div className="text-center mycart-content">
                                   <div className="col-lg-3 col-md-3 col-sm-3 hidden-xs  row">
                                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                             <h3>
                                                  <span className="add-to-wishlist glyphicon glyphicon-heart" />
                                             </h3>
                                        </div>
                                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                             <h3>
                                                  <span className="remove-product-cart glyphicon glyphicon-remove" />
                                             </h3>
                                        </div>
                                   </div>
                                   <div className="col-lg-5 col-md-5 col-sm-5 col-xs-9 ">
                                        <Link
                                             to={`/products/${
                                                  product.type
                                             }/quicklook/${product.uniqueid}`}
                                        >
                                             <img
                                                  className="p_image"
                                                  title={name}
                                                  alt={`${type} ${name} ${colorname} BAB`}
                                                  src={imgaddresses[0]}
                                             />
                                        </Link>
                                        <Link
                                             to={`/products/${
                                                  product.type
                                             }/quicklook/${product.uniqueid}`}
                                        >
                                             <h4 className="product-name">
                                                  {name}{' '}
                                                  {colorname.replace(/-/g, ' ')}
                                                  -{size}
                                             </h4>
                                        </Link>
                                        <h4 className="p_price">
                                             {symbolchart[selectedCountry]}{' '}
                                             {productTotalPrice /
                                                  productTotalQuantity}
                                        </h4>
                                        <h5
                                             onClick={() =>
                                                  this.removeFromCart(
                                                       product._id
                                                  )
                                             }
                                             className="visible-xs remove_product"
                                        >
                                             Remove
                                        </h5>
                                   </div>
                                   <div className="text-center col-lg-2 col-md-2 col-sm-2 col-xs-4 hidden-xs ">
                                        <div className="quantity">
                                             <p className="quantity-number">
                                                  {productTotalQuantity}
                                             </p>
                                        </div>
                                   </div>
                                   <div className="col-lg-2 col-md-2 col-sm-2 col-xs-3 ">
                                        <h4 className="subtotal">
                                             {symbolchart[selectedCountry]}{' '}
                                             {productTotalPrice}
                                        </h4>
                                   </div>
                              </div>
                         </div>
                    );
               });
          }
          return returnData;
     }

     renderMobileCart() {
          const { cart, selectedCountry } = this.props;
          const returnData = [];
          if (cart.products) {
               Object.keys(cart.products).forEach(key => {
                    const {
                         product,
                         productTotalPrice,
                         productTotalQuantity
                    } = cart.products[key];
                    const {
                         name,
                         colorname,
                         type,
                         imgaddresses,
                         size
                    } = product;
                    returnData.push(
                         <div className="items item">
                              <div className="image-box">
                                   <Link
                                        to={`/products/${
                                             product.type
                                        }/quicklook/${product.uniqueid}`}
                                   >
                                        <img
                                             title={name}
                                             alt={`${type} ${name} ${colorname} BAB`}
                                             src={imgaddresses[0]}
                                        />
                                   </Link>
                              </div>
                              <div className="product-name">
                                   <Link
                                        to={`/products/${
                                             product.type
                                        }/quicklook/${product.uniqueid}`}
                                   >
                                        <h4>
                                             {name}{' '}
                                             {colorname.replace(/-/g, ' ')}-
                                             {size}
                                        </h4>
                                   </Link>
                                   <h4>
                                        {symbolchart[selectedCountry]}{' '}
                                        {productTotalPrice /
                                             productTotalQuantity}
                                   </h4>
                                   <div className="quantity">
                                        <p className="quantity-number">
                                             {productTotalQuantity}
                                        </p>
                                   </div>
                                   <h5
                                        onClick={() =>
                                             this.removeFromCart(product._id)
                                        }
                                        className="visible-xs remove_product"
                                   >
                                        Remove
                                   </h5>
                              </div>
                              <div className="product-price">
                                   <h4 className="subtotal">
                                        {symbolchart[selectedCountry]}{' '}
                                        {productTotalPrice}
                                   </h4>
                              </div>
                         </div>
                    );
               });
          }
          return returnData;
     }

     render() {
          const { selectedCountry, cart } = this.props;
          return (
               <ContainerComponent subscribeHidden>
                    <div className="container">
                         <div className="text-center">
                              <h1 className="text-center">My Cart</h1>
                         </div>
                         <div className="row hidden-xs">
                              <div className="text-center mycart-index">
                                   <div className="col-lg-3 col-md-3 col-sm-3 hidden-xs  row">
                                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6  ">
                                             <h4>Wishlist</h4>
                                        </div>
                                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6  ">
                                             <h4>Remove</h4>
                                        </div>
                                   </div>
                                   <div className="col-lg-5 col-md-5 col-sm-5  col-xs-9  ">
                                        <h4>Item</h4>
                                   </div>
                                   <div className="col-lg-2 col-md-2 col-sm-2 hidden-xs col-xs-4  ">
                                        <h4>Qty</h4>
                                   </div>
                                   <div className="col-lg-2 col-md-2 col-sm-2 col-xs-3  ">
                                        <h4>Subtotal</h4>
                                   </div>
                              </div>
                         </div>
                         <div
                              id="content-box"
                              className="content-box hidden-xs"
                         >
                              {this.renderCart()}
                         </div>
                         <div className="total-box hidden-xs">
                              <div className="row">
                                   <div className="mycart-total">
                                        <div className="col-lg-3 col-md-3 col-sm-3 hidden-xs  row" />
                                        <div className="text-right col-lg-7 col-md-10 col-sm-10 col-xs-8 ">
                                             <h4>Total</h4>
                                        </div>

                                        <div className="text-center col-lg-2 col-md-2 col-sm-2 col-xs-4">
                                             <h3
                                                  id="total_price"
                                                  className="total-price"
                                             >
                                                  {symbolchart[selectedCountry]}{' '}
                                                  {cart.totalPrice}
                                             </h3>
                                        </div>
                                        <br />
                                        <h6
                                             className="text-center"
                                             style={{ marginTop: '40px' }}
                                        >
                                             Shipping will be included on the
                                             checkout page.
                                        </h6>
                                   </div>
                              </div>
                         </div>
                         <div className="mobile-cart hidden-lg hidden-md hidden-sm">
                              <div className="legend">
                                   <h4>Item</h4>
                                   <h4>Subtotal</h4>
                              </div>
                              <div className="cart-items">
                                   {this.renderMobileCart()}
                              </div>
                              <div className="total-box container">
                                   <div className="row">
                                        <div className="mycart-total">
                                             <div className="col-lg-3 col-md-3 col-sm-3 hidden-xs  row" />
                                             <div className="text-right col-lg-7 col-md-10 col-sm-10 col-xs-8 ">
                                                  <h4>Total</h4>
                                             </div>

                                             <div className="text-center col-lg-2 col-md-2 col-sm-2 col-xs-4">
                                                  <h3
                                                       id="total_price"
                                                       className="total-price"
                                                  >
                                                       {
                                                            symbolchart[
                                                                 selectedCountry
                                                            ]
                                                       }{' '}
                                                       {cart.totalPrice}
                                                  </h3>
                                             </div>
                                             <br />
                                             <h6
                                                  className="text-center"
                                                  style={{ marginTop: '40px' }}
                                             >
                                                  Shipping will be included on
                                                  the checkout page.
                                             </h6>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div className="checkout">
                              <div className="row">
                                   <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 " />
                                   <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12 " />
                                   <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                                        <Link to="/checkout/personal-details">
                                             <div className="checkout-button">
                                                  <p>Buy Now</p>
                                             </div>
                                        </Link>
                                   </div>
                              </div>
                         </div>
                    </div>
               </ContainerComponent>
          );
     }
}

const mapStateToProps = state => {
     const { cart, selectedCountry } = state.userReducer;

     return { cart, selectedCountry };
};

export default connect(
     mapStateToProps,
     { ...userActions, ...sessionActions }
)(MyCartPage);
