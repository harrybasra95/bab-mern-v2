/* eslint-disable indent */
const initialState = {
     coupon: {},
     cart: {
          products: {},
          totalPrice: 0,
          totalQuantity: 0
     },
     user: {},
     isLoggedIn: false,
     loading: true,
     message: '',
     messageType: '',
     selectedCountry: 'in',
     isCartShowing: false,
     isHeaderShowing: false,
     blackOverflow: false,
     isMobile: true,
     deviceHeight: 0,
     trackId: null,
     webpEnabled: true,
     showBottomBar: false
};

export default (state = initialState, action) => {
     switch (action.type) {
          case 'ChangeBottomBarStatus': {
               return {
                    ...state,
                    showBottomBar: action.payload
               };
          }
          case 'webpEnabled': {
               return {
                    ...state,
                    webpEnabled: action.payload
               };
          }
          case 'setDeviceHeight': {
               return {
                    ...state,
                    deviceHeight: action.payload
               };
          }
          case 'showHeader': {
               return {
                    ...state,
                    isHeaderShowing: action.payload,
                    blackOverflow: action.payload
               };
          }
          case 'setBlackOverflow':
               return {
                    ...state,
                    blackOverflow: action.payload,
                    isHeaderShowing: action.payload,
                    isCartShowing: action.payload
               };
          case 'showCart': {
               return {
                    ...state,
                    isCartShowing: action.payload,
                    blackOverflow: action.payload
               };
          }
          case 'setIsMobile': {
               return { ...state, isMobile: action.payload };
          }
          case 'setUserData': {
               return {
                    ...state,
                    user: { ...action.payload },
                    isLoggedIn: true
               };
          }
          case 'selectCountry': {
               return { ...state, selectedCountry: action.payload };
          }
          case 'setCart':
               return { ...state, cart: { ...action.payload } };
          case 'setSessionData':
               return { ...state, ...action.payload };
          case 'loadingState':
               return { ...state, loading: action.payload };
          case 'showMessage': {
               if (typeof action.payload === 'string') {
                    return {
                         ...state,
                         message: action.payload,
                         messageType: 'error'
                    };
               }
               const { error, isSuccess, message } = action.payload;
               return {
                    ...state,
                    message: isSuccess ? message : error,
                    messageType: message ? 'success' : 'error'
               };
          }
          default:
               return state;
     }
};
