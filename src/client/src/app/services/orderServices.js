import { orderApis as api } from '../apis';
import Axios from 'axios';

export const updateLastOrderData = async reqBody => {
    const { data } = await Axios.put(api.updateLastOrderData, reqBody);
    return data;
};

export const pincodeCheck = async pincode => {
    const { data } = await Axios.get(api.pincodeCheck(pincode));
    return data;
};

export const createOrder = async reqBody => {
    const { data } = await Axios.post(api.createOrder, reqBody);
    return data;
};

export const fetchSingleOrder = async orderNumber => {
    const { data } = await Axios.get(api.singleOrder(orderNumber));
    return data;
};

export const fetchAllOrders = async () => {
    const { data } = await Axios.get(api.fetchAllOrders);
    return data;
};
