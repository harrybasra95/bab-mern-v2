import Fingerprint2 from 'fingerprintjs2/fingerprint2';
import { trackingApis } from '../apis';
import axios from 'axios';

export const createId = () => {
     return new Promise(resolve => {
          if (window.requestIdleCallback) {
               requestIdleCallback(function() {
                    Fingerprint2().get(function(components) {
                         resolve(components);
                    });
               });
          } else {
               setTimeout(function() {
                    Fingerprint2().get(function(components) {
                         resolve(components);
                    });
               }, 500);
          }
     });
};

export const startSession = async oldId => {
     const currentId = await createId();
     const reqData = {
          currentId,
          oldId
     };
     const { data } = axios.post(trackingApis.createSession, reqData);
     return data;
};
