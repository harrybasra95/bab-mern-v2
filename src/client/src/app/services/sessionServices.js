import axios from 'axios';
import { sessionApis } from '../apis';
const { createSessionApi, selectCountryApi } = sessionApis;

export const createSession = async () => {
    const { data } = await axios.post(createSessionApi);
    return data;
};

export const getSession = async token => {
    const { data } = await axios.get(`${createSessionApi}/${token}`);
    return data;
};

export const selectCountry = async country => {
    const reqData = { country };
    const { data } = await axios.post(selectCountryApi, reqData);
    return data;
};
