import Axios from 'axios';
import { pageApis } from '../apis';

export const fetchProducts = async (type, name) => {
     const { data } = await Axios.get(pageApis.fetchProductsApi(type, name));
     return data;
};

export const fetchAllProducts = async (type, name) => {
     const { data } = await Axios.get(pageApis.fetchAllProductsApi(type, name));
     return data;
};

export const fetchProductData = async (type, id) => {
     const { data } = await Axios.get(pageApis.fetchProductsDataApi(type, id));
     return data;
};
