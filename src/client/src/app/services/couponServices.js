import { couponApis } from '../apis';
import Axios from 'axios';

export const applyCoupon = async name => {
     const reqBody = {
          name
     };
     const { data } = await Axios.post(couponApis.applyCouponApi, reqBody);
     return data;
};

export const removeCoupon = async () => {
     const { data } = await Axios.get(couponApis.removeCouponApi);
     return data;
};

export const fetchCouponOrders = async couponId => {
     const { data } = await Axios.get(
          couponApis.fetchCouponOrdersApi(couponId)
     );
     return data;
};
