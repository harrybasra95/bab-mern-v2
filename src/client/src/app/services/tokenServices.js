import Cookies from 'universal-cookie';
import axios from 'axios';

export function getToken() {
     const cookies = new Cookies();
     return cookies.get('token');
}

export function setToken(token) {
     const cookies = new Cookies();
     const expires = new Date();
     expires.setDate(expires.getDate() + 365);
     cookies.set('token', token, { path: '/', expires });
     return true;
}

export function setInterceptor(token, selectedCountry) {
     axios.interceptors.request.use(
          function(config) {
               if (token !== null) {
                    config.headers['session-id'] = `${token}`;
               }
               if (selectedCountry !== null) {
                    config.headers['selected-country'] = `${selectedCountry}`;
               }
               return config;
          },
          function(err) {
               return Promise.reject(err);
          }
     );
}
