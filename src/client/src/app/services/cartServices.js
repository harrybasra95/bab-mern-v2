import Axios from 'axios';
import { cartApis } from '../apis';

export const addToCartByUniqueId = async (id, size, quantity) => {
    const reqBody = { id, size, quantity };
    const { data } = await Axios.post(cartApis.addToCartByUniqueId, reqBody);
    return data;
};

export const removeFromCartByUniqueId = async id => {
    const reqBody = { id };
    const { data } = await Axios.post(
        cartApis.removeFromCartByUniqueId,
        reqBody
    );
    return data;
};
