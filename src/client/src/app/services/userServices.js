import axios from 'axios';
import { userApis } from '../apis';
const {
    loginApi,
    subscribeApi,
    logoutApi,
    loginViaFacebookApi,
    loginViaGoogleApi,
    createUserApi
} = userApis;

export const loginUser = async (username, password) => {
    const { data } = await axios.post(loginApi, { username, password });
    return data;
};

export const logoutUser = async () => {
    const { data } = await axios.get(logoutApi);
    return data;
};

export const updateUser = async obj => {
    const { data } = await axios.put(loginApi, { obj });
    return data;
};

export const subscribeUser = async obj => {
    const { data } = await axios.post(subscribeApi, obj);
    return data;
};

export const loginViaFacebook = async obj => {
    const { data } = await axios.post(loginViaFacebookApi, obj);
    return data;
};

export const loginViaGoogle = async obj => {
    const { data } = await axios.post(loginViaGoogleApi, obj);
    return data;
};

export const createUser = async obj => {
    const { data } = await axios.post(createUserApi, obj);
    return data;
};
