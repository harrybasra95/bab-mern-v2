import staticData from './staticData';
import { colors } from './colors';

const googleAnalyticsId = '';
const serverIp = process.env.REACT_SERVER_IP;
const facebookAppId = '';
const googleClientId =
     '';
const defaultCart = {
     items: {},
     totalQty: 0,
     totalPrice: 0
};
const currencyCode = {
     in: 'INR',
     us: 'USD',
     ca: 'CAD',
     au: 'AUD',
     row: 'USD',
     eu: 'EUR',
     uk: 'GBP',
     nz: 'NZD',
     ae: 'AED',
     none: 'INR'
};
const symbolchart = {
     in: 'Rs.',
     us: '&#x24;',
     ca: '&#x24;',
     au: '&#x24;',
     row: '&#x24;',
     eu: '&#8364;',
     uk: '&#163;',
     nz: '&#x24;',
     ae: '&#x62f;&#x2e;&#x625;',
     none: 'Rs.'
};

export {
     googleAnalyticsId,
     serverIp,
     defaultCart,
     staticData,
     symbolchart,
     currencyCode,
     colors,
     facebookAppId,
     googleClientId
};
