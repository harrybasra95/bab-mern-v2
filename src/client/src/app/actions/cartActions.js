export const loadingState = payload => {
    return {
        type: 'loadingState',
        payload
    };
};

export const setCart = payload => {
    return {
        type: 'setCart',
        payload
    };
};

export const showCart = payload => {
    return {
        type: 'showCart',
        payload
    };
};
