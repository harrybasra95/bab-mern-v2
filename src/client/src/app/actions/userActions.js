export const ChangeBottomBarStatus = payload => {
     return {
          type: 'ChangeBottomBarStatus',
          payload
     };
};

export const webpEnabled = payload => {
     return {
          type: 'webpEnabled',
          payload
     };
};

export const loadingState = payload => {
     return {
          type: 'loadingState',
          payload
     };
};
export const setDeviceHeight = payload => {
     return {
          type: 'setDeviceHeight',
          payload
     };
};

export const showMessage = payload => {
     return {
          type: 'showMessage',
          payload
     };
};

export const setUserData = payload => {
     return {
          type: 'setUserData',
          payload
     };
};

export const selectCountry = payload => {
     return {
          type: 'selectCountry',
          payload
     };
};

export const setIsMobile = payload => {
     return {
          type: 'setIsMobile',
          payload
     };
};
export const setBlackOverflow = payload => {
     return {
          type: 'setBlackOverflow',
          payload
     };
};

export const showHeader = payload => {
     return {
          type: 'showHeader',
          payload
     };
};
