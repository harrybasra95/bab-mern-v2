export const loadingState = payload => {
    return {
        type: 'loadingState',
        payload
    };
};

export const setSessionData = payload => {
    return {
        type: 'setSessionData',
        payload
    };
};
