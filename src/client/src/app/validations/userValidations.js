import Validator from 'validator';

const failureMessage = message => ({
    isSuccess: false,
    error: message,
    messageType: 'error'
});

const successMessage = message => ({
    message,
    isSuccess: true,
    messageType: 'success'
});

export const emailValidation = email => {
    if (!email || !Validator.isEmail(email)) {
        return failureMessage('Please enter a valid email');
    }
    return successMessage();
};

export const passwordValidation = password => {
    if (!password || password.length === 0) {
        return failureMessage('Please enter a valid password');
    }
    return successMessage();
};

export const nameValidation = name => {
    if (!name || !Validator.isAlpha(name.replace(' ', ''))) {
        return failureMessage('Please enter a valid name');
    }
    return successMessage();
};

export const phoneValidation = phone => {
    if (!phone || !Validator.isMobilePhone(phone)) {
        return failureMessage('Please enter a valid mobile number');
    }
    return successMessage();
};

export const streetNoValidation = streetNo => {
    if (!streetNo) {
        return failureMessage('Please enter a valid Address');
    }
    return successMessage();
};

export const pincodeValidation = pincode => {
    if (!pincode || pincode.length !== 6) {
        return failureMessage('Please enter a valid pincode');
    }
    return successMessage();
};
