import React, { Component } from 'react';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { googleAnalyticsId } from './config';
import RouterComponent from './Router';
import reducers from './reducers';
import ReactGA from 'react-ga';

if (process.env.TYPE === 'online') {
     ReactGA.initialize(googleAnalyticsId);
     ReactGA.pageview(window.location.pathname + window.location.search);
}
const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
export default class Main extends Component {
     constructor(props) {
          super(props);

          this.state = {};
     }

     render() {
          return (
               <Provider store={store}>
                    <RouterComponent />
               </Provider>
          );
     }
}
