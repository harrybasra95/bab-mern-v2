import { serverIp } from '../config';
const sessionApis = {
    createSessionApi: `${serverIp}/session/`,
    selectCountryApi: `${serverIp}/session/select-country`
};

export default sessionApis;
