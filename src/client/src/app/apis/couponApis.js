import { serverIp } from '../config';

const couponApis = {
     applyCouponApi: `${serverIp}/coupon/apply`,
     removeCouponApi: `${serverIp}/coupon/remove`,
     fetchCouponOrdersApi: couponId => `${serverIp}/coupon/orders/${couponId}`
};

export default couponApis;
