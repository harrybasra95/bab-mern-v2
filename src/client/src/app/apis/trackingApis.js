import { serverIp } from '../config';

const trackingApis = {
     createSession: `${serverIp}/tracking/create`
};

export default trackingApis;
