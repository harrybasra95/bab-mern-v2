import { serverIp } from '../config';
const cartApis = {
    getCart: `${serverIp}/getCart`,
    addToCartByUniqueId: `${serverIp}/cart/add-to-cart-by-unique-id`,
    removeFromCartByUniqueId: `${serverIp}/cart/remove-from-cart-by-unique-id`
};

export default cartApis;
