import { serverIp } from '../config';

const orderApis = {
    createOrder: `${serverIp}/order/create`,
    updateLastOrderData: `${serverIp}/order/last-order-data`,
    fetchAllOrders: `${serverIp}/order/all`,
    singleOrder: orderNumber => `${serverIp}/order/single-order/${orderNumber}`,
    pincodeCheck: pincode =>
        `${serverIp}/order/pincode-check?pincode=${pincode}`
};

export default orderApis;
