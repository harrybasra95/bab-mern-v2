import { serverIp } from '../config';

const pageApis = {
     homePageApi: `${serverIp}/page/apparel`,
     fetchProductsApi: (type, name) =>
          `${serverIp}/products?type=${type}&name=${name}`,
     fetchAllProductsApi: () => `${serverIp}/products/all`,
     fetchProductsDataApi: (type, id) =>
          `${serverIp}/products/product-data?type=${type}&id=${id}`
};

export default pageApis;
