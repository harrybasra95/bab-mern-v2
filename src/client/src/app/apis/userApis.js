import { serverIp } from '../config';
const userApis = {
    createUserApi: `${serverIp}/create-user`,
    loginApi: `${serverIp}/login`,
    loginViaFacebookApi: `${serverIp}/login/facebook`,
    loginViaGoogleApi: `${serverIp}/login/google`,
    logoutApi: `${serverIp}/logout`,
    updateUserApi: `${serverIp}/user`,
    subscribeApi: `${serverIp}/user/subscribe`
};

export default userApis;
