import { serverIp } from '../config';

const tokenApis = {
    getTokenApi: `${serverIp}/getToken`
};

export default tokenApis;
