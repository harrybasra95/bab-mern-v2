import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import Main from './app/Main';

const App = () => {
     return (
          <BrowserRouter>
               <Main />
          </BrowserRouter>
     );
};

export default App;
