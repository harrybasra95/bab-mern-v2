const path = require('path');
const imageMin = require('./imageMin').imageMinFunction;
const entryFile = path.resolve(__dirname, 'src', 'client', 'app.js');
const templateFile = path.resolve(__dirname, 'src', 'client', 'index.html');
const outputDir = path.resolve(__dirname, 'build', 'public');
const Dotenv = require('dotenv-webpack');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const glob = require('glob');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const PurgecssPlugin = require('purgecss-webpack-plugin');
const ImageminWebpWebpackPlugin = require('imagemin-webp-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

const PATHS = {
     src: path.join(__dirname, 'src')
};

imageMin().then(() => {
     console.log('Images Compressed');
});

module.exports = {
     stats: 'errors-only',
     mode: 'production',
     entry: ['@babel/polyfill', entryFile],
     output: {
          filename: '[name].[contentHash].js',
          path: outputDir,
          chunkFilename: '[contentHash].js',
          publicPath: '/'
     },
     optimization: {
          splitChunks: {
               cacheGroups: {
                    default: false,
                    vendors: false,
                    vendor: {
                         name: 'vendor',
                         chunks: 'all',
                         test: /node_modules/,
                         priority: 20
                    },
                    common: {
                         name: 'common',
                         minChunks: 2,
                         chunks: 'async',
                         priority: 10,
                         reuseExistingChunk: true,
                         enforce: true
                    }
               }
          },
          minimizer: [
               new TerserPlugin({
                    parallel: true,
                    terserOptions: {
                         ecma: 6
                    }
               }),
               new OptimizeCSSAssetsPlugin({
                    cssProcessorOptions: {
                         discardComments: { removeAll: true }
                    },
                    canPrint: true
               })
          ]
     },
     module: {
          rules: [
               {
                    test: /\.(js|jsx)$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
               },
               {
                    test: /\.(jpg|png)$/,
                    use: {
                         loader: 'file-loader',
                         options: {
                              name: '[name].webp'
                         }
                    }
               },
               {
                    test: /\.(jpg|png)$/,
                    use: {
                         loader: 'url-loader',
                         options: {
                              limit: 10 * 1024
                         }
                    }
               },
               {
                    test: /\.(styl)$/,
                    use: [
                         {
                              loader: MiniCssExtractPlugin.loader
                         },
                         {
                              loader: 'css-loader'
                         },
                         {
                              loader: 'postcss-loader',
                              options: {
                                   ident: 'postcss',
                                   plugins: [
                                        require('autoprefixer')({
                                             overrideBrowserslist: [
                                                  '> 1%',
                                                  'last 2 versions'
                                             ]
                                        })
                                   ]
                              }
                         },
                         {
                              loader: 'stylus-loader'
                         }
                    ]
               },
               {
                    test: /\.css$/i,
                    include: path.join(
                         __dirname,
                         './node_modules/react-responsive-carousel/lib/styles/carousel.min.css'
                    ),
                    use: ['style-loader', 'css-loader']
               },
               {
                    test: /\.css$/i,
                    exclude: /node_modules/,
                    use: [
                         MiniCssExtractPlugin.loader,
                         'css-loader',
                         {
                              loader: 'postcss-loader',
                              options: {
                                   ident: 'postcss',
                                   plugins: [
                                        require('autoprefixer')({
                                             overrideBrowserslist: [
                                                  '> 1%',
                                                  'last 2 versions'
                                             ]
                                        })
                                   ]
                              }
                         }
                    ]
               }
          ]
     },
     plugins: [
          new CompressionPlugin({
               algorithm: 'gzip'
          }),
          new Dotenv({ path: './prod.env' }),
          new PurgecssPlugin({
               paths: glob.sync(`${PATHS.src}/**/*`, { nodir: true })
          }),
          new HtmlWebpackPlugin({
               template: templateFile,
               publicPath: '/'
          }),
          new MiniCssExtractPlugin({
               filename: '[name].[contentHash].css',
               chunkFilename: '[contentHash].css'
          }),
          new CleanWebpackPlugin({
               cleanOnceBeforeBuildPatterns: ['!img', '*.*']
          }),
          new ImageminWebpWebpackPlugin()
     ]
};
