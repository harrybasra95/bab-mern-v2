const path = require('path');
const entryFile = path.resolve(__dirname, 'src', 'client', 'app.js');
const Dotenv = require('dotenv-webpack');
const templateFile = path.resolve(__dirname, 'src', 'client', 'index.html');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const outputDir = path.resolve(__dirname, 'public');
const ImageminWebpWebpackPlugin = require('imagemin-webp-webpack-plugin');

module.exports = {
     mode: 'development',
     entry: ['@babel/polyfill', entryFile],
     output: {
          filename: 'bundle.js',
          path: outputDir,
          publicPath: '/'
     },
     devServer: {
          hot: true,
          historyApiFallback: true,
          publicPath: '/',
          contentBase: './build/public',
          stats: {
               children: false,
               maxModules: 0
          },
          port: 3000
     },

     module: {
          rules: [
               {
                    test: /\.(js|jsx)$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
               },
               {
                    test: /\.(png|jpe?g|gif)$/,
                    use: {
                         loader: 'file-loader',
                         options: {
                              name: '[name].webp'
                         }
                    }
               },
               {
                    test: /\.(styl)$/,
                    exclude: /node_modules/,
                    use: [
                         {
                              loader: 'style-loader'
                         },
                         {
                              loader: 'css-loader'
                         },
                         {
                              loader: 'stylus-loader'
                         }
                    ]
               },
               {
                    test: /\.css$/i,
                    use: ['style-loader', 'css-loader']
               },
               {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loaders: ['react-hot-loader/webpack', 'babel-loader']
               }
          ]
     },
     plugins: [
          new Dotenv({ path: './dev.env' }),
          new HtmlWebpackPlugin({
               template: templateFile,
               publicPath: '/'
          }),
          new ImageminWebpWebpackPlugin()
     ]
};
