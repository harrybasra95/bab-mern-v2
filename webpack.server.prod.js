var webpack = require('webpack');
var path = require('path');
const Dotenv = require('dotenv-webpack');
const CopyPkgJsonPlugin = require('copy-pkg-json-webpack-plugin');
var fs = require('fs');
var nodeModules = {};

fs.readdirSync('node_modules')
     .filter(function(x) {
          return ['.bin'].indexOf(x) === -1;
     })
     .forEach(function(mod) {
          nodeModules[mod] = 'commonjs ' + mod;
     });

module.exports = {
     stats: 'errors-only',
     entry: './src/server/app.js',
     target: 'node',
     node: {
          __dirname: false
     },
     output: {
          path: path.join(__dirname, 'build'),
          filename: 'backend.js'
     },
     externals: nodeModules,
     plugins: [
          new CopyPkgJsonPlugin(),
          new Dotenv({ path: './prod.env' }),
          new webpack.IgnorePlugin(/\.(css|less)$/),
          new webpack.BannerPlugin({
               banner: 'require("source-map-support").install();',
               raw: true,
               entryOnly: false
          })
     ],
     devtool: 'sourcemap'
};
