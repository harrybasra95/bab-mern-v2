const sharp = require('sharp');
const glob = require('glob');

var getDirectories = function(src, callback) {
     glob(src + '/**/*.webp', callback);
};
getDirectories('./build/public/img', function(err, res) {
     if (err) {
          console.log('Error', err);
     } else {
          res.forEach(file => {
               sharp(file)
                    .resize(100)
                    .toFile(
                         file
                              .replace('.webp', '_min_app.webp')
                              .replace(
                                   '_min_app_min_app.webp',
                                   '_min_app.webp'
                              ),
                         (err, info) => {
                              console.log('saved File');
                         }
                    );
          });
     }
});

const imageResizeFunction = async () => {};

module.exports = { imageResizeFunction };
